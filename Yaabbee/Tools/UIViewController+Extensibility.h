//
//  UIViewController+Reachability.h
//
//
//  Created by S Biswas on 7/22/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface UIViewController (TableBackGroundView)
- (UIImageView *)createTableBackgroundView;
@end

@interface UIViewController (UIKeyboardNotifications)

- (void)registerForKeyboardNotification;
- (void)deRegisterForKeyboardNotification;
@end

@interface UIViewController (BundlePath)
- (id)initWithNibName:(NSString*)nibName useCommonXib:(BOOL)useCommonXib;
@end

@protocol ModalViewControllerDelegate 
	@optional
	- (void)resetOrientation:(UIInterfaceOrientation)orientation;
@end