//
//  constant.h
//
//
//  Created by S Biswas on 7/23/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//
#define SOUND_KEY @"sound_key"
#define SOUND_BELOW_KEY @"sound_below_key"
#define SOUND_IN_KEY @"sound_in_key"
#define SOUND_ABOVE_KEY @"sound_above_key"
#define SOUND_HRDISCONNECTED_KEY @"sound_disconnected_key"

//workout
#define BASE_URL @ "http://www.mynuvita.com/"

#define MOBILITY_EXWORKOUTS @"a:MobilityWorkoutList"
#define MOBILITY_EX_WORKOUTS @"a:Workouts"
#define MOBILITY_WORKOUTS @"a:MobilityWorkouts"
#define MOBILITY_WORKOUT @"a:MobilityWorkout"

#define MOBILITY_WORKOUT_CODE @"a:WorkoutCode"
#define MOBILITY_WORKOUT_ID @"a:WorkoutId"
#define MOBILITY_WORKOUT_NAME @"a:WorkoutName"

//Exercise
#define EXERCISE_ID @"a:ExerciseId"
#define EXERCISE_NAME @"a:ExerciseName"
#define EXERCISE_IMAGE_URL @"a:ImageUrl"
#define EXERCISE_REPEAT @"a:Repeat"
#define EXERCISE_REPS @"a:Reps"
#define EXERCISE_REST @"a:Rest"
#define EXERCISE_VIDEO_URL @"a:VideoUrl"
//exercise
#define MOBILITY_EXERCISES @"a:Exercises"
#define MOBILITY_EXERCISE @"a:MobilityExercise"


//MobilityXHRs
#define MOBILITY_XHRS @"a:MobilityXHRs"
#define MOBILITY_XHR @"a:MobilityXHR"

#define MOBILITY_ABOVE @"a:Above"
#define MOBILITY_BELOW @"a:Below"

#define MOBILITY_CALORIES @"a:Calories"
#define MOBILITY_HRDATEID @"a:HRDateId"

#define MOBILITY_INZONE @"a:InZone"
#define MOBILITY_TOTAL @"a:Total"

#define MOBILITY_WORKOUT_ROUNDS @"a:WorkoutRounds"

//meals
#define MEALS_PHOTO_URL @"a:PhotoUrl"
#define MEALS_PHOTO_DATE @"a:PhotoDateTime"

#define AM_SNACK_KEY @"AmSnack"
#define BREAKFAST_KEY @"Breakfast"
#define DINNER_KEY @"Dinner"
#define LUNCH_KEY @"Lunch"
#define PM_SNACK_KEY @"PmSnack"
#define VITAMIN_KEY @"Vitamin"
#define WATER_KEY @"Water"

#define CALORIES_CONSUMED_KEY @"CaloriesConsumed"
#define DESCRIPTION_KEY @"Description"
#define HEALTH_RANKING_KEY @"HealthRanking"
#define TARGET_CALORIES_KEY @"TargetCalories"
#define WAS_EATEN_KEY @"wasEaten"

#define Lable_BACK_GAP 50
#define CUSTOM_CELL_HEIGHT 118.0

#define HIGHLIGHTED_CELL_COLOR [UIColor colorWithRed:225.0/255.0 green:247.0/255.0 blue:243.0/255.0 alpha:1]

#define ODD_CELL_COLOR [UIColor whiteColor]
#define EVEN_CELL_COLOR [UIColor colorWithRed:242.0/255.0 green:242.0/255.0 blue:242.0/255.0 alpha:1]

#define CELL_EVEN @"Cell_even"
#define CELL_ODD @"Cell_odd"

#define defaultConstraint CGSizeMake(240.0, 480.0)

//font size related
#define bigFontSize 24.0

#define headerFontSize 16.0
#define defaultFontSize 14.0
#define smallFontSize 12.0 //12
#define tooSmallFontSize 15.0 //13

//font name
#define DEFAULT_NORMAL_FONT_NAME @"HelveticaNeue-Regular"

#define DEFAULT_BOLD_FONT_NAME @"HelveticaNeue-Regular"


#define NO_RECORD_FOUND @"No records found"

#define LOADING_TEXT @"Loading..."

//cardio grid1
#define CARDIO_GRID1_HEADER_COLOR [UIColor colorWithRed:66.0/255.0 green:66.0/255.0 blue:66.0/255.0 alpha:1]

#define CARDIO_GRID_BODY_BLACK_COLOR [UIColor colorWithWhite:0.702 alpha:1.000]

#define CARDIO_GRID_BODY_GRAY_COLOR [UIColor colorWithRed:203.0/255.0 green:203.0/255.0 blue:203.0/255.0 alpha:1]

#define CARDIO_GRID_BODY_WHITE_COLOR [UIColor colorWithRed:231.0/255.0 green:231.0/255.0 blue:231.0/255.0 alpha:1]

#define CARDIO_GRID_BODY_YELLOW_COLOR [UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:204.0/255.0 alpha:1]

#define CARDIO_GRID_BODY_GREEN_COLOR [UIColor colorWithRed:215.0/255.0 green:228.0/255.0 blue:189.0/255.0 alpha:1]

#define CARDIO_GRID_BODY_ORANGE_COLOR [UIColor colorWithRed:253.0/255.0 green:234.0/255.0 blue:218.0/255.0 alpha:1]

//
#define CARDIO_PERC_GRAY_COLOR [UIColor colorWithRed:255.0/255.0 green:182.0/255.0 blue:124.0/255.0 alpha:1]
#define CARDIO_PERC_GREEN_COLOR [UIColor colorWithRed:184.0/255.0 green:205.0/255.0 blue:116.0/255.0 alpha:1]
#define CARDIO_PERC_ORANGE_COLOR [UIColor colorWithRed:217.0/255.0 green:123.0/255.0 blue:112.0/255.0 alpha:1]

#define PRELOADER_BG_COLOR [UIColor colorWithRed:94.0/255.0 green:94.0/255.0 blue:94.0/255.0 alpha:0.8]


#define RDX ([NuvitaAppDelegate isIOS7]) ? -15 : -5
#define LDX ([NuvitaAppDelegate isIOS7]) ? 15 : 5

#define APP_CRASH @"app_crash"
#define UPDATE_DEVICE_STATUS @"update_device_status"

typedef enum {
   
    ENGLISH,
    ARABIC,
    BOTH
    
}SelectedLanguage;