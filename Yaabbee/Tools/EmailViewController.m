    //
//  EmailViewController.m
//  GlideWell
//
//  Created by SAYAN BANERJEE on 16/09/11.
//  Copyright 2011 ObjectSol Technologies. All rights reserved.
//

#import "EmailViewController.h"



@implementation EmailViewController


- (void)sendSupportEmail
{
	[self sendEmail:@"support@appsea.net" emailSub:@"MobiSuit - Support" emailBody:@""];
}

- (void) referToFriend
{
	NSString *emailBody = @"I'm using the MobiSuit for the Mobile Application, and so can you. Get access to sync, upload photos  etc.. on the go!\nRegister on to www.mobisuit.com and download on your IPhone to download the app now.";
	
	[self sendEmail:@"" emailSub:@"MobiSuit - " emailBody:emailBody];
}

- (void) sendEmails:(NSMutableArray*)toEmails emailSub:(NSString*)eSub emailBody:(NSString*)ebody
{
	MFMailComposeViewController * controller = [[MFMailComposeViewController alloc] init];
	controller.mailComposeDelegate = self;
	
//	NSMutableArray *arr = [[NSMutableArray alloc] init];
//	
//	arr = nil;
//	arr = [[NSMutableArray alloc] init];
//	[arr addObject:toEmail];
	[controller setToRecipients:toEmails];
	[controller setSubject:eSub];
	[toEmails retain];
	
	NSString *body = ebody;
	[body retain];
	
	[controller setMessageBody:body isHTML:YES];
	//[tempStr release];
	NuvitaAppDelegate *appDelegate = (NuvitaAppDelegate*)[[UIApplication sharedApplication] delegate]; 
    
	if (controller) [appDelegate.forceNavigationController presentViewController:controller animated:YES completion:^{
        //...loading controller
    }];
	///[controller release];
}

- (void) sendEmail:(NSString*)toEmail emailSub:(NSString*)eSub emailBody:(NSString*)ebody
{
	MFMailComposeViewController * controller = [[MFMailComposeViewController alloc] init];
	controller.mailComposeDelegate = self;
	
	NSMutableArray *arr = [[NSMutableArray alloc] init];
	
	arr = nil;
	 arr = [[NSMutableArray alloc] init];
	[arr addObject:toEmail];
	[controller setToRecipients:arr];
	[controller setSubject:eSub];
	[arr retain];
	
	NSString *body = ebody;
	[body retain];
	
	[controller setMessageBody:body isHTML:YES];
	//[tempStr release];
       NuvitaAppDelegate *appDelegate = (NuvitaAppDelegate*)[[UIApplication sharedApplication] delegate]; 
    
	if (controller) [appDelegate.forceNavigationController presentViewController:controller animated:YES completion:^{
        //...loading controller
    }];
	///[controller release];
}



- (void)mailComposeController:(MFMailComposeViewController*)controller  
          didFinishWithResult:(MFMailComposeResult)result 
                        error:(NSError*)error;
{
	if (result == MFMailComposeResultSent) {
		NSLog(@"It's away!");
	}
    NuvitaAppDelegate *appDelegate = (NuvitaAppDelegate*)[[UIApplication sharedApplication] delegate]; 

    [appDelegate.forceNavigationController dismissViewControllerAnimated:YES completion:^{
        //...exit
    }];
}

@end
