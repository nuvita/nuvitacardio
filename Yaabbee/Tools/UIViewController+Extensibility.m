//
//  UIViewController+Reachability.m
//
//
//  Created by S Biswas on 7/22/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//


#import "UIViewController+Extensibility.h"
#import "constant.h"

#pragma mark -
#pragma mark TableBackGroundView

@implementation UIViewController (TableBackGroundView)
- (UIImageView *)createTableBackgroundView
{
//	UIImageView *backView = [[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"LeftNavBackground.png"]] autorelease];
//	return backView;
    return nil;
}
@end

#pragma mark -
#pragma mark UIKeyboardNotifications

@implementation UIViewController (UIKeyboardNotifications)


- (void)registerForKeyboardNotification{
	
	if ([self respondsToSelector:@selector(keyBoardWillAppear:)]) {
		[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyBoardWillAppear:) name:UIKeyboardWillShowNotification object:nil];
	}
//	
	if ([self respondsToSelector:@selector(keyBoardWillDisappear:)]) {
		[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyBoardWillDisappear:) name:UIKeyboardWillHideNotification object:nil];
	}
	
	
	if ([self respondsToSelector:@selector(keyBoardDidAppear:)]) {
		[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyBoardDidAppear:) name:UIKeyboardDidShowNotification object:nil];
	}
	
	
	if ([self respondsToSelector:@selector(keyBoardDidDisappear:)]) {
		[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyBoardDidDisappear:) name:UIKeyboardDidHideNotification object:nil];
	}
}

- (void)deRegisterForKeyboardNotification{
	
	[[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
	[[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
	[[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidShowNotification object:nil];
	[[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidHideNotification object:nil];
}


@end
@implementation UIViewController (BundlePath)

- (id)initWithNibName:(NSString*)nibName useCommonXib:(BOOL)useCommonXib
{

	if(useCommonXib)
	{
		[self initWithNibName:nibName bundle:nil];
	}
	else{
			
		if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
			
			NSString *iPadNibName = [nibName stringByAppendingFormat:@"-iPad"];
			[self initWithNibName:iPadNibName bundle:nil];
			
		}
		else{
			[self initWithNibName:nibName bundle:nil];
		}
		
	}
		
	return self;
}

@end