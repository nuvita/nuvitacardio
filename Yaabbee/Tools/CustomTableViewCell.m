//
//  CustomTableViewCell.m
//
//
//  Created by S Biswas on 7/29/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "CustomTableViewCell.h"
#import "constant.h"

@implementation CustomTableViewCell

@synthesize topLeftLbl;
@synthesize middleLbl;
@synthesize bottomLeftLbl;
@synthesize bottomRightLbl;

@synthesize layoutView;
@synthesize imageView;

@synthesize topLeftContent;
@synthesize bottomLeftContent;
@synthesize bottomRightContent;
@synthesize cellTag;
@synthesize cellText;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        self.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        
        [[NSBundle mainBundle] loadNibNamed:@"CustomCellView" owner:self options:nil];
               
        [self.contentView addSubview:self.layoutView];
       
        if([reuseIdentifier isEqualToString:CELL_EVEN])
            self.layoutView.backgroundColor = EVEN_CELL_COLOR;
        else{
            self.layoutView.backgroundColor = ODD_CELL_COLOR;
        }
    }
    return self;
}


- (IBAction)autoClicked:(id)sender{
    
}

- (void)layoutSubviews{
	[super layoutSubviews];
    
  /*  if (self.cellTag % 2 == 0) {
        UIImageView *backView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"gray_selecter.png"]];
        
        self.backgroundView = backView;
        
        [backView release];
    }*/

    self.topLeftLbl.text = self.topLeftContent;
    self.middleLbl.text = self.cellText;
    self.bottomLeftLbl.text = self.bottomLeftContent;
    self.bottomRightLbl.text = self.bottomRightContent;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)dealloc{
    
    [topLeftLbl release];
    [middleLbl release];
    [bottomLeftLbl release];
    [bottomRightLbl release];

    [layoutView release];
    [imageView release];
    [topLeftContent release];
    [bottomLeftContent release];
    [bottomRightContent release];
    [cellText release];

    [super dealloc];
}
@end
