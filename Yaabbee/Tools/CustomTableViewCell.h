//
//  CustomTableViewCell.h
//
//
//  Created by S Biswas on 7/29/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomTableViewCell : UITableViewCell{
    
    NSInteger cellTag;
    NSString *topLeftContent;
    NSString *cellText;
    NSString *bottomLeftContent;
    NSString *bottomRightContent;
}
@property (nonatomic,retain) IBOutlet UILabel *topLeftLbl;
@property (nonatomic,retain) IBOutlet UILabel *middleLbl;
@property (nonatomic,retain) IBOutlet UILabel *bottomLeftLbl;
@property (nonatomic,retain) IBOutlet UILabel *bottomRightLbl;
@property (nonatomic,retain) IBOutlet UIView *layoutView;
@property (nonatomic,retain) IBOutlet UIImageView *imageView;

@property  (nonatomic,assign) NSInteger cellTag;
@property  (nonatomic,retain) NSString *topLeftContent;
@property  (nonatomic,retain) NSString *bottomLeftContent;
@property  (nonatomic,retain) NSString *bottomRightContent;
@property  (nonatomic,retain) NSString *cellText;

- (IBAction)autoClicked:(id)sender;
@end
