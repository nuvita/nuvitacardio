//
//  EmailViewController.h
//  GlideWell
//
//  Created by SAYAN BANERJEE on 16/09/11.
//  Copyright 2011 ObjectSol Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

#import "NuvitaAppDelegate.h"

@interface EmailViewController : NSObject <MFMailComposeViewControllerDelegate >
{
    
}

- (void) sendEmail:(NSString*)toEmail emailSub:(NSString*)ebody emailBody:(NSString*)ebody;

- (void) sendEmails:(NSMutableArray*)toEmails emailSub:(NSString*)eSub emailBody:(NSString*)ebody;

- (void)sendSupportEmail;
- (void) referToFriend;

@end
