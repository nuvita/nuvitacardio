//
//  HelpViewController.h
//  NuvitaCardio
//
//  Created by John on 1/15/14.
//
//

#import <UIKit/UIKit.h>

@interface HelpViewController : UIViewController

@property (retain, nonatomic) IBOutlet UIWebView *webview;
@end
