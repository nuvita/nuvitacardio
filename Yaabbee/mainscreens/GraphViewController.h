//
//  GraphViewController.h
//  NuvitaCardio
//
//  Created by S Biswas on 16/01/13.
//
//

#import <UIKit/UIKit.h>
#import "request.h"
#import "NuvitaAppDelegate.h"
#import "MyTools.h"
#import "LoginPerser.h"
#import "LoginResponseData.h"
#import "NuvitaAppDelegate.h"
#import "NewLoginViewController.h"
#import "AppUtility.h"
#import "CardioLap.h"
#import "GpsLocation.h"

#import "SVProgressHUD.h"

#import <FacebookSDK/FacebookSDK.h>


#import "ShareStatusViewController.h"

#import "LineChartView.h"

@interface GraphViewController : UIViewController<UIActionSheetDelegate>
{
    NSInteger requestType;
    MyTools *tools;
	UIView *loadingView;

}


@property (retain, nonatomic) CardioLap *cardioLap;

@property (retain, nonatomic) IBOutlet UIView *viewGraphBody;

@property (retain, nonatomic) IBOutlet UILabel *lbCurrHT;


@property (retain, nonatomic) IBOutlet UILabel *lbRT;
@property (retain, nonatomic) IBOutlet UILabel *lbAvg;
@property (retain, nonatomic) IBOutlet UILabel *lbMax;
@property (retain, nonatomic) IBOutlet UILabel *lbTime;
@property (retain, nonatomic) IBOutlet UILabel *lbInZoneTime;
@property (retain, nonatomic) IBOutlet UILabel *lbCalories;
@property (retain, nonatomic) IBOutlet UILabel *lbCalAvgMin;
@property (retain, nonatomic) IBOutlet UILabel *lbAvgPerMin;

//...added John
@property (retain, nonatomic) IBOutlet UIView *headerview;
@property (retain, nonatomic) IBOutlet UIView *footerview;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil val:(id)val;
- (void)buildInfoUI;
- (void)buildGraphUI;

- (void)historyClicked;
- (void)shareMynuvitaClicked;

- (void)saveCardioSession;
- (void)showMessage:(NSString *)title message:(NSString *)message;
- (void)updateGraph;

- (void)shareMapOnFacebook;

- (void)postFacebook;
- (UIImage*)takeScreenShot;
- (void)setGraphPoints:(LineChartView *)lineChartView;

@end
