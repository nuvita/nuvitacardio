//
//  OptionsViewController.m
//  NuvitaCardio
//
//  Created by S Biswas on 16/01/13.
//
//

#import "OptionsViewController.h"
#import "NuvitaAppDelegate.h"

@interface OptionsViewController ()
@end

@implementation OptionsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.tabBarItem.title = @"Options";
        self.tabBarItem.image = [UIImage imageNamed:@"option.png"];
        
        UIImageView *textView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"logo.png"]];
        [self.navigationItem setTitleView:textView];
        [textView release];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 63, 32)];
    [button addTarget:self action:@selector(btnBackClicked:) forControlEvents:UIControlEventTouchUpInside];
    [button setImage:[UIImage imageNamed:@"btn_back.png"] forState:UIControlStateNormal];
    
    UIView *leftButtonView = [[UIView alloc] initWithFrame:button.frame];
    leftButtonView.bounds = CGRectOffset(leftButtonView.bounds, LDX, 0);
    [leftButtonView addSubview:button];
    
    UIBarButtonItem *btnLeft = [[UIBarButtonItem alloc] initWithCustomView:leftButtonView];
    [self.navigationItem setLeftBarButtonItem:btnLeft];
    [btnLeft release];

    
    // Do any additional setup after loading the view from its nib.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    
    if (![NuvitaAppDelegate isIOS7])
        [self buildiPhone4UI];
}

- (void)buildiPhone4UI {
    
    CGRect frame;
    frame = self.headerview.frame;
    frame.origin.y = 0;
    
    self.headerview.frame = frame;
    
    float tableY = frame.origin.y + frame.size.height;
    frame = self.tableview.frame;
    frame.origin.y = tableY;
    frame.size.height = ([NuvitaAppDelegate isPhone5]) ? 428 : 340;
    
    self.tableview.frame = frame;
}

- (void)btnBackClicked:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 4;//[[AppData sharedAppData].arrSessionData count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier = @"HistoryCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
        
        //cell.style = UITableViewCellStyleValue1;
    }
    
    NSString *titleText = @"";
    NSString *subTText = @"";
    NSString *imageName = @"ansuncheck.png";
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];

    switch (indexPath.row) {
        case 0:
        {
            if([prefs boolForKey:SOUND_IN_KEY])
                imageName = @"anscheck.png";
            
            titleText = @"In Zone";
            subTText = [NSString stringWithFormat:@"%d bpm", [AppData sharedAppData].inzoneLowLimit];
        }
            break;
        case 1:
        {
            if([prefs boolForKey:SOUND_ABOVE_KEY])
                imageName = @"anscheck.png";

            titleText = @"Above Zone";
            subTText = [NSString stringWithFormat:@"%d bpm", [AppData sharedAppData].inzoneHighLimit];
        }
            break;
        case 2:
        {
            if([prefs boolForKey:SOUND_BELOW_KEY])
                imageName = @"anscheck.png";

            titleText = @"Below Zone";
            subTText = @"";
        }
            break;
        case 3:
        {
            if([prefs boolForKey:SOUND_HRDISCONNECTED_KEY])
                imageName = @"anscheck.png";

            titleText = @"HR Lost Connection";
            subTText = @"";
        }
            break;
            
        default:
            break;
    }
    
    cell.textLabel.font = [UIFont fontWithName:DEFAULT_NORMAL_FONT_NAME size:headerFontSize];;
    cell.textLabel.text = titleText;
    
    cell.detailTextLabel.font = [UIFont fontWithName:DEFAULT_NORMAL_FONT_NAME size:headerFontSize];;
    cell.detailTextLabel.text = subTText;
    cell.imageView.image = [UIImage imageNamed:imageName];
    
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    Boolean flag = YES;
    
    switch (indexPath.row) {
        case 0:
        {
            if([prefs boolForKey:SOUND_IN_KEY])
                flag = NO;
            
            [prefs setBool:flag forKey:SOUND_IN_KEY];
        }
            break;
        case 1:
        {
            if([prefs boolForKey:SOUND_ABOVE_KEY])
                flag = NO;
            
            [prefs setBool:flag forKey:SOUND_ABOVE_KEY];
        }
            break;
        case 2:
        {
            if([prefs boolForKey:SOUND_BELOW_KEY])
                flag = NO;
            
            [prefs setBool:flag forKey:SOUND_BELOW_KEY];
        }
            break;
        case 3:
        {
            if ([prefs boolForKey:SOUND_HRDISCONNECTED_KEY])
                flag = NO;
            
            [prefs setBool:flag forKey:SOUND_HRDISCONNECTED_KEY];
        }
            break;
            
        default:
            break;
    }
    
    [prefs synchronize];
    //[[AppData sharedAppData] saveSettingsData];
    
    [tableView reloadData];
}

- (void)dealloc {
    [_headerview release];
    [_tableview release];
    [super dealloc];
}
@end
