//
//  GraphViewController.m
//  NuvitaCardio
//
//  Created by S Biswas on 16/01/13.
//
//

#import "GraphViewController.h"

#define HRM_LOW_VALUE 80
#define HRM_MAX_VALUE 160

@interface GraphViewController ()

@end

@implementation GraphViewController

@synthesize cardioLap;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.tabBarItem.title = @"Graph";
        //self.tabBarItem.image = [UIImage imageNamed:@"graph.png"];
        UIImage *normalImg = [UIImage imageNamed:@"graph.png"];
        UIImage *selectedImg = [UIImage imageNamed:@"graph_select.png"];
        
        [self.tabBarItem setFinishedSelectedImage:selectedImg withFinishedUnselectedImage:normalImg];
        
        self.view.backgroundColor = [UIColor colorWithWhite:0.980 alpha:1.000];
        requestType = 0;
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil val:(id)val
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.tabBarItem.title = @"Graph";
        UIImage *normalImg = [UIImage imageNamed:@"graph.png"];
        UIImage *selectedImg = [UIImage imageNamed:@"graph_select.png"];
        
        [self.tabBarItem setFinishedSelectedImage:selectedImg withFinishedUnselectedImage:normalImg];
        //
        UIImageView *textView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"logo.png"]];
        [self.navigationItem setTitleView:textView];

        //
        self.view.backgroundColor = [UIColor colorWithWhite:0.980 alpha:1.000];
        
        requestType = 1;
        self.cardioLap = (CardioLap *)val;
        //
        
        UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 79, 32)];
        [button addTarget:self action:@selector(historyClicked) forControlEvents:UIControlEventTouchUpInside];
        [button setImage:[UIImage imageNamed:@"btn_history.png"] forState:UIControlStateNormal];
        
        UIView *leftButtonView = [[UIView alloc] initWithFrame:button.frame];
        leftButtonView.bounds = CGRectOffset(leftButtonView.bounds, LDX, 0);
        [leftButtonView addSubview:button];
        
        UIBarButtonItem *btnLeft = [[UIBarButtonItem alloc] initWithCustomView:leftButtonView];
        [self.navigationItem setLeftBarButtonItem:btnLeft];
   
        button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 68, 32)];
        [button addTarget:self action:@selector(shareBtnClicked) forControlEvents:UIControlEventTouchUpInside];
        [button setImage:[UIImage imageNamed:@"btn_share.png"] forState:UIControlStateNormal];
        
        UIView *rightButtonView = [[UIView alloc] initWithFrame:button.frame];
        rightButtonView.bounds = CGRectOffset(rightButtonView.bounds, RDX, 0);
        [rightButtonView addSubview:button];
        
        UIBarButtonItem *btnRight = [[UIBarButtonItem alloc] initWithCustomView:rightButtonView];
        [self.navigationItem setRightBarButtonItem:btnRight];
    }
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    tools = [[MyTools alloc]init];
	loadingView = [tools createActivityIndicator1];
}

//- (void)viewWillAppear:(BOOL)animated

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if(requestType == 0)
        self.cardioLap = [AppData sharedAppData].cardioLap;
    else
        ;
    
    
    
    
    
    if (![NuvitaAppDelegate isIOS7])
        [self buildiPhone4UI];
    
    [self buildInfoUI];
    [self buildGraphUI];
    
    

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)historyClicked
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)shareMynuvitaClicked
{
    [self saveCardioSession];
}

- (void)buildiPhone4UI {

    CGRect frame;
    frame = self.headerview.frame;
    frame.origin.y = 0;
    
    self.headerview.frame = frame;
    
    float graphY = frame.origin.y + frame.size.height;
    frame = self.viewGraphBody.frame;
    frame.origin.y = graphY;
    frame.size.height = 265;
    frame.size.height += ([NuvitaAppDelegate isPhone5]) ? 90 : 0;
    
    self.viewGraphBody.frame = frame;
    
    float footerY = frame.origin.y + frame.size.height;
    frame = self.footerview.frame;
    frame.origin.y = footerY;
    
    self.footerview.frame = frame;

}

- (void)buildInfoUI
{
    self.lbCurrHT.text = @"0";
    self.lbRT.text = @"0";
    self.lbAvg.text = @"0";
    self.lbMax.text = @"0";
    
    //
    self.lbTime.text = @"0";
    self.lbInZoneTime.text = @"0";
    
    //
    self.lbCalories.text = @"0";
    self.lbAvgPerMin.text = @"0";
    self.lbCalAvgMin.text = @"0";

    
    if(self.cardioLap)
    {
        self.lbCurrHT.text = [self.cardioLap getHR];
        self.lbRT.text = [self.cardioLap getHR];
        self.lbAvg.text = [self.cardioLap getAvgHR];
        self.lbMax.text = [self.cardioLap getMaxHR];
        
        //
        self.lbTime.text = [self.cardioLap workoutTime];
        self.lbInZoneTime.text = [self.cardioLap getInAboveZoneText];
        
        //
        self.lbCalories.text = [self.cardioLap getCaloriesText];
        self.lbAvgPerMin.text = [self.cardioLap getCalAvg];
        self.lbCalAvgMin.text = [self.cardioLap getCalAvg];
    }
}

- (void)buildGraphUI {
    for (UIView *vw in [self.viewGraphBody subviews]) {
        [vw removeFromSuperview];
    }
    
    LineChartView *lineChartView = [[LineChartView alloc]initWithFrame:CGRectMake(0, 0, self.viewGraphBody.frame.size.width, self.viewGraphBody.frame.size.height)];
    lineChartView.tag = 999;
    lineChartView.backgroundColor = [UIColor colorWithWhite:0.980 alpha:1.000];
    
    [self setGraphPoints:lineChartView];
    [self.viewGraphBody addSubview:lineChartView];
}

- (void)setGraphPoints:(LineChartView *)lineChartView {
    NSMutableArray *pointArr = [[NSMutableArray alloc]init];
    NSInteger durationSeconds = self.cardioLap.durationSeconds;
    int maxValue = [[AppData sharedAppData] inzoneHighLimit];
    int minValue = [[AppData sharedAppData] inzoneLowLimit];
    
    if(self.cardioLap && self.cardioLap.heartRateSamples != nil) {
        int min = durationSeconds/60;//[[self.cardioLap.heartRateSamples allValues] count]/12;
        int ratio = min/11 + 1;
        
        lineChartView.multiple = ratio;
        NSInteger count = 0;
        
        int kk = 0;
        int hrmVal = 0;
        
        
        
        int numOfItem = durationSeconds/5;
        
        for (int ii = 0; ii < numOfItem; ii++) {
            
            NSString *key = [NSString stringWithFormat:@"%d", ii];
            
            if([self.cardioLap.heartRateSamples valueForKey:key] != nil) {
                hrmVal += [[self.cardioLap.heartRateSamples valueForKey:key] integerValue];
            }else
                hrmVal += 0;
            
            if(++kk == ratio) {
                int hrmAvg = hrmVal/ratio;
                
//                if(hrmAvg < HRM_LOW_VALUE) hrmAvg = HRM_LOW_VALUE;
//                if(hrmAvg > HRM_MAX_VALUE) hrmAvg = HRM_MAX_VALUE;
                
                minValue = (hrmAvg < minValue) ? minValue = hrmAvg : minValue; minValue += (minValue == 0) ? 5 : 0;
                maxValue = (hrmAvg > maxValue) ? maxValue = hrmAvg : maxValue;
                
                [pointArr addObject:[NSValue valueWithCGPoint:CGPointMake(count++, hrmAvg)]];
                
                kk = 0;
                hrmVal = 0;
            }
        }
    }

    [lineChartView setMaximum:maxValue + 5];
    [lineChartView setMinimum:minValue - 5];
    [lineChartView setArray:pointArr];
}

- (void)saveCardioSession {
	LoginResponseData *loginResponseData = [LoginPerser getLoginResponse];
	
    [tools startLoading:self./*navigationController.*/view childView:loadingView text:@"Saving cardio session. Wait…."];

	request *r=[[request alloc] initWithTarget:self
								 SuccessAction:@selector(onSucceffulLogin:resData:)
								 FailureAction:@selector(onFailureLogin)];
    
	[r saveCardioSession:[self.cardioLap getCardioSessionXml:loginResponseData.memberID] text:@"46364638743"];
}

- (void)onSucceffulLogin:(ResponseStatus *)responseStatus resData:(NSObject *)obj {
    [tools stopLoading:loadingView];
    
	if (responseStatus == nil || ![responseStatus isUpdate]) {
        [self showMessage:@"Sorry" message:@"Could not save cardio session, please try again later."];
		
		return ;
	}
	
    [self showMessage:@"Info" message:@"Saved successfully."];
}

- (void)onFailureLogin {
	[tools stopLoading:loadingView];
	//[self clearBodyUI];
}

- (void)showMessage:(NSString *)title message:(NSString *)message {
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title
                                                         message:message
                                                        delegate:nil
                                               cancelButtonTitle:@"OK"
                                               otherButtonTitles:nil];
    [alertView show];
}


#pragma ---
#pragma rotate

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskLandscape;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationLandscapeRight;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

/*
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    BOOL result = NO;
    if (interfaceOrientation == UIInterfaceOrientationLandscapeLeft ||
        interfaceOrientation == UIInterfaceOrientationLandscapeRight) {
        result = YES;
    }
    return result;
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskLandscape;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationLandscapeLeft;
}
*/


- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation duration:(NSTimeInterval)duration {

}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    [self buildGraphUI];

}

- (void)updateGraph
{
    //
//    if([AppData sharedAppData].isInSession)
//        [[AppData sharedAppData].cardioLap updateHRM];

    /*
    NSMutableArray *pointArr = [[[NSMutableArray alloc]init] autorelease];
    NSInteger count = 0;
    if(self.cardioLap && [[self.cardioLap.heartRateSamples allValues] count] > 0)
    {
        int min = [[self.cardioLap.heartRateSamples allValues] count]/12;
        int ratio = min/11 + 1;
        int ii = 0;
        int hrmVal = 0;
        
        for (id data in [self.cardioLap.heartRateSamples allValues]) {
            
            hrmVal += [data integerValue];
            
            if(++ii == ratio)
            {
                int hrmAvg = hrmVal/ratio;
                
                if(hrmAvg < 80) hrmAvg = 80;
                
                if(hrmAvg > 180) hrmAvg = 180;
                
                [pointArr addObject:[NSValue valueWithCGPoint:CGPointMake(count++, hrmAvg)]];
                
                ii = 0;
                hrmVal = 0;
            }
        }
    }
    */
    
    LineChartView *lineChartView = (LineChartView *) [self.viewGraphBody viewWithTag:999];
    //[lineChartView setArray:pointArr];
    [self setGraphPoints:lineChartView];

    [lineChartView setNeedsDisplay];
    
    //
    [self buildInfoUI];
    
}

//
#pragma ---
#pragma share option

- (void)shareBtnClicked {
    //    [self shareMapOnWall];
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Sharing"
                                                              delegate:self
                                                     cancelButtonTitle:@"Cancel"
                                                destructiveButtonTitle:nil
                                                     otherButtonTitles:@"Wellness wall", @"Facebook", nil];
    
    [actionSheet setActionSheetStyle:UIActionSheetStyleBlackTranslucent];
    
    actionSheet.tag = 999;
	[actionSheet showInView:self.tabBarController.view];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    NSLog(@"actionSheet %d", buttonIndex);
    if(buttonIndex == 0)
    {
        [self shareMapOnWall];
    }
    else if(buttonIndex == 1)
    {
        [self shareMapOnFacebook];
    }
    else if(buttonIndex == 2)
    {
        [actionSheet dismissWithClickedButtonIndex:0 animated:YES];
    }
}

//takescreen shots

- (UIImage*)takeScreenShot
{
    UIGraphicsBeginImageContextWithOptions(self.view.frame.size, self.view.opaque, 0.0);
    [self.view.layer renderInContext:UIGraphicsGetCurrentContext()];
    
    UIImage *screenshot = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return screenshot;
    
    //return [UIImage imageNamed:@"btn_bg.png"];
}

//
#pragma --
#pragma share on wall

- (void)shareMapOnWall
{
    
    ShareStatusViewController *vc = [[ShareStatusViewController alloc] initWithNibName:@"ShareStatusViewController" bundle:nil];
    
    vc.imageShare = [self takeScreenShot];
    
    [self.navigationController pushViewController:vc animated:NO];
 
    /*
    NSDateFormatter *dateFormat = [[[NSDateFormatter alloc] init] autorelease];
	[dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
	NSString *dateString = [dateFormat stringFromDate:[NSDate date]];
    dateString = [dateString stringByReplacingOccurrencesOfString:@" " withString:@"T"];
    
    NSString *text = @"Cardio Workout";
    
	LoginResponseData *loginResponseData = [LoginPerser getLoginResponse];
	
    [tools startLoading:self.tabBarController.view childView:loadingView text:@"Sharing. Wait…."];
    
    
	request *r=[[request alloc] initWithTarget:self
								 SuccessAction:@selector(onSucceffulLogin:resData:)
								 FailureAction:@selector(onFailureLogin)];
    NSData *imageContent = UIImagePNGRepresentation([self takeScreenShot]);
    
    
    [r saveWellnessWallPhoto:loginResponseData.memberID date:dateString text:text photo:imageContent];
	[r release];
    */
}


#pragma --
#pragma share on facebook

- (void)shareMapOnFacebook {

    [FBSession openActiveSessionWithReadPermissions:@[@"publish_actions"]//@[@"publish_actions", @"publish_stream", @"photo_upload", @"user_photos"]
                                       allowLoginUI:YES
                                  completionHandler:^(FBSession *session, FBSessionState status, NSError *error) {
                                      if (session.isOpen) {
                                          [self postFacebook];
                                      }
                                  }];

}

- (void)postFacebook {

    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];

    NSMutableDictionary* params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                   @"Cardio Workout", @"message",
                                   UIImagePNGRepresentation([self takeScreenShot]), @"picture",
                                   nil];

    [FBRequestConnection startWithGraphPath:@"me/photos"
                                 parameters:params
                                 HTTPMethod:@"POST"
                          completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {

                              [SVProgressHUD dismiss];
                              [self showMessage:@"Info" message:@"Shared successfully." ];
                          }];
    
}




@end
