//
//  OptionsViewController.h
//  NuvitaCardio
//
//  Created by S Biswas on 16/01/13.
//
//

#import <UIKit/UIKit.h>
#import "AppData.h"
#import "constant.h"

@interface OptionsViewController : UIViewController

@property (retain, nonatomic) IBOutlet UIView *headerview;
@property (retain, nonatomic) IBOutlet UITableView *tableview;

- (void)btnBackClicked:(id)sender;

@end
