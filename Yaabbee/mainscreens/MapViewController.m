//
//  MapViewController.m
//  NuvitaCardio
//
//  Created by Sayan Chatterjee on 27/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MapViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "SVProgressHUD.h"

@implementation MapViewController

@synthesize points = _points;
@synthesize mapView = _mapView;
@synthesize routeLine = _routeLine;
@synthesize routeLineView = _routeLineView;
@synthesize locationManager = _locationManager;
@synthesize cardioLap;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = @"Map";//NSLocalizedString(@"First", @"First");
        self.tabBarItem.title = @"Map";
        //self.tabBarItem.image = [UIImage imageNamed:@"map.png"];
        UIImage *normalImg = [UIImage imageNamed:@"map.png"];
        UIImage *selectedImg = [UIImage imageNamed:@"map_select.png"];
        
        [self.tabBarItem setFinishedSelectedImage:selectedImg withFinishedUnselectedImage:normalImg];
        // Custom initialization
        requestType = 0;
        isMapOn = NO;
        //
        
        //right item
        //UIBarButtonItem *btnRight = [[UIBarButtonItem alloc] initWithTitle:@"Map Off" style:UIBarButtonItemStylePlain target:self action:@selector(mapBtnClicked:)];
        UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 84, 32)];
        [button addTarget:self action:@selector(mapBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
        button.tag = 1001;
        [button setImage:[UIImage imageNamed:@"btn_mapoff.png"] forState:UIControlStateNormal];
        
        UIView *rightButtonView = [[UIView alloc] initWithFrame:button.frame];
        rightButtonView.bounds = CGRectOffset(rightButtonView.bounds, RDX, 0);
        [rightButtonView addSubview:button];
        
        UIBarButtonItem *btnRight = [[UIBarButtonItem alloc] initWithCustomView:rightButtonView];
        [self.navigationItem setRightBarButtonItem:btnRight];
        
        UIButton *button2 = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 94, 32)];
        [button2 addTarget:self action:@selector(mapViewClicked:) forControlEvents:UIControlEventTouchUpInside];
        [button2 setImage:[UIImage imageNamed:@"btn_maptype.png"] forState:UIControlStateNormal];
        
        UIView *leftButtonView = [[UIView alloc] initWithFrame:button2.frame];
        leftButtonView.bounds = CGRectOffset(leftButtonView.bounds, LDX, 0);
        [leftButtonView addSubview:button2];
        
        UIBarButtonItem *btnLeft = [[UIBarButtonItem alloc] initWithCustomView:leftButtonView];
        [self.navigationItem setLeftBarButtonItem:btnLeft];
    }
    
    return self;
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil val:(id)val {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.tabBarItem.title = @"Map";
        UIImage *normalImg = [UIImage imageNamed:@"map.png"];
        UIImage *selectedImg = [UIImage imageNamed:@"map_select.png"];
        
        [self.tabBarItem setFinishedSelectedImage:selectedImg withFinishedUnselectedImage:normalImg];
        //
        UIImageView *textView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"logo.png"]];
        [self.navigationItem setTitleView:textView];
        
        //        
        requestType = 1;
        self.cardioLap = (CardioLap *)val;
    
        UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 79, 32)];
        [button addTarget:self action:@selector(historyClicked) forControlEvents:UIControlEventTouchUpInside];
        [button setImage:[UIImage imageNamed:@"btn_history.png"] forState:UIControlStateNormal];
        
        UIView *leftButtonView = [[UIView alloc] initWithFrame:button.frame];
        leftButtonView.bounds = CGRectOffset(leftButtonView.bounds, LDX, 0);
        [leftButtonView addSubview:button];
        
        UIBarButtonItem *btnLeft = [[UIBarButtonItem alloc] initWithCustomView:leftButtonView];
        [self.navigationItem setLeftBarButtonItem:btnLeft];
 
        button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 68, 32)];
        [button addTarget:self action:@selector(shareBtnClicked) forControlEvents:UIControlEventTouchUpInside];
        [button setImage:[UIImage imageNamed:@"btn_share.png"] forState:UIControlStateNormal];
        
        UIView *rightButtonView = [[UIView alloc] initWithFrame:button.frame];
        rightButtonView.bounds = CGRectOffset(rightButtonView.bounds, RDX, 0);
        [rightButtonView addSubview:button];
        
        UIBarButtonItem *btnRight = [[UIBarButtonItem alloc] initWithCustomView:rightButtonView];
        [self.navigationItem setRightBarButtonItem:btnRight];
    }
    return self;
}

#pragma --
#pragma historyClicked

- (void)historyClicked
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma --
#pragma mapBtnClicked

- (void)mapViewClicked:(id)sender {
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Choose the following map type"
                                                             delegate:self
                                                    cancelButtonTitle:@"Cancel"
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:@"Standard", @"Hybrid", @"Satellite", nil];
   
    [actionSheet setActionSheetStyle:UIActionSheetStyleBlackTranslucent];
    [actionSheet showInView:self.tabBarController.view];
    
}

- (void)mapBtnClicked:(id)sender
{
    UIButton *btnRight = (UIButton *)sender;

    //UIBarButtonItem *btnRight = (UIBarButtonItem *)sender;
    if(btnRight.tag == 1001)
    {
        if(self.cardioLap)
        {
            if(requestType == 0)
            {
                isMapOn = YES;
                self.mapView.showsUserLocation = YES;
                //self.mapView.userInteractionEnabled = YES;
                //self.mapView.userTrackingMode = MKUserTrackingModeFollowWithHeading;
            }
            
            MKCoordinateRegion region;
            if(_currentLocation)
            {
                region.center.latitude =  _currentLocation.coordinate.latitude;
                region.center.longitude = _currentLocation.coordinate.longitude;
                region.span.latitudeDelta = 0.03f;
                region.span.longitudeDelta = 0.03f;
                
                [self.mapView setRegion:region animated:YES];
            }
                        
            //_points = self.cardioLap.arrGpsPos;
            
//            [self updateHeaderUI];
//            [self buildUI];
            [self updateMap];

        }
        
        btnRight.tag = 1000;
        [btnRight setImage:[UIImage imageNamed:@"btn_mapon.png"] forState:UIControlStateNormal];

        //[btnRight setTitle:@"Map On"];
    }
    else if(btnRight.tag == 1000)
    {
        isMapOn = NO;
        [self.mapView setShowsUserLocation:NO];
        //[self.mapView setUserTrackingMode: MKUserTrackingModeNone];
        
        if(_points)
            _points = nil;//[_points removeAllObjects];
        
        btnRight.tag = 1001;
        //[btnRight setTitle:@"Map Off"];
        [btnRight setImage:[UIImage imageNamed:@"btn_mapoff.png"] forState:UIControlStateNormal];

        //
        //[self.mapView setHidden:YES];
        [self updateHeaderUI];
        [self buildUI];
        [self configureRoutes];
    }
    
    //
       /*
    NSString *imgName = @"btn_edit.png";
    
    if(btn.tag == 99)
    {
        imgName = @"btn_done.png";
        btn.tag = 100;
    }
    else
    {
        btn.tag = 99;
    }
    
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 60, 32)];
    [button addTarget:self action:@selector(btnEditClicked:) forControlEvents:UIControlEventTouchUpInside];
    button.tag = btn.tag;
    
    [button setImage:[UIImage imageNamed:imgName] forState:UIControlStateNormal];
    UIBarButtonItem *btnRight = [[UIBarButtonItem alloc] initWithCustomView:button];
    
    [self.navigationItem setRightBarButtonItem:btnRight];
    */
}

#pragma --
#pragma stopTrackingMap

- (void)stopTrackingMap
{
    UIBarButtonItem *btnRight = self.navigationItem.rightBarButtonItem;
    
    //[self.mapView setShowsUserLocation:NO];
    //[self.mapView setUserTrackingMode: MKUserTrackingModeNone];
    _currentLocation = nil;
    if(_points)
        _points = nil;//[_points removeAllObjects];
    
    btnRight.tag = 1001;
    [btnRight setTitle:@"Map Off"];
}

#pragma --
#pragma didReceiveMemoryWarning

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

#pragma --
#pragma viewDidLoad

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    tools = [[MyTools alloc]init];
	loadingView = [tools createActivityIndicator1];
    
    _points = [[NSMutableArray alloc] init];
    _currentLocation = nil;
}

#pragma --
#pragma viewWillAppear

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    NSLog(@"ay ay na bata ini!!!");
    float dmile = 0;
    self.lbDistance.text = [NSString stringWithFormat:@"%+.2f", dmile];
    
    self.lbElapsedTime.text = @"0:00:00";
    
    NuvitaAppDelegate* appDelegate = (NuvitaAppDelegate*) [[UIApplication sharedApplication] delegate];
    [self.mapView setMapType:[appDelegate mapType]];
    
    if(requestType == 0)
        self.cardioLap = [AppData sharedAppData].cardioLap;
    else
        ;
    
    if(self.cardioLap)
    {
        if(requestType == 0)
        {
            if(isMapOn)
                _points = [NSMutableArray arrayWithArray:self.cardioLap.arrGpsPos];//self.cardioLap.arrGpsPos;
            else
            {
                if(_points)
                    _points = nil;//[_points removeAllObjects];
            }
            //self.mapView.showsUserLocation = YES;
            //self.mapView.userInteractionEnabled = YES;
            //self.mapView.userTrackingMode = MKUserTrackingModeFollowWithHeading;
        }
        
        //self.mapView.
        if(requestType == 1)
        {
            _points = [NSMutableArray arrayWithArray:self.cardioLap.arrGpsPos];//self.cardioLap.arrGpsPos;
            
            MKCoordinateRegion region;
            if(_currentLocation)
            {
                region.center.latitude =  _currentLocation.coordinate.latitude;
                region.center.longitude = _currentLocation.coordinate.longitude;
            }
            region.span.latitudeDelta = 0.03f;
            region.span.longitudeDelta = 0.03f;
            
            [self.mapView setRegion:region animated:YES];
        }
        [self updateHeaderUI];
        [self buildUI];
    }
    else
    {
        //[self.mapView setShowsUserLocation:NO];
        //[self.mapView setUserTrackingMode: MKUserTrackingModeNone];
        
        if(_points)
            _points = nil;//[_points removeAllObjects];
    }

    [self configureRoutes];
    
    if (![NuvitaAppDelegate isIOS7])
        [self buildiPhone4UI];
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

// setup map view
//self.mapView = [[MKMapView alloc] initWithFrame:self.view.bounds];
//self.mapView.delegate = self;
//self.mapView.showsUserLocation = YES;
//self.mapView.userInteractionEnabled = YES;
//self.mapView.userTrackingMode = MKUserTrackingModeFollowWithHeading;
//[self.view addSubview:self.mapView];

// configure location manager
// [self configureLocationManager];

#pragma --
#pragma buildUI

- (void)buildiPhone4UI {
    
    CGRect frame;
    frame = self.headerview.frame;
    frame.origin.y = 0;
    
    self.headerview.frame = frame;
    
    frame = self.mapView.frame;
    frame.origin.y = 0;
    frame.size.height = 367;
    frame.size.height += ([NuvitaAppDelegate isPhone5]) ? 88 : 0;
    
    self.mapView.frame = frame;
}

- (void)buildUI
{
    CLLocation *userLocation = nil;
    if([_points count] > 0)
    {
        userLocation = (CLLocation *)[_points objectAtIndex:[_points count] - 1];
    
        CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(userLocation.coordinate.latitude, userLocation.coordinate.longitude);
    
        [self.mapView setCenterCoordinate:coordinate animated:YES];
        
        //
        if(_currentLocation == nil)
        {
            _currentLocation = userLocation;
            MKCoordinateRegion region;
            region.center.latitude =  _currentLocation.coordinate.latitude;
            region.center.longitude = _currentLocation.coordinate.longitude;
            region.span.latitudeDelta = 0.03f;
            region.span.longitudeDelta = 0.03f;
            
            [self.mapView setRegion:region animated:NO];
        }

        _currentLocation = userLocation;

    }
}

- (void)updateHeaderUI
{
    if([_points count]  == 0) return;
    
    //CLLocation *point1  = (CLLocation *)[_points objectAtIndex:0];
    CLLocation *point2  = (CLLocation *)[_points objectAtIndex:[_points count] - 1];

    //CLLocationDistance distance = [point1 distanceFromLocation:point2];

    CGFloat dmile = [AppData calculateDistance:_points];//distance*0.000621371;
    
    self.lbDistance.text = [NSString stringWithFormat:@"%+.2f", dmile];
    
    self.lbElapsedTime.text = [self.cardioLap workoutTime];
    
    CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(point2.coordinate.latitude, point2.coordinate.longitude);
    [self.mapView setCenterCoordinate:coordinate animated:YES];

}

#pragma mark - UIActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
  
    if ([actionSheet tag] == 999) {
        
        if(buttonIndex == 0)
            [self shareMapOnWall];
        else if(buttonIndex == 1)
            [self shareMapOnFacebook];
        else if(buttonIndex == 2)
            [actionSheet dismissWithClickedButtonIndex:0 animated:YES];
    
        
    } else {
        
        if (buttonIndex == 0)
            [self.mapView setMapType:MKMapTypeStandard];
        else if (buttonIndex == 1)
            [self.mapView setMapType:MKMapTypeHybrid];
        else if (buttonIndex == 2)
            [self.mapView setMapType:MKMapTypeSatellite];
        
        NuvitaAppDelegate* appDelegate = (NuvitaAppDelegate*) [[UIApplication sharedApplication] delegate];
        appDelegate.mapType = self.mapView.mapType;
    }
 
}

#pragma mark
#pragma mark Map View

- (void)configureRoutesX
{
    NSLog(@"configure routes!");
    //if(_points == nil || [_points count] == 0) return;
    
    // define minimum, maximum points
	MKMapPoint northEastPoint = MKMapPointMake(0.f, 0.f);
	MKMapPoint southWestPoint = MKMapPointMake(0.f, 0.f);
	
	// create a c array of points.
	MKMapPoint* pointArray = malloc(sizeof(CLLocationCoordinate2D) * _points.count);
    
	// for(int idx = 0; idx < pointStrings.count; idx++)
    for(int idx = 0; idx < _points.count; idx++)
	{
        CLLocation *location = [_points objectAtIndex:idx];
        CLLocationDegrees latitude  = location.coordinate.latitude;
		CLLocationDegrees longitude = location.coordinate.longitude;
        
		// create our coordinate and add it to the correct spot in the array
		CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(latitude, longitude);
		MKMapPoint point = MKMapPointForCoordinate(coordinate);
		
		// if it is the first point, just use them, since we have nothing to compare to yet.
		if (idx == 0) {
			northEastPoint = point;
			southWestPoint = point;
		} else {
			if (point.x > northEastPoint.x)
				northEastPoint.x = point.x;
			if(point.y > northEastPoint.y)
				northEastPoint.y = point.y;
			if (point.x < southWestPoint.x)
				southWestPoint.x = point.x;
			if (point.y < southWestPoint.y)
				southWestPoint.y = point.y;
		}
        
		pointArray[idx] = point;
	}
	
    if (self.routeLine) {
        [self.mapView removeOverlay:self.routeLine];
    }
    
    self.routeLine = [MKPolyline polylineWithPoints:pointArray count:_points.count];
    
    // add the overlay to the map
	if (nil != self.routeLine) {
		[self.mapView addOverlay:self.routeLine];
	}
    
    
    // clear the memory allocated earlier for the points
	free(pointArray);
    
    /*
     double width = northEastPoint.x - southWestPoint.x;
     double height = northEastPoint.y - southWestPoint.y;
     
     _routeRect = MKMapRectMake(southWestPoint.x, southWestPoint.y, width, height);
     
     // zoom in on the route.
     [self.mapView setVisibleMapRect:_routeRect];
     */
}

- (void)configureRoutes
{
    CLLocationCoordinate2D coors[_points.count];

    int i = 0;
    for (CLLocation *location in _points) {
        
        CLLocationDegrees latitude  = location.coordinate.latitude;
		CLLocationDegrees longitude = location.coordinate.longitude;
        
		// create our coordinate and add it to the correct spot in the array
		CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(latitude, longitude);
		//MKMapPoint point = MKMapPointForCoordinate(coordinate);
        coors[i] = coordinate;
        i++;
    }

    MKPolyline *line = [MKPolyline polylineWithCoordinates:coors count:_points.count];

    // replace overlay
    [self.mapView removeOverlays:self.mapView.overlays];
    [self.mapView addOverlay:line];
}

- (MKOverlayView *)mapView:(MKMapView *)mapView viewForOverlay:(id<MKOverlay>)overlay
{
    MKPolylineView *overlayView = [[MKPolylineView alloc] initWithOverlay:overlay];
    overlayView.strokeColor = [UIColor redColor];
    //self.routeLineView.fillColor = [UIColor redColor];
    //self.routeLineView.strokeColor = [UIColor redColor];

    overlayView.lineWidth = 5.f;
    
    return overlayView;
}

/*
 #pragma mark
 #pragma mark Location Manager
 
 - (void)configureLocationManager
 {
 // Create the location manager if this object does not already have one.
 if (nil == _locationManager)
 _locationManager = [[CLLocationManager alloc] init];
 
 _locationManager.delegate = self;
 _locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
 _locationManager.distanceFilter = 50;
 [_locationManager startUpdatingLocation];
 // [_locationManager startMonitoringSignificantLocationChanges];
 }
 
 #pragma mark
 #pragma mark CLLocationManager delegate methods
 - (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
 {
 NSLog(@"%@ ----- %@", self, NSStringFromSelector(_cmd));
 
 // If it's a relatively recent event, turn off updates to save power
 NSDate* eventDate = newLocation.timestamp;
 NSTimeInterval howRecent = [eventDate timeIntervalSinceNow];
 
 if (abs(howRecent) < 2.0)
 {
 NSLog(@"recent: %g", abs(howRecent));
 NSLog(@"latitude %+.6f, longitude %+.6f\n", newLocation.coordinate.latitude, newLocation.coordinate.longitude);
 }
 
 // else skip the event and process the next one
 }
 
 - (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
 {
 NSLog(@"%@ ----- %@", self, NSStringFromSelector(_cmd));
 NSLog(@"error: %@",error);
 }
 */

#pragma mark
#pragma mark MKMapViewDelegate

- (void)mapView:(MKMapView *)mapView didAddOverlayViews:(NSArray *)overlayViews
{
    //NSLog(@"%@ ----- %@", self, NSStringFromSelector(_cmd));
    //NSLog(@"overlayViews: %@", overlayViews);
}

/*
- (MKOverlayView *)mapView:(MKMapView *)mapView viewForOverlay:(id <MKOverlay>)overlay
{
    //NSLog(@"%@ ----- %@", self, NSStringFromSelector(_cmd));
    
	MKOverlayView* overlayView = nil;
	
	if(overlay == self.routeLine)
	{
		//if we have not yet created an overlay view for this overlay, create it now.
        if (self.routeLineView) {
            [self.routeLineView removeFromSuperview];
        }
        
        self.routeLineView = [[MKPolylineView alloc] initWithPolyline:self.routeLine];
        self.routeLineView.fillColor = [UIColor redColor];
        self.routeLineView.strokeColor = [UIColor redColor];
        self.routeLineView.lineWidth = 4.0;//10
        
		overlayView = self.routeLineView;
	}
	
	return overlayView;
}
*/
/*
 - (void)mapViewWillStartLoadingMap:(MKMapView *)mapView
 {
 NSLog(@"mapViewWillStartLoadingMap:(MKMapView *)mapView");
 }
 
 - (void)mapViewDidFinishLoadingMap:(MKMapView *)mapView
 {
 NSLog(@"mapViewDidFinishLoadingMap:(MKMapView *)mapView");
 }
 
 - (void)mapViewDidFailLoadingMap:(MKMapView *)mapView withError:(NSError *)error
 {
 NSLog(@"mapViewDidFailLoadingMap:(MKMapView *)mapView withError:(NSError *)error");
 }
 
 - (void)mapView:(MKMapView *)mapView regionWillChangeAnimated:(BOOL)animated
 {
 NSLog(@"mapView:(MKMapView *)mapView regionWillChangeAnimated:(BOOL)animated");
 NSLog(@"%f, %f", mapView.centerCoordinate.latitude, mapView.centerCoordinate.longitude);
 }
 
 - (void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated
 {
 NSLog(@"%@ ----- %@", self, NSStringFromSelector(_cmd));
 NSLog(@"centerCoordinate: %f, %f", mapView.centerCoordinate.latitude, mapView.centerCoordinate.longitude);
 }
 */

- (void)mapView:(MKMapView *)mapView didAddAnnotationViews:(NSArray *)views
{
    //NSLog(@"%@ ----- %@", self, NSStringFromSelector(_cmd));
    //NSLog(@"annotation views: %@", views);
}

/*
 - (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control
 {
 NSLog(@"%@ ----- %@", self, NSStringFromSelector(_cmd));
 }
 
 - (void)mapView:(MKMapView *)mapView didChangeUserTrackingMode:(MKUserTrackingMode)mode animated:(BOOL)animated
 {
 NSLog(@"mapView:(MKMapView *)mapView didChangeUserTrackingMode:(MKUserTrackingMode)mode animated:(BOOL)animated");
 }
 
 - (void)mapViewWillStartLocatingUser:(MKMapView *)mapView
 {
 NSLog(@"mapViewWillStartLocatingUser:(MKMapView *)mapView");
 }
 
 - (void)mapViewDidStopLocatingUser:(MKMapView *)mapView
 {
 NSLog(@"mapViewDidStopLocatingUser:(MKMapView *)mapView");
 }
 */

#pragma --
#pragma mapView

/*
- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
{
    //NSLog(@"%@ ----- %@", self, NSStringFromSelector(_cmd));
    
    CLLocation *location = [[CLLocation alloc] initWithLatitude:userLocation.coordinate.latitude
                                                      longitude:userLocation.coordinate.longitude];
    
    CLLocationCoordinate2D loc = [location coordinate];
    
    if(!CLLocationCoordinate2DIsValid(loc))return;
        
    // check the zero point
    if  (userLocation.coordinate.latitude == 0.0f ||
         userLocation.coordinate.longitude == 0.0f)
        return;
    
    // check the move distance
    if (_points.count > 0) {
        CLLocationDistance distance = [location distanceFromLocation:_currentLocation];
        if (distance < 5)
            return;
    }
    
    //
    [self addSessionGpsData: location];

    
    if (nil == _points) {
        _points = [[NSMutableArray alloc] init];
    }
    
    [_points addObject:location];
    _currentLocation = location;
    
    NSLog(@"points: %@", _points);
    
    [self configureRoutes];
    [self updateHeaderUI];
    
    CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(userLocation.coordinate.latitude, userLocation.coordinate.longitude);
    [self.mapView setCenterCoordinate:coordinate animated:YES];
    
}
*/

#pragma --
#pragma addSessionGpsData

- (void)addSessionGpsData:(CLLocation *)loc
{
    if([AppData sharedAppData].isInSession)
    {
        NSInteger sessionTime = [[AppData sharedAppData] getSessionTime];
        
        if((sessionTime % 2 == 0) && [AppData sharedAppData].cardioLap != nil)
            [[AppData sharedAppData].cardioLap.arrGpsPos addObject:loc];
    }
}

#pragma ---
#pragma share option

- (void)shareBtnClicked
{
    //    [self shareMapOnWall];
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Sharing"
                                                              delegate:self
                                                     cancelButtonTitle:@"Cancel"
                                                destructiveButtonTitle:nil
                                                    otherButtonTitles:@"Wellness wall", @"Facebook", nil];
    
    [actionSheet setActionSheetStyle:UIActionSheetStyleBlackTranslucent];
    
    actionSheet.tag = 999;
	[actionSheet showInView:self.tabBarController.view];
}

//takescreen shots

- (UIImage*)takeScreenShot
{
    UIGraphicsBeginImageContextWithOptions(self.view.frame.size, self.view.opaque, 0.0);
    [self.view.layer renderInContext:UIGraphicsGetCurrentContext()];
    
    UIImage *screenshot = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return screenshot;
    
    //return [UIImage imageNamed:@"btn_bg.png"];
}

//
#pragma --
#pragma share on wall

- (void)shareMapOnWall {
    
    ShareStatusViewController *vc = [[ShareStatusViewController alloc] initWithNibName:@"ShareStatusViewController" bundle:nil];
    
    vc.imageShare = [self takeScreenShot];
    
    [self.navigationController pushViewController:vc animated:NO];

    /*
     NSDateFormatter *dateFormat = [[[NSDateFormatter alloc] init] autorelease];
     [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
     NSString *dateString = [dateFormat stringFromDate:[NSDate date]];
     dateString = [dateString stringByReplacingOccurrencesOfString:@" " withString:@"T"];
     
     NSString *text = @"Cardio Workout";
     
     LoginResponseData *loginResponseData = [LoginPerser getLoginResponse];
     
     [tools startLoading:self.tabBarController.view childView:loadingView text:@"Sharing. Wait…."];
     
     
     request *r=[[request alloc] initWithTarget:self
     SuccessAction:@selector(onSucceffulLogin:resData:)
     FailureAction:@selector(onFailureLogin)];
     NSData *imageContent = UIImagePNGRepresentation([self takeScreenShot]);
     
     
     [r saveWellnessWallPhoto:loginResponseData.memberID date:dateString text:text photo:imageContent];
     [r release];
     */
}


#pragma --
#pragma share on facebook

- (void)shareMapOnFacebook {
    
    [FBSession openActiveSessionWithReadPermissions:@[@"publish_actions"]//[@"publish_actions", @"publish_stream", @"photo_upload", @"user_photos"]
                                       allowLoginUI:YES
                                  completionHandler:^(FBSession *session, FBSessionState status, NSError *error) {
                                      if (session.isOpen) {
                                          [self postFacebook];
                                      }
                                  }];
    
}

- (void)postFacebook {

    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];

    NSMutableDictionary* params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                   @"Cardio Workout", @"message",
                                   UIImagePNGRepresentation([self takeScreenShot]), @"picture",
                                   nil];

    [FBRequestConnection startWithGraphPath:@"me/photos"
                                 parameters:params
                                 HTTPMethod:@"POST"
                          completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {

                              [SVProgressHUD dismiss];
                              [self showMessage:@"Info" message:@"Shared successfully." ];
                          }];
    
}

- (void)showMessage:(NSString *)title message:(NSString *)message
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title
                                                         message:message
                                                        delegate:nil
                                               cancelButtonTitle:@"OK"
                                               otherButtonTitles:nil];
    [alertView show];
}

#pragma --
#pragma updateMap

- (void)updateMap
{
    if(isMapOn)
    {
        //[[NSMutableArray alloc]]
        _points = [NSMutableArray arrayWithArray:self.cardioLap.arrGpsPos];// self.cardioLap.arrGpsPos;
        [self updateHeaderUI];
        [self buildUI];
        [self configureRoutes];
    }
}

@end
