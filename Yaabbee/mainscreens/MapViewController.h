//
//  MapViewController.h
//  NuvitaCardio
//
//  Created by Sayan Chatterjee on 27/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>

#import <FacebookSDK/FacebookSDK.h>

#import "request.h"
#import "NuvitaAppDelegate.h"
#import "MyTools.h"
#import "LoginPerser.h"
#import "LoginResponseData.h"
#import "NuvitaAppDelegate.h"
#import "NewLoginViewController.h"
#import "AppUtility.h"
#import "CardioLap.h"
#import "GpsLocation.h"
#import "ShareStatusViewController.h"

@interface MapViewController : UIViewController <MKMapViewDelegate, CLLocationManagerDelegate, UIActionSheetDelegate>
{
	// the map view
	MKMapView* _mapView;
	
    // routes points
    NSMutableArray* _points;
    
	// the data representing the route points.
	MKPolyline* _routeLine;
	
	// the view we create for the line on the map
	MKPolylineView* _routeLineView;
	
	// the rect that bounds the loaded points
	MKMapRect _routeRect;
    
    // location manager
    CLLocationManager* _locationManager;
    
    // current location
    CLLocation* _currentLocation;
    
    NSInteger requestType;
    
    MyTools *tools;
	UIView *loadingView;
    BOOL isMapOn;
}

@property (retain, nonatomic) CardioLap *cardioLap;

@property (nonatomic, retain) IBOutlet MKMapView* mapView;
@property (nonatomic, retain) NSMutableArray* points;
@property (nonatomic, retain) MKPolyline* routeLine;
@property (nonatomic, retain) MKPolylineView* routeLineView;

@property (nonatomic, retain) CLLocationManager* locationManager;

@property (retain, nonatomic) IBOutlet UILabel *lbElapsedTime;
@property (retain, nonatomic) IBOutlet UILabel *lbDistance;

@property (retain, nonatomic) IBOutlet UIView *headerview;

- (void) configureRoutes;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil val:(id)val;
- (void)buildUI;
- (void)historyClicked;
//- (void)shareMynuvitaClicked;
- (void)updateHeaderUI;
- (UIImage*)takeScreenShot;
- (void)shareBtnClicked;
- (void)mapBtnClicked:(id)sender;
- (void)shareMapOnWall;
- (void)shareMapOnFacebook;

- (void)showMessage:(NSString *)title message:(NSString *)message;
- (void)postFacebook;
- (void)addSessionGpsData:(CLLocation *)loc;
- (void)stopTrackingMap;
- (void)updateMap;

@end
