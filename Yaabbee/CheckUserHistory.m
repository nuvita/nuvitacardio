//
//  CheckUserHistory.m
//  NuvitaCardio
//
//  Created by John on 7/11/14.
//
//

#import "CheckUserHistory.h"

static CheckUserHistory *sharedAppData = nil;

@implementation CheckUserHistory

+ (CheckUserHistory *)sharedAppData {
    @synchronized(self) {
        if (sharedAppData == nil)
            sharedAppData = [[self alloc] init];
    }

    return sharedAppData;
}

- (void)reloadCheckerHistory {
    _hasPairingHistory = NO;
    _hasSessionHistory = NO;
    _fromTest = NO;
    _trendData = nil;
    _isVO2Starting = NO;
}

@end
