//
//  PhotoShareItemCell.h
//  syncClient
//
//  Created by Sayan Chatterjee on 02/11/11.
//  Copyright 2011 ObjectSol. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface PhotoShareItemCell : UITableViewCell {
	IBOutlet UIButton* itemImage;	
}

@property (nonatomic, retain) IBOutlet UIButton* itemImage;

@end

//
@interface PhotoShareNoteCell : UITableViewCell {
	IBOutlet UITextView *noteTextView;
}

@property (nonatomic, retain) IBOutlet UITextView *noteTextView;

@end

//
@interface PhotoSharePhotoCell : UITableViewCell {
	IBOutlet UIButton* itemImage;	
}

@property (nonatomic, retain) IBOutlet UIButton* itemImage;

@end

//
@interface PhotoShareBtnCell : UITableViewCell {
	IBOutlet UIButton* itemImage;	
}

@property (nonatomic, retain) IBOutlet UIButton* itemImage;

@end


//status queue cell
@interface PhotoShareQueueCell : UITableViewCell {
	IBOutlet UIButton* itemImage;
	IBOutlet UILabel* dateLb;
	IBOutlet UILabel* statusLb;
}

@property (nonatomic, retain) IBOutlet UIButton* itemImage;
@property (nonatomic, retain) IBOutlet UILabel* dateLb;
@property (nonatomic, retain) IBOutlet UILabel* statusLb;

@end
