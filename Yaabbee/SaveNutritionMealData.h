//
//  claimActivityOBJ.h
//  Acclaris
//
//  Created by S Biswas on 09/11/10.
//  Copyright 2010 ObjectSol Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "ResponseStatus.h"

@interface SaveNutritionMealData : NSObject<NSXMLParserDelegate> {
		
	//NSMutableArray *StarItemsArray;
	
	NSMutableString *contentOfString;
	
	ResponseStatus *responseStatus;
}


//
@property(nonatomic,retain)ResponseStatus *responseStatus;

- (void)parseXMLFileAtData:(NSMutableData *)data parseError:(NSError **)error;

@end

