//
//  LoggingDetailsViewController.m
//  NuvitaCardio
//
//  Created by John on 2/12/14.
//
//

#import "LoggingDetailsViewController.h"

#import "StringFile.h"

#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>

@interface LoggingDetailsViewController () <MFMailComposeViewControllerDelegate> {

//    MFMailComposeViewController *picker;
}

@end

@implementation LoggingDetailsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)buildUI {

    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MMM dd, yyyy HH:mm a"];
    
    NSString *requestString = self.textview.text;
    requestString = [requestString stringByAppendingString:[NSString stringWithFormat:@"\n\nAction: %@", [self.requestLog valueForKey:@"method"]]];
    requestString = [requestString stringByAppendingString:[NSString stringWithFormat:@"\nDate: %@", [formatter stringFromDate:[self.requestLog objectForKey:@"dateTime"]]]];
    requestString = [requestString stringByAppendingString:[NSString stringWithFormat:@"Request made: %@", [self.requestLog valueForKey:@"action"]]];
    requestString = [requestString stringByAppendingString:[NSString stringWithFormat:@"\nAPI: %@", [self.requestLog objectForKey:@"api"]]];
    requestString = [requestString stringByAppendingString:[NSString stringWithFormat:@"\nConnection status code: %@", [self.requestLog objectForKey:@"statusCodex"]]];
    
    
    requestString = [requestString stringByAppendingString:[NSString stringWithFormat:@"\n\n\nRequest parameters: \n%@", [self.requestLog objectForKey:@"request"]]];
    requestString = [requestString stringByAppendingString:[NSString stringWithFormat:@"\n\n\nRequest response: \n%@", [self.requestLog objectForKey:@"response"]]];

    self.textview.text = requestString;
}

- (void)didTapMailDetails {
    
    if ([[StringFile sharedAppData] saveFile:self.textview.text withFilename:@"singlefile.txt"]) {
        [self openEmailForm];
    }
}

- (void)openEmailForm {

    MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
    picker.mailComposeDelegate = self;
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"singlefile.txt"];
    NSData *myData = [NSData dataWithContentsOfFile:filePath];

    // Present the mail composition interface.
    Class mailClass = (NSClassFromString(@"MFMailComposeViewController"));
    if ([mailClass canSendMail]) {
        //..
        [picker setToRecipients:@[@"mobileapps@nuvita.com"]];
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy-MMdd-HH:mm"];
        NSString *dateString = [formatter stringFromDate:[self.requestLog valueForKey:@"dateTime"]];
        
        [picker setSubject:[NSString stringWithFormat:@"%@ Request Log", [self.requestLog objectForKey:@"method"]]];
        [picker setMessageBody:@"Attached is the txt file of the webservice request selected" isHTML:NO];
        [picker addAttachmentData:myData mimeType:@"application/txt" fileName:[NSString stringWithFormat:@"%@ %@.txt",dateString, [self.requestLog valueForKey:@"method"]]];
        
        [self presentViewController:picker animated:YES completion:^{
            NSLog(@".....loaded mail view");
        }];
    }
    
}


- (void)mailComposeController:(MFMailComposeViewController *)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError *)error {
    
    if (result == MFMailComposeResultSent) {
        NSLog(@"message sucessfully sent");
    }
    
    [self dismissViewControllerAnimated:YES completion:^{
        //...done sending email
    }];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = [self.requestLog objectForKey:@"method"];
    
    UIBarButtonItem *button = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCompose
                                                                            target:self
                                                                            action:@selector(didTapMailDetails)];
    self.navigationItem.rightBarButtonItem = button;
    
    
    [self buildUI];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
