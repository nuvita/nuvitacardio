//
//  GpsLocation.m
//  NuvitaCardio
//
//  Created by S Biswas on 14/04/13.
//
//

#import "GpsLocation.h"

static GpsLocation *sharedAppData = nil;

@implementation GpsLocation
@synthesize currentLocation;
@synthesize locationManager;

+ (GpsLocation *)sharedGpsLocation {
    @synchronized(self) {
        if (sharedAppData == nil)
            sharedAppData = [[self alloc] init];
    }
    return sharedAppData;
}

// [self configureLocationManager];

- (void)startGpsLocation
{
    isRestart = YES;

    //[self stopGpsLocation];
    //[self configureLocationManager];
    
    if (nil != _locationManager)
    {
        [_locationManager stopUpdatingLocation];
                
        [self performSelector:@selector(discardLocationManager) withObject:nil afterDelay:0.1];
    }
    else
        [self performSelector:@selector(configureLocationManager) withObject:nil afterDelay:0.1];

}

- (void)stopGpsLocation
{
    isRestart = NO;
    if (nil != _locationManager)
    {
        [_locationManager stopUpdatingLocation];
        
//        _locationManager.delegate = nil;
//        [_locationManager release];
//        _locationManager = nil;
        
        [self performSelector:@selector(discardLocationManager) withObject:nil afterDelay:0.1];
    }

}


- (void) discardLocationManager
{
    _locationManager.delegate = nil;
    //[_locationManager release];
    _locationManager = nil;
    
    if (isRestart == YES)
    {
        [self performSelector:@selector(configureLocationManager) withObject:nil afterDelay:0.1];
    }
}

#pragma mark
#pragma mark Location Manager

- (void)configureLocationManager
{
    // Create the location manager if this object does not already have one.
    if (nil == _locationManager)
        _locationManager = [[CLLocationManager alloc] init];
    
    _locationManager.delegate = self;
    _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    _locationManager.distanceFilter = 1;
    
    [_locationManager startUpdatingLocation];
    
    // [_locationManager startMonitoringSignificantLocationChanges];
}


#pragma mark
#pragma mark CLLocationManager delegate methods
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    NSLog(@"%@ ----- %@", self, NSStringFromSelector(_cmd));
    
    // If it's a relatively recent event, turn off updates to save power
    NSDate* eventDate = newLocation.timestamp;
    NSTimeInterval howRecent = [eventDate timeIntervalSinceNow];
    
    if (abs(howRecent) < 2.0)
    {
        NSLog(@"recent: %d", abs(howRecent));
        NSLog(@"latitude %+.6f, longitude %+.6f\n", newLocation.coordinate.latitude, newLocation.coordinate.longitude);
    }
    
    //double offset = .00000;
    
    //
    CLLocation *location = [[CLLocation alloc] initWithLatitude:newLocation.coordinate.latitude
                                                      longitude:newLocation.coordinate.longitude];
    
    CLLocationCoordinate2D loc = [location coordinate];
    
    if(!CLLocationCoordinate2DIsValid(loc))return;
    
    // check the zero point
    if  (newLocation.coordinate.latitude == 0.0f ||
         newLocation.coordinate.longitude == 0.0f)
        return;
    
    // check the move distance
    //if (_points.count > 0) {
    //    CLLocationDistance distance = [location distanceFromLocation:self.currentLocation];
    //    if (distance < 2)
    //        return;
    //}
    
    [self addSessionGpsData: location];
    self.currentLocation = location;
    
    //
    /*
    CLLocation *location = [[CLLocation alloc] initWithLatitude:newLocation.coordinate.latitude + offset
                                                      longitude:newLocation.coordinate.longitude + offset];
    // check the zero point
    if  (newLocation.coordinate.latitude == 0.0f ||
         newLocation.coordinate.longitude == 0.0f)
        return;
    
    // check the move distanc
    //CLLocationDistance distance = [location distanceFromLocation:self.currentLocation];
    //if (distance < 2)//5
    //return;
    
    CLLocationCoordinate2D loc = [location coordinate];
    
    if(CLLocationCoordinate2DIsValid(loc))
        self.currentLocation = location;
    
    [self addSessionGpsData];
    */
}


- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"%@ ----- %@", self, NSStringFromSelector(_cmd));
    NSLog(@"error: %@",error);
}


- (void)addSessionGpsData:(CLLocation *)loc
{
    if([AppData sharedAppData].isInSession)
    {
        NSInteger sessionTime = [[AppData sharedAppData] getSessionTime];
        
        if((sessionTime % 2 == 0) && [AppData sharedAppData].cardioLap != nil)
            [[AppData sharedAppData].cardioLap.arrGpsPos addObject:loc];
    }
}


- (void)addSessionGpsData
{
    if([AppData sharedAppData].isInSession)
    {
        NSInteger sessionTime = [[AppData sharedAppData] getSessionTime];
        
        if((sessionTime % 2 == 0) && [AppData sharedAppData].cardioLap != nil && [GpsLocation sharedGpsLocation].currentLocation)
            [[AppData sharedAppData].cardioLap.arrGpsPos addObject:[GpsLocation sharedGpsLocation].currentLocation];
    }
}

/*
CLLocation *location = [[CLLocation alloc] initWithLatitude:userLocation.coordinate.latitude
                                                  longitude:userLocation.coordinate.longitude];

CLLocationCoordinate2D loc = [location coordinate];

if(!CLLocationCoordinate2DIsValid(loc))return;

// check the zero point
if  (userLocation.coordinate.latitude == 0.0f ||
     userLocation.coordinate.longitude == 0.0f)
return;

// check the move distance
if (_points.count > 0) {
    CLLocationDistance distance = [location distanceFromLocation:_currentLocation];
    if (distance < 5)
        return;
}

[self addSessionGpsData: location];


if (nil == _points) {
    _points = [[NSMutableArray alloc] init];
}

[_points addObject:location];
_currentLocation = location;

NSLog(@"points: %@", _points);

[self configureRoutes];
[self updateHeaderUI];

CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(userLocation.coordinate.latitude, userLocation.coordinate.longitude);
[self.mapView setCenterCoordinate:coordinate animated:YES];
}

#pragma --
#pragma addSessionGpsData

- (void)addSessionGpsData:(CLLocation *)loc
{
    if([AppData sharedAppData].isInSession)
    {
        NSInteger sessionTime = [[AppData sharedAppData] getSessionTime];
        
        if((sessionTime % 2 == 0) && [AppData sharedAppData].cardioLap != nil)
            [[AppData sharedAppData].cardioLap.arrGpsPos addObject:loc];
    }
}
*/

@end
