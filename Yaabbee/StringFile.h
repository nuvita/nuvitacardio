//
//  StringFile.h
//  NuvitaCardio
//
//  Created by John on 2/27/14.
//
//

#import <Foundation/Foundation.h>

@interface StringFile : NSObject

+ (StringFile *)sharedAppData;
- (NSString *)convertLogsToString:(NSArray *)logs;
- (BOOL)saveFile:(NSString *)str;
- (BOOL)saveFile:(NSString *)str withFilename:(NSString *)filename;

@end
