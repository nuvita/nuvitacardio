//
//  AppUtility.m
//  RestPos
//
//  Created by S Biswas on 04/02/13.
//  Copyright (c) 2013 Eezy Apps Pty Ltd. All rights reserved.
//

#import "AppUtility.h"

@implementation AppUtility

#pragma mark - validation methods
+ (BOOL)isInputTextNullOrEmpty:(UITextField*)inputText
{
    NSString *trimmedString = [inputText.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    return (!inputText.text || [@"" isEqualToString:trimmedString]);
}

+ (BOOL) isValidEmail:(NSString*)text
{
    NSString *regEx = @"^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}$";
    NSRange r = [text rangeOfString:regEx options:NSRegularExpressionSearch];
    if (r.location == NSNotFound) {
        UIAlertView *av = [[[UIAlertView alloc] initWithTitle:@"Error"
                                                      message:@"Email address is invalid"
                                                     delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] autorelease];
        [av show];
        return NO;
    }
    
    return YES;
}

+ (BOOL) isValidPhoneNumber:(NSString*)text
{
    NSString *regEx = @"^\\d{10}$";
    NSString *phoneValue = text;
    
    NSRange r = [phoneValue
                 rangeOfString:regEx options:NSRegularExpressionSearch];
    if (r.location == NSNotFound) {
        UIAlertView *av = [[[UIAlertView alloc] initWithTitle:@"Error"
                                                      message:@"Contact number must be 10 digits.\n land: (xx) xxxx-xxxx\n mobile: xxxx-xxx-xxx"
                                                     delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] autorelease];
        [av show];
        return NO;
    }
    
    return YES;
}



+ (NSString*) statHeaderDateFormat
{
    NSDate *date = [NSDate date];
    NSDateFormatter *formatter = [[[NSDateFormatter alloc]init] autorelease];
    [formatter setDateFormat:@"E, dd MMM"];
    NSString *dateString = [formatter stringFromDate:date];
    
    return dateString;
}

+ (NSString*) statHeaderDateFormat2
{
    NSDate *date = [NSDate date];
    NSDateFormatter *formatter = [[[NSDateFormatter alloc]init] autorelease];
    [formatter setDateFormat:@"MMM dd"];
    NSString *dateString = [formatter stringFromDate:date];
    
    return dateString;
}

+ (NSString*) orderHeaderFormatedString
{
    NSDate *date = [NSDate date];
    NSDateFormatter *formatter = [[[NSDateFormatter alloc]init] autorelease];
    [formatter setDateFormat:@"MM/dd/yyyy hh:mm a"];
    NSString *dateString = [formatter stringFromDate:date];
    
    return dateString;
}

+ (NSString*) getStartUTCDate
{
//    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
//    
//    //The Z at the end of your string represents Zulu which is UTC
//    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
//    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
//    
//    NSDate* newTime = [dateFormatter dateFromString:@"2011-10-20T01:10:50Z"];
//    NSLog(@"original time: %@", newTime);

    NSDateFormatter *dateFormatter = [[[NSDateFormatter alloc] init] autorelease];
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
    [dateFormatter setTimeZone:timeZone];
    [dateFormatter setDateFormat:@"MM-dd-yyyy'T'HH:mm:ss'Z'"];//yyyy-MM-dd'T'HH:mm:ss'Z'
    NSString *dateString = [dateFormatter stringFromDate:[NSDate date]];
    
    return dateString;
}



@end


