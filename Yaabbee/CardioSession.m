//
//  CardioSession.m
//  NuvitaCardio
//
//  Created by S Biswas on 01/05/13.
//
//

#import "CardioSession.h"


@implementation CardioSession

@synthesize startTimeUTC;
@synthesize sportEnum;
@synthesize arrCardioLapData;

- (void)dealloc
{
    self.startTimeUTC = nil;
    self.arrCardioLapData = nil;
    
    [super dealloc];
}

/*
- (NSMutableString *)getCardioSessionXml:(NSString *)memberId deviceID:(NSString *)deviceID
{
    self.sportEnum = 0;
    
    NSMutableString *sRequest=[[[NSMutableString alloc] init] autorelease];
    
    
    [sRequest appendString:@"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\" xmlns:nuv=\"http://schemas.datacontract.org/2004/07/NuvitaMobileService\">"];
    
    [sRequest appendString:@"<soapenv:Header/>"];
    [sRequest appendString:@"<soapenv:Body>"];
    [sRequest appendString:@"<tem:SaveCardioWorkout>"];
    [sRequest appendString:@"<tem:cardioWokrout>"];
    
    [sRequest appendString:@"<nuv:CardioLaps>"];
    
    for(CardioLap *lap in self.arrCardioLapData)
        [sRequest appendString:[lap getCardioLapXml]];

    [sRequest appendString:@"</nuv:CardioLaps>"];
    
    [sRequest stringByAppendingFormat:@"<nuv:DeviceId>%@</nuv:DeviceId>", APP_DEVICE_ID];
    [sRequest stringByAppendingFormat:@"<nuv:DeviceName>%@</nuv:DeviceName>", @"NuvitaMobilApp"];
    [sRequest stringByAppendingFormat:@"<nuv:SportEnum>%d</nuv:SportEnum>", self.sportEnum];
    [sRequest stringByAppendingFormat:@"<nuv:StartTimeUTCs>%@</nuv:StartTimeUTC>", self.startTimeUTC];
    [sRequest stringByAppendingFormat:@"<nuv:memberId>%@</nuv:memberId>", memberId];

    
    [sRequest appendString:@"</tem:cardioWokrout>"];
    [sRequest appendString:@"</tem:SaveCardioWorkout>"];
    [sRequest appendString:@"</soapenv:Body>"];
    [sRequest appendString:@"</soapenv:Envelope>"];
    
    return sRequest;
}
*/



@end


/*
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tem="http://tempuri.org/" xmlns:nuv="http://schemas.datacontract.org/2004/07/NuvitaMobileService">
<soapenv:Header/>
<soapenv:Body>
<tem:SaveCardioWorkout>
<!--Optional:-->
<tem:cardioWokrout>
<!--Optional:-->
<nuv:CardioLaps>
<!--Zero or more repetitions:-->
<nuv:CardioLap>
<!--Optional:-->
<nuv:Calories>?</nuv:Calories>
<!--Optional:-->
<nuv:DistanceMeters>?</nuv:DistanceMeters>
<!--Optional:-->
<nuv:DurationSeconds>?</nuv:DurationSeconds>
<!--Optional:-->
<nuv:HeartRateSamples>cid:1175316126525</nuv:HeartRateSamples>
<!--Optional:-->
<nuv:SourceEnum>?</nuv:SourceEnum>
<!--Optional:-->
<nuv:UTCDate>?</nuv:UTCDate>
</nuv:CardioLap>
</nuv:CardioLaps>
<!--Optional:-->
<nuv:DeviceId>?</nuv:DeviceId>
<!--Optional:-->
<nuv:DeviceName>?</nuv:DeviceName>
<!--Optional:-->
<nuv:SportEnum>?</nuv:SportEnum>
<!--Optional:-->
<nuv:StartTimeUTC>?</nuv:StartTimeUTC>
<!--Optional:-->
<nuv:memberId>?</nuv:memberId>
</tem:cardioWokrout>
</tem:SaveCardioWorkout>
</soapenv:Body>
</soapenv:Envelope>
*/