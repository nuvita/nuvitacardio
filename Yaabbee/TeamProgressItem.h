

#import <Foundation/Foundation.h>


@interface TeamProgressItem : NSObject  {
    NSString *Avatar;
	NSString *EmailAddress;
	NSString *FirstName;
	NSString *ProgressPercent;
}

@property(nonatomic,retain)NSString *Avatar;
@property(nonatomic,retain)NSString *EmailAddress;
@property(nonatomic,retain)NSString *FirstName;
@property(nonatomic,retain)NSString *ProgressPercent;

@end


//<a:EmailAddress>ron.mcphee@nuvita.com</a:EmailAddress>
//<a:FirstName>Ron</a:FirstName>
//<a:ProgressPercent>35</a:ProgressPercent>