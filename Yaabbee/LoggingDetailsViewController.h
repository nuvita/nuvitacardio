//
//  LoggingDetailsViewController.h
//  NuvitaCardio
//
//  Created by John on 2/12/14.
//
//

#import <UIKit/UIKit.h>

@interface LoggingDetailsViewController : UIViewController

@property (nonatomic, retain) NSDictionary *requestLog;
@property (retain, nonatomic) IBOutlet UITextView *textview;

@end
