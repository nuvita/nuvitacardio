//
//  DevicesViewController.h
//  NuvitaCardio
//
//  Created by John on 2/21/14.
//
//

#import <UIKit/UIKit.h>
#import "constant.h"
#import "NuvitaAppDelegate.h"
#import "OfflineManager.h"
#import "CMonitor.h"
#import "DeviceCell.h"
#import "JFNavigationTransitionDelegate.h"

@interface DevicesViewController : UIViewController

@property (retain, nonatomic) IBOutlet UITableView *tableview;
@end
