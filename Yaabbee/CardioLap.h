//
//  CardioLap.h
//  NuvitaCardio
//
//  Created by S Biswas on 07/04/13.
//
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface CardioLap : NSObject

@property (retain, nonatomic) NSString *startLapTime;
@property (retain, nonatomic) NSMutableDictionary *heartRateSamples;
@property (assign) NSInteger sourceEnum;

@property (assign) NSInteger durationSeconds;
@property (assign) CGFloat distanceMeters; //0 if unknown
@property (assign) CGFloat calories; //0 if unknown

@property (retain, nonatomic) NSMutableArray *arrGpsPos;

@property (assign) NSInteger zoneBelow; //0 if unknown
@property (assign) NSInteger zoneIn; //0 if unknown
@property (assign) NSInteger zoneAbove; //0 if unknown

@property (assign) NSInteger rhRate;

@property (assign) NSInteger hrZeroDropCount;
@property (assign) BOOL btConnectionLost;

- (void)defVal;

- (void)loadLapData:(NSDictionary *)dict;
- (NSDictionary *)getLapData;

- (NSString *)getCaloriesNumericText;
- (NSString *)getCaloriesText;
- (NSString *)getBelowZoneText;
- (NSString *)getInZoneText;
- (NSString *)getAboveZoneText;
- (NSString *)getInAboveZoneText;

- (NSString *)getFormatedFloatVal:(NSInteger)val;
- (NSString *)getStartSessionTimeText;

- (float)getAvgHrm;
- (NSString *)workoutTime;
- (NSMutableArray *)getGpsData;
- (NSMutableArray *)loadGpsData:(NSMutableArray *)arrData;

- (NSString *)getHR;
- (NSString *)getMaxHR;
//avg rt
- (NSString *)getAvgHR;
- (NSString *)getCalAvg;

- (NSMutableString *)getCardioLapXml;
- (NSString *)getDistanceMetersText;

- (NSMutableString *)getCardioSessionXml:(NSString *)memberId;

//- (NSMutableString *)getCardioSessionXml:(NSString *)memberId deviceID:(NSString *)deviceID;

//week total

- (NSString *)getTotalCaloriesText:(float)val;
- (NSString *)getTotalBelowZoneText:(int)val;
- (NSString *)getTotalInZoneText:(int)val;
- (NSString *)getTotalAboveZoneText:(int)val;
- (NSString *)getTotalInAboveZoneText:(int)val;

- (void)updateHRM;
- (void)addHRM:(NSInteger)hrmValue;
- (NSInteger)getTotalInAboveZone:(int)val;
- (NSString *)getHrmString;

+ (NSString*) getStartUTCDate;

@end


/*

 class CardioLap     {
                 public string UTCDate                                     //Should be UTC time stamp at start of Cardio Lap
                 public byte[] HeartRateSamples                // one sample every 5 seconds
                 public ushort SourceEnum                            //should always be 1 (NuvitaMobilApp)
                 public ushort DurationSeconds                   //should = HeartRateSamples .length *5
                 public ushort DistanceMeters                    //zero if unknown
                 public ushort Calories
 }
*/