//
//  Message.h
//
//
//  Created by Abhijit Mukherjee on 27/08/11.
//  Copyright 2011 CastleRock Research. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "constant.h"

@interface Message : NSObject

@property (nonatomic, retain) NSString *messageText;
@property (nonatomic, retain) NSString *senderName;
@property (nonatomic, retain) NSDate *sentAt;
@property (nonatomic, retain) NSDate *updatedAt;

- (CGSize)computeMessageLengthToFit;
- (CGSize)computeMessageLengthToFit:(CGSize)restrainSize withFontSize:(CGFloat)fontSize;
@end
