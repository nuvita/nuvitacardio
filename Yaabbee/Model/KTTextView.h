#import <UIKit/UIKit.h>

@interface KTTextView : UITextView 
{
   UILabel *_placeholder;
}

@property (nonatomic, copy) NSString *placeholderText;
@property (nonatomic, retain) UIColor *placeholderColor;

@end
