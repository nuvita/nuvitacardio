//
//  CustTableHeaderView.h
//  Nuvita
//
//  Created by Sayan Chatterjee on 08/12/11.
//  Copyright 2011 ObjectSol. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface CustTableHeaderView : UIView {
	NSMutableArray *columns;
}
@property (nonatomic, retain) NSMutableArray *columns;
//- (void)addColumn:(CGFloat)position;
- (void)numberOfCol:(NSInteger) num width:(NSInteger)wd;

@end

