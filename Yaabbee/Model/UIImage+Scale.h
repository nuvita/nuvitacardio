//
//  Imagescall.h
//  mynuvita
//
//  Created by S Biswas on 10/09/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UIImage (ImageScale)
- (UIImage*)scaleToSize:(CGSize)size;
@end
