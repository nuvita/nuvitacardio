//
//  PhotoShareItemCell.m
//  syncClient
//
//  Created by Sayan Chatterjee on 02/11/11.
//  Copyright 2011 ObjectSol. All rights reserved.
//

#import "CustTableCell.h"

@implementation CustTableCell
@synthesize columns;

- (void)addColumn:(CGFloat)position {
	if(!self.columns)  self.columns = [[NSMutableArray alloc] init];
	[self.columns addObject:[NSNumber numberWithFloat:position]];
}

- (void)numberOfCol:(NSInteger) num width:(NSInteger)wd
{
	if(!self.columns)  self.columns = [[NSMutableArray alloc] init];
	
	float gap = wd/num;
	float pos = 0;
	
	for(int ii = 0; ii < num; ii++)
	{
		pos = ii*gap;
		[self.columns addObject:[NSNumber numberWithFloat:pos]];
	}
}

- (void)drawRect:(CGRect)rect {
	CGContextRef ctx = UIGraphicsGetCurrentContext();
	// Use the same color and width as the default cell separator for now
	CGContextSetRGBStrokeColor(ctx, 0.5, 0.5, 0.5, 1.0);
	CGContextSetLineWidth(ctx, 0.25);
	
	for (int i = 0; i < [self.columns count]; i++) {
		CGFloat f = [((NSNumber*) [self.columns objectAtIndex:i]) floatValue];
		CGContextMoveToPoint(ctx, f, 0);
		CGContextAddLineToPoint(ctx, f, self.bounds.size.height);
	}
	
	CGContextStrokePath(ctx);
	
	[super drawRect:rect];
}

@end
