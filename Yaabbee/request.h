//
//  request.h
//  Trail
//
//  Created by mmandal on 02/10/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NuvitaAppDelegate.h"
#import "errorResponseAlert.h"
#import "MyTools.h"

#import "MemberProgressData.h"
#import "TeamProgressData.h"
#import "StarProgressData.h"

#import "CardioWeekData.h"
#import "MobilityWeekData.h"
#import "LessonWeekData.h"
#import "EduLessonWeekData.h"
#import "SaveQuizData.h"
#import "SaveMobilityData.h"
#import "NutritionDayData.h"
#import "SaveNutritionMealData.h"
#import "SaveNutritionDayData.h"
#import "MobilityXData.h"
#import "SaveMobilityXWorkout.h"
#import "WellnessWallData.h"

#import "MobilityWorkoutsData.h"
#import "MobilityExerciseData.h"
#import "SaveWellnessWallData.h"
#import "SaveMealsCameraData.h"

@interface request : NSObject 
{
	NuvitaAppDelegate *app;
	NSMutableData *d2;
	NSString *whichAPI;
	MyTools *tools;
	
	id target;
	SEL successHandler;
	SEL failureHandler;
	BOOL isstatus;
    BOOL isOfflineData;
}

@property (strong, nonatomic) OfflineManager *offlineManager;

- (id)initWithTarget:(id)actionTarget
	  SuccessAction:(SEL)successAction 
	  FailureAction:(SEL)failureAction;
//- (NSMutableString*)createHeaderfirstLogin;

- (NSString *) stringToHex:(NSString *)str;

- (void)getProposedCardioProgram:(NSDictionary *)data;
- (void)saveProposedCardioProgram:(NSDictionary *)data;
- (void)deleteVo2MaxXML:(NSDictionary *)data;
- (void)getHealthMeasurementCurrentTrends:(NSDictionary *)data;
- (void)getVo2MaxFromRRService:(NSString *)requestXML;
- (void)getActivityLevel;

- (void)loginRequest:(NSString *)user pass:(NSString *)password;
- (void)saveCardioSession:(NSString *)requestXml text:(NSString *)text;
- (void)saveCardioSessionOffline:(NSString *)requestXml text:(NSString *)text;

- (void)getTeamProgress:(NSString *)memberID date:(NSString *)weekDate;
- (void)getMemberProgress:(NSString *)memberID date:(NSString *)weekDate;
- (void)getNutritionDay:(NSString *)memberID date:(NSString *)date;
- (void)getCardioWeek:(NSString *)memberID date:(NSString *)date;

//- (void)getStars:(NSString *)memberID date:(NSString *)weekDate;
//- (void)getMobilityWeek:(NSString *)memberID date:(NSString *)date;


- (void)getStars:(NSString *)memberID date:(NSString *)weekDate;
- (void)getGlobalStars:(NSString *)pageNum date:(NSString *)weekDate;

- (void)getMobilityWeek:(NSString *)memberID date:(NSString *)date;
- (void)getMobilityXWeek:(NSString *)memberID date:(NSString *)date;
- (void)saveMobilityXWeek:(NSString *)memberID date:(NSString *)date WorkoutId:(NSString*)WorkoutId Rounds:(NSString*)Rounds;

- (void)getEducationWeek:(NSString *)memberID date:(NSString *)weekDate;
- (void)getEducationLessonWeek:(NSString *)memberID date:(NSString *)weekDate lessonNumber:(NSString *)lessonNumber;

- (void)getWellnessWall:(NSString *)memberID pageNumber:(NSString *)pageNumber;
- (void)saveWellnessWall:(NSString *)memberID date:(NSString *)date text:(NSString*)text photo:(NSString*)photo;
- (void)saveMealsCamera:(NSString *)memberID date:(NSString *)date mealEnum:(NSString *)mealEnum photo:(NSData *)photo;

- (void)saveWellnessWallPhoto:(NSString *)memberID date:(NSString *)date text:(NSString *)text photo:(NSData *)photo;


- (void)saveQuiz:(NSString *)memberID date:(NSString *)weekDate lessonNumber:(NSString *)lessonNumber correctAnswers:(NSString*)correctAnswers totalQuestions:(NSString*)totalQuestions;

- (void)SaveMobility:(NSString *)xmldata;

- (void)saveNutritionMeal:(NSString *)memberID date:(NSString *)date mealEnum:(NSString *)mealEnum description:(NSString*)description caloriesConsumed:(NSString*)caloriesConsumed healthRanking:(NSString*)healthRanking targetCalories:(NSString*)targetCalories;

- (void)saveNutritionDay:(NSString *)memberID date:(NSString *)date Breakfast:(NSString *)Breakfast AMSnack:(NSString*)AMSnack PMSnack:(NSString*)PMSnack Water:(NSString*)Water Vitamin:(NSString*)Vitamin;

- (void)callPostMethod:(NSMutableString *)sRequest Action:(NSString *)action API:(NSString *)api;
- (NSString *)getUTCFormateDate:(NSDate *)localDate;

//
- (void)getMobilityWorkouts:(NSString *)memberID date:(NSString *)date;
//- (void)getMobilityExercises:(NSString *)workoutId;
- (void)getMobilityExercises:(NSString *)memberID workoutId:(NSString *)workoutId;

- (BOOL)isNetAvalaible;
- (void)callMultiPostMethod:(NSData *)postBody Action:(NSString *)action API:(NSString *)api;

@end
