//
//  InputViewController.m
//  NuvitaCardio
//
//  Created by John on 2/27/14.
//
//

#import "InputViewController.h"

@interface InputViewController () <UITextFieldDelegate> {

    UITextField *textfield;
    CGRect keyboardFrame;
}

@end

@implementation InputViewController

#pragma mark - Methods

- (void)getInputData {
    
    if ([self.delegate respondsToSelector:@selector(inputViewController:didEnterName:)]) {
        [self.delegate inputViewController:self didEnterName:textfield.text];
    }
}

- (void)didTapDoneButton {
//    [self getInputData];
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

#pragma mark - View Life Cycle

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    textfield = [[UITextField alloc] initWithFrame:CGRectMake(20, 0, self.tableview.frame.size.width - 40, 44)];
    textfield.autocapitalizationType = UITextAutocapitalizationTypeWords;
    textfield.placeholder = @"Example: Joe’s Monitor";
    textfield.delegate = self;
    [textfield becomeFirstResponder];
    
   
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardOnScreen:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    
    
    
}

- (void)keyboardOnScreen:(NSNotification *)notification {
    NSDictionary *info  = notification.userInfo;
    NSValue      *value = info[UIKeyboardFrameEndUserInfoKey];
    
    keyboardFrame = [value CGRectValue];
    
    [self updateUI];
}

- (void)updateUI {
    
    CGRect frame = _tableview.frame;
    frame.origin.y = self.view.frame.size.height - keyboardFrame.size.height - _tableview.frame.size.height;
    
    _tableview.frame = frame;
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:YES];
    [self getInputData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    [cell addSubview:textfield];
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(10, 0, tableView.frame.size.width - 20, 22)];
    UILabel *label = [[UILabel alloc] initWithFrame:view.frame];
    label.font = [UIFont systemFontOfSize:14];
    label.text = @"Create a common name for this device: ";
    
    [view addSubview:label];
    return view;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 40)];
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(tableView.frame.size.width - 70, 5, 60, 30)];
    [button setBackgroundImage:[UIImage imageNamed:@"btn_done.png"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(didTapDoneButton) forControlEvents:UIControlEventTouchUpInside];
    
    [view addSubview:button];
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 22;
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    return YES;
}

@end
