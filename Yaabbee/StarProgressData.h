//
//  claimActivityOBJ.h
//  Acclaris
//
//  Created by S Biswas on 09/11/10.
//  Copyright 2010 ObjectSol Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "StarItem.h"
#import "ResponseStatus.h"

@class StarItem;

@interface StarProgressData : NSObject<NSXMLParserDelegate> {
		
	NSMutableArray *StarItemsArray;
	
	NSMutableString *contentOfString;
	
	ResponseStatus *responseStatus;
	StarItem *starItem;
	
	NSString *StarsTitle, *WeekLabel;
}

@property(nonatomic,retain)NSString *StarsTitle, *WeekLabel;
//@property(nonatomic,retain)NSString *TeamName;

@property(nonatomic,retain)NSMutableArray *StarItemsArray;

//
@property(nonatomic,retain)ResponseStatus *responseStatus;

- (void)parseXMLFileAtData:(NSMutableData *)data parseError:(NSError **)error;

@end


//<a:TeamId>0f0a7226-6e59-4719-913d-79041c836299</a:TeamId>
//<a:TeamName>Live Lean Nation</a:TeamName>