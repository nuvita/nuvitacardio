//
//  claimActivityOBJ.h
//  Acclaris
//
//  Created by S Biswas on 09/11/10.
//  Copyright 2010 ObjectSol Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "ResponseStatus.h"

@interface MobilityWorkoutsData : NSObject<NSXMLParserDelegate> {
		
	NSMutableArray *itemsArray;
	NSMutableDictionary *dictItem;
	
	NSMutableString *contentOfString;
	
	ResponseStatus *responseStatus;
}


@property(nonatomic,retain)NSMutableDictionary *dictMobilityWorkouts;

@property(nonatomic,retain)NSString *WeekLabel, *MemberId, *Date;

//
@property(nonatomic,retain)ResponseStatus *responseStatus;

- (void)parseXMLFileAtData:(NSMutableData *)data parseError:(NSError **)error;

- (BOOL)getBoolVal:(NSString*)val;

- (NSString*)getMobilityXml:(NSString*)date;

@end

/*
 
 */
