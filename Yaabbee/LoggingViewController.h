//
//  LoggingViewController.h
//  NuvitaCardio
//
//  Created by John on 2/12/14.
//
//

#import <UIKit/UIKit.h>
#import "NuvitaAppDelegate.h"

@interface LoggingViewController : UIViewController

@property (retain, nonatomic) IBOutlet UITableView *tableview;
@end
