//
//  MenuViewController.m
//  NuvitaCardio
//
//  Created by John on 3/31/14.
//
//

#import "MenuViewController.h"

@interface MenuViewController ()

@property (retain, nonatomic) IBOutlet UITableView *tableview;

@end

@implementation MenuViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

    [self.slidingViewController setAnchorRightRevealAmount:170.0f];
    self.slidingViewController.underLeftWidthLayout = ECFullWidth;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    [self loadSlidingMenu];
}

- (void)loadSlidingMenu {
    
    UIImageView *imageview = (UIImageView *)[self.view viewWithTag:11111];
    if (imageview == nil) {
        imageview = [[UIImageView alloc] initWithFrame:CGRectMake(45, 95, 80, 80)];
        imageview.contentMode = UIViewContentModeScaleAspectFit;
        imageview.backgroundColor = [UIColor grayColor];
        imageview.tag = 11111;
        imageview.layer.cornerRadius = 40;
        imageview.layer.borderWidth = 2.0;
        imageview.layer.borderColor = [UIColor colorWithRed:0.2 green:0.4 blue:0.6 alpha:1.0].CGColor;
        imageview.clipsToBounds = YES;
        
        [self.view addSubview:imageview];
    }

    UIActivityIndicatorView	*progressIndicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(imageview.frame.size.width/2 - 10, imageview.frame.size.height/2 - 10, 40, 40)];
    progressIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhite;
    progressIndicator.autoresizingMask = (UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin);
    [progressIndicator sizeToFit];
    
    [imageview addSubview:progressIndicator];
    [progressIndicator startAnimating];
    
    
    LoginResponseData *loginData = [LoginPerser getLoginResponse];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 180, 170, 14)];
    label.textAlignment = NSTextAlignmentCenter;
    label.font = [UIFont fontWithName:DEFAULT_BOLD_FONT_NAME size:12.0];
    label.text = [NSString stringWithFormat:@"%@", loginData ? loginData.firstName : @""];
    [self.view addSubview:label];
    
    NSArray *objects = [NSArray arrayWithObjects:progressIndicator,[loginData avator], imageview, nil];
    
    if ([NuvitaAppDelegate isNetworkAvailable])
        [self performSelectorInBackground:@selector(loadImage:) withObject:objects];
}

- (void)loadImage:(NSArray *)infoArr {
    
    NSString *urlStr = [infoArr objectAtIndex:1];
    NSURL *url = [[NSURL alloc] initWithString:urlStr];
    NSData *myData = [[NSData alloc] initWithContentsOfURL:url];
    if ([myData length] > 0) {
        UIImage *image = [[UIImage alloc] initWithData:[[NSData alloc] initWithContentsOfURL:url]];
        if (image) {
            UIImageView *imageview = [infoArr lastObject];
            [imageview setImage:image];
        }
    }

    UIActivityIndicatorView	*progressIndicator = [infoArr firstObject];
    [progressIndicator stopAnimating];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        cell.textLabel.font = [UIFont fontWithName:DEFAULT_NORMAL_FONT_NAME size:defaultFontSize];
    }
    
    switch ([indexPath row]) {
        case 0:
            cell.textLabel.text = @"Lock Screen";
            cell.imageView.image = [UIImage imageNamed:@"lock-icon.png"];
            break;
            
        case 1:
            cell.textLabel.text = @"Logout";
            cell.imageView.image = [UIImage imageNamed:@"logout-icon.png"];
            break;
            
//        case 2:
//            cell.textLabel.text = @"Date Picker Test";
//            break;
            
        default:
            break;
    }
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.tableview deselectRowAtIndexPath:indexPath animated:YES];
    if ([self.delegate respondsToSelector:@selector(menuViewController:didSelectMenu:)]) {
        [self.delegate menuViewController:self didSelectMenu:indexPath.row];
    }
}

@end
