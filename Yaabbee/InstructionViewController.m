//
//  InstructionViewController.m
//  NuvitaCardio
//
//  Created by John on 5/24/14.
//
//

#import "InstructionViewController.h"
#import "InitialTestViewController.h"
#import "VO2MaxChartViewController.h"
#import "WeightViewController.h"

@interface InstructionViewController () <JFButtonViewDelegate>

@property (retain, nonatomic) OfflineManager *offlineManager;
@property (retain, nonatomic) InitialTestViewController *initialTestViewController;

@end

@implementation InstructionViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        UIImageView *textView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"logo.png"]];
        [self.navigationItem setTitleView:textView];
        
        UIView *view = [[UIView alloc] initWithFrame:self.view.bounds];
        view.tag = 1000;
        view.backgroundColor = [UIColor whiteColor];
        
        [self.view addSubview:view];
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    //... initiate corebluetooth
    
    if ([[CheckUserHistory sharedAppData] fromTest]) {
        _initialTestViewController = [[AppData sharedAppData] activeInitialTestViewController] ? [[AppData sharedAppData] activeInitialTestViewController] : [[InitialTestViewController alloc] init];
        
        if ([[CheckUserHistory sharedAppData] hasPairingHistory]) {
            if (!_initialTestViewController.peripheral) {
                [_initialTestViewController viewDidLoad];
                [_initialTestViewController btConnectClicked];
            }
        }
        
        [[AppData sharedAppData] setActiveInitialTestViewController:_initialTestViewController];
    }
    
    [self initOfflineManager];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:YES];
    [self.tabBarController.tabBar setHidden:YES];

    [self buildRightNavigationItem:@"Next"];
    [self buildLeftNavigationItem: (_history) ? @"Not Now" : @"Back" ];
    
    [self performSelector:@selector(removeWhiteBackground) withObject:nil afterDelay:0.5];
    [self formatToSuperscriptText];
}

- (void)removeWhiteBackground {
    UIView *view = (UIView *)[self.view viewWithTag:1000];
    [view removeFromSuperview];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationLandscapeRight || interfaceOrientation == UIInterfaceOrientationLandscapeLeft);
}

- (BOOL)shouldAutorotate {
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskAll;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationLandscapeLeft;
}

#pragma mark - Public Methods

- (void)buildRightNavigationItem:(NSString *)title {
    
    JFButtonView *buttonview2 = [[JFButtonView alloc] initWithFrame:CGRectMake(0, 0, 200, CGRectGetHeight(self.navigationController.navigationBar.frame))];
    [buttonview2 setButtonTitle:title];
    [buttonview2 setHeight:CGRectGetHeight(self.navigationController.navigationBar.frame)];
    [buttonview2 setBackButton:NO];
    [buttonview2 setDelegate:self];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:buttonview2];
}

- (void)buildLeftNavigationItem:(NSString *)title {
    
    JFButtonView *buttonview = [[JFButtonView alloc] initWithFrame:CGRectMake(0, 0, 200, CGRectGetHeight(self.navigationController.navigationBar.frame))];
    [buttonview setButtonTitle:title];
    [buttonview setHeight:CGRectGetHeight(self.navigationController.navigationBar.frame)];
    [buttonview setBackButton:YES];
    [buttonview setDelegate:self];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:buttonview];
}

- (void)initOfflineManager {
    
    NuvitaAppDelegate *appDelegate = (NuvitaAppDelegate *)[[UIApplication sharedApplication] delegate];
    _offlineManager = [[OfflineManager alloc] init];
    _offlineManager.managedObjectContext = appDelegate.managedObjectContext;
}

- (void)formatToSuperscriptText {
    for (id subview in [self.view subviews]) {
        if ([subview isKindOfClass:[UILabel class]]) {
            
            UILabel *label = (UILabel *)subview;
            label.text = [[[label.text stringByReplacingOccurrencesOfString:@"vo2" withString:@"VO\u00B2max"] stringByReplacingOccurrencesOfString:@"VO2" withString:@"VO\u00B2"] stringByReplacingOccurrencesOfString:@"Vo2" withString:@"VO\u00B2"];
        }
    }
}

- (void)removeCorebluetoothConnection {
    
    if (_initialTestViewController.peripheral) {
        [_initialTestViewController.manager stopScan];
        [_initialTestViewController.manager cancelPeripheralConnection:_initialTestViewController.peripheral];
        [_initialTestViewController.peripheral setDelegate:nil];
        
    }

    _initialTestViewController.peripheral = nil;
    _initialTestViewController.manager = nil;
}

- (void)didTapBack {
    
    
    
    if ([[CheckUserHistory sharedAppData] fromTest]) {
        
        [self removeCorebluetoothConnection];
        [self.navigationController popToRootViewControllerAnimated:YES];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"LOGOUT" object:nil];
        
    } else {
        
        [self.navigationController popViewControllerAnimated:YES];
        [(NuvitaAppDelegate *)[[UIApplication sharedApplication] delegate] performSelector:@selector(reloadAppDelegateRootViewController) withObject:nil afterDelay:1.0];
    }
}

- (void)showStatPage {
    
    if ([[CheckUserHistory sharedAppData] fromTest]) {

        [self removeCorebluetoothConnection];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"update_connect_btn" object:[NSNumber numberWithBool:YES]];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"FINISH_INITIAL_TEST" object:[NSNumber numberWithInteger:0]];
        
    } else {
        
        [self.navigationController popToRootViewControllerAnimated:YES];
        [(NuvitaAppDelegate *)[[UIApplication sharedApplication] delegate] performSelector:@selector(reloadAppDelegateRootViewController) withObject:nil afterDelay:1.0];
    }
}

- (void)didTapNext {
    
    self.navigationItem.rightBarButtonItem = nil;
    if (_infoData) [self showVo2Max];
    else [self showScalling];
}

- (void)showVo2Max {
    
    VO2MaxChartViewController *vo2MaxChartViewController = [[VO2MaxChartViewController alloc] init];
    vo2MaxChartViewController.resultInfo = _infoData;

    [self.navigationController pushViewController:vo2MaxChartViewController animated:YES];
}

- (void)showScalling {

    WeightViewController *weightViewController = [[WeightViewController alloc] init];
    weightViewController.noRecordFound = YES;
    
    [self.navigationController pushViewController:weightViewController animated:YES];
}

#pragma mark - JFButtonViewDelegate

- (void)buttonView:(JFButtonView *)view didTapButton:(JFButton *)button {
    
    if ([button backButton]) {
        if (_history)[self showStatPage];
        else [self didTapBack];
        return;
    }
    
    [self didTapNext];
}

@end
