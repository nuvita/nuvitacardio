//
//  CardioSession.h
//  NuvitaCardio
//
//  Created by S Biswas on 01/05/13.
//
//

#import <Foundation/Foundation.h>
#import "CardioLap.h"

@interface CardioSession : NSObject

@property (retain, nonatomic) NSMutableArray *arrCardioLapData;
@property (retain, nonatomic) NSString *startTimeUTC;
@property (assign) NSInteger sportEnum;


//- (NSMutableString *)getCardioSessionXml:(NSString *)memberId deviceID:(NSString *)deviceID;


@end
