//
//  InitialTestViewController.m
//  NuvitaCardio
//
//  Created by John on 5/24/14.
//
//

#import "InitialTestViewController.h"
#import "InstructionViewController.h"

@interface InitialTestViewController ()


@property (retain, nonatomic) IBOutlet UILabel *welcomelbl;

@property (retain, nonatomic) OfflineManager *offlineManager;
@property (retain, nonatomic) NuvitaXMLParser *nuvitaXMLParser;
@property (retain, nonatomic) NSTimer *timer;
@property (assign) BOOL isPairingComplete;

@end

@implementation InitialTestViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        UIImageView *textView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"logo.png"]];
        [self.navigationItem setTitleView:textView];
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    //... load custom view

}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(isConnectedToPeripheral:)
                                                 name:@"PAIRING_COMPLETE"
                                               object:nil];

    [[NSBundle mainBundle] loadNibNamed:@"InitialTestViewController" owner:self options:nil];
    
    [[CheckUserHistory sharedAppData] setMustPrompt:YES];
    [self.tabBarController.tabBar setHidden:YES];
 
    self.navigationItem.leftBarButtonItem = nil;
    self.navigationItem.rightBarButtonItem = nil;
    self.navigationItem.hidesBackButton = YES;
    
    LoginResponseData *loginData = [LoginPerser getLoginResponse];
    _welcomelbl.text = [NSString stringWithFormat:@"Welcome %@", [loginData firstName]];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:YES];
    [[CheckUserHistory sharedAppData] setMustPrompt:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
    return (toInterfaceOrientation != UIInterfaceOrientationPortraitUpsideDown || toInterfaceOrientation != UIInterfaceOrientationPortrait);
}

- (BOOL)shouldAutorotate {
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationPortrait;
}

#pragma mark - Public Methods

- (void)initiateScrollingProperty {

    for (id subview in [self.view subviews]) {
        if ([subview isKindOfClass:[UIScrollView class]]) {

            UIScrollView *scrollview = (UIScrollView *)subview;
            UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
            if (orientation == UIInterfaceOrientationPortrait || orientation == UIInterfaceOrientationPortraitUpsideDown)
                scrollview.contentSize = CGSizeMake(320, 568 + 88);
            else
                scrollview.contentSize = CGSizeMake(568, 320);
        }
    }

}

- (void)isConnectedToPeripheral:(NSNotification *)notification {
    
    BOOL connected = [notification.object boolValue];
    if (connected) {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"PAIRING_COMPLETE" object:nil];
        CheckUserHistory *userHistory = [CheckUserHistory sharedAppData];
        userHistory.hasPairingHistory = YES;
        
        
        
        if ([userHistory trendData]) {
            userHistory.fromTest = NO;
            [self showStatPage];
        } else {
            
            userHistory.fromTest = YES;
            [self showVo2SequenceTest];
        }
      
    }
}

- (void)removeCorebluetoothConnection {

    if (self.peripheral) {
        [self.manager stopScan];
        [self.manager cancelPeripheralConnection:self.peripheral];
        [self.peripheral setDelegate:nil];
        
    }
    self.peripheral = nil;
    self.manager = nil;
}

- (void)showStatPage {
    
    [self removeCorebluetoothConnection];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"update_connect_btn" object:[NSNumber numberWithBool:YES]];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"FINISH_INITIAL_TEST" object:[NSNumber numberWithInteger:0]];
}

- (void)showVo2SequenceTest {
    
    InstructionViewController *instructionViewController = [[InstructionViewController alloc] initWithNibName:@"InstructionViewController" bundle:nil];
    instructionViewController.history = [[CheckUserHistory sharedAppData] hasSessionHistory];
    instructionViewController.infoData = [[CheckUserHistory sharedAppData] trendData];
    
    [self.navigationController pushViewController:instructionViewController animated:YES];
    if ([[CheckUserHistory sharedAppData] fromTest]) {
        [(NuvitaAppDelegate *)[[UIApplication sharedApplication] delegate] performSelector:@selector(reloadAppDelegateNavRootViewControllerLandscape)
                                                                                withObject:nil
                                                                                afterDelay:1.0];
    } else {
        [(NuvitaAppDelegate *)[[UIApplication sharedApplication] delegate] performSelector:@selector(reloadAppDelegateRootViewControllerLandscape)
                                                                                withObject:nil
                                                                                afterDelay:1.0];
    }
}

#pragma mark - Web Service Request

- (void)getHealthMeasurementCurrentTrends {
    LoginResponseData *loginResponseData = [LoginPerser getLoginResponse];
    NSDictionary *data = [NSDictionary dictionaryWithObjectsAndKeys:[loginResponseData memberID], @"memberId", nil];
    
    request *r = [[request alloc] initWithTarget:self
                                   SuccessAction:@selector(onSuccessfulFetchRequest:resData:)
                                   FailureAction:@selector(onFailedFetch)];
    
    [r getHealthMeasurementCurrentTrends:data];
}

#pragma mark - Web Service Delegate

- (void)onSuccessfulFetchRequest:(ResponseStatus *)responseStatus resData:(NSObject *)obj {
    _nuvitaXMLParser = (NuvitaXMLParser *)obj;
}

- (void)onFailedFetch {

}

@end
