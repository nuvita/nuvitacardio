//
//  Logging.m
//  NuvitaCardio
//
//  Created by John on 2/12/14.
//
//

#import "Logging.h"

@implementation Logging

- (id)initWithCoder:(NSCoder *)decoder {
    self = [super init];
    if (self) {
        //...
        self.method = [decoder decodeObjectForKey:@"method"];
        self.request = [decoder decodeObjectForKey:@"request"];
        self.action = [decoder decodeObjectForKey:@"action"];
        self.api = [decoder decodeObjectForKey:@"api"];
        self.dateTime = [decoder decodeObjectForKey:@"dateTime"];
        self.response = [decoder decodeObjectForKey:@"response"];
        self.statusCodex = [decoder decodeObjectForKey:@"statusCodex"];
        self.activeRequest = [decoder decodeObjectForKey:@"activeRequest"];
       
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder {
    
    [encoder encodeObject:self.method forKey:@"method"];
    [encoder encodeObject:self.request forKey:@"request"];
    [encoder encodeObject:self.action forKey:@"action"];
    [encoder encodeObject:self.api forKey:@"api"];
    [encoder encodeObject:self.dateTime forKey:@"dateTime"];
    [encoder encodeObject:self.response forKey:@"response"];
    [encoder encodeObject:self.statusCodex forKey:@"statusCodex"];
    [encoder encodeObject:self.activeRequest forKey:@"activeRequest"];
}


- (NSDictionary *)requestData {
    
    NSDictionary *data = [NSDictionary dictionaryWithObjectsAndKeys:
                          self.request, @"request",
                          self.action, @"action",
                          self.api, @"api",
                          self.dateTime, @"dateTime",
                          self.response, @"response",
                          self.statusCodex, @"statusCodex",
                          self.activeRequest, @"activeRequest",
                          self.method, @"method",
                          nil];
    return data;
}

@end
