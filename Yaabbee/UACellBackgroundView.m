
//#define TABLE_CELL_BACKGROUND    {1,1,1,1,0.866,0.866,0.866,1}
#define TABLE_CELL_BACKGROUND    {48.0/255,130.0/255,186.0/255,1.0,5.0/255,50.0/255,79.0/255,1.0}
#define kDefaultMargin 10

#import "UACellBackgroundView.h"

static void addRoundedRectToPath(CGContextRef context, CGRect rect, float ovalWidth,float ovalHeight);

@implementation UACellBackgroundView

@synthesize position;

- (BOOL) isOpaque
{
    return NO;
}

- (void)drawRect:(CGRect)aRect
{
    int lineWidth = 1;
	CGRect rect = [self bounds];
    rect.size.width -= lineWidth;
    rect.size.height -= lineWidth;
    rect.origin.x += lineWidth / 2.0;
    rect.origin.y += lineWidth / 2.0;
	
    CGFloat minx = CGRectGetMinX(rect), midx = CGRectGetMidX(rect), maxx = CGRectGetMaxX(rect);
    CGFloat miny = CGRectGetMinY(rect), midy = CGRectGetMidY(rect), maxy = CGRectGetMaxY(rect);
    maxy += 1;
	
    CGFloat locations[2] = { 0.0, 1.0 };
	
    CGContextRef c = UIGraphicsGetCurrentContext();
    CGColorSpaceRef myColorspace = CGColorSpaceCreateDeviceRGB();
    CGGradientRef myGradient = nil;
    CGFloat components[8] = TABLE_CELL_BACKGROUND;
    CGContextSetStrokeColorWithColor(c, [[UIColor lightGrayColor] CGColor]);
    CGContextSetLineWidth(c, lineWidth);
    CGContextSetAllowsAntialiasing(c, YES);
    CGContextSetShouldAntialias(c, YES);
	
    if (position == UACellBackgroundViewPositionTop)
	{
        CGMutablePathRef path = CGPathCreateMutable();
        CGPathMoveToPoint(path, NULL, minx, maxy);
        CGPathAddArcToPoint(path, NULL, minx, miny, midx, miny, kDefaultMargin);
		//CGPathAddArcToPoint(path, NULL, minx, miny, midx, miny, 100);
        CGPathAddArcToPoint(path, NULL, maxx, miny, maxx, maxy, kDefaultMargin);
        CGPathAddLineToPoint(path, NULL, maxx, maxy);
		//CGPathAddLineToPoint(path, NULL, maxx, 10);
        CGPathAddLineToPoint(path, NULL, minx, maxy);
        CGPathCloseSubpath(path);
		
		CGContextSaveGState(c);
		CGContextAddPath(c, path);
		CGContextClip(c);
		
		myGradient = CGGradientCreateWithColorComponents(myColorspace, components, locations, 2);
		CGContextDrawLinearGradient(c, myGradient, CGPointMake(minx,miny), CGPointMake(minx,maxy), 0);
		
		CGContextAddPath(c, path);
		CGContextStrokePath(c);
		CGContextRestoreGState(c);
		
    } 
	else if (position == UACellBackgroundViewPositionBottom)
	{
		maxy -= 1;
		
		CGMutablePathRef path = CGPathCreateMutable();
        CGPathMoveToPoint(path, NULL, minx, miny);
        CGPathAddArcToPoint(path, NULL, minx, maxy, midx, maxy, kDefaultMargin);
        CGPathAddArcToPoint(path, NULL, maxx, maxy, maxx, miny, kDefaultMargin);
        CGPathAddLineToPoint(path, NULL, maxx, miny);
        CGPathAddLineToPoint(path, NULL, minx, miny);
		CGPathCloseSubpath(path);
		
		CGContextSaveGState(c);
		CGContextAddPath(c, path);
		CGContextClip(c);
		
		myGradient = CGGradientCreateWithColorComponents(myColorspace, components, locations, 2);
		CGContextDrawLinearGradient(c, myGradient, CGPointMake(minx,miny), CGPointMake(minx,maxy), 0);
		
		CGContextAddPath(c, path);
		CGContextStrokePath(c);
		CGContextRestoreGState(c);
    }
	else if (position == UACellBackgroundViewPositionMiddle)
	{
		CGMutablePathRef path = CGPathCreateMutable();
		CGPathMoveToPoint(path, NULL, minx, miny);
		CGPathAddLineToPoint(path, NULL, maxx, miny);
		CGPathAddLineToPoint(path, NULL, maxx, maxy);
		CGPathAddLineToPoint(path, NULL, minx, maxy);
		CGPathAddLineToPoint(path, NULL, minx, miny);
		CGPathCloseSubpath(path);
		
		CGContextSaveGState(c);
		CGContextAddPath(c, path);
		CGContextClip(c);
		
		myGradient = CGGradientCreateWithColorComponents(myColorspace, components, locations, 2);
		CGContextDrawLinearGradient(c, myGradient, CGPointMake(minx,miny), CGPointMake(minx,maxy), 0);
		
		CGContextAddPath(c, path);
		CGContextStrokePath(c);
		CGContextRestoreGState(c);
    }
	else if (position == UACellBackgroundViewPositionSingle)
	{
		maxy -= 1;
		
		CGMutablePathRef path = CGPathCreateMutable();
        CGPathMoveToPoint(path, NULL, minx, midy);
        CGPathAddArcToPoint(path, NULL, minx, miny, midx, miny, kDefaultMargin);
        CGPathAddArcToPoint(path, NULL, maxx, miny, maxx, midy, kDefaultMargin);
        CGPathAddArcToPoint(path, NULL, maxx, maxy, midx, maxy, kDefaultMargin);
        CGPathAddArcToPoint(path, NULL, minx, maxy, minx, midy, kDefaultMargin);
		CGPathCloseSubpath(path);
		
		CGContextSaveGState(c);
		CGContextAddPath(c, path);
		CGContextClip(c);
		
		myGradient = CGGradientCreateWithColorComponents(myColorspace, components, locations, 2);
		CGContextDrawLinearGradient(c, myGradient, CGPointMake(minx,miny), CGPointMake(minx,maxy), 0);
		
		CGContextAddPath(c, path);
		CGContextStrokePath(c);
		CGContextRestoreGState(c);	
    }
	
    CGColorSpaceRelease(myColorspace);
    CGGradientRelease(myGradient);
    return;
}

- (void)dealloc
{
    [super dealloc];
}

- (void)setPosition:(UACellBackgroundViewPosition)newPosition
{
    if (position != newPosition)
	{
        position = newPosition;
        [self setNeedsDisplay];
    }
}

@end

static void addRoundedRectToPath(CGContextRef context, CGRect rect, float ovalWidth,float ovalHeight)
{
    float fw, fh;
	
    if (ovalWidth == 0 || ovalHeight == 0)
	{
        CGContextAddRect(context, rect);
        return;
    }
	
    CGContextSaveGState(context);
	
    CGContextTranslateCTM (context, CGRectGetMinX(rect),CGRectGetMinY(rect));
    CGContextScaleCTM (context, ovalWidth, ovalHeight);
    fw = CGRectGetWidth (rect) / ovalWidth;
    fh = CGRectGetHeight (rect) / ovalHeight;
	
    CGContextMoveToPoint(context, fw, fh/2); 
    CGContextAddArcToPoint(context, fw, fh, fw/2, fh, 1);
    CGContextAddArcToPoint(context, 0, fh, 0, fh/2, 1);
    CGContextAddArcToPoint(context, 0, 0, fw/2, 0, 1);
    CGContextAddArcToPoint(context, fw, 0, fw, fh/2, 1);
    CGContextClosePath(context);
	
    CGContextRestoreGState(context);
}