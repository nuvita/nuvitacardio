

#import "LineChartView.h"
#import "AppData.h"

#define H_VAL_ORIGIN 70

@interface LineChartView()
{
    CALayer *linesLayer;
    
    
    UIView *popView;
    UILabel *disLabel;

    NSInteger xxOffset;
    NSInteger xxRightOffset;

    NSInteger yyOffset;
}

@end

@implementation LineChartView

@synthesize array;

@synthesize hInterval,vInterval;

@synthesize hDesc,vDesc;
@synthesize HD, WD;
@synthesize multiple;


- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        
        hInterval = 12;
        vInterval = 10;
        
        linesLayer = [[CALayer alloc] init];
        linesLayer.masksToBounds = YES;
        linesLayer.contentsGravity = kCAGravityLeft;
        linesLayer.backgroundColor = [[UIColor colorWithRed:0.853 green:1.000 blue:1.000 alpha:1.000] CGColor];
        
        [self.layer addSublayer:linesLayer];
        
        self.multiple = 1;

        self.WD = self.frame.size.width; //304
        self.HD = self.frame.size.height; //345
        
        yyOffset = 20;
        xxOffset = 28;
        xxRightOffset= 8;
        
        self.vDesc = [[NSMutableArray alloc]initWithCapacity:11];
        for (int i=0; i<11; i++) {
            [self.vDesc addObject:[NSString stringWithFormat:@"%d",(i + 7)*10]];
        }
        
        self.hDesc = [[NSMutableArray alloc]initWithCapacity:12];
        
        [self.hDesc addObject:@"1"];
        [self.hDesc addObject:@"2"];
        [self.hDesc addObject:@"3"];
        [self.hDesc addObject:@"4"];
        [self.hDesc addObject:@"5"];
        [self.hDesc addObject:@"6"];
        [self.hDesc addObject:@"7"];
        [self.hDesc addObject:@"8"];
        [self.hDesc addObject:@"9"];
        [self.hDesc addObject:@"10"];
        [self.hDesc addObject:@"11"];
        [self.hDesc addObject:@"12"];
        
    }
    return self;
}

- (NSString *)graphTimeLabel:(NSInteger)val {
    return [NSString stringWithFormat:@"%d",val * self.multiple];
}

- (void)drawRect:(CGRect)rect {
    [self setClearsContextBeforeDrawing: YES];
    
    CGContextRef context = UIGraphicsGetCurrentContext();
 
    CGFloat backLineWidth = 1.0f;
    CGFloat backMiterLimit = 0.f;
    
    CGContextSetLineWidth(context, backLineWidth);
    CGContextSetMiterLimit(context, backMiterLimit);
    CGContextSetLineJoin(context, kCGLineJoinRound);
    CGContextSetLineCap(context, kCGLineCapRound );
    CGContextSetBlendMode(context, kCGBlendModeNormal);
    CGContextSetStrokeColorWithColor(context, [UIColor colorWithWhite:0.788 alpha:1.0].CGColor);
    
    float dx = xxOffset;
    float dy = (self.frame.size.height - yyOffset);
    float vvUnit = (self.HD - yyOffset)/11;
    float hhUnit = (self.WD - xxOffset - xxRightOffset)/12;
    int y = self.frame.size.height;
    float endXX = (self.WD - xxRightOffset);
    

    int yAxisGapValue = round((_maximum - _minimum) / 10.0);
    for (int i = 0; i < 11; i++) {
        
        CGPoint bPoint = CGPointMake(xxOffset, y);
        CGPoint ePoint = CGPointMake(endXX, y);

        NSString *labelString = [NSString stringWithFormat:@"%d", _minimum + (yAxisGapValue * i)];
        [labelString drawInRect:CGRectMake(bPoint.x - xxOffset - 2, bPoint.y - yyOffset - 10, xxOffset, 10)
                       withFont:[UIFont systemFontOfSize:10]
                  lineBreakMode:NSLineBreakByClipping
                      alignment:NSTextAlignmentRight];
        
        
        
        CGContextMoveToPoint(context, bPoint.x, bPoint.y-yyOffset);
        CGContextAddLineToPoint(context, ePoint.x, ePoint.y-yyOffset);
        y -= vvUnit;
        
    }

    CGPoint bPoint = CGPointMake(xxOffset, self.HD);
    CGPoint ePoint = CGPointMake(xxOffset, y - self.HD);
    
    CGContextMoveToPoint(context, bPoint.x, bPoint.y-yyOffset);
    CGContextAddLineToPoint(context, ePoint.x, ePoint.y-yyOffset);

    float fldWd = 20;

    for (int i = 0; i < hDesc.count; i++) {

        NSString *labelString = [self graphTimeLabel:[[hDesc objectAtIndex:i] integerValue]];
        [labelString drawInRect:CGRectMake(xxOffset + (i + 1)*hhUnit - fldWd/2, self.HD - yyOffset, hhUnit, 10)
                       withFont:[UIFont systemFontOfSize:10]
                  lineBreakMode:NSLineBreakByClipping
                      alignment:NSTextAlignmentCenter];
    }
    
    CGContextStrokePath(context);


    CGFloat pointLineWidth = 2.0f;
    CGFloat pointMiterLimit = 5.0f;
    
    CGContextSetLineWidth(context, pointLineWidth);
    CGContextSetMiterLimit(context, pointMiterLimit);
    CGContextSetLineJoin(context, kCGLineJoinRound);
    CGContextSetLineCap(context, kCGLineCapRound );
    CGContextSetBlendMode(context, kCGBlendModeNormal);
    CGContextSetStrokeColorWithColor(context, [UIColor colorWithRed:0.701 green:0.060 blue:0.069 alpha:1.000].CGColor);

    
    float xxUnit = hhUnit/12;
    if([array count] > 0) {
        CGPoint p1 = [[array objectAtIndex:0] CGPointValue];
        p1.y -= _minimum;

        int i = 1;
        CGContextMoveToPoint(context, (p1.x*xxUnit) + xxOffset, (self.HD - yyOffset)-p1.y*vvUnit/yAxisGapValue);

        for (; i<[array count]; i++) {
            p1 = [[array objectAtIndex:i] CGPointValue];
            p1.y -= _minimum;
            
            CGPoint goPoint = CGPointMake(p1.x*xxUnit + xxOffset, (self.HD - yyOffset) - p1.y*vvUnit/yAxisGapValue);
            CGContextAddLineToPoint(context, goPoint.x, goPoint.y);;
        }
    }
    
	CGContextStrokePath(context);
 

    float xxInZone1 = dx;
    float yyInZone1 = (dy - vvUnit*([AppData sharedAppData].inzoneHighLimit - _minimum)/yAxisGapValue);
    float wdInZone = (self.frame.size.width - xxOffset - xxRightOffset) ;
    float hdInZone = (vvUnit * ([AppData sharedAppData].inzoneHighLimit - _minimum) / yAxisGapValue - vvUnit * ([AppData sharedAppData].inzoneLowLimit - _minimum) / yAxisGapValue);

    CGContextSetRGBFillColor(context, 0.0, 1.0, 0.0, 0.5);
    CGContextBeginPath(context);
    CGRect rectP = CGRectMake(xxInZone1, yyInZone1, wdInZone, hdInZone);
    CGContextAddRect(context, rectP);
    CGContextFillPath(context);
}

#pragma --

- (void)btAction:(id)sender{
    [disLabel setText:@"100"];
    
    UIButton *bt = (UIButton*)sender;
    popView.center = CGPointMake(bt.center.x, bt.center.y - popView.frame.size.height/2);
    [popView setAlpha:1.0f];
}

@end
