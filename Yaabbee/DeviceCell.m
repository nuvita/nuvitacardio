//
//  DeviceCell.m
//  NuvitaCardio
//
//  Created by John on 3/4/14.
//
//

#import "DeviceCell.h"

@implementation DeviceCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        self.namelbl = [[UILabel alloc] initWithFrame:CGRectMake(60, 0, 255, 23)];
        [self.namelbl setFont:[UIFont fontWithName:@"HelveticaNeue" size:20.0f]];
        [self.namelbl setTranslatesAutoresizingMaskIntoConstraints:NO];
        [self.contentView addSubview:self.namelbl];
        
        self.seriallbl = [[UILabel alloc] initWithFrame:CGRectMake(60, 23, 255, 12)];
        [self.seriallbl setTextColor:[UIColor grayColor]];
        [self.seriallbl setFont:[UIFont fontWithName:@"HelveticaNeue" size:12.0f]];
        [self.seriallbl setTranslatesAutoresizingMaskIntoConstraints:NO];
        [self.contentView addSubview:self.seriallbl];
        
        self.batterylbl = [[UILabel alloc] initWithFrame:CGRectMake(60, 35, 255, 12)];
        [self.batterylbl setTextColor:[UIColor grayColor]];
        [self.batterylbl setFont:[UIFont fontWithName:@"HelveticaNeue" size:12.0f]];
        [self.batterylbl setTranslatesAutoresizingMaskIntoConstraints:NO];
        [self.contentView addSubview:self.batterylbl];

    }
    
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
