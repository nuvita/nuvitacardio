//
//  GpsLocation.h
//  NuvitaCardio
//
//  Created by S Biswas on 14/04/13.
//
//

#import <Foundation/Foundation.h>

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@interface GpsLocation : NSObject <CLLocationManagerDelegate>
{
// location manager
CLLocationManager* _locationManager;

// current location
//CLLocation* _currentLocation;
    BOOL isRestart;
}

@property (nonatomic, retain) CLLocation* currentLocation;

@property (nonatomic, retain) CLLocationManager* locationManager;

+ (GpsLocation *)sharedGpsLocation;

- (void)configureLocationManager;

- (void)startGpsLocation;
- (void)stopGpsLocation;
- (void)discardLocationManager;
- (void)addSessionGpsData;
- (void)addSessionGpsData:(CLLocation *)loc;

@end
