//
//  claimActivityOBJ.m
//  Acclaris
//
//  Created by S Biswas on 09/11/10.
//  Copyright 2010 ObjectSol Technologies. All rights reserved.
//

#import "SaveMealsCameraData.h"

#define SAVE_MEALS_PHOTO_RESULT @"UploadMealPhotoResult"
#define ERROR_MSG @"a:ErrorMessage"


@implementation SaveMealsCameraData

@synthesize responseStatus;

#pragma mark Parse Xml Data

- (void)parseXMLFileAtData:(NSMutableData *)data parseError:(NSError **)error
{
	NSXMLParser *parser;
	parser = [[NSXMLParser alloc] initWithData:data];
	[parser setDelegate:self];
	[parser setShouldProcessNamespaces:NO];
	[parser setShouldReportNamespacePrefixes:NO];
	[parser setShouldResolveExternalEntities:NO];
	[parser parse];
	
	NSError *parseError = [parser parserError];
	
	if (parseError && error) 
	{
		*error = parseError;
	}
	
	[parser release];
}

- (void)parserDidStartDocument:(NSXMLParser *)parser
{
	responseStatus = [[ResponseStatus alloc]init];
	//self.ItemsArray = [[NSMutableArray alloc]init];
}		

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
	if (qName)
	{
		elementName = qName;
	}
	else 
		if([elementName isEqualToString:SAVE_MEALS_PHOTO_RESULT])
		{
			contentOfString=[NSMutableString string];
			[contentOfString retain];
			return;		
		}
        else
            if([elementName isEqualToString:ERROR_MSG])
            {
                contentOfString=[NSMutableString string];
                [contentOfString retain];
                return;		
            }

}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName     
{	
	if (qName)
	{
		elementName = qName;
	}
	else 
			if([elementName isEqualToString:SAVE_MEALS_PHOTO_RESULT])
			{
				if(contentOfString)
				{
					responseStatus.returnCode = contentOfString;
					[contentOfString release];
					contentOfString = nil;
				}				
			}//
    else
        if([elementName isEqualToString:ERROR_MSG])
        {
            if(contentOfString)
            {
                responseStatus.errorText = contentOfString;
                [contentOfString release];
                contentOfString = nil;
            }				
        }
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
	if(contentOfString)
		[contentOfString appendString:string];
}

- (void)parserDidEndDocument:(NSXMLParser *)parse
{
}	


@end


/*
 
 <SaveMemberQuizResponse xmlns="http://tempuri.org/">
 <SaveMemberQuizResult>false</SaveMemberQuizResult>
 </SaveMemberQuizResponse> 
 
 */

