//
//  DeviceListViewController.h
//  NuvitaCardio
//
//  Created by John on 7/14/14.
//
//

#import <UIKit/UIKit.h>
#import <CoreBluetooth/CoreBluetooth.h>

#import "NuvitaAppDelegate.h"

@class DeviceListViewController;
@protocol DeviceListViewControllerDelegate <NSObject>

- (void)didChooseDevice:(CBPeripheral *)selectedPeripheral;

@end

@interface DeviceListViewController : UIViewController

@property (retain, nonatomic) id <DeviceListViewControllerDelegate> delegate;
@property (retain, nonatomic) NSArray *devices;

@end
