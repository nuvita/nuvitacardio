

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface LineChartView : UIView

@property (assign) NSInteger HD;
@property (assign) NSInteger WD;

@property (assign) NSInteger multiple;
@property (assign) NSInteger maximum;
@property (assign) NSInteger minimum;


@property (assign) NSInteger hInterval;
@property (assign) NSInteger vInterval;

@property (nonatomic, strong) NSMutableArray *hDesc;
@property (nonatomic, strong) NSMutableArray *vDesc;

@property (nonatomic, strong) NSMutableArray *array;

- (NSString *)graphTimeLabel:(NSInteger)val;

@end
