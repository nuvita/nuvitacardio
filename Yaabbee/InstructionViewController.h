//
//  InstructionViewController.h
//  NuvitaCardio
//
//  Created by John on 5/24/14.
//
//

#import <UIKit/UIKit.h>
#import "NuvitaAppDelegate.h"
#import "JFButton.h"
#import "JFButtonView.h"
#import "OfflineManager.h"
#import "NuvitaXMLParser.h"
#import "AppData.h"

@interface InstructionViewController : UIViewController

@property (retain, nonatomic) NSDictionary *infoData;
@property (assign) BOOL history;

@end
