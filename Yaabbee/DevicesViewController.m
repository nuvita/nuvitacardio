//
//  DevicesViewController.m
//  NuvitaCardio
//
//  Created by John on 2/21/14.
//
//

#import "DevicesViewController.h"
#import "AddDeviceViewController.h"

@interface DevicesViewController () <UIActionSheetDelegate> {

    NSMutableArray *arrDevices;
}

@property (strong, nonatomic) JFNavigationTransitionDelegate *navigationTransitionDelegate;
@property (strong, nonatomic) OfflineManager *offlineManager;

@end

@implementation DevicesViewController

#pragma mark - View Life Cycle

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        UIImageView *textView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"logo.png"]];
        [self.navigationItem setTitleView:textView];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    arrDevices = [[NSMutableArray alloc] init];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd
                                                                                           target:self
                                                                                           action:@selector(addNewDevice)];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didFoundNotification)
                                                 name:UPDATE_DEVICE_STATUS
                                               object:nil];
    
    _navigationTransitionDelegate = [[JFNavigationTransitionDelegate alloc] init];
    self.navigationController.delegate = _navigationTransitionDelegate;
    
    //... initialize offline mode
    [self initOfflineManager];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    [self loadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - NotificationCenter Observer

- (void)addNewDevice {
    
    AddDeviceViewController *addDeviceViewController = [[AddDeviceViewController alloc] init];
    addDeviceViewController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:addDeviceViewController animated:YES];
}

- (void)didFoundNotification {
    [self loadData];
}

#pragma mark - Method

- (void)openActionSheetAtIndex:(NSInteger)index {
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Choose from the following"
                                                             delegate:self
                                                    cancelButtonTitle:@"Cancel"
                                               destructiveButtonTitle:@"Delete"
                                                    otherButtonTitles:nil];
    
    [actionSheet setTag:index];
    [actionSheet setActionSheetStyle:UIActionSheetStyleBlackTranslucent];
    [actionSheet showInView:self.tabBarController.view];
}

- (void)initOfflineManager {
    
    NuvitaAppDelegate *appDelegate = (NuvitaAppDelegate *)[[UIApplication sharedApplication] delegate];
    self.offlineManager = [[OfflineManager alloc] init];
    self.offlineManager.managedObjectContext = appDelegate.managedObjectContext;
}

- (void)loadData {
    [arrDevices removeAllObjects];
    [arrDevices addObjectsFromArray:[self.offlineManager getNotIgnoredDevices]];

    [self.tableview reloadData];
}

- (void)deleteMonitor:(CMonitor *)monitor {

    [self.offlineManager deleteMonitor:monitor];
    [self loadData];
}

#pragma mark - UITableViewDataSource

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return @"Paired Devices";
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [arrDevices count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"Cell";
    DeviceCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[DeviceCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    }

    CMonitor *monitor = [arrDevices objectAtIndex:indexPath.row];
    cell.namelbl.text = [monitor inputName];
    cell.seriallbl.text = [monitor serialnumber];
    
    cell.imageView.image = [UIImage imageNamed:@"unconnected-icon.png"];
    cell.batterylbl.text = @"Battery: NA";
    
    if ([monitor.serialnumber isEqualToString:[[AppData sharedAppData] serialNumber]]) {
        cell.imageView.image = [UIImage imageNamed:@"connected-icon.png"];
        
        if ([AppData sharedAppData].cardioBatteryLevel && [[AppData sharedAppData].cardioBatteryLevel length] > 0)
            cell.batterylbl.text = [NSString stringWithFormat:@"Battery: %@", [AppData sharedAppData].cardioBatteryLevel];
    }

    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.tableview deselectRowAtIndexPath:indexPath animated:YES];
    
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if ([cell.imageView.image isEqual:[UIImage imageNamed:@"connected-icon.png"]]) {
        UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:@"Error"
                                                            message:@"Device is currently in use. Please disconnect device to modify this item."
                                                           delegate:nil
                                                  cancelButtonTitle:@"Ok"
                                                  otherButtonTitles:nil];
        [alertview show];
        return;
    }
    
    [self openActionSheetAtIndex:indexPath.row];
}

#pragma mark - UIActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {

    if (buttonIndex == 0) {
        [self deleteMonitor:[arrDevices objectAtIndex:actionSheet.tag]];
    }
//    else if (buttonIndex == 1) {
//        
//        CMonitor *monitor = [arrDevices objectAtIndex:[[_tableview indexPathForSelectedRow] row]];
//        [[NSNotificationCenter defaultCenter] postNotificationName:@"CONNECT_THIS_PERIPHERAL" object:[monitor serialnumber]];
//        [self.navigationController popToRootViewControllerAnimated:YES];
//    }
}

@end
