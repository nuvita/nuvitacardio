//
//  claimActivityOBJ.m
//  Acclaris
//
//  Created by S Biswas on 09/11/10.
//  Copyright 2010 ObjectSol Technologies. All rights reserved.
//

#import "LoginResponseData.h"

@implementation LoginResponseData

@synthesize firstName;
@synthesize lastName;
@synthesize memberID;
@synthesize programName;
@synthesize today;
@synthesize avator;
@synthesize weight;
@synthesize useCameraMeals;


- (id)initWithCoder:(NSCoder *)decoder {
    self = [super init];
    if (self) {
        
        self.firstName = [decoder decodeObjectForKey:@"firstName"];
        self.lastName = [decoder decodeObjectForKey:@"lastName"];
        self.memberID = [decoder decodeObjectForKey:@"memberID"];
        self.programName = [decoder decodeObjectForKey:@"programName"];
        self.today = [decoder decodeObjectForKey:@"today"];
        self.avator = [decoder decodeObjectForKey:@"avator"];
        self.weight = [decoder decodeObjectForKey:@"weight"];
        self.useCameraMeals = [decoder decodeObjectForKey:@"useCameraMeals"];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder {
    
    [encoder encodeObject:firstName forKey:@"firstName"];
    [encoder encodeObject:lastName forKey:@"lastName"];
    [encoder encodeObject:memberID forKey:@"memberID"];
    [encoder encodeObject:programName forKey:@"programName"];
    [encoder encodeObject:today forKey:@"today"];
    [encoder encodeObject:avator forKey:@"avator"];
    [encoder encodeObject:weight forKey:@"weight"];
    [encoder encodeObject:useCameraMeals forKey:@"useCameraMeals"];
}

@end
