//
//  claimActivityOBJ.h
//  Acclaris
//
//  Created by S Biswas on 09/11/10.
//  Copyright 2010 ObjectSol Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "ResponseStatus.h"
#import "constant.h"

@interface NutritionDayData : NSObject<NSXMLParserDelegate> {
		
	//NSMutableArray *StarItemsArray;
	
	NSMutableString *contentOfString;
	
	ResponseStatus *responseStatus;
	
	NSMutableDictionary *currDict;
}


@property(nonatomic,retain)NSMutableDictionary *nutritionDayDict;

@property(nonatomic,retain)NSString *WeekLabel, *MemberId, *Date,*DayLabel;


//
@property(nonatomic,retain)ResponseStatus *responseStatus;

- (void)parseXMLFileAtData:(NSMutableData *)data parseError:(NSError **)error;

- (BOOL)getBoolVal:(NSString*)val;

- (NSString*)getMobilityXml:(NSString*)date;


@end

