//
//  Logging.h
//  NuvitaCardio
//
//  Created by John on 2/12/14.
//
//

#import <Foundation/Foundation.h>

@interface Logging : NSObject

@property (nonatomic, retain) NSString *method;
@property (nonatomic, retain) NSString *request;
@property (nonatomic, retain) NSString *action;
@property (nonatomic, retain) NSString *api;
@property (nonatomic, retain) NSDate *dateTime;
@property (nonatomic, retain) NSString *response;
@property (nonatomic, retain) NSString *statusCodex;
@property (nonatomic, retain) NSMutableURLRequest *activeRequest;

- (NSDictionary *)requestData;

@end
