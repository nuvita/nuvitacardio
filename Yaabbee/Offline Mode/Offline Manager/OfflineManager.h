//
//  OfflineManager.h
//  NuvitaCardio
//
//  Created by John on 1/31/14.
//
//

#import <Foundation/Foundation.h>

@class Session, CMonitor, Authorize, Logs;
@interface OfflineManager : NSObject


//....new
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

- (BOOL)addAuthorize:(NSDictionary *)data;
- (BOOL)isAuthorize:(NSDictionary *)data;
- (BOOL)updateAuthorize:(Authorize *)authorize didFinishTest:(BOOL)done;
- (Authorize *)checkForDuplicateAuthorize:(NSDictionary *)data;

- (Session *)getSessionWithStartLapTimex:(NSString *)lapTime;
- (BOOL)addNewSession:(NSDictionary *)data withStartLapTime:(NSString *)startLapTime;
- (BOOL)addNewSessionFromAppData:(NSDictionary *)data withStartLapTime:(NSString *)startLapTime;
- (BOOL)deleteLastSession;
- (void)deleteSessionWithStartLaptime:(NSString *)lapTime;
- (void)deleteSession:(Session *)session;
- (BOOL)updateSession:(Session *)oldSession ;
- (BOOL)containsSessionHistory;
- (BOOL)updateSession;
- (NSArray *)getAllSession;
- (NSArray *)getAllSessionLabeledUser;

- (CMonitor *)checkForDuplicateMonitorSerialNo:(NSString *)serialno;
- (BOOL)addNewMonitor:(NSDictionary *)data withSerialNo:(NSString *)serialno;
- (void)deleteMonitor:(CMonitor *)monitor;
- (void)cleanIgnoredMonitorsNow;
- (NSArray *)getNotIgnoredDevices;
- (BOOL)containsDeviceHistory;

- (BOOL)addNewLogs:(NSDictionary *)data;
- (NSArray *)getAllLogs;
- (void)deleteLog:(Logs *)log;

@end
