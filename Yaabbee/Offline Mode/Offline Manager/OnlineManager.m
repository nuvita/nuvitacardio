//
//  OnlineManager.m
//  NuvitaCardio
//
//  Created by John on 2/3/14.
//
//

#import "OnlineManager.h"

#import "request.h"
#import "CardioLap.h"

#import "LoginResponseData.h"
#import "MyTools.h"


#import "Session.h"
#import "Authorize.h"

#import "constant.h"
#import "ResponseStatus.h"
#import "LoginPerser.h"

@implementation OnlineManager {
    
}

- (void)syncAllOfflineSession {
    
//    if ([Session countOfEntities] == 0) {
//        return;
//    }
//    
//    if ([Session countOfEntities] > 0) {
//        Session *session = [Session findFirst];
//        
//        CardioLap *cardiolap = [NSKeyedUnarchiver unarchiveObjectWithData:session.cardioLap];
//        LoginResponseData *loginResponseData = [NSKeyedUnarchiver unarchiveObjectWithData:[session.authorize loginReponseData]];
//        request *r = [[request alloc] initWithTarget:self
//                                       SuccessAction:@selector(onSucceffulSaveCardio:resData:)
//                                       FailureAction:@selector(onFailureSaveCardio)];
//        
//        NSLog(@"temp cardio lap: %@", [cardiolap getLapData]);
//        NSLog(@"cardio xml: %@",[cardiolap getCardioSessionXml:loginResponseData.memberID]);
//        
//        [r saveCardioSessionOffline:[cardiolap getCardioSessionXml:loginResponseData.memberID] text:@"46364638743"];
//        [r release];
//    }

}

- (void)syncSession:(Session *)session withCardioLap:(CardioLap *)lapData {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    if (session) {
        _currentSession = session;
        _currentCardioLap = lapData;
        
        LoginResponseData *loginResponseData = [LoginPerser getLoginResponse];
        request *r = [[request alloc] initWithTarget:self
                                       SuccessAction:@selector(onSucceffulSaveCardio:resData:)
                                       FailureAction:@selector(onFailureSaveCardio)];
        
        [r saveCardioSessionOffline:[lapData getCardioSessionXml:loginResponseData.memberID] text:@"46364638743"];
    }
}

- (void)showMessage:(NSString *)message withTitle:(NSString *)title {
    
    UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:title
                                                        message:message
                                                       delegate:nil
                                              cancelButtonTitle:nil
                                              otherButtonTitles:@"Ok", nil];
    [alertview show];
}

- (void)syncSession {
    //...delete submitted session
    if (_currentSession) {

        NuvitaAppDelegate *appDelegate = (NuvitaAppDelegate *)[[UIApplication sharedApplication] delegate];
        OfflineManager *offlineManager = [[OfflineManager alloc] init];
        offlineManager.managedObjectContext = appDelegate.managedObjectContext;
        [offlineManager updateSession:_currentSession];
    }
}

- (void)onSucceffulSaveCardio:(ResponseStatus *)responseStatus resData:(NSObject *)obj {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	if (responseStatus == nil || ![responseStatus isUpdate]) {
        //..failed
        [self syncSession:_currentSession withCardioLap:_currentCardioLap];
		return ;
	}
	
    if ([_delegate respondsToSelector:@selector(onlineManager:isSuccess:)]) {
        [_delegate onlineManager:self isSuccess:YES];
        [self showMessage:@"Cardio session saved successfully" withTitle:@"NuvitaCardio"];
        [self syncSession];
    }
}

- (void)onFailureSaveCardio {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    
    if ([_delegate respondsToSelector:@selector(onlineManager:isSuccess:)]) {
        [_delegate onlineManager:self isSuccess:NO];
        [self showMessage:@"Something went wrong in saving cardio session." withTitle:@"Error"];
    }
    
    
}

@end
