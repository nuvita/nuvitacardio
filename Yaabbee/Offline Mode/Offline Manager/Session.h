//
//  Session.h
//  NuvitaCardio
//
//  Created by John on 7/8/14.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Authorize;

@interface Session : NSManagedObject

@property (nonatomic, retain) id cardioLap;
@property (nonatomic, retain) NSDate * dateModified;
@property (nonatomic, retain) NSNumber * isNewSession;
@property (nonatomic, retain) NSNumber * isSync;
@property (nonatomic, retain) NSNumber * isMigrated;
@property (nonatomic, retain) Authorize *authorize;

@end
