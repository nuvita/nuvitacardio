//
//  Authorize.h
//  NuvitaCardio
//
//  Created by John on 7/8/14.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Session;

@interface Authorize : NSManagedObject

@property (nonatomic, retain) NSDate * dateModified;
@property (nonatomic, retain) NSNumber * initialTest;
@property (nonatomic, retain) id loginReponseData;
@property (nonatomic, retain) NSString * password;
@property (nonatomic, retain) NSString * username;
@property (nonatomic, retain) NSSet *session;
@end

@interface Authorize (CoreDataGeneratedAccessors)

- (void)addSessionObject:(Session *)value;
- (void)removeSessionObject:(Session *)value;
- (void)addSession:(NSSet *)values;
- (void)removeSession:(NSSet *)values;

@end
