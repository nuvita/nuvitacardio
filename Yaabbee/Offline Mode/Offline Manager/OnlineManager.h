//
//  OnlineManager.h
//  NuvitaCardio
//
//  Created by John on 2/3/14.
//
//

#import <Foundation/Foundation.h>

@class Session, OnlineManager;

@protocol OnlineManagerDelegate <NSObject>

- (void)onlineManager:(OnlineManager *)manager isSuccess:(BOOL)success;

@end

@interface OnlineManager : NSObject

@property (nonatomic, retain) Session *currentSession;
@property (nonatomic, retain) CardioLap *currentCardioLap;
@property (nonatomic, strong) id <OnlineManagerDelegate> delegate;

- (void)syncAllOfflineSession;
- (void)syncSession:(Session *)session withCardioLap:(CardioLap *)lapData;

@end
