//
//  OfflineManager.m
//  NuvitaCardio
//
//  Created by John on 1/31/14.
//
//

#import "OfflineManager.h"
#import "AppData.h"

#import "Session.h"
#import "CardioLap.h"
#import "CMonitor.h"
#import "Authorize.h"
#import "Logs.h"

#define AUTHORIZE @"Authorize"
#define SESSION @"Session"
#define CMONITOR @"CMonitor"
#define LOGS @"Logs"

@implementation OfflineManager

#pragma mark - Public Method

- (NSFetchRequest *)getRequestDataEntity:(NSString *)entity withLabel:(NSString *)label andValue:(NSString *)value {
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:entity inManagedObjectContext:[self managedObjectContext]];
    
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K = %@", label, value];
    [fetchRequest setPredicate:predicate];
    [fetchRequest setEntity:entityDescription];
    return fetchRequest;
}

- (NSFetchRequest *)getRequestDataEntity:(NSString *)entity {
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:entity inManagedObjectContext:[self managedObjectContext]];
    
    [fetchRequest setEntity:entityDescription];
    return fetchRequest;
}

#pragma mark - Authorize

- (Authorize *)checkForDuplicateAuthorize:(NSDictionary *)data {
    
    NSError *error;
    NSFetchRequest *fetchRequest = [self getRequestDataEntity:AUTHORIZE withLabel:@"username" andValue:[data valueForKey:@"username"]];
    NSArray *fetchArray = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];

    if ([fetchArray count] > 0) {
        return [fetchArray firstObject];
    }
    
    return nil;
}

- (BOOL)updateAuthorize:(Authorize *)authorize didFinishTest:(BOOL)done {
    
    Authorize *autho = authorize;
    autho.initialTest = [NSNumber numberWithBool:done];
    NSError *error;
    if (![self.managedObjectContext save:&error]) {
        return NO;
    }

    return YES;
}

- (BOOL)addAuthorize:(NSDictionary *)data {
    
    Authorize *authorize = ([self checkForDuplicateAuthorize:data]) ? [self checkForDuplicateAuthorize:data] : [NSEntityDescription insertNewObjectForEntityForName:AUTHORIZE inManagedObjectContext:[self managedObjectContext]];
    authorize.username = [data valueForKey:@"username"];
    authorize.password = [data valueForKey:@"password"];
    authorize.loginReponseData = [data valueForKey:@"loginReponseData"];
    authorize.dateModified = [NSDate date];
    
    //...set current user
    [AppData sharedAppData].authorizeUser = authorize;
        
    NSError *error;
    if (![self.managedObjectContext save:&error]) {
        NSLog(@"Whoops, couldn't save authorize data: %@", [error localizedDescription]);
        return NO;
    }

    return YES;
}

- (BOOL)isAuthorize:(NSDictionary *)data {
    
    Authorize *authorize = [self checkForDuplicateAuthorize:data];
    if (authorize && [authorize.password isEqualToString:[data valueForKey:@"password"]]) {
        
        [AppData sharedAppData].authorizeUser = authorize;
        return YES;
    }
    
    return NO;
}

- (NSArray *)getAllUsers {
    
    NSError *error;
    NSFetchRequest *fetchRequest = [self getRequestDataEntity:AUTHORIZE];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"dateModified"
                                                                   ascending:NO];
    [fetchRequest setSortDescriptors:[NSArray arrayWithObjects:sortDescriptor, nil]];
    NSArray *fetchArray = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    return fetchArray;
}

#pragma mark - Session

- (Session *)getSessionWithStartLapTimex:(NSString *)lapTime {
    
    NSError *error;
    NSFetchRequest *fetchRequest = [self getRequestDataEntity:SESSION];
    NSArray *fetchResult = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    __block Session *foundSession = nil;
    [fetchResult enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
     
        Session *session = (Session *)obj;
        NSDictionary *lapData = (NSDictionary *)session.cardioLap;
        if ([[lapData valueForKey:@"startLapTime"] isEqualToString:lapTime]) {
            foundSession = session;
            stop = YES;
        }
        
    }];
    
    return foundSession;
}

- (BOOL)isNew:(NSString *)lapTime {
    
    Session *session = [self getSessionWithStartLapTimex:lapTime];
    return (session) ? NO : YES;
}

- (BOOL)addNewSession:(NSDictionary *)data withStartLapTime:(NSString *)startLapTime {
    
    Session *session = ([self getSessionWithStartLapTimex:startLapTime]) ? [self getSessionWithStartLapTimex:startLapTime] : [NSEntityDescription insertNewObjectForEntityForName:SESSION inManagedObjectContext:[self managedObjectContext]];
    
    session.cardioLap = [data valueForKey:@"cardiolap"];
    session.authorize = [data valueForKey:@"authorize"];
    session.isSync = [NSNumber numberWithBool:NO];
    session.dateModified = [NSDate date];
    session.isMigrated = [NSNumber numberWithBool:YES];
    
    
    NSError *error;
    if (![self.managedObjectContext save:&error]) {
        NSLog(@"Whoops, couldn't save session data: %@", [error localizedDescription]);
        return NO;
    }

    return YES;
}

- (BOOL)addNewSessionFromAppData:(NSDictionary *)data withStartLapTime:(NSString *)startLapTime{
    
    Session *session = ([self getSessionWithStartLapTimex:startLapTime]) ? [self getSessionWithStartLapTimex:startLapTime] : [NSEntityDescription insertNewObjectForEntityForName:SESSION inManagedObjectContext:[self managedObjectContext]];

    session.cardioLap = [data valueForKey:@"cardiolap"];
    session.authorize = [data valueForKey:@"authorize"];
    session.isSync = [NSNumber numberWithBool:YES];
//    session.dateModified = [NSDate date];
    
    NSError *error;
    if (![self.managedObjectContext save:&error]) {
        NSLog(@"Whoops, couldn't save session data: %@", [error localizedDescription]);
        return NO;
    }
    
    return YES;
}

- (BOOL)deleteLastSession {
    
    NSError *error;
    NSFetchRequest *fetchRequest = [self getRequestDataEntity:SESSION];
    NSArray *fetchResult = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];

    if ([fetchResult count] > 0) {
        
        [self.managedObjectContext deleteObject:[fetchResult lastObject]];
        if ([self.managedObjectContext save:&error]) {
            return YES;
        }
    }
    
    return NO;
}

- (void)deleteSessionWithStartLaptime:(NSString *)lapTime {
    Session *session = [self getSessionWithStartLapTimex:lapTime];
    [self.managedObjectContext deleteObject:session];

    NSError *error;
    if ([self.managedObjectContext save:&error]) {
    
    }
}

- (void)deleteSession:(Session *)session {
    [self.managedObjectContext deleteObject:session];
    NSError *error;
    if ([self.managedObjectContext save:&error]) {
        
    }
}

- (BOOL)updateSession {
    
    NSError *error;
    NSFetchRequest *fetchRequest = [self getRequestDataEntity:SESSION];
    NSArray *fetchResult = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];

    if ([fetchResult count] > 0) {
        Session *session = [fetchResult lastObject];
        session.isSync = [NSNumber numberWithBool:YES];
        session.dateModified = [NSDate date];
        
        if (![self.managedObjectContext save:&error]) {
            return NO;
        }

    }
    
    return YES;
}

- (BOOL)updateSession:(Session *)oldSession {

    Session *session = oldSession;
    session.isSync = [NSNumber numberWithBool:YES];
    session.dateModified = [NSDate date];
    
    NSError *error;
    if (![self.managedObjectContext save:&error]) {
        return NO;
    }
    
    return YES;
}

- (NSArray *)getAllSession {
    NSError *error;
    NSFetchRequest *fetchRequest = [self getRequestDataEntity:SESSION];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"dateModified"
                                                                   ascending:NO];
    [fetchRequest setSortDescriptors:[NSArray arrayWithObjects:sortDescriptor, nil]];
    NSArray *fetchResult = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];

    return fetchResult;
}

- (NSArray *)getUserSessions:(Authorize *)authorize {
    
    NSError *error;
    NSFetchRequest *fetchRequest = [self getRequestDataEntity:SESSION];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"authorize == %@", authorize];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"dateModified"
                                                                   ascending:NO];
    
    [fetchRequest setPredicate:predicate];
    [fetchRequest setSortDescriptors:[NSArray arrayWithObjects:sortDescriptor, nil]];
    
    NSArray *fetchResult = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    return fetchResult;
}

- (NSArray *)getAllSessionLabeledUser {
    
    NSMutableArray *allsession = [[NSMutableArray alloc] init];
    NSArray *users = [self getAllUsers];
    for (Authorize *authorize in users) {
        
        NSDictionary *data = [NSDictionary dictionaryWithObjectsAndKeys:
                              authorize, @"user",
                              [self getUserSessions:authorize], @"sessions",
                              nil];
        [allsession addObject:data];
    }
    
    return allsession;
}

- (BOOL)containsSessionHistory {
    NSError *error;
    NSFetchRequest *fetchRequest = [self getRequestDataEntity:SESSION];
    NSArray *fetchResult = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    if ([fetchResult count] == 0) {
        return NO;
    }
    
    return YES;
}

#pragma mark - CMonitor

- (CMonitor *)checkForDuplicateMonitorSerialNo:(NSString *)serialno {

    NSError *error;
    NSFetchRequest *fetchRequest = [self getRequestDataEntity:CMONITOR withLabel:@"serialnumber" andValue:serialno];
    NSArray *fetchArray = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    __block CMonitor *foundMonitor = nil;
    [fetchArray enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        
        CMonitor *monitor = (CMonitor *)obj;
        if ([monitor.serialnumber isEqualToString:serialno]) {
            foundMonitor = monitor;
            stop = YES;
        }
    }];
    
    return foundMonitor;
}

- (BOOL)addNewMonitor:(NSDictionary *)data withSerialNo:(NSString *)serialno {

    CMonitor *monitor = ([self checkForDuplicateMonitorSerialNo:serialno]) ? [self checkForDuplicateMonitorSerialNo:serialno] : [NSEntityDescription insertNewObjectForEntityForName:CMONITOR inManagedObjectContext:[self managedObjectContext]];
    monitor.serialnumber = [data valueForKey:@"serialnumber"];
    monitor.name = [data valueForKey:@"name"];
    monitor.inputName = [data valueForKey:@"inputName"];
    monitor.ignored = [NSNumber numberWithBool:[[data valueForKey:@"ignored"] boolValue]];
    
    NSError *error;
    if (![self.managedObjectContext save:&error]) {
        NSLog(@"Whoops, couldn't save monitor data: %@", [error localizedDescription]);
        return NO;
    }
    
    return YES;
}

- (void)deleteMonitor:(CMonitor *)monitor {
    [self.managedObjectContext deleteObject:monitor];
    NSError *error;
    if ([self.managedObjectContext save:&error]) {
        
    }
}

- (void)cleanIgnoredMonitorsNow {
    
    NSError *error;
    NSFetchRequest *fetchRequest = [self getRequestDataEntity:CMONITOR];
    NSArray *fetchArray = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];

    [fetchArray enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        CMonitor *monitor = (CMonitor *)obj;
        if ([monitor.ignored boolValue]) {
            [self.managedObjectContext deleteObject:monitor];
        }
    }];
}

- (NSArray *)getNotIgnoredDevices {
    NSError *error;
    NSFetchRequest *fetchRequest = [self getRequestDataEntity:CMONITOR withLabel:@"ignored" andValue:@"NO"];
    NSArray *fetchArray = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
   
    return fetchArray;
}

- (BOOL)containsDeviceHistory {
    NSArray *devices = [self getNotIgnoredDevices];
    if ([devices count] == 0) {
        return NO;
    }
    
    return YES;
}

#pragma mark - Logging

- (BOOL)addNewLogs:(NSDictionary *)data {
    
    Logs *log = [NSEntityDescription insertNewObjectForEntityForName:LOGS inManagedObjectContext:[self managedObjectContext]];
    log.logging = [data valueForKey:@"logs"];
    
    NSError *error;
    if (![self.managedObjectContext save:&error]) {
        NSLog(@"Whoops, couldn't save logs data: %@", [error localizedDescription]);
        return NO;
    }
    
    return YES;
}

- (NSArray *)getAllLogs {
    
    NSError *error;
    NSFetchRequest *fetchRequest = [self getRequestDataEntity:LOGS];
    NSArray *fetchArray = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    return fetchArray;
}

- (void)deleteLog:(Logs *)log {
    [self.managedObjectContext deleteObject:log];
    NSError *error;
    if ([self.managedObjectContext save:&error]) {
        
    }
}

@end
