//
//  CMonitor.h
//  NuvitaCardio
//
//  Created by John on 2/27/14.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface CMonitor : NSManagedObject

@property (nonatomic, retain) NSNumber * ignored;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * serialnumber;
@property (nonatomic, retain) NSString * inputName;

@end
