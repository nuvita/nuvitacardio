//
//  Session.m
//  NuvitaCardio
//
//  Created by John on 7/8/14.
//
//

#import "Session.h"
#import "Authorize.h"


@implementation Session

@dynamic cardioLap;
@dynamic dateModified;
@dynamic isNewSession;
@dynamic isSync;
@dynamic isMigrated;
@dynamic authorize;

@end
