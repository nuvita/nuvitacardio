//
//  Authorize.m
//  NuvitaCardio
//
//  Created by John on 7/8/14.
//
//

#import "Authorize.h"
#import "Session.h"


@implementation Authorize

@dynamic dateModified;
@dynamic initialTest;
@dynamic loginReponseData;
@dynamic password;
@dynamic username;
@dynamic session;

@end
