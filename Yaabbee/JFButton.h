//
//  JFButton.h
//  NuvitaCardio
//
//  Created by John on 6/18/14.
//
//

#import <UIKit/UIKit.h>

@interface JFButton : UIButton

@property (retain, nonatomic) NSString *buttonTitle;
@property (assign) BOOL backButton;
@property (assign) BOOL disabled;


@end
