//
//  NuvitaAppDelegate.h
//
//
//  Created by Srabati on 7/22/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//
//aab946d88bec290e429d60283d6657e875123db6 

//c087a7ef97e2d219c39b76a02b96a702f46dac1a
//iphone5
//41f5a2b179c7cbeacef49ebb2246fb885560f9c2

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

#import "constant.h"
#import "OfflineManager.h"
#import "CrashHandler.h"

#import "ForceTabBarViewController.h"
#import "ForceNavigationViewController.h"

@interface NuvitaAppDelegate : UIResponder <UIApplicationDelegate, UITabBarControllerDelegate> {
	
	NSString *memberID;
	BOOL isidlecheck;
}

@property (nonatomic, assign) BOOL isidlecheck;

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic) MKMapType mapType;

@property (nonatomic, retain)  UITabBarController *forceTabBarViewController;
@property (nonatomic, retain)  UINavigationController *forceNavigationController;
@property (assign) BOOL isLoggedIn;

//...fb session
@property (retain, nonatomic) FBSession *fbSession;

//...core data
@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

@property (strong, nonatomic) OfflineManager *offlineManager;

- (void)reloadAppDelegateNavRootViewController;
- (void)reloadAppDelegateNavRootViewControllerLandscape;
- (void)reloadAppDelegateRootViewController;
- (void)reloadAppDelegateRootViewControllerLandscape;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

- (void)resetViewControllers;
- (void)setTintColorForNavigationControllerWithIndex:(NSInteger)index;

+ (void)updateStatus;
+ (BOOL)isNetworkAvailable;
- (void)setupNetworkCheck;

/////////////////////////////////

- (void)Checktouch;

+ (BOOL)isPhone5;
+ (BOOL)isIOS7;

@end
