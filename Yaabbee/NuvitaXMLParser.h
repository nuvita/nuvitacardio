//
//  NuvitaXMLParser.h
//  mynuvita
//
//  Created by John on 4/28/14.
//
//

#import <Foundation/Foundation.h>
#import "ResponseStatus.h"

@interface NuvitaXMLParser : NSObject

@property (nonatomic, retain) ResponseStatus            *responseStatus;
@property (nonatomic, retain) NSDictionary              *info;

- (void)parseXMLFileAtData:(NSMutableData *)data parseError:(NSError **)error parseService:(NSString *)service;


@end
