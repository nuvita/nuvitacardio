//
//  claimActivityOBJ.h
//  Acclaris
//
//  Created by S Biswas on 09/11/10.
//  Copyright 2010 ObjectSol Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AnswerItem.h"
#import "QuestionItem.h"
#import "ResponseStatus.h"

@class QuestionItem;
@class AnswerItem;

@interface EduLessonWeekData : NSObject<NSXMLParserDelegate> {
		
	//NSMutableArray *ItemsArray_;
	
	NSMutableString *contentOfString;
	
	ResponseStatus *responseStatus;
	QuestionItem *questionItem;
	AnswerItem *answerItem;
}

@property(nonatomic,retain)NSString *MemberId, *WeekLabel, *LessonNumber,*Title,*Text;
//@property(nonatomic,retain)NSString *TeamName;

@property(nonatomic,retain)NSMutableArray *ItemsArray;

//
@property(nonatomic,retain)ResponseStatus *responseStatus;

- (void)parseXMLFileAtData:(NSMutableData *)data parseError:(NSError **)error;

- (int)getNumCorrect;

- (float)getScore;


@end
