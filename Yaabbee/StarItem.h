

#import <Foundation/Foundation.h>


@interface StarItem : NSObject  {
	
	NSString *Name;
    NSString *Organization;
	NSString *ProgressPerecent;
	NSString *ProgressValue;
	NSString *Rank;
}


@property(nonatomic,retain)NSString *Name;
@property(nonatomic,retain)NSString *ProgressPerecent;
@property(nonatomic,retain)NSString *ProgressValue;
@property(nonatomic,retain)NSString *Rank;
@property(nonatomic,retain)NSString *Organization;


@end


//<a:Name>Jess Biggs</a:Name>
//<a:ProgressPerecent>133%</a:ProgressPerecent>
//<a:ProgressValue>160 min</a:ProgressValue>
//<a:Rank>1</a:Rank>
//<a:WeekLabel>Week Of 12/26</a:WeekLabel>