//
//  UserresponcePerser.m
//  Acclaris
//
//  Created by S Biswas on 02/10/10.
//  Copyright 2010 ObjectSol Technologies. All rights reserved.
//

#import "LoginPerser.h"
#import "ResponseStatus.h"
#import "LoginResponseData.h"

#define ERROR_STATUS @"a:ErrorStatus"
#define ERROR_MSG @"a:ErrorMessage"

#define FIRST_NAME @"a:FirstName"
#define LAST_NAME @"a:LastName"
#define MEMBER_ID @"a:MemberId"
#define PROG_NAME @"a:ProgramName"
#define USER_AVATAR @"a:Avatar"
#define USER_WEIGHT @"a:Weight"
#define USE_CAMERA_MEALS @"a:UseCameraMeals"

LoginResponseData *loginResponseData;

@implementation LoginPerser

@synthesize responseStatus;

- (void)parseXMLFileAtData:(NSMutableData *)data parseError:(NSError **)error
{
	app = (NuvitaAppDelegate *)[[UIApplication sharedApplication]delegate];
	
	loginResponseData = [[LoginResponseData alloc]init];
	
	NSXMLParser *parser;
	parser = [[NSXMLParser alloc] initWithData:data];
       [parser setDelegate:self];
	[parser setShouldProcessNamespaces:NO];
       [parser setShouldReportNamespacePrefixes:NO];
       [parser setShouldResolveExternalEntities:NO];
       [parser parse];
       
	NSError *parseError = [parser parserError];
	
	if (parseError && error) 
	{
            *error = parseError;
       }
	
	[parser release];
}

- (void)parserDidStartDocument:(NSXMLParser *)parser
{
	app = (NuvitaAppDelegate *)[[UIApplication sharedApplication]delegate];
	
	responseStatus = [[ResponseStatus alloc]init];
	//errordetails=[[NSMutableArray alloc]init];
}		

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
	if (qName)
	{
		elementName = qName;
	}
	else 
		if([elementName isEqualToString:ERROR_STATUS])
		{
			contentOfString=[NSMutableString string];
			[contentOfString retain];
			return;		
			
		}
		else 
			if([elementName isEqualToString:ERROR_MSG])
			{
				contentOfString=[NSMutableString string];
				[contentOfString retain];
				return;		
			}	
			else 
				if([elementName isEqualToString:FIRST_NAME])
				{
					contentOfString=[NSMutableString string];
					[contentOfString retain];
					return;		
					
				}
				else 
					if([elementName isEqualToString:LAST_NAME])
					{
						contentOfString=[NSMutableString string];
						[contentOfString retain];
						return;		
						
					}
					else 
						if([elementName isEqualToString:MEMBER_ID])
						{
							contentOfString=[NSMutableString string];
							[contentOfString retain];
							return;		
							
						}
						else
							if([elementName isEqualToString:PROG_NAME])
							{
								contentOfString=[NSMutableString string];
								[contentOfString retain];
								return;		
							}
                            else
                                if([elementName isEqualToString:USER_AVATAR])
                                {
                                    contentOfString=[NSMutableString string];
                                    [contentOfString retain];
                                    return;		
                                }
                                else
                                    if([elementName isEqualToString:USER_WEIGHT])
                                    {
                                        contentOfString=[NSMutableString string];
                                        [contentOfString retain];
                                        return;		
                                    }
                                    else
                                        if([elementName isEqualToString:USE_CAMERA_MEALS])
                                        {
                                            contentOfString=[NSMutableString string];
                                            [contentOfString retain];
                                            return;		
                                        }
	
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName     
{	
	if (qName)
	{
		elementName = qName;
	}
	else 
		if([elementName isEqualToString:ERROR_STATUS])
		{
			if(contentOfString)
			{
				responseStatus.returnCode = contentOfString;
				[contentOfString release];
				contentOfString = nil;
			}				
		}
		else 
			if([elementName isEqualToString:ERROR_MSG])
			{
				if(contentOfString)
				{
					responseStatus.errorText = contentOfString;
					[contentOfString release];
					contentOfString = nil;
				}		
			}
			else 
				if([elementName isEqualToString:FIRST_NAME])
				{
					if(contentOfString)
					{
						loginResponseData.firstName = contentOfString;
						//[arrUserinfo addObject:contentOfString];
						[contentOfString release];
						contentOfString = nil;
						}		
				}
				else 
					if([elementName isEqualToString:LAST_NAME])
					{
						if(contentOfString)
						{
							loginResponseData.lastName = contentOfString;
							//[arrUserinfo addObject:contentOfString];
							[contentOfString release];
							contentOfString = nil;
							}	
					}
					else 
						if([elementName isEqualToString:MEMBER_ID])
						{
							if(contentOfString)
							{
								loginResponseData.memberID = contentOfString;
								//[arrUserinfo addObject:contentOfString];
								[contentOfString release];
								contentOfString = nil;
							}	
						}
						else 
							if([elementName isEqualToString:PROG_NAME])
							{
								if(contentOfString)
								{
									loginResponseData.programName = contentOfString;
									//[arrUserinfo addObject:contentOfString];
									[contentOfString release];
									contentOfString = nil;
								}	
							}
                            else 
                                if([elementName isEqualToString:USER_AVATAR])
                                {
                                    if(contentOfString)
                                    {
                                        loginResponseData.avator = contentOfString;
                                        //[arrUserinfo addObject:contentOfString];
                                        [contentOfString release];
                                        contentOfString = nil;
                                    }	
                                }
                                else 
                                    if([elementName isEqualToString:USER_WEIGHT])
                                    {
                                        if(contentOfString)
                                        {
                                            loginResponseData.weight = contentOfString;
                                            //[arrUserinfo addObject:contentOfString];
                                            [contentOfString release];
                                            contentOfString = nil;
                                        }	
                                    }
									else 
                                        if([elementName isEqualToString:USE_CAMERA_MEALS])
                                        {
                                            if(contentOfString)
                                            {
                                                loginResponseData.useCameraMeals = contentOfString;
                                                [contentOfString release];
                                                contentOfString = nil;
                                            }	
                                        }
				
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
	if(contentOfString)
		[contentOfString appendString:string];
}

- (void)parserDidEndDocument:(NSXMLParser *)parse
{
	//myerrorcodeOBJ = (errorcodeOBJ *)[errordetails objectAtIndex:0];
	//responseStatus.returnCode = [NSString stringWithFormat:@"0"];
	//responseStatus.errorText = [NSString stringWithFormat:@""];
	//NSLog(@"**********\n\n\n\n\n\n%@\n\nreturnCode %@\nerrorText %@", responseStatus.returnCode, responseStatus.errorText);
}

+ (LoginResponseData *)setLoginResponse:(LoginResponseData *)loginResponse {
    if (loginResponse) {
        loginResponseData = loginResponse;
        return loginResponseData;
    }
    
    return nil;
}

+ (LoginResponseData *)getLoginResponse
{
	if (loginResponseData) {
		//loginResponseData.memberID=@"8b5ed874-f43d-44f4-a8eb-2884f79e2acd";//need to comment
		//loginResponseData.programName = @"Lifestyle 360";
        //loginResponseData.useCameraMeals = @"false";
		return loginResponseData;
	}
	else {
		return nil;
	}
}

+ (BOOL)isNuvitaX
{
	if (loginResponseData) {
		if([loginResponseData.programName isEqualToString:@"Nuvita X"])
			return YES;
	}
	return NO;
}

+ (BOOL)isLifeStyle360
{
	if (loginResponseData) {
		if([loginResponseData.programName isEqualToString:@"Lifestyle 360"])
			return YES;
	}
	
	return NO;
}

+ (NSString*)getDate:(NSString*)dateText
{
//	@try {
//		return [dateText substringWithRange:NSMakeRange(0, 10)];				
//	}
//	@catch (NSException * e) {
//		
//	}
//	@finally {
//		
//	}
	
	return dateText;
}

@end


/*
 
 
 <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tem="http://tempuri.org/">
 <soapenv:Header/>
 <soapenv:Body>
 <tem:Login>
 <!--Optional:-->
 <tem:userId>ly@mine.com</tem:userId>
 <!--Optional:-->
 <tem:password>none</tem:password>
 </tem:Login>
 </soapenv:Body>
 </soapenv:Envelope>
 
 Response:
 
 <s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">
 <s:Body>
 <LoginResponse xmlns="http://tempuri.org/">
 <LoginResult xmlns:a="http://schemas.datacontract.org/2004/07/NuvitaMobileService" xmlns:i="http://www.w3.org/2001/XMLSchema-instance">
 <a:FirstName>Jack</a:FirstName>
 <a:LastName>Sprat</a:LastName>
 <a:MemberId>139c0353-1005-488a-b11f-7c7bfe226952</a:MemberId>
 <a:ProgramName>Live Younger</a:ProgramName>
 </LoginResult>
 </LoginResponse>
 </s:Body>
 </s:Envelope>
 
 
 */
