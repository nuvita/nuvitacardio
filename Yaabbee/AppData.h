//
//  AppData.h
//  NuvitaCardio
//
//  Created by S Biswas on 07/04/13.
//
//

#import <Foundation/Foundation.h>
#import "CardioLap.h"

#define APP_SETTING_BELOW_ZONE_KEY @"bellow_zone_key"
#define APP_SETTING_IN_ZONE_KEY @"in_zone_key"
#define APP_SETTING_ABOVE_ZONE_KEY @"above_zone_key"
#define APP_SETTING_HBT_KEY @"hbt_key"

@class Authorize, InitialTestViewController;
@interface AppData : NSObject

@property (retain, nonatomic) NSMutableArray *arrSessionData;
@property (retain, nonatomic) CardioLap *cardioLap;

//...found authorize user
@property (retain, nonatomic) Authorize *authorizeUser;
@property (retain, nonatomic) InitialTestViewController *activeInitialTestViewController;
@property (retain, nonatomic) id activeCentralManagerClass;

//zonelimit
@property (assign) NSInteger inzoneHighLimit;
@property (assign) NSInteger inzoneLowLimit;

@property (assign) BOOL isInSession;
@property (assign) NSTimeInterval startSessionTime;
@property (assign) NSTimeInterval pauseSessionTime;
@property (assign) NSTimeInterval startLapTime;

@property (retain, nonatomic) NSString *serialNumber;
@property (retain, nonatomic) NSString *cardioBatteryLevel;

@property (retain, nonatomic) NSMutableDictionary *dictOptions;
@property (retain, nonatomic) NSMutableArray *arrRequest;

+ (AppData*)sharedAppData;

- (void)loadData;
- (void)saveData;
- (CardioLap *)getCardioLap:(NSInteger)index;
- (void)delCardioLap:(NSInteger)index;

//- (void)saveSettingsData;
//- (void)loadSettingsData;
- (NSInteger)getSessionTime;
+ (NSString *)uuid;
+ (CGFloat)calculateDistance:(NSMutableArray *)arr;
+ (NSString *)GetUUID:(CFUUIDRef)theUUID;
+ (NSString *)GetDeviceName;

@end

