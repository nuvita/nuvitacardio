//
//  JBLoadingView.m
//  mynuvita
//
//  Created by John on 9/4/14.
//
//

#import "JBLoadingView.h"

@interface JBLoadingView () <UIAlertViewDelegate>

@property (assign) UIView *loadedParentView;

@end

@implementation JBLoadingView


- (UIImage *)captureView:(UIView *)view {

    CALayer *layer = view.layer;
    UIGraphicsBeginImageContext(view.bounds.size);
    CGContextClipToRect(UIGraphicsGetCurrentContext(), view.bounds);
    [layer renderInContext:UIGraphicsGetCurrentContext()];

    UIImage *screenImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return screenImage;
}

- (UIImageView *)createLoadingView:(UIView *)view lightEffect:(BOOL)effect {
    UIImageView *loadingView = [[UIImageView alloc] initWithFrame:view.bounds];
    loadingView.image = [[self captureView:view] applyLightEffectAtFrame:view.bounds];
    loadingView.tag = 1111;

    UIActivityIndicatorView	*progressIndicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(loadingView.frame.size.width/2 - 20, loadingView.frame.size.height/2 - 20, 40, 40)];
    progressIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
    progressIndicator.color = effect ? [UIColor colorWithWhite:0.5 alpha:1.0] : [UIColor colorWithWhite:0.8 alpha:1.0];
    progressIndicator.autoresizingMask = (UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin);
    [progressIndicator sizeToFit];
    [loadingView addSubview:progressIndicator];
    [progressIndicator startAnimating];

    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0, loadingView.frame.size.height/2 + 20, 320, 20)];
	label.font = [UIFont boldSystemFontOfSize:12.0];
	label.textColor = effect ? [UIColor colorWithWhite:0.5 alpha:1.0] : [UIColor colorWithWhite:0.8 alpha:1.0];
	label.textAlignment = NSTextAlignmentCenter;
	label.backgroundColor = [UIColor clearColor];
    label.text = @"Loading...";

    [loadingView addSubview:label];
    return loadingView;
}



- (void)loadingViewStartAnimating:(UIView *)parentView withLoadingView:(UIView *)loadingView text:(NSString *)loadingText {
    [loadingView setAlpha:0.999];
    [parentView addSubview:loadingView];
    [[[loadingView subviews] objectAtIndex:1] setText:loadingText];
}

- (void)loadingViewStopAnimating:(UIView *)parentView {
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    for (UIView *subview in [parentView subviews]) {
        if ([subview tag] == 1111) {
            [subview removeFromSuperview];
        }
    }
}

- (void)loadingViewStopAnimating:(UIView *)parentView withPrompt:(NSString *)prompt {
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];

    _loadedParentView = parentView;
    UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:@"Error"
                                                        message:prompt
                                                       delegate:self
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles:nil];
    [alertview show];
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    [self loadingViewStopAnimating:_loadedParentView];
}

@end
