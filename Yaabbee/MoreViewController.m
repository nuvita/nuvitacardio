//
//  MoreViewController.m
//  NuvitaCardio
//
//  Created by S Biswas on 08/10/13.
//
//

#import "MoreViewController.h"
#import "HelpViewController.h"
#import "OptionsViewController.h"
#import "DevicesViewController.h"
#import "VO2MaxChartViewController.h"
#import "InstructionViewController.h"
#import "LoggingViewController.h"


@interface MoreViewController () <MFMailComposeViewControllerDelegate> {
    
    NSMutableArray *requestArray;
}

@property (retain, nonatomic) IBOutlet UITableView *tableview;

@property (retain, nonatomic) NuvitaXMLParser *nuvitaXMLParser;
@property (strong, nonatomic) OfflineManager *offlineManager;
@property (strong, nonatomic) JFNavigationTransitionDelegate *navigationTransitionDelegate;

@property (retain, nonatomic) UIActivityIndicatorView *progressIndicator;

@end

@implementation MoreViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.tabBarItem.title = @"More";
        UIImage *normalImg = [UIImage imageNamed:@"more.png"];
        UIImage *selectedImg = [UIImage imageNamed:@"more_select.png"];
        
        [self.tabBarItem setFinishedSelectedImage:selectedImg withFinishedUnselectedImage:normalImg];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    requestArray = [[NSMutableArray alloc] init];
    [self initOfflineManager];
    [self initProgressIndicator];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    [self.tabBarController.tabBar setHidden:NO];
    [self.tableview reloadData];
    
    _navigationTransitionDelegate = [[JFNavigationTransitionDelegate alloc] init];
    self.navigationController.delegate = _navigationTransitionDelegate;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//Orientation
- (BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation{
    return (toInterfaceOrientation != UIInterfaceOrientationPortraitUpsideDown || toInterfaceOrientation != UIInterfaceOrientationPortrait);
}

- (BOOL)shouldAutorotate {
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationPortrait;
}

#pragma mark - Public Methods

- (void)initProgressIndicator {
    _progressIndicator = [[UIActivityIndicatorView alloc] init];
    _progressIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
    _progressIndicator.autoresizingMask = (UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin);
    [_progressIndicator sizeToFit];
}

- (void)showVo2Max:(NSDictionary *)info {
    
    VO2MaxChartViewController *vo2MaxChartViewController = [[VO2MaxChartViewController alloc] init];
    vo2MaxChartViewController.resultInfo = info;
    
    [self.navigationController pushViewController:vo2MaxChartViewController animated:YES];
    [(NuvitaAppDelegate *)[[UIApplication sharedApplication] delegate] performSelector:@selector(reloadAppDelegateRootViewControllerLandscape)
                                                                            withObject:nil
                                                                            afterDelay:0.5];
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return 5;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
//    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
//    }
    
    cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue-Regular" size:16];
    
    switch (indexPath.row) {
        case 0: {
            cell.imageView.image = [UIImage imageNamed:@"help-icon.png"];
            cell.textLabel.text = @"Nuvita Cardio Monitor Help";
            
        } break;

        case 1: {
            cell.imageView.image = [UIImage imageNamed:@"setting-icon.png"];
            cell.textLabel.text = @"Options";
        } break;
            
        case 2: {
            cell.imageView.image = [UIImage imageNamed:@"save-icon.png"];
            cell.textLabel.text = @"Paired Devices";
        } break;
            
        case 3: {
            cell.imageView.image = [UIImage imageNamed:@"logs-icon.png"];
            cell.textLabel.text = @"Request Logs";
        } break;
            
        case 4: {
            cell.userInteractionEnabled = [[AppData sharedAppData] isInSession] == NO;
            cell.imageView.image = [UIImage imageNamed:@"groupid-6.png"];
            cell.textLabel.text = @"Measure Cardio Fitness ";
            cell.detailTextLabel.text = @"(Vo\u00B2max)";                           
        } break;
            
        default:
            break;
    }

    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:[tableView indexPathForSelectedRow] animated:YES];
    
    
    
    switch (indexPath.row) {
        case 0: {
            
            HelpViewController *helpViewController = [[HelpViewController alloc] init];
            [self.navigationController pushViewController:helpViewController animated:YES];
        } break;
  
        case 1: {
            
            OptionsViewController *vc = [[OptionsViewController alloc] initWithNibName:@"OptionsViewController" bundle:nil];
            [self.navigationController pushViewController:vc animated:YES];
        } break;
            
        case 2: {
            
            DevicesViewController *vc = [[DevicesViewController alloc] initWithNibName:@"DevicesViewController" bundle:nil];
            [self.navigationController pushViewController:vc animated:YES];
        } break;
            
        case 3: {
            
            UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
            cell.userInteractionEnabled = NO;
            cell.accessoryView = _progressIndicator;
            [_progressIndicator startAnimating];
            [self initLogFiles];
            
        } break;
            
        case 4: {
        
//#warning - ayaw kalimot ug balik
            UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
            cell.userInteractionEnabled = NO;
            cell.accessoryView = _progressIndicator;
            [_progressIndicator startAnimating];
            [self getHealthMeasurementCurrentTrends];
            
        } break;
            
        default:
            break;
    }
    
}

- (void)showInstructionView {
    
    InstructionViewController *instructionViewController = [[InstructionViewController alloc] initWithNibName:@"InstructionViewController" bundle:nil];
    [self.navigationController pushViewController:instructionViewController animated:YES];
    [(NuvitaAppDelegate *)[[UIApplication sharedApplication] delegate] performSelector:@selector(reloadAppDelegateRootViewControllerLandscape)
                                                                            withObject:nil
                                                                            afterDelay:1.0];
}

#pragma mark - Logging

- (void)showVo2Sequence {
//    [self.tableview reloadData];
//    if ([_nuvitaXMLParser.responseStatus isSuccess])
        [self showVo2Max:[_nuvitaXMLParser info]];
//    else [self showInstructionView];
}

- (void)initLogFiles {
    
    [self cleanUpExpiredLogs];
    [requestArray removeAllObjects];
    [requestArray addObjectsFromArray:[self.offlineManager getAllLogs]];
    
    [self.tableview setUserInteractionEnabled:NO];
    [self didTapSend];
}

- (void)didTapSend {
    
    NSString *fileString = [[StringFile sharedAppData] convertLogsToString:requestArray];
    if ([[StringFile sharedAppData] saveFile:fileString]) {
        [self openEmailForm];
    }
}

- (void)openEmailForm {
    
    [self.tableview setUserInteractionEnabled:YES];
    MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
    picker.mailComposeDelegate = self;
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"Logs.txt"];
    NSData *myData = [NSData dataWithContentsOfFile:filePath];
    
    // Present the mail composition interface.
    Class mailClass = (NSClassFromString(@"MFMailComposeViewController"));
    if ([mailClass canSendMail]) {
        //..
        [picker setToRecipients:@[@"mobileapps@nuvita.com"]];
        
        [picker setSubject:@"App Logs"];
        [picker setMessageBody:@"Attached is the txt file of all webservice request" isHTML:NO];
        [picker addAttachmentData:myData mimeType:@"application/txt" fileName:@"Request Logs.txt"];
        
        [self presentViewController:picker animated:YES completion:^{
            NSLog(@".....loaded mail view");
        }];
    }
    
}

- (void)initOfflineManager {
    
    NuvitaAppDelegate *appDelegate = (NuvitaAppDelegate *)[[UIApplication sharedApplication] delegate];
    self.offlineManager = [[OfflineManager alloc] init];
    self.offlineManager.managedObjectContext = appDelegate.managedObjectContext;
}

- (NSInteger)daysBetweenDate:(NSDate*)fromDateTime andDate:(NSDate*)toDateTime {
    NSDate *fromDate;
    NSDate *toDate;
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    [calendar rangeOfUnit:NSDayCalendarUnit startDate:&fromDate
                 interval:NULL forDate:fromDateTime];
    [calendar rangeOfUnit:NSDayCalendarUnit startDate:&toDate
                 interval:NULL forDate:toDateTime];
    NSDateComponents *difference = [calendar components:NSDayCalendarUnit
                                               fromDate:fromDate toDate:toDate options:0];
    
    return [difference day];
}

- (void)cleanUpExpiredLogs {
    
    NSArray *logArray = [self.offlineManager getAllLogs];
    [logArray enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        
        Logs *log = (Logs *)obj;
        Logging *logging = [NSKeyedUnarchiver unarchiveObjectWithData:log.logging];
        NSDictionary *data = [logging requestData];
        
        if ([self daysBetweenDate:[data valueForKey:@"dateTime"] andDate:[NSDate date]] >= 7) {
            [self deleteLog:log];
        }
    }];
}

- (void)deleteLog:(Logs *)log {
    [self.offlineManager deleteLog:log];
}

#pragma mark - MFMailComposeViewControllerDelegate

- (void)mailComposeController:(MFMailComposeViewController *)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError *)error {
    
    
    if (result == MFMailComposeResultSent)
        NSLog(@"Message has been sent");
    
    
    [self dismissViewControllerAnimated:YES completion:^{
        //...done sending email
    }];
}

#pragma mark - Web Service Request

- (void)getHealthMeasurementCurrentTrends {

    _nuvitaXMLParser = nil;
    LoginResponseData *loginResponseData = [LoginPerser getLoginResponse];
    NSDictionary *data = [NSDictionary dictionaryWithObjectsAndKeys:
                          [loginResponseData memberID], @"memberId",
                          nil];
    
    request *r = [[request alloc] initWithTarget:self
                                   SuccessAction:@selector(onSuccessfulFetchRequest:resData:)
                                   FailureAction:@selector(onFailedFetch)];
    
    [r getHealthMeasurementCurrentTrends:data];
}

#pragma mark - Web Service Delegate

- (void)onSuccessfulFetchRequest:(ResponseStatus *)responseStatus resData:(NSObject *)obj {

    _nuvitaXMLParser = (NuvitaXMLParser *)obj;
    [self performSelector:@selector(showVo2Sequence) withObject:nil afterDelay:0.5];
}

- (void)onFailedFetch {

}

@end
