//
//  DeviceCell.h
//  NuvitaCardio
//
//  Created by John on 3/4/14.
//
//

#import <UIKit/UIKit.h>

@interface DeviceCell : UITableViewCell

@property (nonatomic, strong) UILabel *namelbl;
@property (nonatomic, strong) UILabel *seriallbl;
@property (nonatomic, strong) UILabel *batterylbl;

@end
