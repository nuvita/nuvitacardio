

#import <Foundation/Foundation.h>

#import "ResponseStatus.h"


@interface MemberProgressData : NSObject <NSXMLParserDelegate>{

	NSString *CardioLabel;
	NSString *CardioPercent;
	NSString *MobilityLabel;
	NSString *MobilityPercent;
	
	NSString *NutritionLabel;
	NSString *NutritionPercent;
	
	NSString *WeekLabel;
	
	NSMutableString *contentOfString;
	
	ResponseStatus *responseStatus;

}
@property(nonatomic,retain)NSString *CardioLabel;
@property(nonatomic,retain)NSString *CardioPercent;

@property(nonatomic,retain)NSString *MobilityLabel;
@property(nonatomic,retain)NSString *MobilityPercent;

@property(nonatomic,retain)NSString *NutritionLabel;
@property(nonatomic,retain)NSString *NutritionPercent;
@property(nonatomic,retain)NSString *WeekLabel;


//
@property(nonatomic,retain)ResponseStatus *responseStatus;
- (void)parseXMLFileAtData:(NSMutableData *)data parseError:(NSError **)error;

@end

/*
<a:CardioLabel>cardio</a:CardioLabel>
<a:CardioPercent>100</a:CardioPercent>
<a:ErrorMessage i:nil="true"/>
<a:ErrorStatus>false</a:ErrorStatus>
<a:MobilityLabel>mobility</a:MobilityLabel>
<a:MobilityPercent>0</a:MobilityPercent>
<a:NutritionLabel>nutrition</a:NutritionLabel>
<a:NutritionPercent>0</a:NutritionPercent>
<a:WeekLabel>12/12</a:WeekLabel>
*/