//
//  CrashHandler.h
//  CrashHandler

#import <Foundation/Foundation.h>

@interface CrashHandler : NSObject 
	
+ (void) setupLogging:(BOOL)shouldEnableCrashHandler;
@end

void CrashHandlerExceptionHandler(NSException *exception);