//
//  MenuViewController.h
//  NuvitaCardio
//
//  Created by John on 3/31/14.
//
//

#import <UIKit/UIKit.h>
#import "constant.h"
#import "NuvitaAppDelegate.h"
#import "ECSlidingViewController.h"

#import "LoginPerser.h"
#import "LoginResponseData.h"

@class MenuViewController;
@protocol MenuViewControllerDelegate <NSObject>

- (void)menuViewController:(MenuViewController *)viewController didSelectMenu:(NSInteger)index;

@end

@interface MenuViewController : UIViewController

@property (strong, nonatomic) id <MenuViewControllerDelegate> delegate;

@end
