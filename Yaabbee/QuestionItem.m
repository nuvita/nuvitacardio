//
//  claimActivityOBJ.m
//  Acclaris
//
//  Created by S Biswas on 09/11/10.
//  Copyright 2010 ObjectSol Technologies. All rights reserved.
//

#import "QuestionItem.h"

@implementation QuestionItem

@synthesize Text;
@synthesize CorrectAnswer;
@synthesize Answers;

@synthesize isAnswered;
@synthesize answeredIndex;

- (BOOL)isAnsweredCorrect
{
	AnswerItem* item = (AnswerItem*)[Answers objectAtIndex:answeredIndex] ;
	
	return [item.isCorrect boolValue];
}
@end

