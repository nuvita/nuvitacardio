//
//  TransitionAnimator.h
//  NuvitaCardio
//
//  Created by John on 6/14/14.
//
//

#import <Foundation/Foundation.h>

@interface JFTransitionAnimator : NSObject <UIViewControllerAnimatedTransitioning>

@property (nonatomic, assign) BOOL reverse;

- (instancetype)initWithReverse:(BOOL)reverse;
+ (instancetype)transitioningWithReverse:(BOOL)reverse;

@end
