//
//  UserresponcePerser.h
//  Acclaris
//
//  Created by S Biswas on 02/10/10.
//  Copyright 2010 ObjectSol Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NuvitaAppDelegate.h"
#import "LoginResponseData.h"


@class ResponseStatus;

@interface LoginPerser : NSObject<NSXMLParserDelegate> {

	NuvitaAppDelegate *app;
	NSMutableString *contentOfString;
	ResponseStatus *responseStatus;
	//NSMutableArray *errordetails;
	//NSMutableArray *buildinfo;
}

@property(nonatomic,retain)ResponseStatus *responseStatus;

+ (LoginResponseData *)setLoginResponse:(LoginResponseData *)loginResponse;
+ (LoginResponseData *)getLoginResponse;
//+ (NSString *)getMemberID;
- (void)parseXMLFileAtData:(NSMutableData *)data parseError:(NSError **)error;
+ (BOOL)isLifeStyle360;
+ (BOOL)isNuvitaX;
+ (NSString*)getDate:(NSString*)dateText;

@end
