//
//  AppUtility.h
//  RestPos
//
//  Created by S Biswas on 04/02/13.
//  Copyright (c) 2013 Eezy Apps Pty Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AppUtility : NSObject


+ (BOOL)isInputTextNullOrEmpty:(UITextField*)inputText;
+ (BOOL) isValidEmail:(NSString*)text;
+ (BOOL) isValidPhoneNumber:(NSString*)text;

//+ (NSString*) numberFormatedString:(float)val;
+ (NSString*) statHeaderDateFormat;
+ (NSString*) statHeaderDateFormat2;

+ (NSString*) orderHeaderFormatedString;

+ (NSString*) getStartUTCDate;

//+ (NSString*) priceFormatedString:(NSString *)val;

@end


