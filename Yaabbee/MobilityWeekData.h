//
//  claimActivityOBJ.h
//  Acclaris
//
//  Created by S Biswas on 09/11/10.
//  Copyright 2010 ObjectSol Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "ResponseStatus.h"

@interface MobilityWeekData : NSObject<NSXMLParserDelegate> {
		
	//NSMutableArray *StarItemsArray;
	
	NSMutableString *contentOfString;
	
	ResponseStatus *responseStatus;
}


@property(nonatomic,retain)NSString *Date, *FridaySelect, *FridayText, *MondaySelect, *MondayText, *SaturdaySelect, *SaturdayText, *SundaySelect, *SundayText, *ThursdaySelect, *ThursdayText,*TuesdaySelect, *TuesdayText, *WednesdaySelect, *WednesdayText, *isMobilityX;

@property(nonatomic,retain)NSString *WeekLabel, *MemberId;


//
@property(nonatomic,retain)ResponseStatus *responseStatus;

- (void)parseXMLFileAtData:(NSMutableData *)data parseError:(NSError **)error;

- (BOOL)getBoolVal:(NSString*)val;

- (NSString*)getMobilityXml:(NSString*)date;

//- (NSString*)getTotalBelow;
//- (NSString*)getTotalInZone;
//- (NSString*)getTotalAbove;
//- (NSString*)getTotalVal;

@end

//<a:isMobilityX>true</a:isMobilityX>

//<a:AboveRangeHR>146-146 bpm</a:AboveRangeHR>
//<a:AboveRangePct>81-81%</a:AboveRangePct>
//<a:BelowRangeHR>0-0 bpm</a:BelowRangeHR>
//<a:BelowRangePct>0-0%</a:BelowRangePct>

//<a:ErrorMessage i:nil="true"/>
//<a:ErrorStatus>false</a:ErrorStatus>

//<a:Goal>120</a:Goal>
//<a:GoalName>Heart Rate</a:GoalName>
//<a:InZoneRangeHR>126-126 bpm</a:InZoneRangeHR>
//<a:InZoneRangePct>70-70%</a:InZoneRangePct>

//<a:MemberId>8b5ed874-f43d-44f4-a8eb-2884f79e2acd</a:MemberId>
//<a:ProgressPerecent>0</a:ProgressPerecent>
//<a:ProgressValue>0</a:ProgressValue>