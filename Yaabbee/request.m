//
//  request.m
//  Trail
//
//  Created by mmandal on 02/10/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//
#import "NuvitaAppDelegate.h"
#import "request.h"
//#import "SSCrypto.h"
#import "DateFormate.h"
#import "LoginPerser.h"
#import "NuvitaXMLParser.h"

#import "Logs.h"

#define METHOD_URL @"http://tempuri.org/IMobileService/"
#define METHOD_URL2 @"http://tempuri.org/IUploadService/"

#define url @ "http://ws.mynuvita.com/MobileService.svc"
#define url2 @ "http://upload.mynuvita.com/UploadService.svc/mex"
#define url3 @ "http://beta.ws.mynuvita.com/MobileService.svc"

#define urlbeta @ "http://beta.ws.mynuvita.com/mobileservice.svc"

#define LOGIN_METHOD  @"Login"
#define CARDIO_WEEK_METHOD  @"GetCardioWeek"
#define EDU_LESSON_METHOD  @"GetEducationLesson"
#define EDU_WEEK_METHOD  @"GetEducationWeek"
#define MEMBER_PROGRESS_METHOD  @"GetMemberProgress"
#define MOBILITY_METHOD  @"GetMobilityWeek"
#define MOBILITY_X_METHOD  @"GetMobilityXHRSessions"
#define SAVE_MOBILITY_X_METHOD  @"SaveMobilityXWorkout"
#define NUTRITION_METHOD  @"GetNutritionDay"
#define STAR_METHOD  @"GetStars"
#define GSTAR_METHOD  @"GetGlobalCardioStars"

#define GET_PROPOSED_PROGRAM @"GetProposedCardioProgram"
#define SAVE_PROPOSED_PROGRAM @"SaveProposedCardioProgram"
#define DELETE_VO2MAX_RR @"DeleteVo2MaxFromRR"
#define GET_MEASUREMENT_TRENDS @"GetHealthMeasurementCurrentTrends"
#define ACTIVITY_LEVEL @"GetVo2MaxFromRRActivityQuestion"
#define GET_VO2MAX_RR @"GetVo2MaxFromRR"

#define TEAM_PROGRESS_METHOD  @"GetTeamProgress"

#define SAVE_MEMBER_QUIZ_METHOD  @"SaveMemberQuiz"
#define SAVE_NUTRITION_DAY_METHOD  @"SaveNutritionDay"
#define SAVE_NUTRITION_MEAL_METHOD  @"SaveNutritionMeal"

#define SAVE_MOBILITY_WEEK_METHOD  @"SaveMobilityWeek"
#define WELLNESSWALL_METHOD @"GetWellnessWall"
#define SAVE_WELLNESSWALL_METHOD @"SaveWellnessWall"
#define SAVE_WELLNESSWALLPHOTO_METHOD @"UploadWellnessPhoto"
#define SAVE_MEALS_CAMERA_METHOD @"UploadMealPhoto"

#define MOBILITY_WORKOUTS_METHOD  @"GetMobilityWOList"
#define MOBILITY_EXERCISES_METHOD @"GetMemberMobilityExercises"//GetMobilityExercises"

#define SAVE_CARDIO_SESSION_METHOD  @"SaveCardioWorkout"//HrString" //SaveCardioWorkout


#import "Logging.h"


@implementation request

- (id)initWithTarget:(id)actionTarget
	  SuccessAction:(SEL)successAction 
	  FailureAction:(SEL)failureAction
{
	self=[super init];
	if(self==nil)
	{
		return nil;
	}
	
	target=actionTarget;
	successHandler=successAction;
	failureHandler=failureAction;
	
	return self;
}


- (NSString *) stringToHex:(NSString *)str
{   
    NSUInteger len = [str length];
    unichar *chars = malloc(len * sizeof(unichar));
    [str getCharacters:chars];
    
    NSMutableString *hexString = [[NSMutableString alloc] init];
    
    for(NSUInteger i = 0; i < len; i++ )
    {
        [hexString appendString:[NSString stringWithFormat:@"%x", chars[i]]];
    }
    free(chars);
    
    return [hexString autorelease];
}

- (NSString *) stringFromHex:(NSString *)str 
{   
    NSMutableData *stringData = [[[NSMutableData alloc] init] autorelease];
    unsigned char whole_byte;
    char byte_chars[3] = {'\0','\0','\0'};
    int i;
    for (i=0; i < [str length] / 2; i++) {
        byte_chars[0] = [str characterAtIndex:i*2];
        byte_chars[1] = [str characterAtIndex:i*2+1];
        whole_byte = strtol(byte_chars, NULL, 16);
        [stringData appendBytes:&whole_byte length:1]; 
    }
    
    return [[[NSString alloc] initWithData:stringData encoding:NSASCIIStringEncoding] autorelease];
}

- (NSString *)getUTCFormateDate:(NSDate *)localDate
{
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
	[dateFormatter setTimeZone:timeZone];
	[dateFormatter setDateFormat:@"YYYY-MM-dd'T'HH:mm:ss.S"];
	NSString *dateString = [dateFormatter stringFromDate:localDate];
	
	return dateString;
}

#pragma mark -
#pragma mark loginRequest

- (BOOL)isNetAvalaible
{
	if([NuvitaAppDelegate isNetworkAvailable])
	{
		return true;
	}
	else
	{	[target performSelector:failureHandler];
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Info" message:@"Network or Wireless not available, please try again later." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil];
		[alert show];
		
	}
	
	return false;
}

#pragma mark - Vo2Max

- (void)getProposedCardioProgram:(NSDictionary *)data {
    
    whichAPI = GET_PROPOSED_PROGRAM;
	app = (NuvitaAppDelegate *)[[UIApplication sharedApplication]delegate];
    
	NSMutableString *sRequest = [[NSMutableString alloc] init];
    [sRequest appendString:@"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\" xmlns:nuv=\"http://schemas.datacontract.org/2004/07/NuvitaMobileService\">"];
    
    [sRequest appendString:@"<soapenv:Header/>"];
    [sRequest appendString:@"<soapenv:Body>"];
    [sRequest appendString:@"<tem:GetProposedCardioProgram>"];
    
    [sRequest appendFormat:@"<tem:memberId>%@</tem:memberId>", [data valueForKey:@"memberId"]];
    [sRequest appendFormat:@"<tem:Date>%@</tem:Date>", [data valueForKey:@"Date"]];
    
    [sRequest appendString:@"</tem:GetProposedCardioProgram>"];
    [sRequest appendString:@"</soapenv:Body>"];
    [sRequest appendString:@"</soapenv:Envelope>"];

    [self callPostMethod:sRequest Action:[METHOD_URL stringByAppendingString:whichAPI] API:url];
}

- (void)saveProposedCardioProgram:(NSDictionary *)data {
    
    whichAPI = SAVE_PROPOSED_PROGRAM;
	app = (NuvitaAppDelegate *)[[UIApplication sharedApplication]delegate];
    
	NSMutableString *sRequest = [[NSMutableString alloc] init];
    [sRequest appendString:@"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\" xmlns:nuv=\"http://schemas.datacontract.org/2004/07/NuvitaMobileService\">"];
    
    [sRequest appendString:@"<soapenv:Header/>"];
    [sRequest appendString:@"<soapenv:Body>"];
    [sRequest appendString:@"<tem:SaveProposedCardioProgram>"];
    
    [sRequest appendFormat:@"<tem:memberId>%@</tem:memberId>", [data valueForKey:@"memberId"]];
    [sRequest appendFormat:@"<tem:Date>%@</tem:Date>", [data valueForKey:@"Date"]];

    [sRequest appendString:@"</tem:SaveProposedCardioProgram>"];
    [sRequest appendString:@"</soapenv:Body>"];
    [sRequest appendString:@"</soapenv:Envelope>"];

    [self callPostMethod:sRequest Action:[METHOD_URL stringByAppendingString:whichAPI] API:url];
}

- (void)deleteVo2MaxXML:(NSDictionary *)data {
    
    whichAPI = DELETE_VO2MAX_RR;
	app = (NuvitaAppDelegate *)[[UIApplication sharedApplication]delegate];
    
	NSMutableString *sRequest = [[NSMutableString alloc] init];
    [sRequest appendString:@"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\" xmlns:nuv=\"http://schemas.datacontract.org/2004/07/NuvitaMobileService\">"];
    
    [sRequest appendString:@"<soapenv:Header/>"];
    [sRequest appendString:@"<soapenv:Body>"];
    [sRequest appendString:@"<tem:DeleteVo2MaxFromRR>"];
    
    [sRequest appendFormat:@"<tem:memberId>%@</tem:memberId>", [data valueForKey:@"memberId"]];
    [sRequest appendFormat:@"<tem:Date>%@</tem:Date>", [data valueForKey:@"Date"]];

    [sRequest appendString:@"</tem:DeleteVo2MaxFromRR>"];
    [sRequest appendString:@"</soapenv:Body>"];
    [sRequest appendString:@"</soapenv:Envelope>"];
    [self callPostMethod:sRequest Action:[METHOD_URL stringByAppendingString:whichAPI] API:url];
}

- (void)getHealthMeasurementCurrentTrends:(NSDictionary *)data {
    
    whichAPI = GET_MEASUREMENT_TRENDS;
	app = (NuvitaAppDelegate *)[[UIApplication sharedApplication]delegate];
    
	NSMutableString *sRequest = [[NSMutableString alloc] init];
    [sRequest appendString:@"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\" xmlns:nuv=\"http://schemas.datacontract.org/2004/07/NuvitaMobileService\">"];
    
    [sRequest appendString:@"<soapenv:Header/>"];
    [sRequest appendString:@"<soapenv:Body>"];
    [sRequest appendString:@"<tem:GetHealthMeasurementCurrentTrends>"];
    
    int gid = 6;
    [sRequest appendFormat:@"<tem:memberId>%@</tem:memberId>", [data valueForKey:@"memberId"]];
    [sRequest appendFormat:@"<tem:groupId>%d</tem:groupId>", gid];
    
    [sRequest appendString:@"</tem:GetHealthMeasurementCurrentTrends>"];
    [sRequest appendString:@"</soapenv:Body>"];
    [sRequest appendString:@"</soapenv:Envelope>"];
    [self callPostMethod:sRequest Action:[METHOD_URL stringByAppendingString:whichAPI] API:url];
}

- (void)getVo2MaxFromRRService:(NSString *)requestXML {
    
    whichAPI = GET_VO2MAX_RR;
	app = (NuvitaAppDelegate *)[[UIApplication sharedApplication]delegate];
	
	NSMutableString *sRequest = [[NSMutableString alloc] init];
	[sRequest appendString:requestXML];

    NSLog(@"request: %@", sRequest);
    [self callPostMethod:sRequest Action:[METHOD_URL stringByAppendingString:whichAPI] API:url];
}

#pragma mark - GetActivityLevel

- (void)getActivityLevel {
    
    if(![self isNetAvalaible]) return;
	
	whichAPI = ACTIVITY_LEVEL;
	app = (NuvitaAppDelegate *)[[UIApplication sharedApplication]delegate];
    
    NSMutableString *sRequest=[[NSMutableString alloc] init];
	[sRequest appendString:@"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\"><soapenv:Header/><soapenv:Body><tem:GetVo2MaxFromRRActivityQuestion>"];
	[sRequest appendString:@"</tem:GetVo2MaxFromRRActivityQuestion></soapenv:Body></soapenv:Envelope>"];
    [self callPostMethod:sRequest Action:[METHOD_URL stringByAppendingString:whichAPI] API:url];
}

#pragma mark -
#pragma mark loginRequest


- (void)loginRequest:(NSString *)user pass:(NSString *)password {

	if(![self isNetAvalaible]) return;
	
	whichAPI = LOGIN_METHOD;
	app=(NuvitaAppDelegate *)[[UIApplication sharedApplication]delegate];
	
	NSMutableString *sRequest=[[NSMutableString alloc] init];
	//sRequest=[self createHeader];
	[sRequest appendString:@"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\"><soapenv:Header/><soapenv:Body><tem:Login><tem:userId>"];
 	[sRequest appendString:user];
	[sRequest appendString:@"</tem:userId><tem:password>"];
	[sRequest appendString:password];
	[sRequest appendString:@"</tem:password></tem:Login></soapenv:Body></soapenv:Envelope>"];
	[self callPostMethod:sRequest Action:[METHOD_URL stringByAppendingString:whichAPI] API:url];
}

#pragma mark -
#pragma mark SAVE_CARDIO_SESSION_METHOD OFFLINE

- (void)saveCardioSessionOffline:(NSString *)requestXml text:(NSString *)text {
    
    isOfflineData = YES;
	whichAPI = SAVE_CARDIO_SESSION_METHOD;//@"senduserPass";
	app=(NuvitaAppDelegate *)[[UIApplication sharedApplication]delegate];
	
	NSMutableString *sRequest=[[NSMutableString alloc] init];
	[sRequest appendString:requestXml];
    
    [self callPostMethod:sRequest Action:[METHOD_URL stringByAppendingString:whichAPI] API:url];
}

#pragma mark -
#pragma mark SAVE_CARDIO_SESSION_METHOD

- (void)saveCardioSession:(NSString *)requestXml text:(NSString *)text {
  
	if(![self isNetAvalaible]) return;
	
	whichAPI = SAVE_CARDIO_SESSION_METHOD;
	app=(NuvitaAppDelegate *)[[UIApplication sharedApplication]delegate];
	
	NSMutableString *sRequest=[[NSMutableString alloc] init];
	[sRequest appendString:requestXml];
    
    [self callPostMethod:sRequest Action:[METHOD_URL stringByAppendingString:whichAPI] API:url];
}


#pragma mark -
#pragma mark getTeamProgress

- (void)getTeamProgress:(NSString *)memberID date:(NSString *)weekDate
{
	if(![self isNetAvalaible]) return;
	
	whichAPI = TEAM_PROGRESS_METHOD;//@"senduserPass";
	app=(NuvitaAppDelegate *)[[UIApplication sharedApplication]delegate];
	
	NSMutableString *sRequest=[[NSMutableString alloc] init];
	//sRequest=[self createHeader];
	[sRequest appendString:@"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\"><soapenv:Header/><soapenv:Body>"];
	[sRequest appendString:@"<tem:GetTeamProgress><tem:memberID>"];
 	[sRequest appendString:memberID];//append Username
	[sRequest appendString:@"</tem:memberID><tem:weekDate>"];
	[sRequest appendString:weekDate];//append Password
	[sRequest appendString:@"</tem:weekDate></tem:GetTeamProgress>"];
	[sRequest appendString:@"</soapenv:Body></soapenv:Envelope>"];
	NSLog(@"request string: %@",sRequest);
	[self callPostMethod:sRequest Action:[METHOD_URL stringByAppendingString:whichAPI] API:url];
}


#pragma mark -
#pragma mark getMemberProgress

- (void)getMemberProgress:(NSString *)memberID date:(NSString *)weekDate
{
	if(![self isNetAvalaible]) return;
	
	whichAPI = MEMBER_PROGRESS_METHOD;//@"senduserPass";
	app=(NuvitaAppDelegate *)[[UIApplication sharedApplication]delegate];
		
		 
	NSMutableString *sRequest=[[NSMutableString alloc] init];
	//sRequest=[self createHeader];
	[sRequest appendString:@"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\"><soapenv:Header/><soapenv:Body>"];
	[sRequest appendString:@"<tem:GetMemberProgress><tem:memberID>"];
 	[sRequest appendString:memberID];//append Username
	[sRequest appendString:@"</tem:memberID><tem:weekDate>"];
	[sRequest appendString:weekDate];//append Password
	[sRequest appendString:@"</tem:weekDate></tem:GetMemberProgress>"];
	[sRequest appendString:@"</soapenv:Body></soapenv:Envelope>"];
	NSLog(@"request string: %@",sRequest);
	[self callPostMethod:sRequest Action:[METHOD_URL stringByAppendingString:whichAPI] API:url];
}


#pragma mark -
#pragma mark getNutritionDay

- (void)getNutritionDay:(NSString *)memberID date:(NSString *)date {
	if(![self isNetAvalaible]) return;
	
	whichAPI = NUTRITION_METHOD;//@"senduserPass";
	app=(NuvitaAppDelegate *)[[UIApplication sharedApplication]delegate];
	

	
	NSMutableString *sRequest=[[NSMutableString alloc] init];
	//sRequest=[self createHeader];
	[sRequest appendString:@"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\"><soapenv:Header/><soapenv:Body>"];
	[sRequest appendString:@"<tem:GetNutritionDay><tem:memberID>"];
 	[sRequest appendString:memberID];//append Username
	[sRequest appendString:@"</tem:memberID><tem:date>"];
	[sRequest appendString:date];//append Password
	[sRequest appendString:@"</tem:date></tem:GetNutritionDay>"];
	[sRequest appendString:@"</soapenv:Body></soapenv:Envelope>"];
	NSLog(@"request string: %@",sRequest);
	[self callPostMethod:sRequest Action:[METHOD_URL stringByAppendingString:whichAPI] API:url];
}

#pragma mark -
#pragma mark getCardioWeek

- (void)getCardioWeek:(NSString *)memberID date:(NSString *)date {
//	if(![self isNetAvalaible]) return;
	
	whichAPI = CARDIO_WEEK_METHOD;
	app=(NuvitaAppDelegate *)[[UIApplication sharedApplication]delegate];
		
	NSMutableString *sRequest=[[NSMutableString alloc] init];

	[sRequest appendString:@"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\"><soapenv:Header/><soapenv:Body>"];
	[sRequest appendString:@"<tem:GetCardioWeek><tem:memberID>"];
 	[sRequest appendString:memberID];
	[sRequest appendString:@"</tem:memberID><tem:weekDate>"];
	[sRequest appendString:date];
	[sRequest appendString:@"</tem:weekDate></tem:GetCardioWeek>"];
	[sRequest appendString:@"</soapenv:Body></soapenv:Envelope>"];

	[self callPostMethod:sRequest Action:[METHOD_URL stringByAppendingString:whichAPI] API:url];
}


#pragma mark -
#pragma mark GetStars

- (void)getStars:(NSString *)memberID date:(NSString *)weekDate
{
	if(![self isNetAvalaible]) return;
	
	whichAPI = STAR_METHOD;//@"senduserPass";
	app=(NuvitaAppDelegate *)[[UIApplication sharedApplication]delegate];
	
	NSMutableString *sRequest=[[NSMutableString alloc] init];
	//sRequest=[self createHeader];
	[sRequest appendString:@"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\"><soapenv:Header/><soapenv:Body>"];
	[sRequest appendString:@"<tem:GetStars><tem:memberID>"];
 	[sRequest appendString:memberID];//append Username
	[sRequest appendString:@"</tem:memberID><tem:weekDate>"];
	[sRequest appendString:weekDate];//append Password
	[sRequest appendString:@"</tem:weekDate></tem:GetStars>"];
	[sRequest appendString:@"</soapenv:Body></soapenv:Envelope>"];
	NSLog(@"request string: %@",sRequest);
	[self callPostMethod:sRequest Action:[METHOD_URL stringByAppendingString:whichAPI] API:url];
}

#pragma mark -
#pragma mark GetGlobalStars

- (void)getGlobalStars:(NSString *)pageNum date:(NSString *)weekDate
{
	if(![self isNetAvalaible]) return;
	
	whichAPI = GSTAR_METHOD;//@"senduserPass";
	app=(NuvitaAppDelegate *)[[UIApplication sharedApplication]delegate];
	
	NSMutableString *sRequest=[[NSMutableString alloc] init];
	//sRequest=[self createHeader];
	[sRequest appendString:@"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\"><soapenv:Header/><soapenv:Body>"];
	[sRequest appendString:@"<tem:GetGlobalCardioStars><tem:weekDate>"];
 	[sRequest appendString:weekDate];//append Username
	[sRequest appendString:@"</tem:weekDate><tem:PageNumber>"];
	[sRequest appendString:pageNum];//append Password
	[sRequest appendString:@"</tem:PageNumber></tem:GetGlobalCardioStars>"];
	[sRequest appendString:@"</soapenv:Body></soapenv:Envelope>"];
	NSLog(@"request string: %@",sRequest);
	[self callPostMethod:sRequest Action:[METHOD_URL stringByAppendingString:whichAPI] API:url];
}



#pragma mark -
#pragma mark GetMobilityWeek

- (void)getMobilityWeek:(NSString *)memberID date:(NSString *)weekDate
{
	if(![self isNetAvalaible]) return;
	
	whichAPI = MOBILITY_METHOD;//@"senduserPass";
	app=(NuvitaAppDelegate *)[[UIApplication sharedApplication]delegate];
	
	NSMutableString *sRequest=[[NSMutableString alloc] init];
	//sRequest=[self createHeader];
	[sRequest appendString:@"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\"><soapenv:Header/><soapenv:Body>"];
	[sRequest appendString:@"<tem:GetMobilityWeek><tem:memberID>"];
 	[sRequest appendString:memberID];//append Username
	[sRequest appendString:@"</tem:memberID><tem:weekDate>"];
	[sRequest appendString:weekDate];//append Password
	[sRequest appendString:@"</tem:weekDate></tem:GetMobilityWeek>"];
	[sRequest appendString:@"</soapenv:Body></soapenv:Envelope>"];
	NSLog(@"request string: %@",sRequest);
	[self callPostMethod:sRequest Action:[METHOD_URL stringByAppendingString:whichAPI] API:url];
}

#pragma mark -
#pragma mark getMobilityWorkouts

- (void)getMobilityWorkouts:(NSString *)memberID date:(NSString *)weekDate
{
	if(![self isNetAvalaible]) return;
	
	whichAPI = MOBILITY_WORKOUTS_METHOD;//@"senduserPass";
	app=(NuvitaAppDelegate *)[[UIApplication sharedApplication]delegate];
	
	NSMutableString *sRequest=[[NSMutableString alloc] init];
	//sRequest=[self createHeader];
	[sRequest appendString:@"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\"><soapenv:Header/><soapenv:Body>"];
	[sRequest appendString:@"<tem:GetMobilityWOList><tem:memberId>"];
 	[sRequest appendString:memberID];//append Username
	[sRequest appendString:@"</tem:memberId><tem:date>"];
	[sRequest appendString:weekDate];//append Password
	[sRequest appendString:@"</tem:date></tem:GetMobilityWOList>"];
	[sRequest appendString:@"</soapenv:Body></soapenv:Envelope>"];
	NSLog(@"request string: %@",sRequest);
	[self callPostMethod:sRequest Action:[METHOD_URL stringByAppendingString:whichAPI] API:url];
}

/*
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tem="http://tempuri.org/">
<soapenv:Header/>
<soapenv:Body>
<tem:GetMemberMobilityExercises>
<!--Optional:-->
<tem:memberId>?</tem:memberId>
<!--Optional:-->
<tem:WorkoutId>?</tem:WorkoutId>
</tem:GetMemberMobilityExercises>
</soapenv:Body>
</soapenv:Envelope>
*/

#pragma mark -
#pragma mark getMobilityExercises

- (void)getMobilityExercises:(NSString *)memberID workoutId:(NSString *)workoutId
{
    if(![self isNetAvalaible]) return;
	
	whichAPI = MOBILITY_EXERCISES_METHOD;//@"senduserPass";
	app=(NuvitaAppDelegate *)[[UIApplication sharedApplication]delegate];
	
	NSMutableString *sRequest=[[NSMutableString alloc] init];
	//sRequest=[self createHeader];
	[sRequest appendString:@"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\"><soapenv:Header/><soapenv:Body>"];
    
    [sRequest appendString:@"<tem:GetMemberMobilityExercises><tem:memberId>"];
 	[sRequest appendString:memberID];//append Username
	[sRequest appendString:@"</tem:memberId>"];
	[sRequest appendString:@"<tem:WorkoutId>"];
 	[sRequest appendString:workoutId];//append Username
	[sRequest appendString:@"</tem:WorkoutId></tem:GetMemberMobilityExercises>"];
	[sRequest appendString:@"</soapenv:Body></soapenv:Envelope>"];
	NSLog(@"request string: %@",sRequest);
	[self callPostMethod:sRequest Action:[METHOD_URL stringByAppendingString:whichAPI] API:url];
}

#pragma mark -
#pragma mark GetMobilityXWeek

- (void)getMobilityXWeek:(NSString *)memberID date:(NSString *)date
{
	if(![self isNetAvalaible]) return;
	
	whichAPI = MOBILITY_X_METHOD;//@"senduserPass";
	app=(NuvitaAppDelegate *)[[UIApplication sharedApplication]delegate];
	
	NSMutableString *sRequest=[[NSMutableString alloc] init];
	//sRequest=[self createHeader];
	[sRequest appendString:@"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\"><soapenv:Header/><soapenv:Body>"];
	[sRequest appendString:@"<tem:GetMobilityXHRSessions><tem:memberID>"];
 	[sRequest appendString:memberID];//append Username
	[sRequest appendString:@"</tem:memberID><tem:Date>"];
	[sRequest appendString:date];//append Password
	[sRequest appendString:@"</tem:Date></tem:GetMobilityXHRSessions>"];
	[sRequest appendString:@"</soapenv:Body></soapenv:Envelope>"];
	NSLog(@"request string: %@",sRequest);
	[self callPostMethod:sRequest Action:[METHOD_URL stringByAppendingString:whichAPI] API:url];
}

/*
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tem="http://tempuri.org/">
<soapenv:Header/>
<soapenv:Body>
<tem:GetMobilityXHRSessions>
<!--Optional:-->
<tem:memberID>8b5ed874-f43d-44f4-a8eb-2884f79e2acd</tem:memberID>
<!--Optional:-->
<tem:Date>03-04-2012</tem:Date>
</tem:GetMobilityXHRSessions>
</soapenv:Body>
</soapenv:Envelope>
*/

#pragma mark -
#pragma mark SaveMobilityXWeek

- (void)saveMobilityXWeek:(NSString *)memberID date:(NSString *)date WorkoutId:(NSString*)WorkoutId Rounds:(NSString*)Rounds
{
	if(![self isNetAvalaible]) return;
	
	whichAPI = SAVE_MOBILITY_X_METHOD;//@"senduserPass";
	app=(NuvitaAppDelegate *)[[UIApplication sharedApplication]delegate];
	
	NSMutableString *sRequest=[[NSMutableString alloc] init];
	//sRequest=[self createHeader];
	[sRequest appendString:@"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\"><soapenv:Header/><soapenv:Body>"];
	[sRequest appendString:@"<tem:SaveMobilityXWorkout><tem:memberId>"];
 	[sRequest appendString:memberID];//append Username
	[sRequest appendString:@"</tem:memberId><tem:HRDateId>"];
	[sRequest appendString:date];//append Password
	[sRequest appendString:@"</tem:HRDateId>"];
	
	[sRequest appendString:@"<tem:WorkoutId>"];
	[sRequest appendString:WorkoutId];//append Password
	[sRequest appendString:@"</tem:WorkoutId>"];
	
	[sRequest appendString:@"<tem:Rounds>"];
	[sRequest appendString:Rounds];//append Password
	[sRequest appendString:@"</tem:Rounds>"];
	
	[sRequest appendString:@"</tem:SaveMobilityXWorkout>"];
	[sRequest appendString:@"</soapenv:Body></soapenv:Envelope>"];
	NSLog(@"request string: %@",sRequest);
	[self callPostMethod:sRequest Action:[METHOD_URL stringByAppendingString:whichAPI] API:url];
}

/*
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tem="http://tempuri.org/">
<soapenv:Header/>
<soapenv:Body>
<tem:SaveMobilityXWorkout>
<tem:memberId>8b5ed874-f43d-44f4-a8eb-2884f79e2acd</tem:memberId>
<tem:HRDateId>2/22/2012 7:37:31 PM</tem:HRDateId>
<tem:WorkoutId>1</tem:WorkoutId>
<tem:Rounds>3</tem:Rounds>
</tem:SaveMobilityXWorkout>
</soapenv:Body>
</soapenv:Envelope>
=============================


*/

#pragma mark -
#pragma mark GetEducationWeek

- (void)getEducationWeek:(NSString *)memberID date:(NSString *)weekDate
{
	if(![self isNetAvalaible]) return;
	
	whichAPI = EDU_WEEK_METHOD;//@"senduserPass";
	app=(NuvitaAppDelegate *)[[UIApplication sharedApplication]delegate];
	
	NSMutableString *sRequest=[[NSMutableString alloc] init];
	//sRequest=[self createHeader];
	[sRequest appendString:@"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\"><soapenv:Header/><soapenv:Body>"];
	[sRequest appendString:@"<tem:GetEducationWeek><tem:memberID>"];
 	[sRequest appendString:memberID];//append Username
	[sRequest appendString:@"</tem:memberID><tem:weekDate>"];
	[sRequest appendString:weekDate];//append Password
	[sRequest appendString:@"</tem:weekDate></tem:GetEducationWeek>"];
	[sRequest appendString:@"</soapenv:Body></soapenv:Envelope>"];
	NSLog(@"request string: %@",sRequest);
	[self callPostMethod:sRequest Action:[METHOD_URL stringByAppendingString:whichAPI] API:url];
}

#pragma mark -
#pragma mark GetEducationLessonWeek

- (void)getEducationLessonWeek:(NSString *)memberID date:(NSString *)weekDate lessonNumber:(NSString *)lessonNumber
{
	if(![self isNetAvalaible]) return;
	
	whichAPI = EDU_LESSON_METHOD;//@"senduserPass";
	app=(NuvitaAppDelegate *)[[UIApplication sharedApplication]delegate];
	
	NSMutableString *sRequest=[[NSMutableString alloc] init];
	//sRequest=[self createHeader];
	[sRequest appendString:@"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\"><soapenv:Header/><soapenv:Body>"];
	[sRequest appendString:@"<tem:GetEducationLesson><tem:memberID>"];
 	[sRequest appendString:memberID];//append Username
	[sRequest appendString:@"</tem:memberID><tem:weekDate>"];
	[sRequest appendString:weekDate];//append Password
	[sRequest appendString:@"</tem:weekDate><tem:lessonNumber>"];
	[sRequest appendString:lessonNumber];//append Password
	[sRequest appendString:@"</tem:lessonNumber></tem:GetEducationLesson>"];
	[sRequest appendString:@"</soapenv:Body></soapenv:Envelope>"];
	NSLog(@"request string: %@",sRequest);
	[self callPostMethod:sRequest Action:[METHOD_URL stringByAppendingString:whichAPI] API:url];
}

#pragma mark -
#pragma mark SAVE_WELLNESSWALL_METHOD

- (void)saveWellnessWall:(NSString *)memberID date:(NSString *)date text:(NSString *)text photo:(NSString *)photo
{
	if(![self isNetAvalaible]) return;
	
	whichAPI = SAVE_WELLNESSWALL_METHOD;//@"senduserPass";
	app=(NuvitaAppDelegate *)[[UIApplication sharedApplication]delegate];
	
	NSMutableString *sRequest = [[NSMutableString alloc] init];
	[sRequest appendString:@"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\" xmlns:nuv=\"http://schemas.datacontract.org/2004/07/NuvitaMobileService\"><soapenv:Header/><soapenv:Body>"];
    
	[sRequest appendString:@"<tem:SaveWellnessWall><tem:WellnessWallUpload><nuv:Date>"];
 	[sRequest appendString:date];//append Username
	[sRequest appendString:@"</nuv:Date><nuv:Photo>"];
	[sRequest appendString:photo];//append Password
    [sRequest appendString:@"</nuv:Photo><nuv:Text>"];
	[sRequest appendString:text];//append Password
 	[sRequest appendString:@"</nuv:Text><nuv:memberId>"];
	[sRequest appendString:memberID];//append Password
	[sRequest appendString:@"</nuv:memberId></tem:WellnessWallUpload></tem:SaveWellnessWall>"];
    
    
	[sRequest appendString:@"</soapenv:Body></soapenv:Envelope>"];
	NSLog(@"request string: %@",sRequest);
	[self callPostMethod:sRequest Action:[METHOD_URL stringByAppendingString:whichAPI] API:url];
}

#pragma mark -
#pragma mark SAVE_PHOTO_WELLNESSWALL_METHOD

- (void)saveWellnessWallPhoto:(NSString *)memberID date:(NSString *)date text:(NSString *)text photo:(NSData *)photo
{

	if(![self isNetAvalaible]) return;
	
	whichAPI = SAVE_WELLNESSWALLPHOTO_METHOD;//@"senduserPass";
	app=(NuvitaAppDelegate *)[[UIApplication sharedApplication]delegate];
	
	NSMutableString *sRequest=[[NSMutableString alloc] init];
	
//    <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tem="http://tempuri.org/">
//    <soapenv:Header>
//    <tem:Text>this is objectsol</tem:Text>
//    <tem:PhotoSize>12121</tem:PhotoSize>
//    <tem:MemberId>8b5ed874-f43d-44f4-a8eb-2884f79e2acd</tem:MemberId>
//    <tem:Date>2012-08-23T00:00:00</tem:Date>
//    </soapenv:Header>
//    <soapenv:Body>
//    <tem:WellnessWallUpload>
//    <tem:Stream>cid:1123203778207</tem:Stream>
//    </tem:WellnessWallUpload>
//    </soapenv:Body>
//    </soapenv:Envelope>
    
    [sRequest appendString:@"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\">"];
    [sRequest appendString:@"<soapenv:Header>"];
    //
    [sRequest appendString:@"<tem:Text>"];
 	[sRequest appendString:text];
	[sRequest appendString:@"</tem:Text>"];
    //
    [sRequest appendString:@"<tem:PhotoSize>"];
 	[sRequest appendString:[NSString stringWithFormat:@"%d", [photo length]]];
	[sRequest appendString:@"</tem:PhotoSize>"];
    //
	[sRequest appendString:@"<tem:MemberId>"];
 	[sRequest appendString:memberID];
	[sRequest appendString:@"</tem:MemberId>"];
    //
    [sRequest appendString:@"<tem:Date>"];
 	[sRequest appendString:date];
	[sRequest appendString:@"</tem:Date>"];
    
    [sRequest appendString:@"</soapenv:Header>"];
    //body
    [sRequest appendString:@"<soapenv:Body>"];
    [sRequest appendString:@"<tem:WellnessWallUpload><tem:Stream>"];
    [sRequest appendString:@"<inc:Include href=\"cid:123456789123456\" xmlns:inc=\"http://www.w3.org/2004/08/xop/include\"/>"];
    [sRequest appendString:@"</tem:Stream></tem:WellnessWallUpload>"];
	[sRequest appendString:@"</soapenv:Body></soapenv:Envelope>"];
    
    //Multi part
    NSString *boundary = [NSString stringWithFormat:@"Abcd328726387263872638263809"];
    //
    NSMutableData *body = [NSMutableData data];
    
    //application/xop+xml;charset=utf-8;type="text/xml"
    //[body appendData:@"\r\n"];
    //[body appendData:boundary];
    //[body appendData:@"\r\n"];
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"Content-Type: application/xop+xml; charset=UTF-8; type=\"text/xml\"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Transfer-Encoding: 8bit\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    // "
    
    [body appendData:[[NSString stringWithFormat:@"Content-ID: <12345678912345671>\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithString:sRequest] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    
    //
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"Content-Type: application/octet-stream\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Transfer-Encoding: binary\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-ID: <123456789123456>\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    
    //[body appendData:[[NSString stringWithString:@"Content-Disposition: attachment; name=\"Photo\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    
    //NSString* photoText = [[NSString alloc] initWithData:photo encoding:NSUTF8StringEncoding];
    
    //NSLog(@"photo :%@", photoText);
    
    [body appendData:[NSData dataWithData:photo]];//photo
        
    [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    //end
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    	[self callMultiPostMethod:body Action:[METHOD_URL2 stringByAppendingString:whichAPI] API:url2];
}


#pragma mark -
#pragma mark saveMealsCamera
/*
- (void)saveMealsCamera:(NSString *)memberID date:(NSString *)date mealEnum:(NSString *)mealEnum photo:(NSString *)photo
{
	if(![self isNetAvalaible]) return;
	
	whichAPI = SAVE_MEALS_CAMERA_METHOD;
	app=(NuvitaAppDelegate *)[[UIApplication sharedApplication]delegate];
	
	NSMutableString *sRequest=[[NSMutableString alloc] init];
	//sRequest=[self createHeader];
	[sRequest appendString:@"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\">"];
    [sRequest appendString:@"<soapenv:Header>"];

//    <tem:MemberId>?</tem:MemberId>
//    <tem:MealEnum>?</tem:MealEnum>
//    <tem:FileSize>?</tem:FileSize>
//    <tem:Date>?</tem:Date>
//    </soapenv:Header>
//    //
//    <soapenv:Body>
//    <tem:MealPhoto>
//    <tem:Stream>cid:1013303619222</tem:Stream>
//    </tem:MealPhoto>
//    </soapenv:Body>
//    </soapenv:Envelope>
    //
	[sRequest appendString:@"<tem:MemberId>"];
 	[sRequest appendString:memberID];
	[sRequest appendString:@"</tem:MemberId>"];
    //
    [sRequest appendString:@"<tem:MealEnum>"];
 	[sRequest appendString:mealEnum];
	[sRequest appendString:@"</tem:MealEnum>"];
    //
    [sRequest appendString:@"<tem:FileSize>"];
 	[sRequest appendString:[NSString stringWithFormat:@"%d", [photo length]]];
	[sRequest appendString:@"</tem:FileSize>"];
    //
    [sRequest appendString:@"<tem:Date>"];
 	[sRequest appendString:date];
	[sRequest appendString:@"</tem:Date>"];
 
    [sRequest appendString:@"</soapenv:Header>"];
    //body
    [sRequest appendString:@"<soapenv:Body>"];
    [sRequest appendString:@"<tem:MealPhoto><tem:Stream>"];
	[sRequest appendString:photo];
    [sRequest appendString:@"</tem:Stream></tem:MealPhoto>"];
	[sRequest appendString:@"</soapenv:Body></soapenv:Envelope>"];
	NSLog(@"request string: %@",sRequest);
    
    [self callPostMethod:sRequest Action:[METHOD_URL2 stringByAppendingString:whichAPI] API:url2];
}
*/

- (void)saveMealsCamera:(NSString *)memberID date:(NSString *)date mealEnum:(NSString *)mealEnum photo:(NSData *)photo
{
	if(![self isNetAvalaible]) return;
	
	whichAPI = SAVE_MEALS_CAMERA_METHOD;//@"senduserPass";
	app=(NuvitaAppDelegate *)[[UIApplication sharedApplication]delegate];
	
	NSMutableString *sRequest=[[NSMutableString alloc] init];
	//sRequest=[self createHeader];
//	[sRequest appendString:@"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\"><soapenv:Header/><soapenv:Body>"];
//	[sRequest appendString:@"<tem:UploadMealPhoto><tem:memberId>"];
// 	[sRequest appendString:memberID];//append Username
//	[sRequest appendString:@"</tem:memberId><tem:date>"];
//	[sRequest appendString:date];//append Password
//    [sRequest appendString:@"</tem:date><tem:mealEnum>"];
//	[sRequest appendString:mealEnum];//append Password
// 	[sRequest appendString:@"</tem:mealEnum><tem:Photo>"];
//	[sRequest appendString:@"cid:12345678912345"];//append Password
//	[sRequest appendString:@"</tem:Photo></tem:UploadMealPhoto>"];
//	[sRequest appendString:@"</soapenv:Body></soapenv:Envelope>"];
//	NSLog(@"request string: %@",sRequest);
    [sRequest appendString:@"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\">"];
    [sRequest appendString:@"<soapenv:Header>"];
    //
	[sRequest appendString:@"<tem:MemberId>"];
 	[sRequest appendString:memberID];
	[sRequest appendString:@"</tem:MemberId>"];
    //
    [sRequest appendString:@"<tem:MealEnum>"];
 	[sRequest appendString:mealEnum];
	[sRequest appendString:@"</tem:MealEnum>"];
    //
    [sRequest appendString:@"<tem:FileSize>"];
 	[sRequest appendString:[NSString stringWithFormat:@"%d", [photo length]]];
	[sRequest appendString:@"</tem:FileSize>"];
    //
    [sRequest appendString:@"<tem:Date>"];
 	[sRequest appendString:date];
	[sRequest appendString:@"</tem:Date>"];
    
    [sRequest appendString:@"</soapenv:Header>"];
    //body
    [sRequest appendString:@"<soapenv:Body>"];
    [sRequest appendString:@"<tem:MealPhoto><tem:Stream>"];
    	[sRequest appendString:@"<inc:Include href=\"cid:123456789123456\" xmlns:inc=\"http://www.w3.org/2004/08/xop/include\"/>"];
    [sRequest appendString:@"</tem:Stream></tem:MealPhoto>"];
	[sRequest appendString:@"</soapenv:Body></soapenv:Envelope>"];
    
    NSLog(@"Camera Photo save request:%@",sRequest);
    
    //Multi part
    NSString *boundary = [NSString stringWithFormat:@"Abcd328726387263872638263809"];
    //
    NSMutableData *body = [NSMutableData data];
    
    //application/xop+xml;charset=utf-8;type="text/xml"
    //[body appendData:@"\r\n"];
    //[body appendData:boundary];
    //[body appendData:@"\r\n"];
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];

    [body appendData:[[NSString stringWithFormat:@"Content-Type: application/xop+xml; charset=UTF-8; type=\"text/xml\"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Transfer-Encoding: 8bit\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    // "

    [body appendData:[[NSString stringWithFormat:@"Content-ID: <12345678912345671>\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithString:sRequest] dataUsingEncoding:NSUTF8StringEncoding]];    
    [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];

    //
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    //Content-Type: image/x-png; name="/quit_btn@2x.png""
    //Content-Disposition: attachment; name="quit_btn@2x.png"; filename="/quit_btn@2x.png""

    //[body appendData:[[NSString stringWithString:@"Content-Disposition: attachment; name=\"file\"; filename=\"file.jpg\"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    //; name=test.jpg application/octet-stream
    [body appendData:[[NSString stringWithFormat:@"Content-Type: application/octet-stream; name=test.jpg\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Transfer-Encoding: binary\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-ID: <123456789123456>\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    
    //[body appendData:[[NSString stringWithString:@"Content-Disposition: attachment; name=\"Photo\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[NSData dataWithData:photo]];
    
    [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    //end
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    //[request setHTTPBody:body];
//    NSMutableString* reqData = [NSString stringWithUTF8String:[body bytes]];
//   reqData = [[NSMutableString alloc] initWithData:body encoding:NSASCIIStringEncoding];
//    NSLog(@"Request body : %@",reqData);
    
	[self callMultiPostMethod:body Action:[METHOD_URL2 stringByAppendingString:whichAPI] API:url2];
}

/*
 (void)saveMealsCamera:(NSString *)memberID date:(NSString *)date mealEnum:(NSString *)mealEnum photo:(NSData *)photo
 {
 if(![self isNetAvalaible]) return;
 
 whichAPI = SAVE_MEALS_CAMERA_METHOD;//@"senduserPass";
 app=(NuvitaAppDelegate *)[[UIApplication sharedApplication]delegate];
 
 NSMutableString *sRequest=[[NSMutableString alloc] init];
 //sRequest=[self createHeader];
 //	[sRequest appendString:@"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\"><soapenv:Header/><soapenv:Body>"];
 //	[sRequest appendString:@"<tem:UploadMealPhoto><tem:memberId>"];
 // 	[sRequest appendString:memberID];//append Username
 //	[sRequest appendString:@"</tem:memberId><tem:date>"];
 //	[sRequest appendString:date];//append Password
 //    [sRequest appendString:@"</tem:date><tem:mealEnum>"];
 //	[sRequest appendString:mealEnum];//append Password
 // 	[sRequest appendString:@"</tem:mealEnum><tem:Photo>"];
 //	[sRequest appendString:@"cid:12345678912345"];//append Password
 //	[sRequest appendString:@"</tem:Photo></tem:UploadMealPhoto>"];
 //	[sRequest appendString:@"</soapenv:Body></soapenv:Envelope>"];
 //	NSLog(@"request string: %@",sRequest);
 [sRequest appendString:@"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\">"];
 [sRequest appendString:@"<soapenv:Header>"];
 //
 [sRequest appendString:@"<tem:MemberId>"];
 [sRequest appendString:memberID];
 [sRequest appendString:@"</tem:MemberId>"];
 //
 [sRequest appendString:@"<tem:MealEnum>"];
 [sRequest appendString:mealEnum];
 [sRequest appendString:@"</tem:MealEnum>"];
 //
 [sRequest appendString:@"<tem:FileSize>"];
 [sRequest appendString:[NSString stringWithFormat:@"%d", [photo length]]];
 [sRequest appendString:@"</tem:FileSize>"];
 //
 [sRequest appendString:@"<tem:Date>"];
 [sRequest appendString:date];
 [sRequest appendString:@"</tem:Date>"];
 
 [sRequest appendString:@"</soapenv:Header>"];
 //body
 [sRequest appendString:@"<soapenv:Body>"];
 [sRequest appendString:@"<tem:MealPhoto><tem:Stream>"];
 [sRequest appendString:@"cid:12345678912345"];
 [sRequest appendString:@"</tem:Stream></tem:MealPhoto>"];
 [sRequest appendString:@"</soapenv:Body></soapenv:Envelope>"];
 //Multi part
 NSString *boundary = [NSString stringWithString:@"---------------------------Boundary Line---------------------------"];
 NSMutableData *body = [NSMutableData data];
 
 //[body appendData:@"\r\n"];
 //[body appendData:boundary];
 //[body appendData:@"\r\n"];
 [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
 
 [body appendData:[[NSString stringWithString:@"Content-Type: application/xop+xml; charset=UTF-8; type=\"text/xml\"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
 [body appendData:[[NSString stringWithString:@"Content-Transfer-Encoding: 8bit\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
 [body appendData:[[NSString stringWithString:@"Content-ID: <12345678912345671>\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
 [body appendData:[[NSString stringWithString:sRequest] dataUsingEncoding:NSUTF8StringEncoding]];    
 [body appendData:[[NSString stringWithString:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
 
 //
 [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
 
 //[body appendData:[[NSString stringWithString:@"Content-Disposition: attachment; name=\"userfile\"; filename=\"test.jpg\"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
 
 [body appendData:[[NSString stringWithString:@"Content-Type: application/octet-stream; name=test.jpg\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
 [body appendData:[[NSString stringWithString:@"Content-Transfer-Encoding: binary\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
 [body appendData:[[NSString stringWithString:@"Content-ID: <12345678912345>\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
 
 [body appendData:photo];
 
 [body appendData:[[NSString stringWithString:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
 //end
 [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
 
 //[request setHTTPBody:body];
 //    NSMutableString* reqData = [[NSMutableString alloc] initWithData:body encoding:NSUTF8StringEncoding];
 
 [self callMultiPostMethod:body Action:[METHOD_URL2 stringByAppendingString:whichAPI] API:url2];
 }

 
 */
#pragma mark -
#pragma mark GetEducationLessonWeek

- (void)getWellnessWall:(NSString *)memberID pageNumber:(NSString *)pageNumber
{
	if(![self isNetAvalaible]) return;
	
	whichAPI = WELLNESSWALL_METHOD;//@"senduserPass";
	app=(NuvitaAppDelegate *)[[UIApplication sharedApplication]delegate];
	
	NSMutableString *sRequest=[[NSMutableString alloc] init];
	//sRequest=[self createHeader];
	[sRequest appendString:@"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\"><soapenv:Header/><soapenv:Body>"];
	[sRequest appendString:@"<tem:GetWellnessWall><tem:memberId>"];
 	[sRequest appendString:memberID];//append Username
	[sRequest appendString:@"</tem:memberId><tem:PageNumber>"];
	[sRequest appendString:pageNumber];//append Password
	[sRequest appendString:@"</tem:PageNumber></tem:GetWellnessWall>"];
	[sRequest appendString:@"</soapenv:Body></soapenv:Envelope>"];
	NSLog(@"request string: %@",sRequest);
	[self callPostMethod:sRequest Action:[METHOD_URL stringByAppendingString:whichAPI] API:url];
}


#pragma mark -
#pragma mark saveQuiz

- (void)saveQuiz:(NSString *)memberID date:(NSString *)weekDate lessonNumber:(NSString *)lessonNumber correctAnswers:(NSString*)correctAnswers totalQuestions:(NSString*)totalQuestions
{
	if(![self isNetAvalaible]) return;
	
	whichAPI = SAVE_MEMBER_QUIZ_METHOD;//@"senduserPass";
	app=(NuvitaAppDelegate *)[[UIApplication sharedApplication]delegate];
	
	NSMutableString *sRequest=[[NSMutableString alloc] init];
	//sRequest=[self createHeader];
	[sRequest appendString:@"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\"><soapenv:Header/><soapenv:Body>"];
	[sRequest appendString:@"<tem:SaveMemberQuiz><tem:memberId>"];
 	[sRequest appendString:memberID];//append Username
	[sRequest appendString:@"</tem:memberId><tem:date>"];
	[sRequest appendString:weekDate];//append Password
	[sRequest appendString:@"</tem:date><tem:lessonNumber>"];
	[sRequest appendString:lessonNumber];//append Password
	[sRequest appendString:@"</tem:lessonNumber>"];
	
	[sRequest appendString:@"<tem:correctAnswers>"];
	[sRequest appendString:correctAnswers];//append Password
	[sRequest appendString:@"</tem:correctAnswers>"];
	
	[sRequest appendString:@"</tem:SaveMemberQuiz></soapenv:Body></soapenv:Envelope>"];
	NSLog(@"request string: %@",sRequest);
	[self callPostMethod:sRequest Action:[METHOD_URL stringByAppendingString:whichAPI] API:url];
}


#pragma mark -
#pragma mark SaveMobility

- (void)SaveMobility:(NSString *)xmldata
{
	if(![self isNetAvalaible]) return;
	
	whichAPI = SAVE_MOBILITY_WEEK_METHOD;//@"senduserPass";
	app=(NuvitaAppDelegate *)[[UIApplication sharedApplication]delegate];
	
	NSMutableString *sRequest=[[NSMutableString alloc] init];
	//sRequest=[self createHeader];
	[sRequest appendString:@"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\" xmlns:nuv=\"http://schemas.datacontract.org/2004/07/NuvitaMobileService\"><soapenv:Header/><soapenv:Body>"];
	 //<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\"><soapenv:Header/><soapenv:Body>"];
	
	[sRequest appendString:@"<tem:SaveMobilityWeek><tem:mobilityWeek>"];
 	[sRequest appendString:xmldata];//append Password
	[sRequest appendString:@"</tem:mobilityWeek>"];
	
	[sRequest appendString:@"</tem:SaveMobilityWeek></soapenv:Body></soapenv:Envelope>"];
	NSLog(@"request string: %@",sRequest);
	[self callPostMethod:sRequest Action:[METHOD_URL stringByAppendingString:whichAPI] API:url];
}

#pragma mark -
#pragma mark SaveNutritionMeal

- (void)saveNutritionMeal:(NSString *)memberID date:(NSString *)date mealEnum:(NSString *)mealEnum description:(NSString*)description caloriesConsumed:(NSString*)caloriesConsumed healthRanking:(NSString*)healthRanking targetCalories:(NSString*)targetCalories
{
	if(![self isNetAvalaible]) return;
	
	whichAPI = SAVE_NUTRITION_MEAL_METHOD;//@"senduserPass";
	app=(NuvitaAppDelegate *)[[UIApplication sharedApplication]delegate];
	
	NSMutableString *sRequest=[[NSMutableString alloc] init];
	//sRequest=[self createHeader];
	//[sRequest appendString:@"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\"><soapenv:Header/><soapenv:Body>"];	
	[sRequest appendString:@"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\">"];
	[sRequest appendString:@"<soapenv:Header/>"];
	
	[sRequest appendString:@"<soapenv:Body>"];
	
	[sRequest appendString:@"<tem:SaveNutritionMeal><tem:memberId>"];
 	[sRequest appendString:memberID];//append Username
	[sRequest appendString:@"</tem:memberId><tem:date>"];
	[sRequest appendString:date];//append Password
	[sRequest appendString:@"</tem:date><tem:mealEnum>"];
	[sRequest appendString:mealEnum];//append Password
	[sRequest appendString:@"</tem:mealEnum>"];
	
	[sRequest appendString:@"<tem:description>"];
	[sRequest appendString:description];//append Password
	[sRequest appendString:@"</tem:description>"];
	
	[sRequest appendString:@"<tem:caloriesConsumed>"];
	[sRequest appendString:caloriesConsumed];//append Password
	[sRequest appendString:@"</tem:caloriesConsumed>"];	
	
	[sRequest appendString:@"<tem:healthRanking>"];
	[sRequest appendString:healthRanking];//append Password
	[sRequest appendString:@"</tem:healthRanking>"];	
	
	[sRequest appendString:@"<tem:targetCalories>"];
	[sRequest appendString:targetCalories];//append Password
	[sRequest appendString:@"</tem:targetCalories>"];
	
	[sRequest appendString:@"</tem:SaveNutritionMeal></soapenv:Body></soapenv:Envelope>"];
	NSLog(@"request string: %@",sRequest);
	[self callPostMethod:sRequest Action:[METHOD_URL stringByAppendingString:whichAPI] API:url];
}

/*
 <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tem="http://tempuri.org/">
 <soapenv:Header/>
 <soapenv:Body>
 <tem:SaveNutritionMeal>
 <tem:memberId>8b5ed874-f43d-44f4-a8eb-2884f79e2acd</tem:memberId>
 <tem:date>2012-02-05T00:00:00</tem:date>
 <tem:mealEnum>4</tem:mealEnum>
 <tem:description>tyt</tem:description>
 <tem:caloriesConsumed>34</tem:caloriesConsumed>
 <tem:healthRanking>4</tem:healthRanking>
 <tem:targetCalories>554</tem:targetCalories>
 </tem:SaveNutritionMeal>
 </soapenv:Body>
 </soapenv:Envelope>
*/

#pragma mark -
#pragma mark SaveNutritionMeal

- (void)saveNutritionDay:(NSString *)memberID date:(NSString *)date Breakfast:(NSString *)Breakfast AMSnack:(NSString*)AMSnack PMSnack:(NSString*)PMSnack Water:(NSString*)Water Vitamin:(NSString*)Vitamin
{
	if(![self isNetAvalaible]) return;
	
	whichAPI = SAVE_NUTRITION_DAY_METHOD;//@"senduserPass";
	app=(NuvitaAppDelegate *)[[UIApplication sharedApplication]delegate];
	
	NSMutableString *sRequest=[[NSMutableString alloc] init];
	//sRequest=[self createHeader];
	[sRequest appendString:@"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\">"];
	[sRequest appendString:@"<soapenv:Header/>"];
	
	[sRequest appendString:@"<soapenv:Body>"];
	
	[sRequest appendString:@"<tem:SaveNutritionDay><tem:memberId>"];
 	[sRequest appendString:memberID];//append Username
	[sRequest appendString:@"</tem:memberId><tem:date>"];
	[sRequest appendString:date];//append Password
	[sRequest appendString:@"</tem:date><tem:breakfast>"];
	[sRequest appendString:Breakfast];//append Password
	[sRequest appendString:@"</tem:breakfast>"];
		
	[sRequest appendString:@"<tem:amSnack>"];
	[sRequest appendString:AMSnack];//append Password
	[sRequest appendString:@"</tem:amSnack>"];
	
	[sRequest appendString:@"<tem:pmSnack>"];
	[sRequest appendString:PMSnack];//append Password
	[sRequest appendString:@"</tem:pmSnack>"];	
	
	[sRequest appendString:@"<tem:water>"];
	[sRequest appendString:Water];//append Password
	[sRequest appendString:@"</tem:water>"];	
	
	[sRequest appendString:@"<tem:vitamin>"];
	[sRequest appendString:Vitamin];//append Password
	[sRequest appendString:@"</tem:vitamin>"];
	
	[sRequest appendString:@"</tem:SaveNutritionDay></soapenv:Body></soapenv:Envelope>"];
	NSLog(@"request string: %@",sRequest);
	[self callPostMethod:sRequest Action:[METHOD_URL stringByAppendingString:whichAPI] API:url];
}


/*
 <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tem="http://tempuri.org/">
 <soapenv:Header/>
 <soapenv:Body>
 <tem:SaveNutritionDay>
 <tem:memberId>8b5ed874-f43d-44f4-a8eb-2884f79e2acd</tem:memberId>
 <tem:date>02-05-2012</tem:date>
 <tem:Breakfast>false</tem:Breakfast>
 <tem:AMSnack>true</tem:AMSnack>
 <tem:PMSnack>false</tem:PMSnack>
 <tem:Water>true</tem:Water>
 <tem:Vitamin>false</tem:Vitamin>
 </tem:SaveNutritionDay>
 </soapenv:Body>
 </soapenv:Envelope>
*/


#pragma mark -
#pragma mark CallPostMethod

- (void)initOfflineManager {
    
    NuvitaAppDelegate *appDelegate = (NuvitaAppDelegate *)[[UIApplication sharedApplication] delegate];
    self.offlineManager = [[OfflineManager alloc] init];
    self.offlineManager.managedObjectContext = appDelegate.managedObjectContext;
}


- (void)callPostMethod:(NSMutableString *)sRequest Action:(NSString *)action API:(NSString *)api {
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    
    [self initOfflineManager];
    Logging *logs = [[Logging alloc] init];
    logs.request = sRequest;
    logs.action = action;
    logs.dateTime = [NSDate date];//[formatter stringFromDate:[NSDate date]];
    logs.api = api;
    logs.method = whichAPI;
    
    NSData *postBody;
    postBody=[sRequest dataUsingEncoding:NSUTF8StringEncoding];
    NSURL *apiURL=[NSURL URLWithString:api];
    NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:apiURL];

    [request addValue:@"text/xml" forHTTPHeaderField:@"Content-Type"];
    [request addValue:action forHTTPHeaderField:@"SOAPAction"];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postBody];
    
    logs.activeRequest = request;
    [[[AppData sharedAppData] arrRequest] addObject:logs];
    
 
    NSURLConnection *conn=[[NSURLConnection alloc] initWithRequest:request delegate:self];
	if (conn)
	{
		;
	}
}

#pragma mark -
#pragma mark CallPostMethod

- (void)callMultiPostMethod:(NSData *)postBody Action:(NSString *)action API:(NSString *)api
{
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
	
//	NSData *postBody;
//	postBody=[sRequest dataUsingEncoding:NSUTF8StringEncoding];
	NSURL *apiURL=[NSURL URLWithString:api];
	NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:apiURL];
	
    //NSLog(@"soapaction %@", action);
        //
    NSString *boundary = [NSString stringWithFormat:@"Abcd328726387263872638263809"];

    NSString *contentType = [NSString stringWithFormat:@ "multipart/related; type=\"application/xop+xml\"; start=\"<12345678912345671>\"; start-info=\"text/xml\"; boundary=%@",boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    [request setValue:[NSString stringWithFormat:@"%d",[postBody length]]  forHTTPHeaderField:@"Content-Length"];
    [request addValue:action forHTTPHeaderField:@"SOAPAction"];
	[request setHTTPMethod:@"POST"];
    
	[request setHTTPBody:postBody];
	NSURLConnection *conn=[[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    //	NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:apiURL];
    //	[request setHTTPMethod:@"GET"];
    //	NSURLConnection *conn=[[NSURLConnection alloc] initWithRequest:request delegate:self];
	
	if (conn)
	{
		;
	}
}


#pragma mark -
#pragma mark Connection Deligate

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {


    //...scan for logs request
    NSArray *activeRequest = [[AppData sharedAppData] arrRequest];
    for (Logging *logs in activeRequest) {
        if ([logs.activeRequest isEqual:connection.currentRequest]) {
            logs.statusCodex = [NSString stringWithFormat:@"%d",[(NSHTTPURLResponse*) response statusCode]];
        }
    }

	if ([(NSHTTPURLResponse *) response statusCode] != 200){
            isstatus=YES;
            errorResponseAlert *myerrorResponseAlert=[[errorResponseAlert alloc]init];
        
            if (!isOfflineData) {
                isOfflineData = NO;
//                [myerrorResponseAlert showAlert];
  
            }
    
            [target performSelector:failureHandler withObject:nil withObject:nil];
            [myerrorResponseAlert release], myerrorResponseAlert = nil;
            return;
	}
	
	if(d2)
		[d2 release];
	d2 = [[NSMutableData alloc]init];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data  {
    NSString *data_Response1 = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
//    NSLog(@"Response_of_submit =%@",data_Response1);

    NSArray *activeRequest = [[AppData sharedAppData] arrRequest];
    for (Logging *logs in activeRequest) {
        if ([logs.activeRequest isEqual:connection.currentRequest]) {
            
            logs.response = data_Response1;
            

            NSData *dataObject = [NSKeyedArchiver archivedDataWithRootObject:logs];
            NSDictionary *data = [NSDictionary dictionaryWithObjectsAndKeys:
                                  dataObject, @"logs",
                                  nil];
            if ([self.offlineManager addNewLogs:data]) {

            }
        }
    }
    
    
    
	[d2 appendData:data];	
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    
    
	if(isstatus==YES) {
		isstatus=NO;
		return;
	} else {
		NSString *data_Response = [[NSString alloc] initWithData:d2 encoding:NSUTF8StringEncoding];
		//NSLog(@"Response_of_submit =%@",data_Response);
		
		NSString *XHTMLsearchForWarning = @"XHTML";
		NSRange rangeXHTMLWarning = [data_Response rangeOfString : XHTMLsearchForWarning];
		
		NSString *HTMLsearchForWarning = @"HTML";
		NSRange rangeHTMLWarning = [data_Response rangeOfString : HTMLsearchForWarning];
		if (rangeXHTMLWarning.location != NSNotFound || rangeHTMLWarning.location != NSNotFound) {
			[target performSelector:failureHandler];
			UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Error" message:@"Wrong response format." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
			[alert show];
			[alert release];
			return ;
		}
		
		if ([whichAPI isEqualToString:LOGIN_METHOD]) {
			
			NSError *parseError = nil;
			LoginPerser *loginPerser = [[LoginPerser alloc] init];
			[loginPerser parseXMLFileAtData:d2 parseError:&parseError];	
			
			[target performSelector:successHandler withObject:loginPerser.responseStatus withObject:nil];
			[loginPerser release];
		}else if ([whichAPI isEqualToString:GET_PROPOSED_PROGRAM]) {
			
			NSError *parseError = nil;
			NuvitaXMLParser *perser = [[NuvitaXMLParser alloc] init];
            [perser parseXMLFileAtData:d2 parseError:&parseError parseService:GET_PROPOSED_PROGRAM];
			
			[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
			[target performSelector:successHandler withObject:perser.responseStatus withObject:perser];
            
		}else if ([whichAPI isEqualToString:SAVE_PROPOSED_PROGRAM]) {
			
			NSError *parseError = nil;
			NuvitaXMLParser *perser = [[NuvitaXMLParser alloc] init];
            [perser parseXMLFileAtData:d2 parseError:&parseError parseService:SAVE_PROPOSED_PROGRAM];
			
			[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
			[target performSelector:successHandler withObject:perser.responseStatus withObject:perser];
            
		}else if ([whichAPI isEqualToString:DELETE_VO2MAX_RR]) {
			
			NSError *parseError = nil;
			NuvitaXMLParser *perser = [[NuvitaXMLParser alloc] init];
            [perser parseXMLFileAtData:d2 parseError:&parseError parseService:DELETE_VO2MAX_RR];
			
			[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
			[target performSelector:successHandler withObject:perser.responseStatus withObject:perser];
            
		}else if ([whichAPI isEqualToString:ACTIVITY_LEVEL]) {
			
			NSError *parseError = nil;
			NuvitaXMLParser *perser = [[NuvitaXMLParser alloc] init];
            [perser parseXMLFileAtData:d2 parseError:&parseError parseService:ACTIVITY_LEVEL];
			
			[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
			[target performSelector:successHandler withObject:perser.responseStatus withObject:perser];
            
		}
        else if ([whichAPI isEqualToString:GET_VO2MAX_RR]) {
            
            NSError *parseError = nil;
			NuvitaXMLParser *perser = [[NuvitaXMLParser alloc] init];
            [perser parseXMLFileAtData:d2 parseError:&parseError parseService:GET_VO2MAX_RR];
			
			[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
			[target performSelector:successHandler withObject:perser.responseStatus withObject:perser];
            
		}else if ([whichAPI isEqualToString:GET_MEASUREMENT_TRENDS]) {
            
            NSError *parseError = nil;
			NuvitaXMLParser *perser = [[NuvitaXMLParser alloc] init];
            [perser parseXMLFileAtData:d2 parseError:&parseError parseService:GET_MEASUREMENT_TRENDS];
			
			[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
			[target performSelector:successHandler withObject:perser.responseStatus withObject:perser];
		}
		else if ([whichAPI isEqualToString:TEAM_PROGRESS_METHOD]) {
			
			NSError *parseError = nil;
			TeamProgressData *perser = [[TeamProgressData alloc] init];
			[perser parseXMLFileAtData:d2 parseError:&parseError];	
			
			//[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
			[target performSelector:successHandler withObject:perser.responseStatus withObject:perser];
			
			//[perser release];
		}	
		else if ([whichAPI isEqualToString:MEMBER_PROGRESS_METHOD]) {
			
			NSError *parseError = nil;
			MemberProgressData *perser = [[MemberProgressData alloc] init];
			[perser parseXMLFileAtData:d2 parseError:&parseError];	
			
			//[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
			[target performSelector:successHandler withObject:perser.responseStatus withObject:perser];
			
			//[perser release];
		}	
		else if ([whichAPI isEqualToString:STAR_METHOD] || [whichAPI isEqualToString:GSTAR_METHOD]) {
			
			NSError *parseError = nil;
			StarProgressData *perser = [[StarProgressData alloc] init];
			[perser parseXMLFileAtData:d2 parseError:&parseError];	
			
			//[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
			[target performSelector:successHandler withObject:perser.responseStatus withObject:perser];
			
			//[perser release];
		}	
		else if ([whichAPI isEqualToString:CARDIO_WEEK_METHOD]) {
    
			NSError *parseError = nil;
			CardioWeekData *perser = [[CardioWeekData alloc] init];
			[perser parseXMLFileAtData:d2 parseError:&parseError];	
			
			[target performSelector:successHandler withObject:perser.responseStatus withObject:perser];
		}	
		else if ([whichAPI isEqualToString:MOBILITY_METHOD]) {
			
			NSError *parseError = nil;
			MobilityWeekData *perser = [[MobilityWeekData alloc] init];
			//[perser retain];
			[perser parseXMLFileAtData:d2 parseError:&parseError];	
			
			[target performSelector:successHandler withObject:perser.responseStatus withObject:perser];
		}
		else if ([whichAPI isEqualToString:MOBILITY_X_METHOD]) {
			NSError *parseError = nil;
			
			//NSDictionary* data = [XMLReader dictionaryForXMLData:d2 error:&parseError];
			
			MobilityXData *perser = [[MobilityXData alloc] init];
			//[perser retain];
			[perser parseXMLFileAtData:d2 parseError:&parseError];	
		
			[target performSelector:successHandler withObject:perser.responseStatus withObject:perser];
		}
        else if ([whichAPI isEqualToString:MOBILITY_WORKOUTS_METHOD]) {
			NSError *parseError = nil;
                    
			MobilityWorkoutsData *perser = [[MobilityWorkoutsData alloc] init];
			//[perser retain];
			[perser parseXMLFileAtData:d2 parseError:&parseError];	
            
			[target performSelector:successHandler withObject:perser.responseStatus withObject:perser];
		}
        else if ([whichAPI isEqualToString:MOBILITY_EXERCISES_METHOD]) {
			NSError *parseError = nil;
						
			MobilityExerciseData *perser = [[MobilityExerciseData alloc] init];
			//[perser retain];
			[perser parseXMLFileAtData:d2 parseError:&parseError];	
            
			[target performSelector:successHandler withObject:perser.responseStatus withObject:perser];
		}
		else if ([whichAPI isEqualToString:EDU_WEEK_METHOD]) {
			
			NSError *parseError = nil;
			LessonWeekData *perser = [[LessonWeekData alloc] init];
			[perser parseXMLFileAtData:d2 parseError:&parseError];	
			
			[target performSelector:successHandler withObject:perser.responseStatus withObject:perser];
		}
		else if ([whichAPI isEqualToString:EDU_LESSON_METHOD]) {
			
			NSError *parseError = nil;
			EduLessonWeekData *perser = [[EduLessonWeekData alloc] init];
			[perser parseXMLFileAtData:d2 parseError:&parseError];	
			
			[target performSelector:successHandler withObject:perser.responseStatus withObject:perser];
		}
		else if ([whichAPI isEqualToString:SAVE_MEMBER_QUIZ_METHOD]) {
			
			NSError *parseError = nil;
			SaveQuizData *perser = [[SaveQuizData alloc] init];
			[perser parseXMLFileAtData:d2 parseError:&parseError];	
			
			[target performSelector:successHandler withObject:perser.responseStatus withObject:nil];
		}
        else if ([whichAPI isEqualToString:SAVE_WELLNESSWALL_METHOD]) {
			
			NSError *parseError = nil;
			SaveWellnessWallData *perser = [[SaveWellnessWallData alloc] init];
			[perser parseXMLFileAtData:d2 parseError:&parseError];	
			
			[target performSelector:successHandler withObject:perser.responseStatus withObject:nil];
		}
        else if ([whichAPI isEqualToString:SAVE_MEALS_CAMERA_METHOD]) {
			
			NSError *parseError = nil;
			SaveMealsCameraData *perser = [[SaveMealsCameraData alloc] init];
			[perser parseXMLFileAtData:d2 parseError:&parseError];	
			
			[target performSelector:successHandler withObject:perser.responseStatus withObject:nil];
		}
        else if ([whichAPI isEqualToString:SAVE_WELLNESSWALLPHOTO_METHOD]) {
			
			NSError *parseError = nil;
			SaveMealsCameraData *perser = [[SaveMealsCameraData alloc] init];
			[perser parseXMLFileAtData:d2 parseError:&parseError];	
			
			[target performSelector:successHandler withObject:perser.responseStatus withObject:nil];
		}
        else if ([whichAPI isEqualToString:SAVE_CARDIO_SESSION_METHOD]) {
			
			NSError *parseError = nil;
			SaveMealsCameraData *perser = [[SaveMealsCameraData alloc] init];
			[perser parseXMLFileAtData:d2 parseError:&parseError];
			
			[target performSelector:successHandler withObject:perser.responseStatus withObject:nil];
		}
		else if ([whichAPI isEqualToString:SAVE_MOBILITY_WEEK_METHOD]) {
			
			NSError *parseError = nil;
			SaveMobilityData *perser = [[SaveMobilityData alloc] init];
			[perser parseXMLFileAtData:d2 parseError:&parseError];	
			
			[target performSelector:successHandler withObject:perser.responseStatus withObject:nil];
		}
		else if ([whichAPI isEqualToString:SAVE_MOBILITY_X_METHOD]) {
			
			NSError *parseError = nil;
			SaveMobilityXWorkout *perser = [[SaveMobilityXWorkout alloc] init];
			[perser parseXMLFileAtData:d2 parseError:&parseError];	
			
			[target performSelector:successHandler withObject:perser.responseStatus withObject:nil];
		}
		else if ([whichAPI isEqualToString:NUTRITION_METHOD]) {
			
			NSError *parseError = nil;
			NutritionDayData *perser = [[NutritionDayData alloc] init];
			[perser parseXMLFileAtData:d2 parseError:&parseError];	
			
			[target performSelector:successHandler withObject:perser.responseStatus withObject:perser];
		}
		else if ([whichAPI isEqualToString:SAVE_NUTRITION_MEAL_METHOD]) {
			
			NSError *parseError = nil;
			SaveNutritionMealData *perser = [[SaveNutritionMealData alloc] init];
			[perser parseXMLFileAtData:d2 parseError:&parseError];	
			
			[target performSelector:successHandler withObject:perser.responseStatus withObject:nil];
		}
		else if ([whichAPI isEqualToString:SAVE_NUTRITION_DAY_METHOD]) {
			
			NSError *parseError = nil;
			SaveNutritionDayData *perser = [[SaveNutritionDayData alloc] init];
			[perser parseXMLFileAtData:d2 parseError:&parseError];	
			
			[target performSelector:successHandler withObject:perser.responseStatus withObject:nil];
		}
        else if ([whichAPI isEqualToString:WELLNESSWALL_METHOD]) {
			
			NSError *parseError = nil;
			WellnessWallData *perser = [[WellnessWallData alloc] init];
			[perser parseXMLFileAtData:d2 parseError:&parseError];	
			
			[target performSelector:successHandler withObject:perser.responseStatus withObject:perser];
		}
	}
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    
    JLog(@"[error localizedDescription]: %@",[error localizedDescription]);
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sorry" message:[error localizedDescription] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
	[alert show];
	[alert release];
	//[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
	[target performSelector:failureHandler];
}


@end

/*
 
 <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tem="http://tempuri.org/">
 <soapenv:Header/>
 <soapenv:Body>
 <tem:GetTeamProgress>
 <!--Optional:-->
 <tem:memberID>8b5ed874-f43d-44f4-a8eb-2884f79e2acd</tem:memberID>
 <!--Optional:-->
 <tem:weekDate>12-18-2010</tem:weekDate>
 </tem:GetTeamProgress>
 </soapenv:Body>
 </soapenv:Envelope>
 
 
 <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tem="http://tempuri.org/">
 <soapenv:Header/>
 <soapenv:Body>
 <tem:GetMemberProgress>
 <!--Optional:-->
 <tem:memberID>8b5ed874-f43d-44f4-a8eb-2884f79e2acd</tem:memberID>
 <!--Optional:-->
 <tem:weekDate>12-18-2011</tem:weekDate>
 </tem:GetMemberProgress>
 </soapenv:Body>
 </soapenv:Envelope>
 
 
 */

/*
 
 <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tem="http://tempuri.org/" xmlns:nuv="http://schemas.datacontract.org/2004/07/NuvitaMobileService">
 <soapenv:Header/>
 <soapenv:Body>
 <tem:SaveCardioWorkoutHrString>
 <!--Optional:-->
 <tem:cardioWorkout>
 <!--Optional:-->
 <nuv:CardioLaps>
 <!--Zero or more repetitions:-->
 <nuv:CardioLapHrString>
 <!--Optional:-->
 <nuv:Calories>?</nuv:Calories>
 <!--Optional:-->
 <nuv:DistanceMeters>?</nuv:DistanceMeters>
 <!--Optional:-->
 <nuv:DurationSeconds>?</nuv:DurationSeconds>
 <!--Optional:-->
 <nuv:HeartRateSamples>?</nuv:HeartRateSamples>
 <!--Optional:-->
 <nuv:SourceEnum>?</nuv:SourceEnum>
 <!--Optional:-->
 <nuv:UTCDate>?</nuv:UTCDate>
 </nuv:CardioLapHrString>
 </nuv:CardioLaps>
 <!--Optional:-->
 <nuv:DeviceId>?</nuv:DeviceId>
 <!--Optional:-->
 <nuv:DeviceName>?</nuv:DeviceName>
 <!--Optional:-->
 <nuv:SportEnum>?</nuv:SportEnum>
 <!--Optional:-->
 <nuv:StartTimeUTC>?</nuv:StartTimeUTC>
 <!--Optional:-->
 <nuv:memberId>?</nuv:memberId>
 </tem:cardioWorkout>
 </tem:SaveCardioWorkoutHrString>
 </soapenv:Body>
 </soapenv:Envelope>
 
 */

