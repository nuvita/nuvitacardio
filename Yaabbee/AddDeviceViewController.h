//
//  AddDeviceViewController.h
//  NuvitaCardio
//
//  Created by John on 7/11/14.
//
//

#import <UIKit/UIKit.h>
#import <CoreBluetooth/CoreBluetooth.h>

#import "OfflineManager.h"
#import "CMonitor.h"
#import "LoginPerser.h"
#import "LoginResponseData.h"

@interface AddDeviceViewController : UIViewController

@end
