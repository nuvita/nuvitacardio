//
//  InputViewController.h
//  NuvitaCardio
//
//  Created by John on 2/27/14.
//
//

#import <UIKit/UIKit.h>

@protocol InputViewControllerDelegate;
@interface InputViewController : UIViewController

@property (retain, nonatomic) IBOutlet UITableView *tableview;
@property (strong, nonatomic) id <InputViewControllerDelegate> delegate;

@end

@protocol InputViewControllerDelegate <NSObject>
@optional

- (void)inputViewController:(InputViewController *)viewcontroller didEnterName:(NSString *)deviceName;

@end
