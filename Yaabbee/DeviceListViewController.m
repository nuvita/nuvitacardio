//
//  DeviceListViewController.m
//  NuvitaCardio
//
//  Created by John on 7/14/14.
//
//

#import "DeviceListViewController.h"

@interface DeviceListViewController ()

@property (retain, nonatomic) OfflineManager *offlineManager;

@end

@implementation DeviceListViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self initOfflineManager];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)initOfflineManager {
    
    NuvitaAppDelegate *appDelegate = (NuvitaAppDelegate *)[[UIApplication sharedApplication] delegate];
    _offlineManager = [[OfflineManager alloc] init];
    _offlineManager.managedObjectContext = appDelegate.managedObjectContext;
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_devices count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    NSString *serial = [AppData GetUUID:[[_devices objectAtIndex:indexPath.row] UUID]];
    cell.textLabel.text = [NSString stringWithFormat:@". . . . . .%@", [serial substringFromIndex:MAX((int)[serial length] - 6, 0)]];
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return @"Found Devices";
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:[tableView indexPathForSelectedRow] animated:YES];
    
    if (![_offlineManager checkForDuplicateMonitorSerialNo:[AppData GetUUID:[[_devices objectAtIndex:indexPath.row] UUID]]]) {
        [_delegate didChooseDevice:(CBPeripheral *)[_devices objectAtIndex:indexPath.row]];
        [self.navigationController popViewControllerAnimated:YES];
        return;
    }
    
    UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:@"Error"
                                                        message:@"Device already exist."
                                                       delegate:nil
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles:nil];
    [alertview show];
}

@end
