//
//  JFNavigationTransitionDelegate.h
//  NuvitaCardio
//
//  Created by John on 6/14/14.
//
//

#import <Foundation/Foundation.h>

@interface JFNavigationTransitionDelegate : NSObject <UINavigationControllerDelegate>

@property (nonatomic, strong) id <UIViewControllerAnimatedTransitioning> pushTransitioning;
@property (nonatomic, strong) id <UIViewControllerAnimatedTransitioning> popTransitioning;

@end
