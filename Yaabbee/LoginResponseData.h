//
//  claimActivityOBJ.h
//  Acclaris
//
//  Created by S Biswas on 09/11/10.
//  Copyright 2010 ObjectSol Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface LoginResponseData : NSObject {

	NSString *firstName;
	NSString *lastName;
	NSString *memberID;
	NSString *programName;
    NSString *avatar;
    NSString *weight;
    NSString*useCameraMeals;
}
@property(nonatomic,retain)NSString *firstName;
@property(nonatomic,retain)NSString *lastName;
@property(nonatomic,retain)NSString *memberID;
@property(nonatomic,retain)NSString *programName;
@property(nonatomic,retain)NSString *today;
@property(nonatomic,retain)NSString *avator;
@property(nonatomic,retain)NSString *weight;
@property(nonatomic,retain)NSString *useCameraMeals;

@end
