//
//  NuvitaAppDelegate.m
//
//
//  Created by Srabati on 7/22/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "NuvitaAppDelegate.h"
#import "NewLoginViewController.h"
#import "Reachability.h"

#import "StatViewController.h"
#import "GraphViewController.h"
#import "MapViewController.h"
#import "HistoryViewController.h"
#import "OptionsViewController.h"
#import "MoreViewController.h"
#import "InitialSlidingViewController.h"
#import "InitialTestViewController.h"

#import "OnlineManager.h"
#import "OfflineManager.h"

#import "CMonitor.h"

@interface NuvitaAppDelegate ()

@end

@implementation NuvitaAppDelegate

@synthesize isidlecheck;
@synthesize window = _window;
@synthesize isLoggedIn;

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

NetworkStatus    remoteHostStatus;
int l = 1;

//static NSString * const kRecipesStoreName = @"Nuvita.sqlite";

void reportAppCrash() {

    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *logPath = [documentsDirectory stringByAppendingPathComponent:@"console.log"];
    freopen([logPath cStringUsingEncoding:NSASCIIStringEncoding],"a+",stderr);

    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:@"YES" forKey:APP_CRASH];
    [defaults synchronize];
}

void HandleException(NSException *exception) {
    //Save somewhere that your app has crashed.
    reportAppCrash();
}

- (void)initAppUUID {

    NSString *UUID = [[NSUserDefaults standardUserDefaults] objectForKey:@"kApplicationUUIDKey"];
    if (!UUID) {
        CFUUIDRef uuid = CFUUIDCreate(NULL);
        UUID = (NSString *)CFBridgingRelease(CFUUIDCreateString(NULL, uuid));
        CFRelease(uuid);

        [[NSUserDefaults standardUserDefaults] setObject:UUID forKey:@"kApplicationUUIDKey"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

- (void)cleanIgnoredMonitor {

    if ([self.offlineManager respondsToSelector:@selector(cleanIgnoredMonitorsNow)]) {
        [self.offlineManager cleanIgnoredMonitorsNow];
    }
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    // attempt to extract a token from the url
    return [FBAppCall handleOpenURL:url
                  sourceApplication:sourceApplication
                        withSession:_fbSession];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // FBSample logic
    // if the app is going away, we close the session object
    [_fbSession close];
    [self saveContext];
}


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    // Add the tab bar controller's current view as a subview of the window
    self.isLoggedIn = FALSE;

    //...offline mode
    self.offlineManager = [[OfflineManager alloc] init];
    self.offlineManager.managedObjectContext = self.managedObjectContext;

    //...track if ever app crashes
//    NSSetUncaughtExceptionHandler(&HandleException);
    [CrashHandler setupLogging:YES];

    //...clean ignored transmitter
    [self cleanIgnoredMonitor];
    [self setupNetworkCheck];
    
    //sound
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    if(![prefs boolForKey:SOUND_KEY]) {
        [prefs setBool:YES forKey:SOUND_BELOW_KEY];
        [prefs setBool:YES forKey:SOUND_IN_KEY];
        [prefs setBool:YES forKey:SOUND_ABOVE_KEY];
        [prefs setBool:YES forKey:SOUND_KEY];
        [prefs setBool:YES forKey:SOUND_HRDISCONNECTED_KEY];

        [prefs synchronize];
    }

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(loadLoginView)
                                                 name:@"LOGOUT"
                                               object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didFinishInitialTest:)
                                                 name:@"FINISH_INITIAL_TEST"
                                               object:nil];

    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    NewLoginViewController *newLoginViewController = [[NewLoginViewController alloc] initWithNibName:@"NewLoginViewController" bundle:nil];

    self.forceNavigationController = [[ForceNavigationViewController alloc] initWithRootViewController:newLoginViewController];
    self.window.rootViewController = self.forceNavigationController;
    [self.window makeKeyAndVisible];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    /*
     Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
     Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
     */
}


- (void)applicationDidEnterBackground:(UIApplication *)application {

    //As we are going into the background, I want to start a background task to clean up the disk caches
    /*
     if ([[UIDevice currentDevice] respondsToSelector:@selector(isMultitaskingSupported)]) { //Check if our iOS version supports multitasking I.E iOS 4
     if ([[UIDevice currentDevice] isMultitaskingSupported]) { //Check if device supports mulitasking
     UIApplication *application = [UIApplication sharedApplication]; //Get the shared application instance

     __block UIBackgroundTaskIdentifier background_task; //Create a task object

     background_task = [application beginBackgroundTaskWithExpirationHandler: ^{
     [application endBackgroundTask:background_task]; //Tell the system that we are done with the tasks
     background_task = UIBackgroundTaskInvalid; //Set the task to be invalid
     //System will be shutting down the app at any point in time now
     }];

     //Background tasks require you to use asyncrous tasks
     dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
     //Perform your tasks that your application requires

     //I do what i need to do here.... synchronously...

     [application endBackgroundTask: background_task]; //End the task so the system knows that you are done with what you need to perform
     background_task = UIBackgroundTaskInvalid; //Invalidate the background_task
     });
     }
     }
     */
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    /*
     Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
     */
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    /*
     Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     */

//    [FBAppEvents activateApp];
    [FBAppCall handleDidBecomeActiveWithSession:_fbSession];
}

- (void)saveContext {
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

#pragma mark - Core Data stack

- (NSManagedObjectContext *)managedObjectContext {
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }

    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}

- (NSManagedObjectModel *)managedObjectModel {
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"Nuvita" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }

    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"Nuvita.sqlite"];

    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:
                             [NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption,
                             [NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption, nil];

    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:options error:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }

    return _persistentStoreCoordinator;
}

#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory {
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

#pragma mark - Application Settings

- (void)resetViewControllers {

    NSInteger count = [[self.forceTabBarViewController viewControllers] count];

    for(int index = 0; index < count; index ++)
        [self setTintColorForNavigationControllerWithIndex:index];
}

- (void)setTintColorForNavigationControllerWithIndex:(NSInteger)index{

    UINavigationController *navController = (UINavigationController *)[[self.forceTabBarViewController viewControllers] objectAtIndex:index];
    UIImageView *textView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"logo.png"]];
    [navController.navigationBar.topItem setTitleView:textView];
    [navController.navigationBar setTintColor:[UIColor colorWithRed:0.2 green:0.4 blue:0.7 alpha:1.0]];
}

- (void)reloadTabControllerAtSelectedIndex:(NSInteger)index {

    [[CheckUserHistory sharedAppData] setFromTest:NO];
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    UIViewController *viewController1 = [[InitialSlidingViewController alloc] initWithNibName:@"InitialSlidingViewController" bundle:nil];
    UIViewController *viewController2 = [[GraphViewController alloc] initWithNibName:@"GraphViewController" bundle:nil];
    UIViewController *viewController3 = [[MapViewController alloc] initWithNibName:@"MapViewController" bundle:nil];
    UIViewController *viewController4 = [[HistoryViewController alloc] initWithNibName:@"HistoryViewController" bundle:nil];
    UIViewController *viewController5 = [[MoreViewController alloc] initWithNibName:@"MoreViewController" bundle:nil];

    UINavigationController *navCon1 = [[UINavigationController alloc] initWithRootViewController:viewController1];
    UINavigationController *navCon2 = [[UINavigationController alloc] initWithRootViewController:viewController2];
    UINavigationController *navCon3 = [[UINavigationController alloc] initWithRootViewController:viewController3];
    UINavigationController *navCon4 = [[UINavigationController alloc] initWithRootViewController:viewController4];
    UINavigationController *navCon5 = [[UINavigationController alloc] initWithRootViewController:viewController5];

    self.forceTabBarViewController = [[ForceTabBarViewController alloc] init];
    self.forceTabBarViewController.viewControllers = @[navCon1, navCon2, navCon3, navCon4, navCon5];
    self.forceTabBarViewController.selectedIndex = index;
    self.window.rootViewController = self.forceTabBarViewController;

    [self resetViewControllers];
    [self.window makeKeyAndVisible];
}

#pragma mark - Reachibility

- (void)setupNetworkCheck {
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityChanged:) name:@"kNetworkReachabilityChangedNotification" object:nil];
	[[Reachability sharedReachability] setHostName:@"www.google.com"];
	[NuvitaAppDelegate updateStatus];
}

- (void)reachabilityChanged:(NSNotification*)notification {
	[NuvitaAppDelegate updateStatus];
}

+ (void)updateStatus {
	remoteHostStatus   = [[Reachability sharedReachability] remoteHostStatus];
}

+ (BOOL)isNetworkAvailable {
	[NuvitaAppDelegate updateStatus];
	if(remoteHostStatus == NotReachable)
		return NO;
	else
		return YES;
}

#pragma mark - <Checktouch>

- (void)Checktouch {
	if(isidlecheck==YES) {
		isidlecheck=NO;
	    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(Check) object: nil];
		l = 1;
	} else {
		if (l == 1) {
			[self performSelector:@selector(Check) withObject:nil afterDelay:300.0];
			l = 2;
		}
	}

	[self performSelector:@selector(Checktouch) withObject:nil afterDelay:1.0];
}

+ (BOOL)isPhone5 {
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone){
        if ([[UIScreen mainScreen] respondsToSelector: @selector(scale)]) {
            CGSize result = [[UIScreen mainScreen] bounds].size;
            CGFloat scale = [UIScreen mainScreen].scale;
            result = CGSizeMake(result.width * scale, result.height * scale);

            if(result.height == 1136) {
                return YES;
            }
        }
    }

    return NO;
}

+ (BOOL)isIOS7 {

    NSArray *version = [[UIDevice currentDevice].systemVersion componentsSeparatedByString:@"."];
    if ([[version objectAtIndex:0] intValue] < 7.0) {
        return NO;
    }

    return YES;
}

- (void)navigationController:(UINavigationController *)navigationController
      willShowViewController:(UIViewController *)viewController
                    animated:(BOOL)animated {

    if ([UIDevice currentDevice].orientation != UIInterfaceOrientationPortrait) {
        if (![viewController isKindOfClass:[GraphViewController class]]) {
            UIWindow *window = [[UIApplication sharedApplication] keyWindow];
            UIView *view = [window.subviews objectAtIndex:0];
            [view removeFromSuperview];
            [window insertSubview:view atIndex:0];
        }
    }
}

#pragma mark - Reload Orientation

- (void)reloadAppDelegateNavRootViewController {

    [[[UIApplication sharedApplication].delegate window] setRootViewController:nil];
    [(ForceNavigationViewController *)self.forceNavigationController setOrientation:UIInterfaceOrientationPortrait];
    [(ForceNavigationViewController *)self.forceNavigationController setSupportedInterfaceOrientatoin:UIInterfaceOrientationMaskPortrait];
    [[[UIApplication sharedApplication].delegate window] setRootViewController:self.forceNavigationController];
}

- (void)reloadAppDelegateNavRootViewControllerLandscape {

    [[[UIApplication sharedApplication].delegate window] setRootViewController:nil];
    [(ForceNavigationViewController *)self.forceNavigationController setOrientation:UIInterfaceOrientationLandscapeRight];
    [(ForceNavigationViewController *)self.forceNavigationController setSupportedInterfaceOrientatoin:UIInterfaceOrientationMaskLandscape];
    [[[UIApplication sharedApplication].delegate window] setRootViewController:self.forceNavigationController];
}

- (void)reloadAppDelegateRootViewController {

    [[[UIApplication sharedApplication].delegate window] setRootViewController:nil];
    [(ForceTabBarViewController *)self.forceTabBarViewController setOrientation:UIInterfaceOrientationPortrait];
    [(ForceTabBarViewController *)self.forceTabBarViewController setSupportedInterfaceOrientatoin:UIInterfaceOrientationMaskPortrait];
    [[[UIApplication sharedApplication].delegate window] setRootViewController:self.forceTabBarViewController];
}

- (void)reloadAppDelegateRootViewControllerLandscape {

    [[[UIApplication sharedApplication].delegate window] setRootViewController:nil];
    [(ForceTabBarViewController *)self.forceTabBarViewController setOrientation:UIInterfaceOrientationLandscapeRight];
    [(ForceTabBarViewController *)self.forceTabBarViewController setSupportedInterfaceOrientatoin:UIInterfaceOrientationMaskLandscape];
    [[[UIApplication sharedApplication].delegate window] setRootViewController:self.forceTabBarViewController];
}

#pragma mark - NewLoginViewControllerDelegate

- (void)loadLoginView {

    [[[UIApplication sharedApplication].delegate window] setRootViewController:nil];
    self.forceTabBarViewController.viewControllers = nil;

    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    NewLoginViewController *newLoginViewController = [[NewLoginViewController alloc] initWithNibName:@"NewLoginViewController" bundle:nil];

    self.forceNavigationController = [[ForceNavigationViewController alloc] initWithRootViewController:newLoginViewController];
    self.window.rootViewController = self.forceNavigationController;
    [self.window makeKeyAndVisible];
}

- (void)didFinishInitialTest:(NSNotification *)notification {

    [[[UIApplication sharedApplication].delegate window] setRootViewController:nil];
    self.forceNavigationController.viewControllers = nil;

    self.isLoggedIn = TRUE;
    NSInteger index = [notification.object integerValue];
    [self reloadTabControllerAtSelectedIndex:index];
}

@end
