//
//  StringFile.m
//  NuvitaCardio
//
//  Created by John on 2/27/14.
//
//

#import "StringFile.h"
#import "Logs.h"
#import "Logging.h"

#import "LoginResponseData.h"
#import "LoginPerser.h"

static StringFile *sharedAppData = nil;

@implementation StringFile

+ (StringFile *)sharedAppData {
    @synchronized(self) {
        if (sharedAppData == nil)
            sharedAppData = [[self alloc] init];
    }
    return sharedAppData;
}

- (NSString *)convertLogsToString:(NSArray *)logs {
    
    NSString *finalString = @"";
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MMM dd, yyyy HH:mm a"];
    
    LoginResponseData *loginResponseData = [LoginPerser getLoginResponse];
    finalString = [finalString stringByAppendingFormat:@"* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\n"];
    finalString = [finalString stringByAppendingFormat:@"Name: %@ %@",[loginResponseData.firstName capitalizedString], [loginResponseData.lastName capitalizedString]];
    finalString = [finalString stringByAppendingFormat:@"\nEmail: %@",[[NSUserDefaults standardUserDefaults] valueForKey:@"USER_ID"]];
    finalString = [finalString stringByAppendingFormat:@"\n* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *"];
    
    for (int i = 0; i < [logs count] ; i++) {
        
        Logs *log = [logs objectAtIndex:i];
        Logging *logging = [NSKeyedUnarchiver unarchiveObjectWithData:log.logging];
        
        NSDictionary *data = [logging requestData];
        NSString *requestString = @"";
        requestString = [requestString stringByAppendingString:[NSString stringWithFormat:@"\n\nAction: %@", [data valueForKey:@"method"]]];
        requestString = [requestString stringByAppendingString:[NSString stringWithFormat:@"\nDate: %@", [formatter stringFromDate:[data valueForKey:@"dateTime"]]]];
        requestString = [requestString stringByAppendingString:[NSString stringWithFormat:@"\nRequest made: %@", [data valueForKey:@"action"]]];
        requestString = [requestString stringByAppendingString:[NSString stringWithFormat:@"\nAPI: %@", [data valueForKey:@"api"]]];
        requestString = [requestString stringByAppendingString:[NSString stringWithFormat:@"\nConnection status code: %@", [data valueForKey:@"statusCodex"]]];
        
        requestString = [requestString stringByAppendingString:[NSString stringWithFormat:@"\n\n\nRequest parameters: \n%@", [data valueForKey:@"request"]]];
        requestString = [requestString stringByAppendingString:[NSString stringWithFormat:@"\n\n\nRequest response: \n%@", [data valueForKey:@"response"]]];
        
        finalString = [finalString stringByAppendingString:[NSString stringWithFormat:@"%@%@",(i == 0) ? @"\n" : @"\n\n\n* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\n\n",requestString]];
    }
    
    return finalString;
}

- (BOOL)saveFile:(NSString *)str {
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents directory
    
    NSError *error;
    BOOL succeed = [str writeToFile:[documentsDirectory stringByAppendingPathComponent:@"Logs.txt"]
                         atomically:YES
                           encoding:NSUTF8StringEncoding
                              error:&error];

    return succeed;
}

- (BOOL)saveFile:(NSString *)str withFilename:(NSString *)filename {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents directory
    
    LoginResponseData *loginResponseData = [LoginPerser getLoginResponse];
    NSString *finalString = @"";
    finalString = [finalString stringByAppendingFormat:@"* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\n"];
    finalString = [finalString stringByAppendingFormat:@"Name: %@ %@",[loginResponseData.firstName capitalizedString], [loginResponseData.lastName capitalizedString]];
    finalString = [finalString stringByAppendingFormat:@"\nEmail: %@",[[NSUserDefaults standardUserDefaults] valueForKey:@"USER_ID"]];
    finalString = [finalString stringByAppendingFormat:@"\n* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *"];
    finalString = [finalString stringByAppendingFormat:@"\n\n%@",str];
    
    NSError *error;
    BOOL succeed = [finalString writeToFile:[documentsDirectory stringByAppendingPathComponent:filename]
                         atomically:YES
                           encoding:NSUTF8StringEncoding
                              error:&error];
    
    return succeed;
}

@end
