//
//  AppData.m
//  NuvitaCardio
//
//  Created by S Biswas on 07/04/13.
//
//

#import "AppData.h"

#define MYNUVITA_CARDIO_APP_KEY @"Mynuvita_Cardio_App_Key" 
#define MYNUVITA_CARDIO_APP_SETTING_KEY @"Mynuvita_Cardio_App_Setting_Key"



static AppData *sharedAppData = nil;

@implementation AppData

@synthesize arrSessionData;
@synthesize cardioLap;
@synthesize inzoneHighLimit;
@synthesize inzoneLowLimit;
@synthesize dictOptions;

@synthesize isInSession;
@synthesize startSessionTime;
@synthesize startLapTime;
@synthesize pauseSessionTime;
@synthesize serialNumber;
@synthesize arrRequest;

- (id)init
{
    self = [super init];
    if (self) {
        [self loadData];
        //[self loadSettingsData];
    }
    return self;
}

- (void)loadData
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    // getting an NSString
    self.arrSessionData = [[prefs arrayForKey:MYNUVITA_CARDIO_APP_KEY] mutableCopy];
    
    if (!self.arrSessionData) {
        self.arrSessionData = [[NSMutableArray alloc] init];
    }
    
    if (!self.arrRequest) {
        self.arrRequest = [[NSMutableArray alloc] init];
    }
}

- (void)saveData {
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    // getting an NSString
    [prefs setObject:self.arrSessionData forKey:MYNUVITA_CARDIO_APP_KEY];
    
    [prefs synchronize];
}


//
- (NSInteger)getSessionTime
{
    return [[NSDate date] timeIntervalSince1970] - self.startSessionTime;
}

/*
- (void)loadSettingsData
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    // getting an NSString
    self.dictOptions = [[prefs arrayForKey:MYNUVITA_CARDIO_APP_SETTING_KEY] mutableCopy];
    
    if (self.dictOptions == nil) {
        self.dictOptions = [[NSMutableDictionary alloc] init];
        [self.dictOptions setValue:[NSNumber numberWithBool:YES] forKey:APP_SETTING_BELOW_ZONE_KEY];
        [self.dictOptions setValue:[NSNumber numberWithBool:YES] forKey:APP_SETTING_IN_ZONE_KEY];
        [self.dictOptions setValue:[NSNumber numberWithBool:YES] forKey:APP_SETTING_ABOVE_ZONE_KEY];
        [self.dictOptions setValue:[NSNumber numberWithBool:NO] forKey:APP_SETTING_HBT_KEY];
    }
}

- (void)saveSettingsData
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    // getting an NSString
    [prefs setObject:self.dictOptions forKey:MYNUVITA_CARDIO_APP_SETTING_KEY];
    
    [prefs synchronize];
}
*/

+ (AppData *)sharedAppData {
    @synchronized(self) {
        if (sharedAppData == nil)
            sharedAppData = [[self alloc] init];
    }
    return sharedAppData;
}

- (CardioLap *)getCardioLap:(NSInteger)index
{
    if(index < [self.arrSessionData  count])
    {
        CardioLap *obj = [[[CardioLap alloc] init] autorelease];
        
        [obj loadLapData:[self.arrSessionData objectAtIndex:index]];
        
        return obj;
    }
    
    return nil;
}

- (void)delCardioLap:(NSInteger)index
{
    if(index < [self.arrSessionData  count])
    {
        [self.arrSessionData removeObjectAtIndex:index];
        
        [self saveData];
    }
    
    return;
}

+ (NSString *)uuid
{
    NSString *uuidString = nil;
    CFUUIDRef uuid = CFUUIDCreate(NULL);
    if (uuid) {
        uuidString = (NSString *)CFUUIDCreateString(NULL, uuid);
        CFRelease(uuid);
    }
    return [uuidString autorelease];
}


- (void)dealloc
{
    self.arrSessionData = nil;
    self.cardioLap = nil;
    
    [super dealloc];
}

+ (CGFloat)calculateDistance:(NSMutableArray *)arr
{
    CGFloat distance = 0;
    for(int idx = 0; arr != nil && idx < [arr count] - 1; ++idx) {
        //CLLocationCoordinate2D firstCoord = [arr[idx] coordinate];
        //CLLocationCoordinate2D secondCoord = [arr[idx + 1] coordinate];
        
        CLLocation *point1  = (CLLocation *)[arr objectAtIndex:idx];
        CLLocation *point2  = (CLLocation *)[arr objectAtIndex:(idx + 1)];
                
        distance += [point1 distanceFromLocation:point2]*0.000621371;
        
//        CLLocation* first = [[CLLocation alloc] initWithLatitude:firstCoord.latitude longitude:secondCoord.latitude];
//        CLLocation* second = [[CLLocation alloc] initWithLatitude:secondCoord.latitude longitude:secondCoord.longitude];
        
        //distance += [first distanceFromLocation:second]*0.000621371;
        
    }
    
    return distance;
}

+ (NSString *)GetUUID:(CFUUIDRef)theUUID {
    CFStringRef string = CFUUIDCreateString(NULL, theUUID);
    return ( NSString *)string;
}

+ (NSString *)GetDeviceName {
    return [[UIDevice currentDevice] name];
}

@end

/*
 
 NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
 
 // saving an NSString
 [prefs setObject:@"TextToSave" forKey:@"keyToLookupString"];
 
 // saving an NSInteger
 [prefs setInteger:42 forKey:@"integerKey"];
 
 // saving a Double
 [prefs setDouble:3.1415 forKey:@"doubleKey"];
 
 // saving a Float
 [prefs setFloat:1.2345678 forKey:@"floatKey"];
 
 // This is suggested to synch prefs, but is not needed (I didn't put it in my tut)
 [prefs synchronize];
 
 Retrieving
 
 NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
 
 // getting an NSString
 NSString *myString = [prefs stringForKey:@"keyToLookupString"];
 
 // getting an NSInteger
 NSInteger myInt = [prefs integerForKey:@"integerKey"];
 
 // getting an Float
 float myFloat = [prefs floatForKey:@"floatKey"];
 

 
 */
