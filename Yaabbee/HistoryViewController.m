//
//  HistoryViewController.m
//  NuvitaCardio
//
//  Created by S Biswas on 08/04/13.
//
//

#import "HistoryViewController.h"

#import "GraphViewController.h"
#import "MapViewController.h"

#import "OfflineManager.h"
#import "OnlineManager.h"
#import "Session.h"
#import "Authorize.h"

@interface HistoryViewController () <OnlineManagerDelegate>

@property (retain, nonatomic) OfflineManager *offlineManager;
@property (retain, nonatomic) NSMutableArray *sessions;
@property (retain, nonatomic) NSIndexPath *accessoryIndexPath;
@property (retain, nonatomic) NSIndexPath *editIndexPath;
@property (retain, nonatomic) OnlineManager *onlineManager;

@property (strong, nonatomic) JBLoadingView *loadingView;

@end

@implementation HistoryViewController


#pragma mark - Public Method

- (void)initOfflineManager {
    
    NuvitaAppDelegate *appDelegate = (NuvitaAppDelegate *)[[UIApplication sharedApplication] delegate];
    self.offlineManager = [[OfflineManager alloc] init];
    self.offlineManager.managedObjectContext = appDelegate.managedObjectContext;
    
    _onlineManager = [[OnlineManager alloc] init];
    _onlineManager.delegate = self;
}

- (NSInteger)daysBetweenDate:(NSDate *)fromDateTime andDate:(NSDate *)toDateTime {
    NSDate *fromDate;
    NSDate *toDate;
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    [calendar rangeOfUnit:NSDayCalendarUnit startDate:&fromDate
                 interval:NULL forDate:fromDateTime];
    [calendar rangeOfUnit:NSDayCalendarUnit startDate:&toDate
                 interval:NULL forDate:toDateTime];
    NSDateComponents *difference = [calendar components:NSDayCalendarUnit
                                               fromDate:fromDate toDate:toDate options:0];
    
    return [difference day];
}

- (void)deleteExpireSession {
    NSArray *sessions = [_offlineManager getAllSession];
    for (Session *session in sessions) {
        if ([self daysBetweenDate:[session dateModified] andDate:[NSDate date]] >= 7) {
            [_offlineManager deleteSession:session];
        }
    }
}

#pragma mark - View Life Cycle

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    if (self) {
        self.tabBarItem.title = @"History";

        UIImage *normalImg = [UIImage imageNamed:@"history.png"];
        UIImage *selectedImg = [UIImage imageNamed:@"history_select.png"];
        
        [self.tabBarItem setFinishedSelectedImage:selectedImg withFinishedUnselectedImage:normalImg];
        //[UIImage imageNamed:@"option.png"];
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

    _loadingView = [[JBLoadingView alloc] init];
    
    //bar item
   // UIBarButtonItem *btn = [[UIBarButtonItem alloc] initWithTitle:@"Edit" style:UIBarButtonItemStylePlain target:self action:@selector(btnEditClicked:)];
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 51, 32)];
    [button addTarget:self action:@selector(btnEditClicked:) forControlEvents:UIControlEventTouchUpInside];
    [button setImage:[UIImage imageNamed:@"btn_edit.png"] forState:UIControlStateNormal];
    button.tag = 99;
    
    UIView *rightButtonView = [[UIView alloc] initWithFrame:button.frame];
    rightButtonView.bounds = CGRectOffset(rightButtonView.bounds, RDX, 0);
    [rightButtonView addSubview:button];
    
    UIBarButtonItem *btn = [[UIBarButtonItem alloc] initWithCustomView:rightButtonView];
    [self.navigationItem setRightBarButtonItem:btn];
    
    //...initialize offline mode
    _sessions = [[NSMutableArray alloc] init];
    [self initOfflineManager];
}

- (void)buildiPhone4UI {
    
    CGRect frame;
    frame = self.headerview.frame;
    frame.origin.y = 0;
    
    self.headerview.frame = frame;
    
    float tableY = frame.origin.y + frame.size.height;
    frame = self.tableViewHistory.frame;
    frame.origin.y = tableY;
    frame.size.height = ([NuvitaAppDelegate isPhone5]) ? 428 : 340;
    
    self.tableViewHistory.frame = frame;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    self.tableViewHistory.userInteractionEnabled = [[AppData sharedAppData] isInSession] == NO;
    
    if (![NuvitaAppDelegate isIOS7])
        [self buildiPhone4UI];

    if ([[[AppData sharedAppData] arrSessionData] count] > 0) {
        [self migrateToNewDatabase];
    } else {
        [self deleteExpireSession];
        [self loadData];
    }
}

- (void)migrateToNewDatabase {
    
    NSArray *appSessions = [[NSArray alloc] initWithArray:[[AppData sharedAppData] arrSessionData]];
    for (NSDictionary *cardioLapData in appSessions) {
        
        CardioLap *cardioLap = [[CardioLap alloc] init];
        [cardioLap loadLapData:cardioLapData];
        NSDictionary *data = [NSDictionary dictionaryWithObjectsAndKeys:
                              [[AppData sharedAppData] authorizeUser], @"authorize",
                              cardioLapData, @"cardiolap",
                              nil];
        if ([_offlineManager addNewSessionFromAppData:data withStartLapTime:[cardioLap startLapTime]]) {
            //....
            NSLog(@"migrated!!!");
        }
    }
    
    [[[AppData sharedAppData] arrSessionData] removeAllObjects];
    [[AppData sharedAppData] saveData];
    [self loadData];
}

- (void)loadData {
    [_sessions removeAllObjects];
    [_sessions addObjectsFromArray:[_offlineManager getAllSession]];

    [self.tableViewHistory reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)btnEditClicked:(id)sender {
    UIButton *btn = (UIButton *)sender;
    
    self.tableViewHistory.editing = btn.tag == 99;
    
    NSString *imgName = @"btn_edit.png";
    
    if(btn.tag == 99)
    {
        imgName = @"btn_done.png";
        btn.tag = 100;
    }
    else
    {
        btn.tag = 99;
    }
    
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 63, 32)];
    [button addTarget:self action:@selector(btnEditClicked:) forControlEvents:UIControlEventTouchUpInside];
    button.tag = btn.tag;
    [button setImage:[UIImage imageNamed:imgName] forState:UIControlStateNormal];
    
    UIView *rightButtonView = [[UIView alloc] initWithFrame:button.frame];
    rightButtonView.bounds = CGRectOffset(rightButtonView.bounds, RDX, 0);
    [rightButtonView addSubview:button];
    
    UIBarButtonItem *btnRight = [[UIBarButtonItem alloc] initWithCustomView:rightButtonView];
    [self.navigationItem setRightBarButtonItem:btnRight];

}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_sessions count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"HistoryCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
//    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
//    }
    
    CardioLap *cardioLap = [[CardioLap alloc] init];
    [cardioLap loadLapData:(NSDictionary *)[[_sessions objectAtIndex:indexPath.row] cardioLap]];
    if ([indexPath row] == 0){
        cell.userInteractionEnabled = ([[[[AppData sharedAppData] cardioLap] startLapTime] isEqualToString:[cardioLap startLapTime]]) ? NO : YES;
    }
    
    cell.textLabel.font = [UIFont fontWithName:DEFAULT_NORMAL_FONT_NAME size:headerFontSize];;
    cell.textLabel.backgroundColor = [UIColor clearColor];
    cell.textLabel.text = [cardioLap getStartSessionTimeText];

    cell.detailTextLabel.font = [UIFont fontWithName:DEFAULT_NORMAL_FONT_NAME size:headerFontSize];;
    cell.detailTextLabel.backgroundColor = [UIColor clearColor];
    cell.detailTextLabel.text = [cardioLap workoutTime];

    cell.accessoryType = ([[[_sessions objectAtIndex:indexPath.row] isSync] boolValue]) ? UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryDetailButton;
    return cell;
    
}

- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath {

    _accessoryIndexPath = indexPath;
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Choose the following option"
                                                              delegate:self
                                                     cancelButtonTitle:@"Cancel"
                                                destructiveButtonTitle:@"Save Session"
                                                     otherButtonTitles:@"Discard Session", nil];
    
    [actionSheet setActionSheetStyle:UIActionSheetStyleBlackTranslucent];
    
    actionSheet.tag = 11111;
	[actionSheet showInView:self.tabBarController.view];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self openSession];
}


- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    _editIndexPath = indexPath;
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [self delExcersiseSession:indexPath.row];
        
    }
    
}

- (void)openSession {
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Choose the following option"
                                                              delegate:self
                                                     cancelButtonTitle:@"Cancel"
                                                destructiveButtonTitle:nil
                                                     otherButtonTitles:@"Session Graph", @"Session Map", nil];
    
    [actionSheet setActionSheetStyle:UIActionSheetStyleBlackTranslucent];
    
    actionSheet.tag = 99999;
	[actionSheet showInView:self.tabBarController.view];
    
}

- (void)delExcersiseSession:(NSInteger)index {
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Are you sure you want to delete this session?"
                                                              delegate:self
                                                     cancelButtonTitle:@"No"
                                                destructiveButtonTitle:@"Yes"
                                                     otherButtonTitles:nil];
    
    actionSheet.tag = 100;
    [actionSheet setActionSheetStyle:UIActionSheetStyleBlackTranslucent];
	[actionSheet showInView:self.tabBarController.view];
}

- (void)unsavedSessionAction:(NSInteger)index {

    if (index == 0) {
        
        //...sync session
        CardioLap *cardioLap = [[CardioLap alloc] init];
        [cardioLap loadLapData:(NSDictionary *)[[_sessions objectAtIndex:_accessoryIndexPath.row] cardioLap]];
        
        Session *session = [_offlineManager getSessionWithStartLapTimex:cardioLap.startLapTime];
    
        if ([_onlineManager respondsToSelector:@selector(syncSession:withCardioLap:)]) {
            [_loadingView loadingViewStartAnimating:[self view]
                                    withLoadingView:[_loadingView createLoadingView:[self view] lightEffect:YES]
                                               text:@"Saving cardio session. Please wait..."];
            [_onlineManager syncSession:session withCardioLap:cardioLap];
        }
        
    } else if (index == 1) {
 
        //...delete session locally and from arrsession
        UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:nil
                                                            message:@"Are you sure you want to delete this session?"
                                                           delegate:self
                                                  cancelButtonTitle:@"No"
                                                  otherButtonTitles:@"Yes", nil];
        [alertview show];
        
    }
}

#pragma mark - OnlineManagerDelegate

- (void)onlineManager:(OnlineManager *)manager isSuccess:(BOOL)success {
    [_loadingView loadingViewStopAnimating:[self view]];
    [self loadData];
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (buttonIndex == 1) {

        //...delete from core data
        CardioLap *cardioLap = [[CardioLap alloc] init];
        [cardioLap loadLapData:(NSDictionary *)[[_sessions objectAtIndex:_accessoryIndexPath.row] cardioLap]];
        [_offlineManager deleteSessionWithStartLaptime:cardioLap.startLapTime];
        
        
        [self performSelector:@selector(loadData) withObject:nil afterDelay:1.0];
    }
    
    [_tableViewHistory deselectRowAtIndexPath:[_tableViewHistory indexPathForSelectedRow] animated:YES];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (actionSheet.tag == 99999) {
        
        NSIndexPath *indexPath = [_tableViewHistory indexPathForSelectedRow];
    
        if (buttonIndex == 0) {
            
            CardioLap *cardioLap = [[CardioLap alloc] init];
            [cardioLap loadLapData:(NSDictionary *)[[_sessions objectAtIndex:indexPath.row] cardioLap]];
            
            GraphViewController *vc = [[GraphViewController alloc] initWithNibName:@"GraphViewController" bundle:nil val:cardioLap];
            [self.navigationController pushViewController:vc animated:YES];
            
        } else if(buttonIndex == 1) {
            
            CardioLap *cardioLap = [[CardioLap alloc] init];
            [cardioLap loadLapData:(NSDictionary *)[[_sessions objectAtIndex:indexPath.row] cardioLap]];
            
            MapViewController *vc = [[MapViewController alloc] initWithNibName:@"MapViewController" bundle:nil val:cardioLap];
            [self.navigationController pushViewController:vc animated:YES];
            
        } else if(buttonIndex == 2) {
            
            [actionSheet dismissWithClickedButtonIndex:0 animated:YES];
        }
        
        [_tableViewHistory deselectRowAtIndexPath:[_tableViewHistory indexPathForSelectedRow] animated:YES];
        
    } else if (actionSheet.tag == 11111){
        
        [self unsavedSessionAction:buttonIndex];
        
    } else if (actionSheet.tag == 100) {
        
        if (buttonIndex == 0) {
            
            CardioLap *cardioLap = [[CardioLap alloc] init];
            [cardioLap loadLapData:(NSDictionary *)[[_sessions objectAtIndex:_editIndexPath.row] cardioLap]];
            [_offlineManager deleteSessionWithStartLaptime:cardioLap.startLapTime];
            
            [self loadData];
            
        } else if(buttonIndex == 1) {
            
            [actionSheet dismissWithClickedButtonIndex:0 animated:YES];
        }
    }
    
}


@end
