//
//  CardioLap.m
//  NuvitaCardio
//
//  Created by S Biswas on 07/04/13.
//
//

#import "CardioLap.h"

#define START_LAP_TIME @"startLapTime"
#define HEART_RATE_SAMPLE @"reartRateSamples"
#define SOURCE_ENUM @"sourceEnum"

#define DURATION_SECONDS @"durationSeconds"
#define DISTANCE_METERS @"distanceMeters"
#define CALORIES @"calories"

#define GPOS_POS @"arrGpsPos"

#define ZONE_BELOW @"zoneBelow"
#define ZONE_IN @"zoneIn"
#define ZONE_ABOVE @"zoneAbove"

#define APP_DEVICE_ID @"Nuvita iPhone App V2.0"


@implementation CardioLap

@synthesize startLapTime;
@synthesize heartRateSamples;
@synthesize sourceEnum;

@synthesize durationSeconds;
@synthesize distanceMeters; //0 if unknown
@synthesize calories; //0 if unknown

@synthesize arrGpsPos;

@synthesize zoneBelow;
@synthesize zoneIn;
@synthesize zoneAbove;
@synthesize rhRate;

//- (id)initWithCoder:(NSCoder *)decoder {
//    self = [super init];
//    if (self) {
//        //...
////        self = [decoder decodeObjectForKey:@"cardiolap"];
//        self.startLapTime = [decoder decodeObjectForKey:@"startLapTime"];
//        self.heartRateSamples = [decoder decodeObjectForKey:@"heartRateSamples"];
//        self.sourceEnum = [[decoder decodeObjectForKey:@"sourceEnum"] integerValue];
//        self.durationSeconds = [[decoder decodeObjectForKey:@"durationSeconds"] integerValue];
//        self.distanceMeters = [[decoder decodeObjectForKey:@"distanceMeters"] floatValue];
//        self.calories = [[decoder decodeObjectForKey:@"calories"] floatValue];
//        self.arrGpsPos = [decoder decodeObjectForKey:@"arrGpsPos"];
//        self.zoneBelow = [[decoder decodeObjectForKey:@"zoneBelow"] integerValue];
//        self.zoneIn = [[decoder decodeObjectForKey:@"zoneIn"] integerValue];
//        self.zoneAbove = [[decoder decodeObjectForKey:@"zoneAbove"] integerValue];
//        self.rhRate = [[decoder decodeObjectForKey:@"rhRate"] integerValue];
//    }
//    return self;
//}
//
//- (void)encodeWithCoder:(NSCoder *)encoder {
//    [encoder encodeObject:startLapTime forKey:@"startLapTime"];
//    [encoder encodeObject:heartRateSamples forKey:@"heartRateSamples"];
//    [encoder encodeObject:[NSNumber numberWithInteger:sourceEnum] forKey:@"sourceEnum"];
//    [encoder encodeObject:[NSNumber numberWithInteger:durationSeconds] forKey:@"durationSeconds"];
//    [encoder encodeObject:[NSNumber numberWithFloat:distanceMeters] forKey:@"distanceMeters"];
//    [encoder encodeObject:[NSNumber numberWithFloat:calories] forKey:@"calories"];
//    [encoder encodeObject:arrGpsPos forKey:@"arrGpsPos"];
//    [encoder encodeObject:[NSNumber numberWithInteger:zoneBelow] forKey:@"zoneBelow"];
//    [encoder encodeObject:[NSNumber numberWithInteger:zoneIn] forKey:@"zoneIn"];
//    [encoder encodeObject:[NSNumber numberWithInteger:zoneAbove] forKey:@"zoneAbove"];
//    [encoder encodeObject:[NSNumber numberWithInteger:rhRate] forKey:@"rhRate"];
//}

- (id)init
{
    self = [super init];
    if (self) {
        [self defVal];
    }
    return self;
}


- (void)defVal
{
    self.startLapTime = @"";//[NSDate date];
    self.heartRateSamples = [[NSMutableDictionary alloc] init];
    self.sourceEnum = 1;//allawys
    
    self.durationSeconds = 0;
    self.distanceMeters = 0; //0 if unknown
    self.calories = 0; //0 if unknown
    self.arrGpsPos = [[NSMutableArray alloc] init];

    self.zoneBelow = 0;
    self.zoneIn = 0;
    self.zoneAbove = 0;
}

- (void)updateHRM {

}

- (void)addHRM:(NSInteger)hrmValue {
    
//    hrmValue = 0;//arc4random() % 2;
//    NSLog(@"hrm: %d", hrmValue);
    
    NSInteger sessionTime = [[AppData sharedAppData] getSessionTime];
    NSInteger numOfHrm = (sessionTime / 5);
    NSString *key = [NSString stringWithFormat:@"%d", numOfHrm];
    
    if([[self.heartRateSamples allKeys] count] == 0) {
        [self.heartRateSamples setValue:[NSNumber numberWithInteger:hrmValue] forKey:@"0"];
        return;
    }
    
    _btConnectionLost = NO;
    if(![self.heartRateSamples valueForKey:key]) {

        if (hrmValue == 0) {
            //...hr prompt bt connection lost
            _hrZeroDropCount += 1;
            if (_hrZeroDropCount >= 7) {
                //.. prompt lost connection
                _hrZeroDropCount = 0;
                _btConnectionLost = YES;
                [self.heartRateSamples setValue:[NSNumber numberWithInteger:hrmValue] forKey:key];
                return;
            }
         
            hrmValue = [[self.heartRateSamples valueForKey:[NSString stringWithFormat:@"%d", numOfHrm - 1]] integerValue];
            [self.heartRateSamples setValue:[NSNumber numberWithInt:hrmValue] forKey:key];
            
        } else {
            
            _hrZeroDropCount = 0;
            [self.heartRateSamples setValue:[NSNumber numberWithInteger:hrmValue] forKey:key];
        }
    }
    
//    NSLog(@"samples: %@", self.heartRateSamples);
}

- (void)loadLapData:(NSDictionary *)dict
{
    self.startLapTime = [dict objectForKey:START_LAP_TIME];
    self.heartRateSamples = [dict objectForKey:HEART_RATE_SAMPLE];
 
    self.sourceEnum = [[dict objectForKey:SOURCE_ENUM] integerValue];
    self.durationSeconds = [[dict objectForKey:DURATION_SECONDS] integerValue];

    self.distanceMeters = [[dict objectForKey:DISTANCE_METERS] floatValue];
    self.calories = [[dict objectForKey:CALORIES] floatValue];
    self.arrGpsPos = [self loadGpsData:[dict objectForKey:GPOS_POS]];
    
    self.zoneBelow = [[dict objectForKey:ZONE_BELOW] floatValue];
    self.zoneIn = [[dict objectForKey:ZONE_IN] floatValue];
    self.zoneAbove = [[dict objectForKey:ZONE_ABOVE] floatValue];
}

- (NSDictionary *)getLapData
{
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];// autorelease];
    
    [dict setObject:self.startLapTime forKey:START_LAP_TIME];
    [dict setObject:self.heartRateSamples forKey:HEART_RATE_SAMPLE];
    
    [dict setObject:[NSNumber numberWithInt:self.sourceEnum] forKey:SOURCE_ENUM];


    [dict setObject:[NSNumber numberWithInt:self.durationSeconds] forKey:DURATION_SECONDS];
    
    NSNumber *number = [NSNumber numberWithFloat:self.distanceMeters];
    [dict setObject:number forKey:DISTANCE_METERS];
    
    number = [NSNumber numberWithFloat:self.calories];
    [dict setObject:number forKey:CALORIES];

    [dict setObject:[self getGpsData] forKey:GPOS_POS];

    //number = [NSNumber numberWithFloat:self.zoneBelow];
    [dict setObject:[NSNumber numberWithInt:self.zoneBelow] forKey:ZONE_BELOW];
    
    //number = [NSNumber numberWithFloat:self.zoneIn];
    [dict setObject:[NSNumber numberWithInt:self.zoneIn] forKey:ZONE_IN];
    
    //number = [NSNumber numberWithFloat:self.zoneAbove];
    [dict setObject:[NSNumber numberWithInt:self.zoneAbove] forKey:ZONE_ABOVE];

    
    return dict;
}

- (NSMutableArray *)getGpsData
{
    NSMutableArray *arr = [[NSMutableArray alloc] init];// autorelease];
    
    for (CLLocation *loc in self.arrGpsPos) {
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];// autorelease];

        [dict setValue:[NSNumber numberWithDouble: loc.coordinate.latitude] forKey:@"GPS_LAT"];
        [dict setValue:[NSNumber numberWithDouble: loc.coordinate.longitude] forKey:@"GPS_LONG"];
        [arr addObject:dict];
    }
    
    return arr;
}

- (NSMutableArray *)loadGpsData:(NSMutableArray *)arrData
{
    NSMutableArray *arr = [[NSMutableArray alloc] init];// autorelease];
    
    for (NSMutableDictionary *loc in arrData) {
        
        CLLocation *location = [[CLLocation alloc] initWithLatitude:[[loc objectForKey:@"GPS_LAT"] doubleValue]
                                                          longitude:[[loc objectForKey:@"GPS_LONG"] doubleValue]];// autorelease];

        
        [arr addObject:location];
    }
    
    return arr;
}

//0.1416*(105) - 8.303= 6.656Cal/min. My total Calories for the workout is now 6.656*76.5 = 502Calories.
- (NSString *)getCaloriesText
{
    //    return [NSString stringWithFormat:@"%02i:%02i:%02i", hours, minutes, seconds];
    if([self getAvgHrm] < 60)
        return @"0";
    //return @"23";
    float calV =  (0.1416*([self getAvgHrm]) - 8.303)*self.durationSeconds/60;
    
    return [NSString stringWithFormat:@"%d", (int)calV];//@"%.1f"
}

- (NSString *)getCaloriesNumericText
{
    //    return [NSString stringWithFormat:@"%02i:%02i:%02i", hours, minutes, seconds];
    if([self getAvgHrm] < 60)
        return @"0";
    //return @"23";
    int calV =  (0.1416*([self getAvgHrm]) - 8.303)*self.durationSeconds/60;
    
    return [NSString stringWithFormat:@"%d", calV];
}

- (NSString *)getBelowZoneText
{
    return [self getFormatedFloatVal:self.zoneBelow];
}

- (NSString *)getInZoneText
{
    return [self getFormatedFloatVal:self.zoneIn];
}

- (NSString *)getAboveZoneText
{
    return [self getFormatedFloatVal:self.zoneAbove];
}

- (NSString *)getInAboveZoneText
{
    return [self getFormatedFloatVal:(self.zoneIn + self.zoneAbove)];
}

- (NSString *)workoutTime
{
    return [self getFormatedFloatVal: self.durationSeconds];
}

- (NSString *)getFormatedFloatVal:(NSInteger)val
{
    NSInteger seconds = val % 60;
    NSInteger minutes = (val / 60) % 60;
    NSInteger hours = (val / 3600);
    if(hours > 0)
        return [NSString stringWithFormat:@"%i:%02i:%02i", hours, minutes, seconds];
    else
        return [NSString stringWithFormat:@"%02i:%02i", minutes, seconds];
}

//

- (NSString *)getStartSessionTimeText
{
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];// autorelease];
    //
    //    //The Z at the end of your string represents Zulu which is UTC
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
    //[dateFormatter setDateFormat:@"MM-dd-yyyy"];

    NSDate* newTime = [dateFormatter dateFromString:self.startLapTime];
    
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];// autorelease];

    [df setDateFormat:@"MM-dd-yyyy HH:mm a"];
    
    return [df stringFromDate:newTime];
}

+ (NSString*) getStartUTCDate
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];// autorelease];
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
    [dateFormatter setTimeZone:timeZone];
    //2013-08-04T05:57:27Z
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];//yyyy-MM-dd'T'HH:mm:ss'Z'
    NSString *dateString = [dateFormatter stringFromDate:[NSDate date]];
    
    return dateString;
}

- (float)getAvgHrm
{
    NSInteger val = 0;
    if([[self.heartRateSamples allValues]count] == 0) return 0;
    
    int count = 0;
    
    for (id hr in [self.heartRateSamples allValues]) {
        if([hr integerValue] != 0)
        {
            val += [hr integerValue];
            count++;
        }
    }
    return val / count;//[self.heartRateSamples count];
}

//graph page
- (NSString *)getHR
{
    if(self.rhRate == 0) return @"0";
    
    return [NSString stringWithFormat:@"%i", self.rhRate];
}

//avg rt
- (NSString *)getAvgHR
{
    if([[self.heartRateSamples allValues] count] == 0) return @"0";
    
    return [NSString stringWithFormat:@"%d", (int)[self getAvgHrm]];
}


//max RT
- (NSString *)getMaxHR
{
    if([[self.heartRateSamples allValues] count] == 0) return @"0";
    
    return [NSString stringWithFormat:@"%d", [[[self.heartRateSamples allValues] valueForKeyPath:@"@max.intValue"] intValue]];
}

//Cal Avg
- (NSString *)getCalAvg
{
    if([self getAvgHrm] < 60)
        return @"0";

    float calV =  (0.1416*([self getAvgHrm]) - 8.303)*self.durationSeconds/60;

    //float calV =  ((0.1416*([self getAvgHrm]) - 8.303)*self.durationSeconds/60)*(1/(self.durationSeconds/60));
    int min = (self.durationSeconds/60);
    if(min == 0) min = 1;
    
    return [NSString stringWithFormat:@"%.1f", (calV / min)];
}


//
- (NSString *)getDistanceMetersText
{
    return @"0";
}

//
#pragma --
#pragma week total cardio value

- (NSString *)getTotalCaloriesText:(float)val
{
    float calV = val;
    
    if([self getAvgHrm] < 60)
        ;
    else
        calV +=  (0.1416*([self getAvgHrm]) - 8.303)*self.durationSeconds/60;
    
    return [NSString stringWithFormat:@"%d", (int)calV];//@"%.1f"
}

- (NSString *)getTotalBelowZoneText:(int)val
{
    return [self getFormatedFloatVal:(self.zoneBelow + val)];
}

- (NSString *)getTotalInZoneText:(int)val
{
    return [self getFormatedFloatVal:(self.zoneIn + val)];
}

- (NSString *)getTotalAboveZoneText:(int)val
{
    return [self getFormatedFloatVal:(self.zoneAbove + val)];
}

- (NSString *)getTotalInAboveZoneText:(int)val
{
    return [self getFormatedFloatVal:(self.zoneIn + self.zoneAbove + val)];
}

- (NSInteger)getTotalInAboveZone:(int)val
{
    return (self.zoneIn + self.zoneAbove + val);
}


#pragma --
#pragma end week total
//

//- (void)dealloc
//{
//    self.startLapTime = nil;
//    self.heartRateSamples = nil;
//    self.arrGpsPos = nil;
//    
//    [super dealloc];
//}

- (NSString *)getHrmString
{
    NSString *hfmString = @"";
    
    int numOfItem = self.durationSeconds/5;
    int hrmVal = 0;
    
    for (int ii = 0; ii < numOfItem; ii++)
    {
        NSString *key = [NSString stringWithFormat:@"%d", ii];
        
        if([self.heartRateSamples valueForKey:key] != nil)
        {
            hrmVal = [[self.heartRateSamples valueForKey:key] integerValue];
        }
        else
            hrmVal = 0;
        
        if(ii == 0)
            hfmString = [NSString stringWithFormat:@"%@%d", hfmString, hrmVal];
        else
            hfmString = [NSString stringWithFormat:@"%@,%d", hfmString, hrmVal];
        
    }//endfor
    
    return hfmString;
}

- (NSMutableString *)getCardioLapXml
{
    NSMutableString *sRequest = [[NSMutableString alloc] init];// autorelease];
    
//    [sRequest appendString:@"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\"><soapenv:Header/><soapenv:Body>"];

	[sRequest appendString:@"<nuv:CardioLap>"];
	[sRequest appendFormat:@"<nuv:Calories>%@</nuv:Calories>", [self getCaloriesNumericText]];
	[sRequest appendFormat:@"<nuv:DistanceMeters>%@</nuv:DistanceMeters>", [self getDistanceMetersText]];
    [sRequest appendFormat:@"<nuv:DurationSeconds>%d</nuv:DurationSeconds>", self.durationSeconds];
    
    [sRequest appendFormat:@"<nuv:HeartRateSamplesString>%@</nuv:HeartRateSamplesString>", [self getHrmString]];
    [sRequest appendString:@"<nuv:HeartRateSamples>null</nuv:HeartRateSamples>"];
    
    [sRequest appendFormat:@"<nuv:SecondsAboveZone>%d</nuv:SecondsAboveZone>", self.zoneAbove];
    [sRequest appendFormat:@"<nuv:SecondsBelowZone>%d</nuv:SecondsBelowZone>", self.zoneBelow];
    [sRequest appendFormat:@"<nuv:SecondsInZone>%d</nuv:SecondsInZone>", self.zoneIn];

    [sRequest appendFormat:@"<nuv:SourceEnum>%d</nuv:SourceEnum>", self.sourceEnum];
    [sRequest appendFormat:@"<nuv:UTCDate>%@</nuv:UTCDate>", self.startLapTime];

    [sRequest appendString:@"</nuv:CardioLap>"];

    //<nuv:HeartRateSamples>cid:1175316126525</nuv:HeartRateSamples>
   
    
    return sRequest;
}


- (NSMutableString *)getCardioSessionXml:(NSString *)memberId {
    NSInteger sportEnum = 0;
    
    NSMutableString *sRequest = [[NSMutableString alloc] init];// autorelease];
    
    
    [sRequest appendString:@"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\" xmlns:nuv=\"http://schemas.datacontract.org/2004/07/NuvitaMobileService\">"];
    
    [sRequest appendString:@"<soapenv:Header/>"];
    [sRequest appendString:@"<soapenv:Body>"];
    [sRequest appendString:@"<tem:SaveCardioWorkout>"];
    [sRequest appendString:@"<tem:cardioWorkout>"];
    
    [sRequest appendString:@"<nuv:CardioLaps>"];
    
    //for(CardioLap *lap in self.arrCardioLapData)
    [sRequest appendString:[self getCardioLapXml]];
    
    [sRequest appendString:@"</nuv:CardioLaps>"];
    
    [sRequest appendFormat:@"<nuv:DeviceId>%@</nuv:DeviceId>", APP_DEVICE_ID];
    [sRequest appendFormat:@"<nuv:DeviceName>%@</nuv:DeviceName>", [AppData GetDeviceName]];
    [sRequest appendFormat:@"<nuv:SportEnum>%d</nuv:SportEnum>", sportEnum];
    [sRequest appendFormat:@"<nuv:StartTimeUTC>%@</nuv:StartTimeUTC>", self.startLapTime];
    [sRequest appendFormat:@"<nuv:memberId>%@</nuv:memberId>", memberId];
        
    [sRequest appendString:@"</tem:cardioWorkout>"];
    [sRequest appendString:@"</tem:SaveCardioWorkout>"];
    [sRequest appendString:@"</soapenv:Body>"];
    [sRequest appendString:@"</soapenv:Envelope>"];
    
    return sRequest;
}

/*
 <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tem="http://tempuri.org/" xmlns:nuv="http://schemas.datacontract.org/2004/07/NuvitaMobileService">
 <soapenv:Header/>
 <soapenv:Body>
 <tem:SaveCardioWorkoutHrString>
 <!--Optional:-->
 <tem:cardioWorkout>
 <!--Optional:-->
 <nuv:CardioLaps>
 <!--Zero or more repetitions:-->
 <nuv:CardioLapHrString>
 <!--Optional:-->
 <nuv:Calories>?</nuv:Calories>
 <!--Optional:-->
 <nuv:DistanceMeters>?</nuv:DistanceMeters>
 <!--Optional:-->
 <nuv:DurationSeconds>?</nuv:DurationSeconds>
 <!--Optional:-->
 <nuv:HeartRateSamples>?</nuv:HeartRateSamples>
 <!--Optional:-->
 <nuv:SourceEnum>?</nuv:SourceEnum>
 <!--Optional:-->
 <nuv:UTCDate>?</nuv:UTCDate>
 </nuv:CardioLapHrString>
 </nuv:CardioLaps>
 <!--Optional:-->
 <nuv:DeviceId>?</nuv:DeviceId>
 <!--Optional:-->
 <nuv:DeviceName>?</nuv:DeviceName>
 <!--Optional:-->
 <nuv:SportEnum>?</nuv:SportEnum>
 <!--Optional:-->
 <nuv:StartTimeUTC>?</nuv:StartTimeUTC>
 <!--Optional:-->
 <nuv:memberId>?</nuv:memberId>
 </tem:cardioWorkout>
 </tem:SaveCardioWorkoutHrString>
 </soapenv:Body>
 </soapenv:Envelope>
 
 */

@end
