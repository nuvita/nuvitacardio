//
//  AddDeviceViewController.m
//  NuvitaCardio
//
//  Created by John on 7/11/14.
//
//

#import "AddDeviceViewController.h"
#import "DeviceListViewController.h"
#import "InputViewController.h"

#import "UIImage+BlurredFrame.h"

@interface AddDeviceViewController () <CBCentralManagerDelegate, DeviceListViewControllerDelegate, InputViewControllerDelegate>

@property (retain, nonatomic) IBOutlet UILabel *welcomelbl;
@property (nonatomic, strong) NSMutableArray *heartRateMonitors;
@property (nonatomic, strong) CBCentralManager *manager;
@property (nonatomic, strong) CBPeripheral *peripheral;
@property (nonatomic, retain) NSTimer *timer;
@property (assign) NSInteger countdown;

@property (nonatomic, retain) OfflineManager *offlineManager;

@end

@implementation AddDeviceViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    _heartRateMonitors = [[NSMutableArray alloc] init];
    _manager = [[CBCentralManager alloc] initWithDelegate:self queue:nil];
    [self initOfflineManager];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Scan"
                                                                              style:UIBarButtonItemStylePlain
                                                                             target:self
                                                                             action:@selector(didTapConnect)];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    
    LoginResponseData *loginData = [LoginPerser getLoginResponse];
    _welcomelbl.text = [NSString stringWithFormat:@"Welcome %@", [loginData firstName]];
    
//    NSLog(@"screen: %f-%f", self.tabBarController.view.frame.size.width, self.tabBarController.view.frame.size.height);
//    
//    CGRect frame = self.tabBarController.view.frame;
//    UIImage *imageBlurred = [self captureScreenInRect:frame];
//    imageBlurred = [imageBlurred applyLightEffectAtFrame:frame];
//    
//    UIImageView *imageview = [[UIImageView alloc] initWithFrame:frame];
//    imageview.image = imageBlurred;
//    
//    [self.tabBarController.view addSubview:imageview];
}

//- (UIImage *)captureScreenInRect:(CGRect)captureFrame {
//    CALayer *layer = self.view.layer;
//    UIGraphicsBeginImageContext(self.view.bounds.size);
//    CGContextClipToRect(UIGraphicsGetCurrentContext(),captureFrame);
//    [layer renderInContext:UIGraphicsGetCurrentContext()];
//    UIImage *screenImage = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
//    return screenImage;
//}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:YES];
    self.navigationItem.rightBarButtonItem.enabled = YES;
    [self stopScan];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Public Methods

- (void)initOfflineManager {
    
    NuvitaAppDelegate *appDelegate = (NuvitaAppDelegate *)[[UIApplication sharedApplication] delegate];
    _offlineManager = [[OfflineManager alloc] init];
    _offlineManager.managedObjectContext = appDelegate.managedObjectContext;
}

- (void)didTapConnect {
    [self startScan];
}

- (void)startScan {
    self.navigationItem.rightBarButtonItem.enabled = NO;
    [_heartRateMonitors removeAllObjects];
    if ([_manager state] == CBCentralManagerStatePoweredOff || _manager == nil) {
        _manager = [[CBCentralManager alloc] initWithDelegate:self queue:nil];
    }

    NSDictionary *scanOptions = [NSDictionary dictionaryWithObject:[NSNumber numberWithBool:YES] forKey:CBCentralManagerScanOptionAllowDuplicatesKey];
    [_manager scanForPeripheralsWithServices:[NSArray arrayWithObject:[CBUUID UUIDWithString:@"180D"]]
                                         options:scanOptions];
    
    if (_timer == nil) {
        _countdown = 0;
        _timer = [NSTimer scheduledTimerWithTimeInterval:1.0f
                                                  target:self
                                                selector:@selector(checkDevicesConnected)
                                                userInfo:nil
                                                 repeats:YES];
    }
    
}

- (void)stopScan {
    if (_manager) {
        [_manager stopScan];
        _manager = nil;
    }
    
    if (_timer) {
        [_timer invalidate];
        _timer = nil;
    }
}

- (void)checkDevicesConnected {
    
    _countdown ++;
    if ([_heartRateMonitors count] > 0 && _countdown == 5) {
        [_timer invalidate];
        _timer = nil;
        
        [self stopScan];
        [self showDeviceList];
        return;
    }
    
    if (_countdown == 10) {
        [_timer invalidate];
        _timer = nil;
        UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:@"Sorry"
                                                            message:@"No device found."
                                                           delegate:self
                                                  cancelButtonTitle:@"Cancel"
                                                  otherButtonTitles:@"Scan Again?", nil];
        [alertview show];
        self.navigationItem.rightBarButtonItem.enabled = YES;
    }
}

- (void)showDeviceList {
    
    DeviceListViewController *deviceListViewController = [[DeviceListViewController alloc] init];
    deviceListViewController.devices = _heartRateMonitors;
    deviceListViewController.hidesBottomBarWhenPushed = YES;
    deviceListViewController.delegate = self;
   
    [self.navigationController pushViewController:deviceListViewController animated:YES];
}

- (void)loadInputView {
    
    InputViewController *inputViewController = [[InputViewController alloc] init];
    inputViewController.modalTransitionStyle = UIModalTransitionStylePartialCurl;
    inputViewController.delegate = self;
    
    [self.navigationController presentViewController:inputViewController animated:YES completion:^{
        
    }];
}

#pragma mark - InputViewControllerDelegate

- (void)inputViewController:(InputViewController *)viewcontroller didEnterName:(NSString *)deviceName {
    
    if ([deviceName length] == 0) {
  
        UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:@"Error"
                                                            message:@"Please enter device name to continue."
                                                           delegate:nil
                                                  cancelButtonTitle:@"Ok"
                                                  otherButtonTitles:nil];
        [alertview show];
        [self performSelector:@selector(loadInputView) withObject:nil afterDelay:1.0];
        return;
    }
    
    if (_peripheral) {
        
        NSDictionary *data = [NSDictionary dictionaryWithObjectsAndKeys:
                              [AppData GetUUID:[_peripheral UUID]], @"serialnumber",
                              [_peripheral name], @"name",
                              deviceName, @"inputName",
                              [NSNumber numberWithBool:NO], @"ignored",
                              nil];
        
        if ([_offlineManager addNewMonitor:data withSerialNo:[AppData GetUUID:[_peripheral UUID]]]) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"update_connect_btn" object:[NSNumber numberWithBool:YES]];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"CONNECT_THIS_PERIPHERAL" object:[AppData GetUUID:[_peripheral UUID]]];
            [self.navigationController popToRootViewControllerAnimated:YES];
        }
    }
}

#pragma mark - DeviceListViewControllerDelegate

- (void)didChooseDevice:(CBPeripheral *)selectedPeripheral {
    _peripheral = selectedPeripheral;
    [self performSelector:@selector(loadInputView) withObject:nil afterDelay:1.0];
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 1) {
        [self performSelector:@selector(startScan) withObject:nil afterDelay:0.5];
        return;
    }
    
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - CBCentralManager delegate methods

- (BOOL) isLECapableHardware {
    NSString * state = nil;
    switch ([_manager state]) {
        case CBCentralManagerStateUnsupported:
            state = @"The platform/hardware doesn't support Bluetooth Low Energy.";
            break;
        case CBCentralManagerStateUnauthorized:
            state = @"The app is not authorized to use Bluetooth Low Energy.";
            break;
        case CBCentralManagerStatePoweredOff:
            state = @"Bluetooth is currently powered off.";
            break;
        case CBCentralManagerStatePoweredOn:
            return TRUE;
        case CBCentralManagerStateUnknown:
        default:
            return FALSE;
    }
    
    return FALSE;
}

- (void) centralManagerDidUpdateState:(CBCentralManager *)central {
    
    if(![self isLECapableHardware]) {
        [AppData sharedAppData].serialNumber = @"";
    }
}

- (void) centralManager:(CBCentralManager *)central
  didDiscoverPeripheral:(CBPeripheral *)aPeripheral
      advertisementData:(NSDictionary *)advertisementData
                   RSSI:(NSNumber *)RSSI {
    
    
    if(![_heartRateMonitors containsObject:aPeripheral])
        [_heartRateMonitors addObject:aPeripheral];
}

//- (void)connectToPeripheral {
//    
//    [self stopScan];
//    if ([_heartRateMonitors count] > 0) {
//        
//        self.peripheral = [_heartRateMonitors objectAtIndex:0];
//        [AppData sharedAppData].serialNumber = [AppData GetUUID:self.peripheral.UUID];
////        CMonitor *foundMonitor = [_offlineManager checkForDuplicateMonitorSerialNo:[[AppData sharedAppData] serialNumber]];
////        
////        if (!foundMonitor && [[self.peripheral name] isEqualToString:@"Nuvita14RJM20"]) {
//        
//            NSString *serial = [[AppData sharedAppData] serialNumber];
//            NSString *trimmedString = [serial substringFromIndex:MAX((int)[serial length] - 6, 0)];
//            UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:@"Found"
//                                                                message:[NSString stringWithFormat:@"New Nuvita Cardio (...%@) monitor found.  Do you wish to pair?", trimmedString]
//                                                               delegate:self
//                                                      cancelButtonTitle:@"No"
//                                                      otherButtonTitles:@"Yes", nil];
//            [alertview show];
////            return;
////        }
////        
////        if (![[self.peripheral name] isEqualToString:@"Nuvita14RJM20"]) {
////            UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:@"Error!"
////                                                                message:@"Invalid transmitter detected, Please connect a Nuvita Cardio monitor to continue?"
////                                                               delegate:nil
////                                                      cancelButtonTitle:@"Ok"
////                                                      otherButtonTitles:nil];
////            [alertview show];
////            return;
////        }
////        
////        
//    }
//    
//}

@end
