//
//  JFButtonView.m
//  NuvitaCardio
//
//  Created by John on 6/18/14.
//
//

#import "JFButtonView.h"
#import "JFButton.h"

@interface JFButtonView ()

@property (retain, nonatomic) JFButton *button;

@end

@implementation JFButtonView
@synthesize buttonTitle = _buttonTitle;

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self loadButton];
    }
    
    return self;
}

- (void)setButtonTitle:(NSString *)buttonTitle {
    [_button setTitle:buttonTitle forState:UIControlStateNormal];
}

- (void)setHeight:(CGFloat)height {
    
    CGSize size = [[_button titleForState:UIControlStateNormal] sizeWithFont:[_button.titleLabel font]];
    _button.frame = CGRectMake(0, 0, size.width + 20, height);
    self.frame = CGRectMake(0, 0, size.width + 20, height);
}

- (void)setBackButton:(BOOL)backButton {
    
    int offset = (backButton) ? 10 : -10;
    self.bounds = CGRectOffset(self.bounds, offset, 0);
    
    [_button setBackButton:backButton];
    [_button setNeedsDisplay];
}

- (void)setDisableButton:(BOOL)disableButton {
    [_button setButtonTitle:[_button titleForState:UIControlStateNormal]];
    [_button setDisabled:disableButton];
    [_button setNeedsDisplay];
}

- (void)userLongPressed:(id)sender {
    UILongPressGestureRecognizer *lp = (UILongPressGestureRecognizer *)sender;
    if (lp.state == UIGestureRecognizerStateEnded) {
        [UIView animateWithDuration:.2
                              delay:0
                            options: UIViewAnimationOptionAllowUserInteraction
                         animations:^{
                             self.alpha = 1.0;
                         }
                         completion:^(BOOL completed) {
                             
                         }
         ];
        
        if ([_delegate respondsToSelector:@selector(buttonView:didTapButton:)]) {
            [_delegate buttonView:self didTapButton:_button];
        }

    } else if (lp.state == UIGestureRecognizerStateBegan){
        [UIView animateWithDuration:.2
                              delay:0
                            options: UIViewAnimationOptionAllowUserInteraction
                         animations:^{
                             self.alpha = 0.2;
                         }
                         completion:^(BOOL completed) {
                             
                         }
         ];

    }
}

- (void)didTapButton:(id)sender {
    
    [UIView animateWithDuration:.2
                          delay:0
                        options: UIViewAnimationOptionAllowUserInteraction
                     animations:^{
                         self.alpha = 0.2;
                     }
                     completion:^(BOOL completed) {
                         self.alpha = 1.0;
                     }
     ];
    
    if ([_delegate respondsToSelector:@selector(buttonView:didTapButton:)]) {
        [_delegate buttonView:self didTapButton:_button];
    }
    
}

- (void)loadButton {
    
    [self.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    _button = [[JFButton alloc] initWithFrame:CGRectZero];
    _button.alpha = 1;
    
    [_button setTitleColor:[UIColor colorWithRed:0.2 green:0.4 blue:0.7 alpha:1.0] forState:UIControlStateNormal];
    [_button setTitle:@"Button" forState:UIControlStateNormal];
    [_button addTarget:self action:@selector(didTapButton:) forControlEvents:UIControlEventTouchUpInside];
    
    UILongPressGestureRecognizer *gr = [[UILongPressGestureRecognizer alloc] init];
    gr.minimumPressDuration = 0.1;
    [gr addTarget:self action:@selector(userLongPressed:)];
    [_button addGestureRecognizer:gr];
    

    CGSize size = [[_button titleForState:UIControlStateNormal] sizeWithFont:[_button.titleLabel font]];
    _button.frame = CGRectMake(0, 0, size.width + 10, size.height);

    [self addSubview:_button];
    
}



@end
