//
//  Vo2MaxChart.h
//  mynuvita
//
//  Created by John on 4/29/14.
//
//

#import <UIKit/UIKit.h>

@interface Vo2MaxChart : UIView

@property (retain, nonatomic) NSArray *xDataList;
@property (retain, nonatomic) NSArray *normCategoryList;

@property (assign) NSInteger ratio;

@end
