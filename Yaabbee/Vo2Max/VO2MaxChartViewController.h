//
//  VO2MaxChartViewController.h
//  mynuvita
//
//  Created by John on 4/29/14.
//
//

#import <UIKit/UIKit.h>
#import "MyTools.h"
#import "request.h"
#import "NuvitaXMLParser.h"
#import "LoginPerser.h"
#import "LoginResponseData.h"
#import "Vo2MaxChart.h"
#import "Vo2MaxGraph.h"
#import "JFButtonView.h"
#import "JFButton.h"

#include <math.h>

@interface VO2MaxChartViewController : UIViewController

@property (nonatomic, retain) NSDictionary *groupInfo;
@property (nonatomic, retain) NSDictionary *resultInfo;

@end
