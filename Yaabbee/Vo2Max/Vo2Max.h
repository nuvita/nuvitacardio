//
//  Vo2Max.h
//  mynuvita
//
//  Created by John on 4/25/14.
//
//

#import <Foundation/Foundation.h>

@interface Vo2Max : NSObject

@property (nonatomic, retain) NSMutableArray *rrValues;
@property (retain, nonatomic) NSMutableArray *hrValues;
@property (nonatomic, retain) NSString *dateCreated;
@property (assign) CGFloat height;
@property (assign) CGFloat weight;
@property (assign) NSInteger activityLevel;
@property (assign) NSInteger duration;
@property (assign) NSInteger *hrAverage;


- (void)saveRRInterval:(NSString *)rriStringValue;
- (void)saveHRValue:(NSString *)hrStringValue;

- (NSString *)getRRValuesString;
- (NSString *)getHRValuesString;
+ (NSString *)getDateCreated;

- (NSMutableString *)generateVo2MaxXML:(NSString *)memberID;

@end
