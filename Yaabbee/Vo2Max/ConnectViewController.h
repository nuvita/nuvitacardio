//
//  ConnectViewController.h
//  mynuvita
//
//  Created by John on 5/14/14.
//
//

#import <UIKit/UIKit.h>
#import <CoreBluetooth/CoreBluetooth.h>

@interface ConnectViewController : UIViewController

@property (retain, nonatomic) NSMutableDictionary *data;

@end
