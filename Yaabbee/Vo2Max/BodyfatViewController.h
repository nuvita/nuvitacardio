//
//  BodyfatViewController.h
//  mynuvita
//
//  Created by John on 3/20/14.
//
//

#import <UIKit/UIKit.h>
#import "MyTools.h"
#import "LoginPerser.h"
#import "LoginResponseData.h"
#import "request.h"
#import "NuvitaXMLParser.h"
#import "Vo2Max.h"
#import "NuvitaAppDelegate.h"
#import "OfflineManager.h"

@interface BodyfatViewController : UIViewController

@property (retain, nonatomic) NSString *vo2max;

@end
