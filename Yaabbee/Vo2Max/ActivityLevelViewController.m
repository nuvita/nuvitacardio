//
//  ActivityLevelViewController.m
//  mynuvita
//
//  Created by John on 5/8/14.
//
//

#import "ActivityLevelViewController.h"
#import "ConnectViewController.h"

@interface ActivityLevelViewController () <JFButtonViewDelegate>

@property (retain, nonatomic) IBOutlet UIToolbar *toolbar;
@property (retain, nonatomic) IBOutlet UITableView *tableview;
@property (retain, nonatomic) IBOutlet UILabel *titlelbl;

@property (retain, nonatomic) NSArray *activityList;
@property (assign) NSInteger selectedIndex;

@end

@implementation ActivityLevelViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        UIImageView *textView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"logo.png"]];
        [self.navigationItem setTitleView:textView];
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    _selectedIndex = -1;
    _titlelbl.text = [_info valueForKey:@"a:Text"];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    [self updateNavigationUI];
    
    _activityList = [_info valueForKey:@"a:Answers"];
    [_tableview reloadData];
}

- (void)updateNavigationUI {
    
    JFButtonView *buttonview = [[JFButtonView alloc] initWithFrame:CGRectMake(0, 0, 200, CGRectGetHeight(self.navigationController.navigationBar.frame))];
    [buttonview setButtonTitle:@"Next"];
    [buttonview setHeight:CGRectGetHeight(self.navigationController.navigationBar.frame)];
    [buttonview setBackButton:NO];
    [buttonview setDisableButton:YES];
    [buttonview setDelegate:self];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:buttonview];
    self.navigationItem.rightBarButtonItem.enabled = NO;
}

#pragma mark - JFButtonViewDelegate

- (void)buttonView:(JFButtonView *)view didTapButton:(JFButton *)button {
    
    if (![button backButton]) {
        [self didTapNext];
    }
}

#pragma mark - Public Methods

- (void)didTapNext {
    
    self.navigationItem.rightBarButtonItem = nil;
    [_data setObject:[[_activityList objectAtIndex:_selectedIndex] valueForKey:@"b:value"] forKey:@"activity"];

    ConnectViewController *connectViewController = [[ConnectViewController alloc] init];
    connectViewController.data = _data;
    connectViewController.hidesBottomBarWhenPushed = YES;
    
    [self.navigationController pushViewController:connectViewController animated:YES];
    
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_activityList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    cell.textLabel.font = [UIFont fontWithName:@"Arial" size:([NuvitaAppDelegate isPhone5]) ? 14 : 12.5];
    cell.textLabel.textAlignment = NSTextAlignmentLeft;
    cell.textLabel.numberOfLines = 0;
    cell.textLabel.text = [[_activityList objectAtIndex:indexPath.row] valueForKey:@"b:key"];

    cell.imageView.image = (_selectedIndex == indexPath.row) ? [UIImage imageNamed:@"check-flat.png"]: [UIImage imageNamed:@"uncheck-flat.png"];


    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    _selectedIndex = indexPath.row;
    
    
    JFButtonView *buttonview = (JFButtonView *)self.navigationItem.rightBarButtonItem.customView;
    [buttonview setDisableButton:NO];
    [buttonview setButtonTitle:@"Next"];
    self.navigationItem.rightBarButtonItem.enabled = YES;
    [_tableview performSelector:@selector(reloadData) withObject:nil afterDelay:0.2];
}

@end
