//
//  ResultViewController.h
//  NuvitaCardio
//
//  Created by John on 6/17/14.
//
//

#import <UIKit/UIKit.h>
#import "Vo2MaxChart.h"
#import "request.h"
#import "NuvitaXMLParser.h"
#import "LoginResponseData.h"
#import "LoginPerser.h"

@interface ResultViewController : UIViewController

@property (retain, nonatomic) NSDictionary *resultInfo;

@end
