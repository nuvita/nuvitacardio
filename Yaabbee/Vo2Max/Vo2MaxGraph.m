//
//  Vo2MaxGraph.m
//  NuvitaCardio
//
//  Created by John on 5/16/14.
//
//

#import "Vo2MaxGraph.h"

@interface Vo2MaxGraph ()

@property (assign) NSInteger xxOffset;
@property (assign) NSInteger yyOffset;

@property (retain, nonatomic) NSMutableArray *xxLabel;

@end

@implementation Vo2MaxGraph

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        _xxLabel = [[NSMutableArray alloc] init];
        
        _xxOffset = 20;
        _yyOffset = 10;
        
        int min = 0;
        int sec = 5;
        for (int i = 0; i < 10; i++) {
            sec = (sec + 30 == 35) ? 35 : 5;
            NSString *formattedSec = [NSString stringWithFormat:@"%d", sec];
            if (sec == 5) {
                min += 1;
                formattedSec = @"05";
            }
            
            [_xxLabel addObject:[NSString stringWithFormat:@"%d:%@", min, formattedSec]];
        }
        
        UILabel *hr = [[UILabel alloc] initWithFrame:CGRectMake(_xxOffset + 10, -15, self.frame.size.width, 12)];
        hr.font = [UIFont systemFontOfSize:10.0];
        hr.text = @"Heart Rate (HR)";
        
        UILabel *hrv = [[UILabel alloc] initWithFrame:CGRectMake(_xxOffset + 10, self.frame.size.height / 2 + 2, self.frame.size.width, 12)];
        hrv.font = [UIFont systemFontOfSize:10.0];
        hrv.text = @"Heart Rate Variance (HRV)";
        
        [self addSubview:hr];
        [self addSubview:hrv];
    }
    
    return self;
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    
    [self setClearsContextBeforeDrawing: YES];
    
    float height = self.frame.size.height;
    float widht = self.frame.size.width;
    
    //...draw graph background
//    CGFloat colors [] = {
//        0.0, 0.0, 1.0, 0.1,
//        1.0, 1.0, 1.0, 1.0
//    };
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
//    CGColorSpaceRef baseSpace = CGColorSpaceCreateDeviceRGB();
//    CGGradientRef gradient = CGGradientCreateWithColorComponents(baseSpace, colors, NULL, 2);
//    CGColorSpaceRelease(baseSpace), baseSpace = NULL;
//    CGContextSaveGState(context);
//
//    CGRect gframe = CGRectMake(_xxOffset, 0, widht - _xxOffset, height - _yyOffset);
//    CGContextAddRect(context, gframe);
//    CGContextClip(context);
//
//    CGPoint startPoint = CGPointMake(CGRectGetMidX(gframe), CGRectGetMinY(gframe));
//   CGPoint endPoint = CGPointMake(CGRectGetMidX(gframe), CGRectGetMaxY(gframe));
//    CGContextDrawLinearGradient(context, gradient, startPoint, endPoint, 0);
//    CGGradientRelease(gradient), gradient = NULL;
//
//    CGContextRestoreGState(context);
    
    
    // Drawing code
    CGPoint bPoint = CGPointMake(_xxOffset, 0);
    CGPoint ePoint = CGPointMake(_xxOffset, height - _yyOffset);
    
    CGContextSetLineWidth(context, 0.5f);
    CGContextSetMiterLimit(context, 0.0f);
    CGContextSetLineJoin(context, kCGLineJoinRound);
    CGContextSetLineCap(context, kCGLineCapRound);
    CGContextSetBlendMode(context, kCGBlendModeNormal);
    CGContextSetStrokeColorWithColor(context, [UIColor blackColor].CGColor);
    CGContextSaveGState(context);
    
    CGContextMoveToPoint(context, bPoint.x, bPoint.y);
    CGContextAddLineToPoint(context, ePoint.x, ePoint.y);
    CGContextAddLineToPoint(context, widht, ePoint.y);
    CGContextStrokePath(context);
    
    CGContextMoveToPoint(context, bPoint.x, ePoint.y / 2);
    CGContextAddLineToPoint(context, widht, ePoint.y / 2);
    CGContextStrokePath(context);
    
    float topGap = ((height - _yyOffset) / 2) / 4;
    int fmax = _maxline + 10;
    int fmin = _minline - 10;
    int topOffset = (fmax - fmin) / 3;
    int topValueHolder = fmin;
    

    bPoint = CGPointMake(_xxOffset, (height - _yyOffset) / 2);
    int topYCount = 4;
    for (int i = 0; i < topYCount; i ++) {
        
        bPoint.y -= topGap;
        
        if ([_arrPoints count] > 0) {
 
            UILabel *label = (UILabel *)[self viewWithTag:i + 100];
            if (label == nil) {
                label = [[UILabel alloc] initWithFrame:CGRectMake(-5, bPoint.y - 5, 20, 10)];
                label.tag = i + 100;
                label.backgroundColor = [UIColor clearColor];
                label.textAlignment = NSTextAlignmentRight;
                label.font = [UIFont systemFontOfSize:8.0];
                
                [self addSubview:label];
            }
            

            label.text = [NSString stringWithFormat:@"%d", topValueHolder];
            topValueHolder += (int)topOffset;
        }
        
        CGContextRestoreGState(context);
        CGContextMoveToPoint(context, bPoint.x, bPoint.y);
        CGContextAddLineToPoint(context, bPoint.x - 5, bPoint.y);
        CGContextStrokePath(context);
        CGContextSaveGState(context);
        
        CGFloat dashes[] = {2,2};
        CGContextSetLineDash(context, 2.0, dashes, 2);
        CGContextSetStrokeColorWithColor(context, [UIColor colorWithWhite:0.7 alpha:1.0].CGColor);
        CGContextMoveToPoint(context, bPoint.x, bPoint.y);
        CGContextAddLineToPoint(context, widht, bPoint.y);
        CGContextStrokePath(context);
        
    }
    
    float bmax = _maxbar + 10;
    float bmin = _minbar - 10;
    float botOffset = (bmax - bmin) / 4;
    
    bPoint = CGPointMake(_xxOffset, (height - _yyOffset) / 2);
    for (int i = 3; i > 0; i --) {
        
        bPoint.y += topGap;
        
        if ([_arrPoints count] > 0) {
            
            UILabel *label = (UILabel *)[self viewWithTag:i + 300];
            if (label == nil) {
                label = [[UILabel alloc] initWithFrame:CGRectMake(-10, bPoint.y - 5, 25, 10)];
                label.tag = i + 300;
                label.backgroundColor = [UIColor clearColor];
                label.textAlignment = NSTextAlignmentRight;
                label.font = [UIFont systemFontOfSize:8.0];
                
                [self addSubview:label];
            }
            
            
            label.text = [NSString stringWithFormat:@"%d", (int)bmin + ((i + 1) * (int)botOffset)];
        }
        
        CGContextRestoreGState(context);
        CGContextMoveToPoint(context, bPoint.x, bPoint.y);
        CGContextAddLineToPoint(context, bPoint.x - 5, bPoint.y);
        CGContextStrokePath(context);
        CGContextSaveGState(context);
        
        CGFloat dashes[] = {2,2};
        CGContextSetLineDash(context, 2.0, dashes, 2);
        CGContextSetStrokeColorWithColor(context, [UIColor colorWithWhite:0.7 alpha:1.0].CGColor);
        CGContextMoveToPoint(context, bPoint.x, bPoint.y);
        CGContextAddLineToPoint(context, widht, bPoint.y);
        CGContextStrokePath(context);
    }
    
    CGContextStrokePath(context);
    CGContextRestoreGState(context);
    
    //... draw x labels
    float xxGap = (widht - _xxOffset) / [_xxLabel count];
    CGPoint xPoint = CGPointMake(_xxOffset, height - _yyOffset);
    for (int i = 0; i < [_xxLabel count]; i++) {
        
        xPoint.x += xxGap;
        CGContextMoveToPoint(context, xPoint.x, xPoint.y);
        CGContextAddLineToPoint(context, xPoint.x, xPoint.y + 5);
        
        UILabel *label = (UILabel *)[self viewWithTag:i + 200];
        if (label == nil) {
            label = [[UILabel alloc] initWithFrame:CGRectMake(xPoint.x - 15, xPoint.y + 5, 30, 10)];
            label.tag = i + 200;
            label.backgroundColor = [UIColor clearColor];
            label.textAlignment = NSTextAlignmentCenter;
            label.font = [UIFont systemFontOfSize:8.0];
            [self addSubview:label];
        }
        
        label.text = [NSString stringWithFormat:@"%@", [_xxLabel objectAtIndex:i]];

    }
    
    CGContextStrokePath(context);

    //... draw line top graph
    float topAxisHeight = (height - _yyOffset) / 2;
    float yyRatio = topAxisHeight / 4; //100
    float xxRatio = ((widht - _xxOffset) / [_xxLabel count]) / 6;
    
    CGContextSetLineWidth(context, 2.0f);
    CGContextSetMiterLimit(context, 5.0f);
    CGContextSetLineJoin(context, kCGLineJoinRound);
    CGContextSetLineCap(context, kCGLineCapRound );
    CGContextSetBlendMode(context, kCGBlendModeNormal);
    CGContextSetStrokeColorWithColor(context, [UIColor colorWithRed:0.9 green:0.6 blue:0.2 alpha:1.0].CGColor);
    CGContextSetFillColorWithColor(context, [UIColor colorWithRed:0.9 green:0.6 blue:0.2 alpha:0.2].CGColor);
    
    if ([_arrPoints count] > 0) {
        
        for (int i = 0; i < [_arrPoints count]; i++) {
            
            if (i == 0) {
                
//                CGPoint init = [[[_arrPoints objectAtIndex:i] valueForKey:@"topPoints"] CGPointValue];
//                init.y += topOffset - fmin;
//                
//                CGPoint first = CGPointMake(((init.x + 0) * xxRatio) + _xxOffset, topAxisHeight - (init.y * yyRatio / topOffset));
//                CGContextMoveToPoint(context, first.x, first.y);
                
            } else {
                
                CGPoint prev = [[[_arrPoints objectAtIndex:i - 1] valueForKey:@"topPoints"] CGPointValue];
                prev.y += topOffset - fmin;
                
                CGPoint first = CGPointMake(((prev.x + 1) * xxRatio) + _xxOffset, topAxisHeight - (prev.y * yyRatio / topOffset));
                first.x -= xxRatio;
                
                CGPoint last = [[[_arrPoints objectAtIndex:i] valueForKey:@"topPoints"] CGPointValue];
                last.y += topOffset - fmin;
                
                CGPoint second = CGPointMake(((last.x + 1) * xxRatio) + _xxOffset, topAxisHeight - (last.y * yyRatio / topOffset));
                second.x -= xxRatio;
//                CGContextAddLineToPoint(context, second.x, second.y);
                
                CGContextBeginPath (context);
                CGMutablePathRef pathRef = CGPathCreateMutable();
                
                CGPathMoveToPoint(pathRef, NULL, first.x, first.y);
                CGPathAddLineToPoint(pathRef, NULL, second.x, second.y);
                CGContextAddPath(context, pathRef);
                CGContextStrokePath(context);
                
                CGPathAddLineToPoint(pathRef, NULL, second.x, topAxisHeight);
                CGPathAddLineToPoint(pathRef, NULL, first.x, topAxisHeight);
                
                CGContextAddPath(context, pathRef);
                CGContextFillPath(context);
                CGPathRelease(pathRef);

            }
            
        }
    }

    CGContextSetStrokeColorWithColor(context, [UIColor colorWithRed:0.6 green:0.7 blue:0.2 alpha:1.0].CGColor);
    
    float botAxisHeight = (height - _yyOffset) / 2 - _yyOffset;
    float yyRatioBot = botAxisHeight / 4; //100
    float xxRatioBot = ((widht - _xxOffset) / [_xxLabel count]) / 6;

    if ([_arrPoints count] > 0) {
        
        [_arrPoints enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            
            CGPoint p = [[obj valueForKey:@"botPoints"] CGPointValue];
            p.y -= bmin;

            if (idx > 0) {
                
                CGPoint goPoint = CGPointMake((p.x * xxRatioBot) + _xxOffset, (height - _yyOffset) - (p.y * yyRatioBot / botOffset));
                goPoint.y += yyRatioBot;
                goPoint.x -= 3;
  
                for (int i = 1; i <= 5; i++) {
                    
                    float y1 = (height - _yyOffset - 1);
                    float y2 = goPoint.y;
                    if (y2 >= y1) {
                        goPoint.y = y1;
                    }
                    
                    CGContextMoveToPoint(context, goPoint.x + i, (height - _yyOffset - 1));
                    CGContextAddLineToPoint(context, goPoint.x + i, goPoint.y);
                }
            }
        }];
        
    }
    
    CGContextStrokePath(context);

}

@end
