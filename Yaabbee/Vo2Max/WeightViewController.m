//
//  WeightViewController.m
//  mynuvita
//
//  Created by John on 3/20/14.
//
//

#import "WeightViewController.h"
#import "ActivityLevelViewController.h"

@interface WeightViewController () <UITextFieldDelegate, JFButtonViewDelegate>

@property (retain, nonatomic) NuvitaXMLParser *nuvitaXMLParser;

@property (retain, nonatomic) NSString *height;
@property (retain, nonatomic) NSString *weight;
@property (retain, nonatomic) UITextField *heightlbl;
@property (retain, nonatomic) UITextField *weightlbl;
@property (retain, nonatomic) UITextField *selectedlbl;
@property (retain, nonatomic) IBOutlet UIPickerView *pickerview;
@property (retain, nonatomic) IBOutlet UITableView *tableview;

@property (retain, nonatomic) NSArray *weightArray;
@property (retain, nonatomic) NSArray *heightArray;
@property (retain, nonatomic) NSArray *componentArray1;
@property (retain, nonatomic) NSArray *componentArray2;
@property (retain, nonatomic) NSMutableArray *componentArray;


@property (assign) CGFloat constantOffset;
@property (assign) int sum;

@end

@implementation WeightViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        UIImageView *textView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"logo.png"]];
        [self.navigationItem setTitleView:textView];
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:YES];
    [self.tabBarController.tabBar setHidden:YES];
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    NSDictionary *info = [NSDictionary dictionaryWithObjectsAndKeys:
                          @"5", @"ft",
                          @"5", @"inch",
                          @"150", @"weight",
                          nil];
    
    if (![prefs objectForKey:@"scalling"]) {
        [prefs setObject:info forKey:@"scalling"];
        [prefs synchronize];
    }
    
    [self updateNavigationUI];
    [self loadComponentData];
}

//- (void)viewWillDisappear:(BOOL)animated {
//    [super viewWillDisappear:YES];
//    
//    BOOL isFromTest = [[[NSUserDefaults standardUserDefaults] valueForKey:@"isFromTest"] boolValue];
//    if (!isFromTest) {
//        NSArray *viewControllers = self.navigationController.viewControllers;
//        if ([viewControllers indexOfObject:self] == NSNotFound && [viewControllers count] == 1) {
//            // View is disappearing because it was popped from the stack
//            [self.tabBarController.tabBar setHidden:NO];
//            [(NuvitaAppDelegate *)[[UIApplication sharedApplication] delegate] performSelector:@selector(reloadAppDelegateRootViewController) withObject:nil afterDelay:1.0];
//        }
//    }
//}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationLandscapeRight || interfaceOrientation == UIInterfaceOrientationLandscapeLeft);
}

- (BOOL)shouldAutorotate {
    return YES;
}


- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskAll;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationLandscapeLeft;
}

- (void)updateNavigationUI {
    
    JFButtonView *buttonview = [[JFButtonView alloc] initWithFrame:CGRectMake(0, 0, 200, CGRectGetHeight(self.navigationController.navigationBar.frame))];
    [buttonview setButtonTitle:@"Next"];
    [buttonview setHeight:CGRectGetHeight(self.navigationController.navigationBar.frame)];
    [buttonview setBackButton:NO];
    [buttonview setDelegate:self];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:buttonview];
}

#pragma mark - JFButtonViewDelegate

- (void)buttonView:(JFButtonView *)view didTapButton:(JFButton *)button {
    
    if (![button backButton]) {
        [self didTapNext];
    }
}

#pragma mark - Public Method

- (void)showAlert:(NSString *)alert withMessage:(NSString *)message {
    
    UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:alert
                                                        message:message
                                                       delegate:nil
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles:nil];
    [alertview show];
}

- (void)didTapNext {
 
    self.navigationItem.rightBarButtonItem.enabled = NO;
   
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSDictionary *info = [NSDictionary dictionaryWithObjectsAndKeys:
                          [[_componentArray objectAtIndex:0] objectAtIndex:[_pickerview selectedRowInComponent:0]], @"ft",
                          [[_componentArray objectAtIndex:1] objectAtIndex:[_pickerview selectedRowInComponent:1]], @"inch",
                          [[_componentArray objectAtIndex:2] objectAtIndex:[_pickerview selectedRowInComponent:2]], @"weight",
                          nil];
    
    [prefs setObject:info forKey:@"scalling"];
    [prefs synchronize];

    [self getActivityLevel];
}

- (void)showActivityLevelQuestions {
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSDictionary *infox = [prefs objectForKey:@"scalling"];
    
    int feet = [[infox objectForKey:@"ft"] intValue] * 12;
    int heigt = feet + [[infox objectForKey:@"inch"] intValue];

    
    NSMutableDictionary *data = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                 [NSString stringWithFormat:@"%d", heigt], @"height",
                                 [infox objectForKey:@"weight"], @"weight",
                                 nil];
    [data setObject:[NSNumber numberWithBool:_noRecordFound] forKey:@"noRecord"];
    
    
    NSDictionary *info = _nuvitaXMLParser.info;
    ActivityLevelViewController *activityLevelViewController = [[ActivityLevelViewController alloc] init];
    activityLevelViewController.info = info;
    activityLevelViewController.data = data;
    
    activityLevelViewController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:activityLevelViewController animated:YES];
}

- (void)loadComponentData {
    
    _componentArray = [[NSMutableArray alloc] init];
    
    NSMutableArray *array = [[NSMutableArray alloc] init];
    for (int i = 0; i < 10; i ++) {
        [array addObject:[NSString stringWithFormat:@"%d", i+1]];
    }
    
    [_componentArray addObject:array];

    array = [[NSMutableArray alloc] init];
    for (int i = 0; i < 12; i ++) {
        [array addObject:[NSString stringWithFormat:@"%d", i]];
    }
    
    [_componentArray addObject:array];


    array = [[NSMutableArray alloc] init];
    for (int i = 30; i <= 500; i++) {
        [array addObject:[NSString stringWithFormat:@"%d", i]];
    }
    
    [_componentArray addObject:array];
    
    [_pickerview reloadAllComponents];
    [self performSelector:@selector(animateData) withObject:nil afterDelay:0.3];
    
}

- (void)animateData {
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSDictionary *info = [prefs objectForKey:@"scalling"];
  
    [_pickerview selectRow:[[_componentArray objectAtIndex:0] indexOfObject:[info valueForKey:@"ft"]] inComponent:0 animated:YES];
    [_pickerview selectRow:[[_componentArray objectAtIndex:1] indexOfObject:[info objectForKey:@"inch"]] inComponent:1 animated:YES];
    [_pickerview selectRow:[[_componentArray objectAtIndex:2] indexOfObject:[info objectForKey:@"weight"]] inComponent:2 animated:YES];
    self.navigationItem.rightBarButtonItem.enabled = YES;
}

#pragma mark - WS Method

- (void)getActivityLevel {
    request *r = [[request alloc] initWithTarget:self
                                   SuccessAction:@selector(onSucceffulFetchActivity:resData:)
                                   FailureAction:@selector(onFailedFetch)];
    [r getActivityLevel];
}

#pragma mark - WS Method Delegate

- (void)onSucceffulFetchActivity:(ResponseStatus *)responseStatus resData:(NSObject *)obj {
    _nuvitaXMLParser = (NuvitaXMLParser *)obj;
    if (_nuvitaXMLParser) {
        
        [self showActivityLevelQuestions];
    }
}

- (void)onFailedFetch {
    
}

#pragma mark - UIPickerViewDataSource

// returns the number of 'columns' to display.
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return [_componentArray count];
}

// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return [[_componentArray objectAtIndex:component] count];
}

#pragma mark - UIPickerViewDelegate

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    NSString *label = @"";
    switch (component) {
        case 0:
            label = @"ft";
            break;
        case 1:
            label = @"inch";
            break;
        case 2:
            label = @"lbs";
            break;
            
        default:
            break;
    }
    
    return [[[_componentArray objectAtIndex:component] objectAtIndex:row] stringByAppendingString:[NSString stringWithFormat:@" %@", label]];
    
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
//    self.navigationItem.rightBarButtonItem.enabled = YES;
}

//#pragma mark - UITableViewDataSource
//
//- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//    return 2;
//}
//
//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
//    return 1;
//}
//
//- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
//    return @"Height and weight are used to calculate your V02Max.";
//}
//
//- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
//    
//    static NSString *cellIdentifier = @"Cell";
//    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
//    
//    if (cell == nil) {
//        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellIdentifier];
//        cell.selectionStyle = UITableViewCellSelectionStyleNone;
//    }
//    
//    UIFont *font = [UIFont fontWithName:@"Arial" size:16];
//    cell.textLabel.font = font;
//    
//    CGRect frame = self.tableview.frame;
//    switch ([indexPath row]) {
//        case 0: {
//            cell.textLabel.text = @"Height";
//            
//            UITextField *txtField = (UITextField *)[cell.contentView viewWithTag:indexPath.row + 200];
//            if (txtField == nil) {
//                txtField = [[UITextField alloc]initWithFrame:CGRectMake(frame.size.width/2 - 10, 2, frame.size.width/2, 40)];
//                txtField.autoresizingMask = UIViewAutoresizingFlexibleHeight;
//                txtField.autoresizesSubviews = YES;
//                txtField.textAlignment = NSTextAlignmentRight;
//                txtField.delegate = self;
//                txtField.font = font;
//                
//                
//                [cell addSubview:txtField];
//            }
//            
//            _heightlbl = txtField;
//            txtField.text = _height;
//            
//        } break;
//            
//        case 1: {
//            cell.textLabel.text = @"Weight";
//            
//            UITextField *txtField = (UITextField *)[cell viewWithTag:indexPath.row + 200];
//            if (txtField == nil) {
//                txtField = [[UITextField alloc]initWithFrame:CGRectMake(frame.size.width/2 - 10, 2, frame.size.width/2, 40)];
//                txtField.tag = indexPath.row + 200;
//                txtField.autoresizingMask = UIViewAutoresizingFlexibleHeight;
//                txtField.autoresizesSubviews = YES;
//                txtField.textAlignment = NSTextAlignmentRight;
//                txtField.delegate = self;
//                txtField.font = font;
//                
//                
//                [cell addSubview:txtField];
//            }
//            
//            _weightlbl = txtField;
//            txtField.text = _weight;
//            
//        } break;
//            
//        default:
//            break;
//    }
//
//    return cell;
//}

//#pragma mark - UITableViewDelegate
//
//- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section {
//    if([view isKindOfClass:[UITableViewHeaderFooterView class]]){
//        UITableViewHeaderFooterView *tableViewHeaderFooterView = (UITableViewHeaderFooterView *) view;
//        tableViewHeaderFooterView.textLabel.font = [UIFont fontWithName:@"Arial" size:16];
//        tableViewHeaderFooterView.textLabel.text = [tableViewHeaderFooterView.textLabel.text capitalizedString];
//    }
//}
//
//- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
//    if (indexPath.row == 0) {
//        [_heightlbl becomeFirstResponder];
//        return;
//    }
//    
//    [_weightlbl becomeFirstResponder];
//}
//
//- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
//    return 1;
//}
//
//#pragma mark - UITextFieldDelegate
//
//- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
//    self.navigationItem.rightBarButtonItem.enabled = ([_heightlbl.text length] > 0 && [_weightlbl.text length] > 0);
//    
//    if (!string.length) {
//        return YES;
//    }
//    
//    if (textField.keyboardType == UIKeyboardTypeDecimalPad) {
//        if ([string rangeOfCharacterFromSet:[[NSCharacterSet decimalDigitCharacterSet] invertedSet]].location != NSNotFound && [string rangeOfString:@"."].location == NSNotFound) {
//            
//            return NO;
//        }
//    }
//    
//    return YES;
//}
//
//- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
//    self.navigationItem.rightBarButtonItem.enabled = ([_heightlbl.text length] > 0 && [_weightlbl.text length] > 0);
//    
//    [self reloadHeightData:textField];
//    [self animatePicker:NO];
//    return NO;
//}
//
//- (void)animatePicker:(BOOL)done {
//
//    CGRect frame = _pickerview.frame;
//    [UIView beginAnimations:nil context:NULL];
//    [UIView setAnimationDuration:.50];
//    [UIView setAnimationDelegate:self];
//    
//    int distance = (frame.origin.y == 320) ? frame.size.height : 0;
//    frame.origin.y += (!done) ? -distance : distance;
//    _pickerview.frame = frame;
//    
//    [UIView commitAnimations];
//}

//- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
//    [self.view endEditing:YES];
////    [self animatePicker:YES];
//}

@end