//
//  VO2MaxViewController.h
//  mynuvita
//
//  Created by John on 3/20/14.
//
//

#import "Vo2MaxGraph.h"
#import "MyTools.h"
#import "OfflineManager.h"
#import "LoginResponseData.h"
#import "LoginPerser.h"
#import "AppData.h"

#import "request.h"

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>
#import <CoreBluetooth/CoreBluetooth.h>

#import <AudioToolbox/AudioToolbox.h>
#import <AVFoundation/AVFoundation.h>

@interface VO2MaxViewController : UIViewController

@property (retain, nonatomic) NSDictionary *data;

@end
