//
//  WeightViewController.h
//  mynuvita
//
//  Created by John on 3/20/14.
//
//

#import <UIKit/UIKit.h>
#import "MyTools.h"
#import "request.h"
#import "NuvitaXMLParser.h"

@class WeightViewController;
@protocol WeightViewControllerDelegate <NSObject>

- (void)weightViewController:(WeightViewController *)viewController didFinishWithData:(NSDictionary *)data;

@end

@interface WeightViewController : UIViewController

@property (retain, nonatomic) id <WeightViewControllerDelegate> delegate;
@property (assign) BOOL noRecordFound;

@end
