//
//  Vo2MaxGraph.h
//  NuvitaCardio
//
//  Created by John on 5/16/14.
//
//

#import <UIKit/UIKit.h>

@interface Vo2MaxGraph : UIView

@property (retain, nonatomic) NSArray *arrPoints;

@property (assign) CGFloat maxline;
@property (assign) CGFloat minline;

@property (assign) CGFloat maxbar;
@property (assign) CGFloat minbar;

@end
