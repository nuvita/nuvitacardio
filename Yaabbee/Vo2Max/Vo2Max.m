//
//  Vo2Max.m
//  mynuvita
//
//  Created by John on 4/25/14.
//
//

#import "Vo2Max.h"

@implementation Vo2Max

- (id)init {
    self = [super init];
    if (self) {
        
        _rrValues = [[NSMutableArray alloc] init];
        _hrValues = [[NSMutableArray alloc] init];
        _dateCreated = @"";
        _height = 0;
        _weight = 0;
        _activityLevel = 0;
        _duration = 0;
    }
    
    return self;
}

- (void)saveRRInterval:(NSString *)rriStringValue {
    [_rrValues addObject:rriStringValue];
}

- (void)saveHRValue:(NSString *)hrStringValue {
    [_hrValues addObject:hrStringValue];
}

- (NSString *)getRRValuesString {
    NSString *rrValueString = [_rrValues componentsJoinedByString:@","];
    return rrValueString;
}

- (NSString *)getHRValuesString {
    NSString *hrValueString = [_rrValues componentsJoinedByString:@","];
    return hrValueString;
}

+ (NSString *)getDateCreated {
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
    [dateFormatter setTimeZone:timeZone];
    [dateFormatter setDateFormat:@"MM-dd-yyyy"];
    
    NSString *dateString = [dateFormatter stringFromDate:[NSDate date]];
    return dateString;
}

- (NSMutableString *)generateVo2MaxXML:(NSString *)memberID {
   
    NSMutableString *sRequest = [[NSMutableString alloc] init];

    [sRequest appendString:@"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\" xmlns:nuv=\"http://schemas.datacontract.org/2004/07/NuvitaMobileService\">"];
    
    [sRequest appendString:@"<soapenv:Header/>"];
    [sRequest appendString:@"<soapenv:Body>"];
    [sRequest appendString:@"<tem:GetVo2MaxFromRR>"];
    
    [sRequest appendFormat:@"<tem:memberId>%@</tem:memberId>", memberID];
    [sRequest appendFormat:@"<tem:date>%@</tem:date>", _dateCreated];
    [sRequest appendFormat:@"<tem:height>%.2f</tem:height>", (double)_height];
    [sRequest appendFormat:@"<tem:weight>%.2f</tem:weight>", (double)_weight];
    [sRequest appendFormat:@"<tem:ActivityLevel>%ld</tem:ActivityLevel>", (long)_activityLevel];
    [sRequest appendFormat:@"<tem:RRValues>%@</tem:RRValues>", [self getRRValuesString]];
    [sRequest appendFormat:@"<tem:HRValues>%@</tem:HRValues>", [self getHRValuesString]];
    [sRequest appendFormat:@"<tem:Duration>%ld</tem:Duration>", (long)_duration];
    [sRequest appendFormat:@"<tem:AverageHR>%ld</tem:AverageHR>", (long)_hrAverage];


    [sRequest appendString:@"</tem:GetVo2MaxFromRR>"];
    [sRequest appendString:@"</soapenv:Body>"];
    [sRequest appendString:@"</soapenv:Envelope>"];

    return sRequest;
}

//- (void)showDatePicker:(NSString *)label {
//    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:[label stringByAppendingString:@"\n\n\n\n\n\n\n\n\n\n\n\n\n"]
//                                                      delegate:self
//                                             cancelButtonTitle:@"Done"
//                                        destructiveButtonTitle:nil
//                                             otherButtonTitles:nil];
//
//    // Add the picker
//    UIDatePicker *pickerView = [[UIDatePicker alloc] init];
//    pickerView.datePickerMode = UIDatePickerModeDate;
//    pickerView.minimumDate = [self getDateFromStringFormat:_startDate];//(_startDate) ? [self getDateFromStringFormat:_startDate] : nil;
//    NSLog(@"minimum: %@", pickerView.minimumDate);
//    [pickerView addTarget:self action:@selector(didSelectDate:) forControlEvents:UIControlEventValueChanged];
//
//    [actionSheet addSubview:pickerView];
//    [actionSheet showFromTabBar:self.tabBarController.tabBar];
//
//    CGRect pickerRect = pickerView.bounds;
//    pickerRect.origin.y = -60;
//    pickerRect.size.width = self.tabBarController.view.bounds.size.width - 16;
//    pickerView.bounds = pickerRect;
//}
//
//- (void)didSelectDate:(id)sender {
//    _selectedDate = (NSDate *)[sender date];
//}
//
//- (NSDate *)getDateFromStringFormat:(NSString *)string {
//
//    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//    NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
//    [dateFormatter setTimeZone:timeZone];
//    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
//
//    NSDate *stringDate = [dateFormatter dateFromString:string];
//    return stringDate;
//}
//
//- (NSString *)getStringFromDateFormat:(NSDate *)date {
//
//    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//    NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
//    [dateFormatter setTimeZone:timeZone];
//    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
//
//    NSString *dateString = [dateFormatter stringFromDate:date];
//    return dateString;
//}
//
//#pragma mark - UIActionSheetDelegate
//
//- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
//
//    if (!_startDate) {
//
//        _startDate = [self getStringFromDateFormat:_selectedDate];
//
//        [self showDatePicker:@"Pick end date: "];
//        return;
//    }
//
//    _endDate = [self getStringFromDateFormat:_selectedDate];
//    [self getHealthMeasurementTrends];
//}

@end
