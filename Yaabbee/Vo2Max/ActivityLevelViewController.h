//
//  ActivityLevelViewController.h
//  mynuvita
//
//  Created by John on 5/8/14.
//
//

#import <UIKit/UIKit.h>

@class ActivityLevelViewController;
@protocol ActivityLevelViewControllerDelegate <NSObject>

- (void)activityLevelViewController:(ActivityLevelViewController *)viewController didFinishWithData:(NSDictionary *)data;

@end

@interface ActivityLevelViewController : UIViewController

@property (retain, nonatomic) id <ActivityLevelViewControllerDelegate> delegate;

@property (nonatomic, retain) NSDictionary *info;
@property (retain, nonatomic) NSMutableDictionary *data;

@end
