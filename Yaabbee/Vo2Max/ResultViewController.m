//
//  ResultViewController.m
//  NuvitaCardio
//
//  Created by John on 6/17/14.
//
//

#import "ResultViewController.h"
#import "BodyfatViewController.h"
#import "InitialTestViewController.h"

@interface ResultViewController ()

@property (nonatomic, retain) NSMutableArray *graphDataList;
@property (retain, nonatomic) NSMutableArray *normCategoryList;
@property (retain, nonatomic) Vo2MaxChart *vo2MaxChart;
@property (nonatomic, retain) OfflineManager *offlineManager;

@property (retain, nonatomic) IBOutlet UIView *lineGraphView;
@property (retain, nonatomic) IBOutlet UILabel *headerlbl;

@property (nonatomic, retain) MyTools *tools;
@property (nonatomic, retain) UIView *loadingView;

@property (assign) BOOL isDontSave;
@property (assign) BOOL isSuccess;

@end

@implementation ResultViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        UIImageView *textView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"logo.png"]];
        [self.navigationItem setTitleView:textView];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    _graphDataList = [[NSMutableArray alloc] init];
    _normCategoryList = [[NSMutableArray alloc] init];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
 
    _headerlbl.text = [NSString stringWithFormat:@"Test complete. Your VO2max measured today is %@, which place you in the %@ category for your age and gender. Do you want to save this result?", [_resultInfo valueForKey:@"a:Value"], [_resultInfo valueForKey:@"a:NormCategory"]];
    
    [self getHealthMeasurementCurrentTrends];
    [self formatToSuperscriptText];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Line Chart View

- (void)buildGraphUI {
    for (UIView *subview in [_lineGraphView subviews]) {
        [subview removeFromSuperview];
    }
    
    _vo2MaxChart = [[Vo2MaxChart alloc] initWithFrame:CGRectMake(0, 0, _lineGraphView.frame.size.width, _lineGraphView.frame.size.height)];
    _vo2MaxChart.backgroundColor = [UIColor whiteColor];
    
    [self setGraphPoints:_vo2MaxChart];
    [_lineGraphView addSubview:_vo2MaxChart];
}

- (void)setGraphPoints:(Vo2MaxChart *)chart {
    
    NSMutableArray *pointArr = [[NSMutableArray alloc] init];

    __block float max = 0.0;
    int ratio = ceil([_graphDataList count] / 5.0);
    [_graphDataList enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        
        NSDictionary *data = obj;
        if (data) {
            
            CGFloat value = [[data objectForKey:@"a:Value"] doubleValue];
            if (max < value)
                max = value;
            
            
            NSDictionary *info = [NSDictionary dictionaryWithObjectsAndKeys:
                                  [NSValue valueWithCGPoint:CGPointMake(idx, (double)value)], @"points",
                                  [data valueForKey:@"a:Date"], @"label",
                                  [data valueForKey:@"a:MeasurementTypeId"], @"type",
                                  nil];
            
            [pointArr addObject:info];
        }
        
    }];
    
    if (max > [[[_normCategoryList lastObject] valueForKey:@"a:Value"] floatValue]) {
        [_normCategoryList addObject:[NSDictionary dictionaryWithObjectsAndKeys:
                                      [NSNumber numberWithInteger:max + 5], @"a:Value",
                                      @"", @"a:Label",
                                      nil]];
    }
    
    _vo2MaxChart.ratio = ratio;
    [_vo2MaxChart setNormCategoryList:_normCategoryList];
    [_vo2MaxChart setXDataList:pointArr];
    
}

#pragma mark - Public Methods

- (void)formatToSuperscriptText {
    for (id subview in [self.view subviews]) {
        if ([subview isKindOfClass:[UILabel class]]) {
            
            UILabel *label = (UILabel *)subview;
            label.text = [[[label.text stringByReplacingOccurrencesOfString:@"vo2" withString:@"VO\u00B2"] stringByReplacingOccurrencesOfString:@"VO2" withString:@"VO\u00B2"] stringByReplacingOccurrencesOfString:@"Vo2" withString:@"VO\u00B2"];
        }
    }
}

- (void)loadData:(NSDictionary *)data {
    
    if (data) {
        
        [_normCategoryList removeAllObjects];
        [_normCategoryList addObjectsFromArray:[data valueForKey:@"a:HealthTrendsNorms"]];
        
        NSArray *healthTrendsSeriesSet = [data valueForKey:@"a:HealthTrendsSeriesSet"];
        [healthTrendsSeriesSet enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            
            if ([[obj valueForKey:@"a:MeasurementTypeId"] intValue] == 20 || [[obj valueForKey:@"a:MeasurementTypeId"] intValue] == 11) {
                [_graphDataList removeAllObjects];
                
                NSArray *healthTrends = [obj valueForKey:@"a:HealthTrends"];
                for (NSMutableDictionary *data in healthTrends) {
                    
                    [data setObject:[obj valueForKey:@"a:MeasurementTypeId"] forKey:@"a:MeasurementTypeId"];
                    [_graphDataList addObject:data];
                }
            }
            
        }];
        
        [self buildGraphUI];
    }
}

- (void)initOfflineManager {
    
    NuvitaAppDelegate *appDelegate = (NuvitaAppDelegate *)[[UIApplication sharedApplication] delegate];
    self.offlineManager = [[OfflineManager alloc] init];
    self.offlineManager.managedObjectContext = appDelegate.managedObjectContext;
}

- (void)showProgram {
    
    BodyfatViewController *bodyfatViewController = [[BodyfatViewController alloc] init];
    bodyfatViewController.vo2max = [_resultInfo objectForKey:@"a:Value"];

    [self.navigationController pushViewController:bodyfatViewController animated:YES];
}

#pragma mark - IBAction

- (IBAction)didChooseOption:(id)sender {
    
    if ([sender tag] == 1) {
        [self showProgram];
        return;
    }
    
    [self deleteVo2MaxFromRR];
}

- (void)popToBeginningView {
    [self.navigationController.viewControllers enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        
        UIViewController *viewController = (UIViewController *)obj;
        if ([NSStringFromClass([viewController class]) isEqualToString:@"InstructionViewController"] || [NSStringFromClass([viewController class]) isEqualToString:@"VO2MaxChartViewController"]) {
            [self.navigationController popToViewController:viewController animated:YES];
        }
    }];
}

- (void)dismissResult:(BOOL)success {
    [self popToBeginningView];
}

- (void)repeatTest {
    
//    [[NSNotificationCenter defaultCenter] postNotificationName:@"RECONNECT" object:nil];
    [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:1] animated:YES];
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {

    [self getHealthMeasurementCurrentTrends];
}

#pragma mark - Web Service Request

- (void)getHealthMeasurementCurrentTrends {
    
    [_tools startLoading:[self.navigationController.topViewController view] childView:_loadingView text:@"Loading..."];
    
    LoginResponseData *loginResponseData = [LoginPerser getLoginResponse];
    NSDictionary *data = [NSDictionary dictionaryWithObjectsAndKeys:[loginResponseData memberID], @"memberId", nil];
    
    request *r = [[request alloc] initWithTarget:self
                                   SuccessAction:@selector(onSuccessfulFetchRequest:resData:)
                                   FailureAction:@selector(onFailedFetch)];
    
    [r getHealthMeasurementCurrentTrends:data];
}

- (void)deleteVo2MaxFromRR {
    [_tools startLoading:self.tabBarController.view childView:_loadingView text:@"Loading..."];
    
    LoginResponseData *loginResponseData = [LoginPerser getLoginResponse];
    request *r = [[request alloc] initWithTarget:self
                                   SuccessAction:@selector(onSuccessfulDeleteVo2max:resData:)
                                   FailureAction:@selector(onFailedFetch)];
    
    NSDictionary *data = [NSDictionary dictionaryWithObjectsAndKeys:
                          [loginResponseData memberID], @"memberId",
                          [_resultInfo objectForKey:@"a:Date"], @"Date",
                          nil];
    
    [r deleteVo2MaxXML:data];
}

- (void)onSuccessfulFetchRequest:(ResponseStatus *)responseStatus resData:(NSObject *)obj {
    [_tools stopLoading:_loadingView];
    
    _isSuccess = [responseStatus isSuccess];
    if (_isDontSave) {
        [self dismissResult:_isSuccess];
        return;
    }
    
    if ([responseStatus isSuccess]) {
        
        NSDictionary *info = [[NSDictionary alloc] initWithDictionary:[(NuvitaXMLParser *)obj info]];
        [self loadData:info];
    }
}

- (void)onSuccessfulDeleteVo2max:(ResponseStatus *)responseStatus resData:(id)obj {
    [_tools stopLoading:_loadingView];

    if ([responseStatus isSuccess]) {
        _isDontSave = YES;
        UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:@"VO\u00B2max"
                                                            message:@"Your result was not saved."
                                                           delegate:self
                                                  cancelButtonTitle:@"Ok"
                                                  otherButtonTitles:nil];
        [alertview show];
    }
}

- (void)onFailedFetch {
    
    [_tools stopLoading:_loadingView];
    UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:@"Error"
                                                        message:@"Something went wrong! Would you like to report this issue?"
                                                       delegate:nil
                                              cancelButtonTitle:@"No"
                                              otherButtonTitles:@"Yes", nil];
    [alertview show];
}

@end
