//
//  ConnectViewController.m
//  mynuvita
//
//  Created by John on 5/14/14.
//
//

#import "ConnectViewController.h"
#import "VO2MaxViewController.h"
#import "ResultViewController.h"

@interface ConnectViewController () <JFButtonViewDelegate>

@property (nonatomic, retain) NSTimer *pulseTimer;
@property (assign) uint16_t heartRate;
@property (retain, nonatomic) IBOutlet UIButton *heartImageBtn;

@property (retain, nonatomic) IBOutlet UILabel *headerlbl;
@property (retain, nonatomic) IBOutlet UITableView *tableview;
@property (retain, nonatomic) NSArray *list;

@end

@implementation ConnectViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        UIImageView *textView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"logo.png"]];
        [self.navigationItem setTitleView:textView];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateWithHRMData:)
                                                 name:@"ADDHRM"
                                               object:nil];

    _list = [[NSArray alloc] initWithObjects:
             @"Make sure your NuvitaCardio monitor is properly attached and you are seeing your heart rate in the middle of the red heart.",
             @"Sit or lie in a quiet place for a few minutes before beginning the test. The test will take 3-5 minutes once started.",
             @"Refrain from movement or conversation during the test and try to relax as much as possible.",
             @"You will get the best results if it has been 12 hours or more since exercise.",
             @"Any conditions or substances that effect heart rate may effect the results.",
             nil];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    [[CheckUserHistory sharedAppData] setIsVO2Starting:YES];
    [self updateNavigationUI];
    
    _headerlbl.text = @"VO\u00B2max test instructions";
}

- (void)viewWillDisappear:(BOOL)animated {
    self.navigationItem.rightBarButtonItem.enabled = NO;
    [[CheckUserHistory sharedAppData] setIsVO2Starting:NO];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [super viewWillDisappear:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)updateNavigationUI {
    
    JFButtonView *buttonview = [[JFButtonView alloc] initWithFrame:CGRectMake(0, 0, 200, CGRectGetHeight(self.navigationController.navigationBar.frame))];
    [buttonview setButtonTitle:@"Start"];
    [buttonview setHeight:CGRectGetHeight(self.navigationController.navigationBar.frame)];
    [buttonview setBackButton:NO];
    [buttonview setDisableButton:YES];
    [buttonview setDelegate:self];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:buttonview];
    self.navigationItem.rightBarButtonItem.enabled = NO;
}

#pragma mark - JFButtonViewDelegate

- (void)buttonView:(JFButtonView *)view didTapButton:(JFButton *)button {
    
    if (![button backButton]) {
        [self didTapNext];
    }
}

#pragma mark - Central Manager Public Methods

- (void)didTapNext {
    
    self.navigationItem.rightBarButtonItem = nil;
    VO2MaxViewController *voMaxViewController = [[VO2MaxViewController alloc] init];
    voMaxViewController.data = _data;
    voMaxViewController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:voMaxViewController animated:YES];
}

- (IBAction)didTapConnectBtn:(id)sender {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"RECONNECT" object:nil];
}

- (void)updateWithHRMData:(NSNotification *)notification {
    
    NSData *data = (NSData *)notification.object;
    if(data == nil) return;

    const uint8_t *reportData = [data bytes];
    uint16_t bpm = 0;
    
    if ((reportData[0] & 0x01) == 0) {
        bpm = reportData[1];
    } else {
        bpm = CFSwapInt16LittleToHost(*(uint16_t *)(&reportData[1]));
    }

    if (bpm != 0) {
        
        JFButtonView *buttonview = (JFButtonView *)self.navigationItem.rightBarButtonItem.customView;
        [buttonview setDisableButton:NO];
        [buttonview setButtonTitle:@"Start"];
        
        [self.navigationItem.rightBarButtonItem setEnabled:YES];
        
        [self.heartImageBtn setTitle:[NSString stringWithFormat:@"%d", bpm] forState:UIControlStateNormal];
        self.heartRate = bpm;
        self.pulseTimer = [NSTimer scheduledTimerWithTimeInterval:(60. / self.heartRate)
                                                           target:self
                                                         selector:@selector(doHeartBeat)
                                                         userInfo:nil
                                                          repeats:NO];
    } else {
        
        [self.navigationItem.rightBarButtonItem setEnabled:NO];
        [self.heartImageBtn setTitle:@"Connect" forState:UIControlStateNormal];
        self.heartRate = 0;
    }
}

- (void)doHeartBeat {
    
    CALayer *layer = [self heartImageBtn].layer;
    CABasicAnimation *pulseAnimation = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
    pulseAnimation.toValue = [NSNumber numberWithFloat:1.1];
    pulseAnimation.fromValue = [NSNumber numberWithFloat:1.0];
    
    pulseAnimation.duration = 60. / self.heartRate / 2.;
    pulseAnimation.repeatCount = 1;
    pulseAnimation.autoreverses = YES;
    pulseAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
    [layer addAnimation:pulseAnimation forKey:@"scale"];
}

#pragma mark - UITableViewDataSource UITableViewDelegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_list count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.textLabel.font = [UIFont fontWithName:@"Arial" size:13];
    cell.textLabel.textAlignment = NSTextAlignmentLeft;
    cell.textLabel.numberOfLines = 0;
    cell.textLabel.text = [_list objectAtIndex:indexPath.row];
    
    cell.imageView.image = [UIImage imageNamed:@"default-radio.png"];
    return cell;
}

//- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
//
//    if ([cell respondsToSelector:@selector(tintColor)]) {
//        CGRect frame = cell.frame;
//
//        CGSize size = [[_list objectAtIndex:indexPath.row] sizeWithFont:[UIFont fontWithName:@"Arial" size:[NuvitaAppDelegate isPhone5] ? 13 : 11.5]];
//
//        NSLog(@"cell line: %f", roundf((size.width / (tableView.frame.size.width - 38))));
////        CGSize size = [[_list objectAtIndex:indexPath.row] sizeWithFont:[UIFont fontWithName:@"Arial" size:[NuvitaAppDelegate isPhone5] ? 13 : 11.5]];
//        int lines = roundf(size.width / (tableView.frame.size.width - 38));
//        frame.size.height = size.height * lines;
//    }
//}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {

    CGSize size = [[_list objectAtIndex:indexPath.row] sizeWithFont:[UIFont fontWithName:@"Arial" size:13]];
    NSLog(@"height: %f", size.height);
    int lines = roundf(size.width / tableView.frame.size.width) + 1;
    NSLog(@"final: %f", size.height * lines);
    return size.height * lines;
}

@end
