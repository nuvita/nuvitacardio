//
//  BodyfatViewController.m
//  mynuvita
//
//  Created by John on 3/20/14.
//
//

#import "BodyfatViewController.h"
#import "InitialTestViewController.h"
#import "StatViewController.h"

#define FAKE_TABLE_HEIGHT 44
#define FAKE_TABLE_WIDTH 180
#define FAKE_TABLE_XOFFSET 15
#define FAKE_TABLE_YOFFSET 150

@interface BodyfatViewController ()

@property (retain, nonatomic) IBOutlet UIView *vo2trendGraphBody;
@property (retain, nonatomic) IBOutlet UIView *cardioGraphBody;
@property (nonatomic, retain) MyTools *tools;
@property (nonatomic, retain) UIView *loadingView;
@property (retain, nonatomic) IBOutlet UILabel *label;

@property (retain, nonatomic) InitialTestViewController *initialTestViewController;
@property (retain, nonatomic) OfflineManager *offlineManager;
@property (assign) BOOL isFirstTime;
@property (assign) BOOL finish;

@end

@implementation BodyfatViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        UIImageView *textView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"logo.png"]];
        [self.navigationItem setTitleView:textView];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    _tools = [[MyTools alloc] init];
    _loadingView = [_tools createActivityIndicatorFromView:self.tabBarController.view];
    _isFirstTime = NO;
    
    [self initOfflineManager];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    [self.tabBarController.tabBar setHidden:YES];
    
    LoginResponseData *loginData = [LoginPerser getLoginResponse];
    _label.text = [NSString stringWithFormat:@"%@, here is your recommended cardio program based on your recent exercise history and current Vo2max of %@.", [loginData firstName], _vo2max];
    
    
    
    if ([[AppData sharedAppData] activeInitialTestViewController]) {
        _initialTestViewController = [[AppData sharedAppData] activeInitialTestViewController];
        [self removeCorebluetoothConnection];
    }
    
    if ([[AppData sharedAppData] activeCentralManagerClass]) {
        StatViewController *viewController = (StatViewController *)[[AppData sharedAppData] activeCentralManagerClass];
        [self removeCBCentralManagerConnection:viewController];
    }
    
    [self getProposedCardioProgram];
}

- (void)removeCBCentralManagerConnection:(StatViewController *)controller {
    
    if (controller.peripheral != nil) {
        [controller.manager stopScan];
        [controller.manager cancelPeripheralConnection:controller.peripheral];
        [controller.peripheral setDelegate:nil];
        
    }
    controller.peripheral = nil;
    controller.manager = nil;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationLandscapeRight || interfaceOrientation == UIInterfaceOrientationLandscapeLeft);
}

- (BOOL)shouldAutorotate {
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskAll;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationLandscapeRight;
}

#pragma mark - Public Methods

- (void)initOfflineManager {
    
    NuvitaAppDelegate *appDelegate = (NuvitaAppDelegate *)[[UIApplication sharedApplication] delegate];
    self.offlineManager = [[OfflineManager alloc] init];
    self.offlineManager.managedObjectContext = appDelegate.managedObjectContext;
}

- (void)resetButtons {
    
    for (id object in [self.view subviews]) {
        if ([object isKindOfClass:[UIButton class]]) {
            UIButton *button = (UIButton *)object;
            [button setHidden:YES];
            [button removeTarget:self action:@selector(didTapOption:) forControlEvents:UIControlEventTouchUpInside];
            
            if ([button tag] == 2) {
                [button setHidden:NO];
                [button setTag:5];
                [button setTitle:@"Finish" forState:UIControlStateNormal];
                [button addTarget:self action:@selector(didTapFinish) forControlEvents:UIControlEventTouchUpInside];
            }
        }
    }
}

- (void)removeCorebluetoothConnection {
    
    if (_initialTestViewController.peripheral) {
        [_initialTestViewController.manager stopScan];
        [_initialTestViewController.manager cancelPeripheralConnection:_initialTestViewController.peripheral];
        [_initialTestViewController.peripheral setDelegate:nil];
        
    }
    _initialTestViewController.peripheral = nil;
    _initialTestViewController.manager = nil;
}

- (IBAction)didTapOption:(id)sender {
    
    //...disable temporarily button
    UIButton *button = (UIButton *)sender;
    button.enabled = NO;
    
    if ([sender tag] == 1) {
        [self saveProposedCardioProgram];
    }else {
        UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:@"Success"
                                                            message:@"You have chosen to keep your current cardio program."
                                                           delegate:self
                                                  cancelButtonTitle:@"Ok"
                                                  otherButtonTitles:nil];
        [alertview show];
    }
}

- (void)didTapFinish {
    
    _finish = YES;
    [self saveProposedCardioProgram];
}

- (void)buildTableUI:(NSDictionary *)data {

    if ([[CheckUserHistory sharedAppData] fromTest]) [self removeCorebluetoothConnection];
    if ([[data objectForKey:@"a:CurrentTargetZonesHigh"] isEqualToString:@"0"]) {
        _isFirstTime = YES;
        [self resetButtons];
    }
    
    int tagOffset = 100;
    int y = (_isFirstTime) ? (self.view.frame.size.height / 2) - (FAKE_TABLE_HEIGHT * 2 / 2) + 20 : (self.view.frame.size.height / 2) - (FAKE_TABLE_HEIGHT * 2 / 2);
    int count = (_isFirstTime) ? 2 : 3;
    for (int i = 0; i < count; i++) {
 
        int height = (i == 0) ? FAKE_TABLE_HEIGHT + 20 : FAKE_TABLE_HEIGHT;
        if (i > 0) {
            
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(FAKE_TABLE_XOFFSET, y, FAKE_TABLE_WIDTH, height)];
            label.textColor = (i == 1 && !_isFirstTime) ? [UIColor lightGrayColor] : [UIColor blackColor];
            label.textAlignment = NSTextAlignmentCenter;
            label.layer.borderColor = [UIColor blackColor].CGColor;
            label.layer.borderWidth = 2.0f;
            label.tag = tagOffset;
            
            [self.view addSubview:label];
        }
        
        
        int x = FAKE_TABLE_XOFFSET + FAKE_TABLE_WIDTH - 2;
        for (int j = 0; j < 2; j++) {
            
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(x, y, FAKE_TABLE_WIDTH, height)];
            label.textColor = (i == 1 && !_isFirstTime) ? [UIColor lightGrayColor] : [UIColor blackColor];
            label.textAlignment = NSTextAlignmentCenter;
            label.layer.borderColor = [UIColor blackColor].CGColor;
            label.layer.borderWidth = 2.0f;
            label.tag = tagOffset + j + 1;
            
            [self.view addSubview:label];
            x += FAKE_TABLE_WIDTH - 2;
        }
        
        tagOffset += 100 ;
        y += height - 2;
    }
    
    [self loadTableData:data];
}

- (void)loadTableData:(NSDictionary *)data {

    UIColor *blueclr = [UIColor colorWithRed:0.415 green:0.568 blue:1.0 alpha:1.0];
    UIFont *headerFont = [UIFont boldSystemFontOfSize:14.0];
    UIFont *defaultFont = [UIFont systemFontOfSize:14.0];
    
    UILabel *label01 = (UILabel *)[self.view viewWithTag:101];
    label01.font = headerFont;
    label01.backgroundColor = blueclr;
    label01.textColor = [UIColor whiteColor];
    
    UILabel *label02 = (UILabel *)[self.view viewWithTag:102];
    label02.font = headerFont;
    label02.backgroundColor = blueclr;
    label02.textColor = [UIColor whiteColor];
    
    UILabel *label04 = (UILabel *)[self.view viewWithTag:200];
    label04.font = defaultFont;
    
    UILabel *label05 = (UILabel *)[self.view viewWithTag:300];
    label05.font = defaultFont;
    
    UILabel *label06 = (UILabel *)[self.view viewWithTag:201];
    label06.font = defaultFont;
    
    UILabel *label07 = (UILabel *)[self.view viewWithTag:202];
    label07.font = defaultFont;

    UILabel *label09 = (UILabel *)[self.view viewWithTag:301];
    label09.font = defaultFont;
    
    UILabel *label10 = (UILabel *)[self.view viewWithTag:302];
    label10.font = defaultFont;

    label01.text = @"Target Zones";
    label02.text = @"Weekly time in zone";
    
    label04.text = (_isFirstTime) ? @"Your Personal Program" : @"Current Program";
    label05.text = @"New Program";
    
    if (_isFirstTime) {
        
        label06.text = [NSString stringWithFormat:@"%@ - %@", [data objectForKey:@"a:NewTargetZonesLow"], [data objectForKey:@"a:NewTargetZonesHigh"]];
        label07.text = [NSString stringWithFormat:@"%@ min", [data objectForKey:@"a:NewMinutesInZone"]];
        return;
    }
    
    label06.text = [NSString stringWithFormat:@"%@ - %@", [data objectForKey:@"a:CurrentTargetZonesLow"], [data objectForKey:@"a:CurrentTargetZonesHigh"]];
    label07.text = [NSString stringWithFormat:@"%@ min", [data objectForKey:@"a:CurrentMinutesInZone"]];
    
    label09.text = [NSString stringWithFormat:@"%@ - %@", [data objectForKey:@"a:NewTargetZonesLow"], [data objectForKey:@"a:NewTargetZonesHigh"]];
    label10.text = [NSString stringWithFormat:@"%@ min", [data objectForKey:@"a:NewMinutesInZone"]];
    
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"update_connect_btn" object:[NSNumber numberWithBool:YES]];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"FINISH_INITIAL_TEST" object:[NSNumber numberWithInteger:[[CheckUserHistory sharedAppData] fromTest] ? 0 : 4]];
}

#pragma mark - Web Service

- (void)getProposedCardioProgram {
    
    [_tools startLoading:self.tabBarController.view childView:_loadingView text:@"Loading..."];
    
    LoginResponseData *loginResponseData = [LoginPerser getLoginResponse];
    request *r = [[request alloc] initWithTarget:self
                                   SuccessAction:@selector(onGetProposedCardioProgram:resData:)
                                   FailureAction:@selector(onFailedFetch)];
    
    NSDictionary *data = [NSDictionary dictionaryWithObjectsAndKeys:
                          [loginResponseData memberID], @"memberId",
                          [Vo2Max getDateCreated], @"Date", nil];
    
    [r getProposedCardioProgram:data];
}

- (void)saveProposedCardioProgram {
    
    [_tools startLoading:self.tabBarController.view childView:_loadingView text:@"Loading..."];
    
    LoginResponseData *loginResponseData = [LoginPerser getLoginResponse];
    request *r = [[request alloc] initWithTarget:self
                                   SuccessAction:@selector(onSaveProposedCardioProgram:resData:)
                                   FailureAction:@selector(onFailedFetch)];
    
    NSDictionary *data = [NSDictionary dictionaryWithObjectsAndKeys:
                          [loginResponseData memberID], @"memberId",
                          [Vo2Max getDateCreated], @"Date", nil];
    
    [r saveProposedCardioProgram:data];
}

- (void)onGetProposedCardioProgram:(ResponseStatus *)responseStatus resData:(id)obj {
    [_tools stopLoading:_loadingView];

    NSDictionary *info = [[NSDictionary alloc] initWithDictionary:[(NuvitaXMLParser *)obj info]];
    [self buildTableUI:info];
}

- (void)onSaveProposedCardioProgram:(ResponseStatus *)responseStatus resData:(id)obj {
    [_tools stopLoading:_loadingView];
    
    if (_finish) {
        UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:@"Success"
                                                            message:@"You now have a cardio program personalized to your current Vo2max."
                                                           delegate:self
                                                  cancelButtonTitle:@"Ok"
                                                  otherButtonTitles:nil];
        [alertview show];
        return;
    }

    if (!_isFirstTime) {
        NSDictionary *info = [(NuvitaXMLParser *)obj info];
        UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:@"Success"
                                                            message:[info objectForKey:@"a:Message"]
                                                           delegate:self
                                                  cancelButtonTitle:@"Ok"
                                                  otherButtonTitles:nil];
        [alertview show];
    }
    
}

- (void)onFailedFetch {
    
    [_tools stopLoading:_loadingView];
    UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:@"Error"
                                                        message:@"Something went wrong! Would you like to report this issue?"
                                                       delegate:nil
                                              cancelButtonTitle:@"No"
                                              otherButtonTitles:@"Yes", nil];
    [alertview show];
}

@end
