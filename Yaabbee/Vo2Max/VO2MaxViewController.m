//
//  VO2MaxViewController.m
//  mynuvita
//
//  Created by John on 3/20/14.
//
//

#import "VO2MaxViewController.h"
#import "ResultViewController.h"

#import "Vo2Max.h"

#define HRM_LOW_VALUE 70
#define HRM_MAX_VALUE 90

@interface VO2MaxViewController () {

    NSTimeInterval startSessionTime;
    NSInteger exerciseCount;
    NuvitaXMLParser *nuvitaXMLParser;
}

@property (retain, nonatomic) IBOutlet UIView *chartBodyView;
@property (nonatomic, retain) Vo2MaxGraph *vo2MaxGraph;

@property (retain, nonatomic) IBOutlet UILabel *timerlbl;
@property (retain, nonatomic) IBOutlet UILabel *rrLabel;
@property (retain, nonatomic) IBOutlet UILabel *hrmlbl;

@property (nonatomic, strong) NSMutableDictionary *heartRateSamples;
@property (nonatomic, strong) NSMutableDictionary *rriRateSamples;

@property (nonatomic, retain) MyTools *tools;
@property (nonatomic, retain) UIView *loadingView;
@property (nonatomic, retain) UITextField *weightfld;
@property (nonatomic, retain) UITextField *heightfld;

@property (nonatomic, retain) Vo2Max *vo2max;
@property (assign) BOOL isDone;

@property (retain, nonatomic) NSTimer *nsTimer;
@property (assign) NSInteger initialCountdown;

@property (retain, nonatomic) NSTimer *lostConnectionTimer;
@property (assign) NSInteger lostConnectionCount;
@property (assign) NSInteger lostConnectionCountLimit;

@end

@implementation VO2MaxViewController

#pragma mark - View Life Cycle

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        UIImageView *textView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"logo.png"]];
        [self.navigationItem setTitleView:textView];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.hidesBackButton = YES;

    // Do any additional setup after loading the view from its nib.
    _tools = [[MyTools alloc] init];
    _loadingView = [_tools createActivityIndicatorFromView:self.tabBarController.view];
    
    self.heartRateSamples = [[NSMutableDictionary alloc] init];
    self.rriRateSamples = [[NSMutableDictionary alloc] init];
    [self buildGraphUI];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Cancel"
                                                                              style:UIBarButtonItemStylePlain
                                                                             target:self
                                                                             action:@selector(didTapDone)];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateWithHRMData:)
                                                 name:@"ADDHRM"
                                               object:nil];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    [[CheckUserHistory sharedAppData] setIsVO2Starting:YES];
    
    if (!_vo2max && !_isDone) {
  
        _vo2max = [[Vo2Max alloc] init];
        _vo2max.dateCreated = [Vo2Max getDateCreated];
        _vo2max.activityLevel = [[_data valueForKey:@"activity"] integerValue];
        _vo2max.height = [[_data valueForKey:@"height"] floatValue];
        _vo2max.weight = [[_data valueForKey:@"weight"] floatValue];
        
        startSessionTime = [[NSDate date] timeIntervalSince1970];
        [self startExerciseTimer];
        
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [[CheckUserHistory sharedAppData] setIsVO2Starting:NO];
    if (_nsTimer) {
        [_nsTimer invalidate];
        _nsTimer = nil;
    }

    if (_lostConnectionTimer) {
        [_lostConnectionTimer invalidate];
        _lostConnectionTimer = nil;
    }
    [super viewWillDisappear:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationLandscapeRight || interfaceOrientation == UIInterfaceOrientationLandscapeLeft);
}

- (BOOL)shouldAutorotate {
    return YES;
}


- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskAll;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationLandscapeRight	;
}

#pragma mark - Public Method

- (void)popToBeginningView {
    [self.navigationController.viewControllers enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        
        UIViewController *viewController = (UIViewController *)obj;
        if ([NSStringFromClass([viewController class]) isEqualToString:@"InstructionViewController"] || [NSStringFromClass([viewController class]) isEqualToString:@"VO2MaxChartViewController"]) {
            [self.navigationController popToViewController:viewController animated:YES];
        }
    }];
}

- (void)didTapDone {
    _isDone = YES;
    [self popToBeginningView];
}

- (void)buildGraphUI {
    for (UIView *subview in [self.chartBodyView subviews]) {
        [subview removeFromSuperview];
    }
    
    _vo2MaxGraph = [[Vo2MaxGraph alloc]initWithFrame:CGRectMake(0, 0, self.chartBodyView.frame.size.width, self.chartBodyView.frame.size.height)];
    _vo2MaxGraph.tag = 999;
    _vo2MaxGraph.backgroundColor = [UIColor whiteColor];
  
    [self setGraphPoints:_vo2MaxGraph];
    [self.chartBodyView addSubview:_vo2MaxGraph];
}

- (void)updateGraph {
  
    Vo2MaxGraph *vo2maxGraph = (Vo2MaxGraph *) [self.chartBodyView viewWithTag:999];
    [self setGraphPoints:vo2maxGraph];
    
    [vo2maxGraph setNeedsDisplay];
}

- (void)setGraphPoints:(Vo2MaxGraph *)vo2maxGraph {
    
    NSMutableArray *pointArr = [[NSMutableArray alloc]init];
    NSInteger durationSeconds = exerciseCount;
    
    float max = HRM_MAX_VALUE;
    float min = HRM_LOW_VALUE;
    
    float bmax = 750;
    float bmin = 400;
    
    NSInteger gap = durationSeconds/30;
    NSInteger ratio = gap/11 + 1;
    
    if(self.heartRateSamples != nil) {
        
        
        NSInteger count = 0;
        int kk = 0;
        int hrmVal = 0;
        int rriVal = 0;
        
        
        NSInteger numOfItem = durationSeconds/5;
        
        for (int ii = 0; ii < numOfItem; ii++) {
            
            NSString *key = [NSString stringWithFormat:@"%d", ii];
            NSString *rrikey = [NSString stringWithFormat:@"%d", ii];
            
            if([self.heartRateSamples valueForKey:key] != nil) {
                hrmVal += [[self.heartRateSamples valueForKey:key] integerValue];
            } else
                hrmVal += 0;
            
            if ([self.rriRateSamples valueForKey:rrikey] != nil) {
                rriVal += [[self.rriRateSamples valueForKey:rrikey] integerValue];
            }else
                rriVal += bmin;
            
            
            if(++kk == ratio) {

                int hrmAvg = hrmVal/ratio;
                int rriAvg = rriVal/ratio;//(rriVal/ratio == 0) ? bmin : rriVal/ratio;
                
                min = (hrmAvg < min) ? min = hrmAvg : min;
                max = (hrmAvg > max) ? max = hrmAvg : max;
                bmin = (rriAvg < bmin) ? bmin = rriAvg : bmin;
                bmax = (rriAvg > bmax) ? bmax = rriAvg : bmax;
                
                int index = count ++;
                NSDictionary *info = [NSDictionary dictionaryWithObjectsAndKeys:
                                      [NSValue valueWithCGPoint:CGPointMake(index, hrmAvg)], @"topPoints",
                                      [NSValue valueWithCGPoint:CGPointMake(index, rriAvg)], @"botPoints",
                                      nil];
                
                [pointArr addObject:info];
                
                kk = 0;
                hrmVal = 0;
                rriVal = 0;
            }
            
            [vo2maxGraph setMaxline:max];
            [vo2maxGraph setMinline:min];
            [vo2maxGraph setMaxbar:bmax];
            [vo2maxGraph setMinbar:bmin];
        }
        
        [vo2maxGraph setArrPoints:pointArr];
    }
 
}


- (void)updateWithHRMData:(NSNotification *)notification {
   
    NSData *data = (NSData *)notification.object;
    if(data == nil) return;
    
    const uint8_t *reportData = [data bytes];
    uint16_t bpm = 0;
    uint16_t rri = 0;

    if ((reportData[0] & 0x01) == 0) {
        bpm = reportData[1];
    } else {
        bpm = CFSwapInt16LittleToHost(*(uint16_t *)(&reportData[1]));
    }

    //...Display hr and save
    if (bpm != 0)
        [self addHRM:bpm];
    _hrmlbl.text = [NSString stringWithFormat:@"HR: %d bpm", bpm];

    //...check if lost connection
    if (bpm == 0) {
        if (!_lostConnectionTimer)
            [self peripheralDidLostConnection];
    } else {

        if (_lostConnectionTimer) {
            [_lostConnectionTimer invalidate];
            _lostConnectionTimer = nil;
        }
    }

    //...Get RRI
    if ((reportData[0] & 0x04) == 0) {
        NSLog(@"%@", @"Data are not present");
    } else {

        rri = CFSwapInt16LittleToHost(*(uint16_t *)(&reportData[2]));
        self.rrLabel.text = [NSString stringWithFormat:@"HRV: %d ms", rri];

        if (rri != 0 && [_vo2max.rrValues count] <= 250) {
            [_vo2max saveRRInterval:[NSString stringWithFormat:@"%f", rri/1024.0]];
            [self addRRI:rri];
        }
    }
}

- (void)peripheralDidLostConnection {
    if (!_lostConnectionTimer) {
        _lostConnectionCount = [[NSDate date] timeIntervalSince1970];
        _lostConnectionTimer = [NSTimer scheduledTimerWithTimeInterval:1.0f
                                                               target:self
                                                             selector:@selector(didLostConnection)
                                                             userInfo:nil
                                                              repeats:YES];
    }
}

- (void)didLostConnection {
    int count = [[NSDate date] timeIntervalSince1970] - _lostConnectionCount;
    if (count == 10) {
        [_lostConnectionTimer invalidate];
        _lostConnectionTimer = nil;

        //..play disconnect
        [self playSound:2];

        //..check if reach limit
        _lostConnectionCountLimit ++;
        if (_lostConnectionCountLimit >= 6) {
            _lostConnectionCountLimit = 0;

            if (_nsTimer) {
                [_nsTimer invalidate];
                _nsTimer = nil;
            }
            
            [[NSNotificationCenter defaultCenter] removeObserver:self];
            UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:@"Error"
                                                                message:@"Heart rate connection has been lost and the Vo2 measurement cannot continue."
                                                               delegate:nil
                                                      cancelButtonTitle:@"Ok"
                                                      otherButtonTitles:nil];
            [alertview show];

        }
    }
}

- (void)addHRM:(NSInteger)hrmValue {
    NSInteger sessionTime = [[NSDate date] timeIntervalSince1970] - startSessionTime;

    if([[self.heartRateSamples allKeys] count] == 0) {
        [self.heartRateSamples setValue:[NSNumber numberWithInteger:hrmValue] forKey:@"0"];
    }

    NSInteger numOfHrm = (sessionTime / 5);
    NSString *key = [NSString stringWithFormat:@"%ld", (long)numOfHrm];
    
    if(![self.heartRateSamples valueForKey:key])
        [self.heartRateSamples setValue:[NSNumber numberWithInteger:hrmValue] forKey:key];
    
}

- (void)addRRI:(NSInteger)rri {
    NSInteger sessionTime = [[NSDate date] timeIntervalSince1970] - startSessionTime;
    
    if([[self.rriRateSamples allKeys] count] == 0) {
        [self.rriRateSamples setValue:[NSNumber numberWithFloat:rri] forKey:@"0"];
    }
    
    NSInteger numOfRii = (sessionTime / 5);
    NSString *key = [NSString stringWithFormat:@"%d", numOfRii];
    
    if(![self.rriRateSamples valueForKey:key] && rri != 0)
        [self.rriRateSamples setValue:[NSNumber numberWithInt:rri] forKey:key];
    
}

- (void)showResult:(NSDictionary *)details {
    
    NSMutableDictionary *info = [[NSMutableDictionary alloc] initWithDictionary:details];
    [info setObject:_data forKey:@"data"];
    
    ResultViewController *resultViewController = [[ResultViewController alloc] init];
    resultViewController.resultInfo = info;
    resultViewController.hidesBottomBarWhenPushed = YES;
    
    [self.navigationController pushViewController:resultViewController animated:YES];
}

#pragma mark - Audio

- (void)playSound:(NSInteger)index {
    
    NSString *soundName = @"";
    switch(index) {
        case 0:
            soundName = @"Start";
            break;
        case 1:
            soundName = @"Complete";
            break;

        case 2:
            soundName = @"HRconnectionLost";
            break;

        default:
            break;
    }
    
    NSURL* soundFileURL = [[NSBundle mainBundle] URLForResource:soundName withExtension:@"mp3"];
    
    NSError *sessionError = nil;
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:&sessionError];
    
    SystemSoundID soundID;
    AudioServicesCreateSystemSoundID((__bridge CFURLRef)soundFileURL, &soundID);
    
    AudioServicesAddSystemSoundCompletion(soundID,
                                          NULL,
                                          NULL,
                                          systemAudioCallback2,
                                          (__bridge void*) self);
    
    AudioServicesPlayAlertSound(soundID);
  
}

void systemAudioCallback2(SystemSoundID soundId, void *clientData) {
    AudioServicesRemoveSystemSoundCompletion(soundId);
    AudioServicesDisposeSystemSoundID(soundId);
}

#pragma mark - Timer

- (void)startExerciseTimer {

    UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:@"VO\u00B2max Test"
                                                        message:@"VO\u00B2 measurement starting and will take 3-5 minutes. Please relax!"
                                                       delegate:nil
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles:nil];
    [alertview show];
    
    //... play start audio
    [self playSound:0];
    if (!_nsTimer) {
        _nsTimer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                                   target:self
                                                 selector:@selector(timerEvent:)
                                                 userInfo:nil
                                                  repeats:YES];
    }
    
}

- (void)resetTimer {
    [_nsTimer invalidate];
    _nsTimer = nil;
}

- (void)timerEvent:(id)sender {
    
    exerciseCount = ([[NSDate date] timeIntervalSince1970] - startSessionTime);
    _vo2max.duration = exerciseCount;
    
    //... update UI
    [self updateGraph];
    [self.timerlbl setText:[self getTimerInFormatedVal]];

    if ([_vo2max.rrValues count] >= 250 || exerciseCount >= 305) {
        
        //... play start audio
        [self playSound:1];
     
        //...get HRaverage
        int sum = 0;
        for (NSString *key in [self.heartRateSamples allKeys]) {
            sum += [[self.heartRateSamples valueForKey:key] integerValue];
        }
        _vo2max.hrAverage = sum/[[self.heartRateSamples allKeys] count];

        NSLog(@"hr samples: %@", self.heartRateSamples);

        //... call webservice
        [self getVo2MaxFromRR:_vo2max];
        
        //... reset all
        [self resetTimer];
    }

}

- (NSString *)getTimerInFormatedVal {
    
    NSInteger countdown = 305 - exerciseCount;
    NSInteger seconds = countdown % 60;
    NSInteger minutes = (countdown / 60) % 60;
    return [NSString stringWithFormat:@"%02i:%02i", minutes, seconds];
}

#pragma mark - WS Method

- (void)getVo2MaxFromRR:(Vo2Max *)vo2max {
    [_tools startLoading:self.tabBarController.view childView:_loadingView text:@"Loading..."];
    
    LoginResponseData *loginResponseData = [LoginPerser getLoginResponse];
    request *r = [[request alloc] initWithTarget:self
                                   SuccessAction:@selector(onSuccessfulGetVo2max:resData:)
                                   FailureAction:@selector(onFailedFetch)];
    
    [r getVo2MaxFromRRService:[_vo2max generateVo2MaxXML:[loginResponseData memberID]]];
}

#pragma mark WS Delegate

- (void)showAlert:(NSString *)alert withMessage:(NSString *)message withTag:(NSInteger)tag {
    UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:alert
                                                        message:message
                                                       delegate:self
                                              cancelButtonTitle:@"No"
                                              otherButtonTitles:@"Yes", nil];
    [alertview setTag:tag];
    [alertview show];
}

- (void)onSuccessfulGetVo2max:(ResponseStatus *)responseStatus resData:(id)obj {
    [_tools stopLoading:_loadingView];
    
    _vo2max = nil;
    nuvitaXMLParser = (NuvitaXMLParser *)obj;
    
    if ([responseStatus isSuccess]) {
        
        NSDictionary *info = nuvitaXMLParser.info;
        [self showResult:info];
        return;
    }
    
    UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:@"Error"
                                                        message:[responseStatus errorText]
                                                       delegate:nil
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles:nil];
    [alertview show];
}

- (void)onFailedFetch {
    
    [_tools stopLoading:_loadingView];
    UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:@"Error"
                                                        message:@"Something went wrong! Would you like to report this issue?"
                                                       delegate:nil
                                              cancelButtonTitle:@"No"
                                              otherButtonTitles:@"Yes", nil];
    [alertview show];
}

@end
