//
//  errorcodeOBJ.m
//  Acclaris
//
//  Created by Sayan banerjee on 21/11/10.
//  Copyright 2010 Objectsol. All rights reserved.
//

#import "ResponseStatus.h"


@implementation ResponseStatus

@synthesize returnCode;
@synthesize errorText;

- (BOOL)isSuccess
{
	if(self.returnCode == nil || [self.returnCode isEqualToString:@"true"])  return NO;
	
	return YES;
}

- (BOOL)isFailed
{
	if(self.returnCode == nil || [self.returnCode isEqualToString:@"false"])  return YES;
	
	return YES;
}

- (BOOL)isUpdate
{
	if(self.errorText && [self.errorText length] > 0)  return NO;
	
	return YES;
}

@end
