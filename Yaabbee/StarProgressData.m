//
//  claimActivityOBJ.m
//  Acclaris
//
//  Created by S Biswas on 09/11/10.
//  Copyright 2010 ObjectSol Technologies. All rights reserved.
//

#import "StarProgressData.h"

#define ERROR_STATUS @"a:ErrorStatus"
#define ERROR_MSG @"a:ErrorMessage"

#define START_TITLE @"a:StarsTitle"
#define STAR @"a:Star"

#define NAME @"a:Name"
#define ORGANIZATION @"a:Organization"
#define PROGRESS_PERC @"a:ProgressPerecent"
#define PROGRESS_VAL @"a:ProgressValue"
#define RANK @"a:Rank"
#define WEEK_LABEL @"a:WeekLabel"

//static TeamProgressData *_sharedInstance = nil;

@implementation StarProgressData

@synthesize StarsTitle,WeekLabel;
//@synthesize TeamName;

@synthesize StarItemsArray;

@synthesize responseStatus;


#pragma mark Parse Xml Data

- (void)parseXMLFileAtData:(NSMutableData *)data parseError:(NSError **)error
{
	NSXMLParser *parser;
	parser = [[NSXMLParser alloc] initWithData:data];
	[parser setDelegate:self];
	[parser setShouldProcessNamespaces:NO];
	[parser setShouldReportNamespacePrefixes:NO];
	[parser setShouldResolveExternalEntities:NO];
	[parser parse];
	
	NSError *parseError = [parser parserError];
	
	if (parseError && error) 
	{
		*error = parseError;
	}
	
	[parser release];
}

- (void)parserDidStartDocument:(NSXMLParser *)parser
{
	responseStatus = [[ResponseStatus alloc]init];
	self.StarItemsArray = [[NSMutableArray alloc]init];
}		

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
	if (qName)
	{
		elementName = qName;
	}
	else 
		if([elementName isEqualToString:ERROR_STATUS])
		{
			contentOfString=[NSMutableString string];
			[contentOfString retain];
			return;		
				
		}
	else 
		if([elementName isEqualToString:ERROR_MSG])
		{
			contentOfString=[NSMutableString string];
			[contentOfString retain];
			return;		
		}
	else 
		if([elementName isEqualToString:START_TITLE])
		{
			contentOfString=[NSMutableString string];
			[contentOfString retain];
			return;		
			
		}
		else
			if([elementName isEqualToString:WEEK_LABEL])
			{
				contentOfString=[NSMutableString string];
				[contentOfString retain];
				return;		
				
			}
		else 
			if([elementName isEqualToString:STAR])
			{
				starItem = [[StarItem alloc]init];
				[starItem retain];
				return;		
			}
			else 
				if([elementName isEqualToString:NAME])
				{
					contentOfString=[NSMutableString string];
					[contentOfString retain];
					return;		
				}
                else 
                    if([elementName isEqualToString:ORGANIZATION])
                    {
                        contentOfString=[NSMutableString string];
                        [contentOfString retain];
                        return;		
                    }
				else
					if([elementName isEqualToString:PROGRESS_PERC])
					{
						contentOfString=[NSMutableString string];
						[contentOfString retain];
						return;		
						
					}
					else
						if([elementName isEqualToString:PROGRESS_VAL])
						{
							contentOfString=[NSMutableString string];
							[contentOfString retain];
							return;		
							
						}
						else
							if([elementName isEqualToString:RANK])
							{
								contentOfString=[NSMutableString string];
								[contentOfString retain];
								return;		
								
							}
							
		
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName     
{	
	if (qName)
	{
		elementName = qName;
	}
	else 
			if([elementName isEqualToString:ERROR_STATUS])
			{
				if(contentOfString)
				{
					responseStatus.returnCode = contentOfString;
					[contentOfString release];
					contentOfString = nil;
				}				
			}
			else 
				if([elementName isEqualToString:ERROR_MSG])
				{
					if(contentOfString)
					{
						responseStatus.errorText = contentOfString;
						[contentOfString release];
						contentOfString = nil;
					}		
				}
	else 
		if([elementName isEqualToString:START_TITLE])
		{
			if(contentOfString)
			{
				self.StarsTitle = contentOfString;
				[contentOfString release];
				contentOfString = nil;
			}
		}
		else 
			if([elementName isEqualToString:WEEK_LABEL])
			{
				if(contentOfString)
				{
					self.WeekLabel = contentOfString;
					[contentOfString release];
					contentOfString = nil;
				}							
			}
		else 
			if([elementName isEqualToString:STAR])
			{
				[self.StarItemsArray addObject:starItem];//
				
				starItem = nil;
			}
		else 
			if([elementName isEqualToString:NAME])
				{
					if(contentOfString)
					{
						starItem.Name = contentOfString;
						[contentOfString release];
						contentOfString = nil;
						
						NSLog(@"Star Name %@", starItem.Name);
					}		
				}
            else 
                if([elementName isEqualToString:ORGANIZATION])
				{
					if(contentOfString)
					{
						starItem.Organization = contentOfString;
						[contentOfString release];
						contentOfString = nil;
					}		
				}	

		else 
			if([elementName isEqualToString:PROGRESS_PERC])
			{
				if(contentOfString)
				{
					starItem.ProgressPerecent = contentOfString;
					[contentOfString release];
					contentOfString = nil;
				}					
			}
			else 
				if([elementName isEqualToString:PROGRESS_VAL])
				{
					if(contentOfString)
					{
						starItem.ProgressValue = contentOfString;
						[contentOfString release];
						contentOfString = nil;
					}						
				}
				else 
					if([elementName isEqualToString:RANK])
					{
						if(contentOfString)
						{
							starItem.Rank = contentOfString;
							[contentOfString release];
							contentOfString = nil;
						}							
					}
					
	
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
	if(contentOfString)
		[contentOfString appendString:string];
}

- (void)parserDidEndDocument:(NSXMLParser *)parse
{
	
	//myerrorcodeOBJ = (errorcodeOBJ *)[errordetails objectAtIndex:0];
//	responseStatus.returnCode = [NSString stringWithFormat:@"0"];
//	responseStatus.errorText = [NSString stringWithFormat:@""];
	//NSLog(@"**********\n\n\n\n\n\n%@\n\nreturnCode %@\nerrorText %@", responseStatus.returnCode, responseStatus.errorText);
}


- (void)dealloc{
	StarItemsArray=nil;
	[super dealloc];
}
- (id)init{
	StarItemsArray=nil;//[[[NSMutableArray alloc]init]autorelease];
	return self;
}


@end


/*
 
 <s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">
 <s:Body>
 <GetStarsResponse xmlns="http://tempuri.org/">
 <GetStarsResult xmlns:a="http://schemas.datacontract.org/2004/07/NuvitaMobileService" xmlns:i="http://www.w3.org/2001/XMLSchema-instance">
 <a:ErrorMessage i:nil="true"/>
 <a:ErrorStatus>false</a:ErrorStatus>
 <a:NuvitaStars>
 <a:Star>
 <a:Name>Jess Biggs</a:Name>
 <a:ProgressPerecent>133%</a:ProgressPerecent>
 <a:ProgressValue>160 min</a:ProgressValue>
 <a:Rank>1</a:Rank>
 <a:WeekLabel>Week Of 12/26</a:WeekLabel>
 </a:Star>
 <a:Star>
 <a:Name>Paul Chavez</a:Name>
 <a:ProgressPerecent>106%</a:ProgressPerecent>
 <a:ProgressValue>127 min</a:ProgressValue>
 <a:Rank>2</a:Rank>
 <a:WeekLabel>Week Of 12/26</a:WeekLabel>
 </a:Star>
 <a:Star>
 <a:Name>Ron McPhee</a:Name>
 <a:ProgressPerecent>99%</a:ProgressPerecent>
 <a:ProgressValue>119 min</a:ProgressValue>
 <a:Rank>3</a:Rank>
 <a:WeekLabel>Week Of 12/26</a:WeekLabel>
 </a:Star>
 <a:Star>
 <a:Name>Alan Antin</a:Name>
 <a:ProgressPerecent>95%</a:ProgressPerecent>
 <a:ProgressValue>114 min</a:ProgressValue>
 <a:Rank>4</a:Rank>
 <a:WeekLabel>Week Of 12/26</a:WeekLabel>
 </a:Star>
 <a:Star>
 <a:Name>Jose Maresma</a:Name>
 <a:ProgressPerecent>93%</a:ProgressPerecent>
 <a:ProgressValue>112 min</a:ProgressValue>
 <a:Rank>5</a:Rank>
 <a:WeekLabel>Week Of 12/26</a:WeekLabel>
 </a:Star>
 <a:Star>
 <a:Name>Fran Sutherlin</a:Name>
 <a:ProgressPerecent>39%</a:ProgressPerecent>
 <a:ProgressValue>59 min</a:ProgressValue>
 <a:Rank>6</a:Rank>
 <a:WeekLabel>Week Of 12/26</a:WeekLabel>
 </a:Star>
 <a:Star>
 <a:Name>Terry Bailey</a:Name>
 <a:ProgressPerecent>17%</a:ProgressPerecent>
 <a:ProgressValue>10 min</a:ProgressValue>
 <a:Rank>7</a:Rank>
 <a:WeekLabel>Week Of 12/26</a:WeekLabel>
 </a:Star>
 </a:NuvitaStars>
 <a:StarsTitle>Cardio Stars</a:StarsTitle>
 </GetStarsResult>
 </GetStarsResponse>
 </s:Body>
 </s:Envelope>
 
 */

