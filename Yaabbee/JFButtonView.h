//
//  JFButtonView.h
//  NuvitaCardio
//
//  Created by John on 6/18/14.
//
//

#import <UIKit/UIKit.h>

@class JFButtonView, JFButton;
@protocol JFButtonViewDelegate <NSObject>

- (void)buttonView:(JFButtonView *)view didTapButton:(JFButton *)button;

@end

@interface JFButtonView : UIView

@property (assign, nonatomic) NSString *buttonTitle;
@property (assign, nonatomic) CGFloat height;
@property (assign, nonatomic) BOOL backButton;
@property (assign, nonatomic) BOOL disableButton;

@property (retain, nonatomic) id <JFButtonViewDelegate> delegate;

@end
