

#import "MemberProgressData.h"

#define ERROR_STATUS @"a:ErrorStatus"
#define ERROR_MSG @"a:ErrorMessage"

#define CARDIO_LABEL @"a:CardioLabel"
#define CARDIO_PERCENT @"a:CardioPercent"
#define MOBILITY_LABEL @"a:MobilityLabel"
#define MOBILITY_PERCENT @"a:MobilityPercent"
#define NUTRITION_LABEL @"a:NutritionLabel"
#define NUTRITION_PERCENT @"a:NutritionPercent"

#define WEEK_LABEL @"a:WeekLabel"


@implementation MemberProgressData

@synthesize CardioLabel;
@synthesize CardioPercent;
@synthesize MobilityLabel;
@synthesize MobilityPercent;

@synthesize NutritionLabel;
@synthesize NutritionPercent;

@synthesize WeekLabel;
@synthesize responseStatus;

#pragma mark Parse Xml Data

- (void)parseXMLFileAtData:(NSMutableData *)data parseError:(NSError **)error
{
	NSXMLParser *parser;
	parser = [[NSXMLParser alloc] initWithData:data];
	[parser setDelegate:self];
	[parser setShouldProcessNamespaces:NO];
	[parser setShouldReportNamespacePrefixes:NO];
	[parser setShouldResolveExternalEntities:NO];
	[parser parse];
	
	NSError *parseError = [parser parserError];
	
	if (parseError && error) 
	{
		*error = parseError;
	}
	
	[parser release];
}

- (void)parserDidStartDocument:(NSXMLParser *)parser
{
	responseStatus = [[ResponseStatus alloc]init];
	//TeamMembers = [[NSMutableArray alloc]init];
}		

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
	if (qName)
	{
		elementName = qName;
	}
	else 
		if([elementName isEqualToString:ERROR_STATUS])
		{
			contentOfString=[NSMutableString string];
			[contentOfString retain];
			return;		
			
		}
		else 
			if([elementName isEqualToString:ERROR_MSG])
			{
				contentOfString=[NSMutableString string];
				[contentOfString retain];
				return;		
			}
			else 
				if([elementName isEqualToString:CARDIO_LABEL])
				{
					contentOfString=[NSMutableString string];
					[contentOfString retain];
					return;		
					
				}
				else 
					if([elementName isEqualToString:CARDIO_PERCENT])
					{
						contentOfString=[NSMutableString string];
						[contentOfString retain];
						return;		
						
					}
					else 
						if([elementName isEqualToString:MOBILITY_LABEL])
						{
							contentOfString=[NSMutableString string];
							[contentOfString retain];
							return;		
							
						}
						else
							if([elementName isEqualToString:MOBILITY_PERCENT])
							{
								contentOfString=[NSMutableString string];
								[contentOfString retain];
								return;		
								
							}
							else
								if([elementName isEqualToString:NUTRITION_LABEL])
								{
									contentOfString=[NSMutableString string];
									[contentOfString retain];
									return;		
									
								}
								else
									if([elementName isEqualToString:NUTRITION_PERCENT])
									{
										contentOfString=[NSMutableString string];
										[contentOfString retain];
										return;		
										
									}
									else
										if([elementName isEqualToString:WEEK_LABEL])
										{
											contentOfString=[NSMutableString string];
											[contentOfString retain];
											return;		
											
										}
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName     
{	
	if (qName)
	{
		elementName = qName;
	}
	else 
		if([elementName isEqualToString:ERROR_STATUS])
		{
			if(contentOfString)
			{
				responseStatus.returnCode = contentOfString;
				[contentOfString release];
				contentOfString = nil;
			}				
		}
		else 
			if([elementName isEqualToString:ERROR_MSG])
			{
				if(contentOfString)
				{
					responseStatus.errorText = contentOfString;
					[contentOfString release];
					contentOfString = nil;
				}		
			}
			else 
				if([elementName isEqualToString:CARDIO_LABEL])
				{
					if(contentOfString)
					{
						self.CardioLabel = contentOfString;
						[contentOfString release];
						contentOfString = nil;
					}		
				}
				else 
					if([elementName isEqualToString:CARDIO_PERCENT])
					{
						if(contentOfString)
						{
							self.CardioPercent = contentOfString;
							[contentOfString release];
							contentOfString = nil;
						}							}
					else 
						if([elementName isEqualToString:MOBILITY_LABEL])
						{
							if(contentOfString)
							{
								self.MobilityLabel = contentOfString;
								[contentOfString release];
								contentOfString = nil;
							}		
						}	
						else 
							if([elementName isEqualToString:MOBILITY_PERCENT])
							{
								if(contentOfString)
								{
									self.MobilityPercent = contentOfString;
									[contentOfString release];
									contentOfString = nil;
								}					
							}
							else 
								if([elementName isEqualToString:NUTRITION_LABEL])
								{
									if(contentOfString)
									{
										self.NutritionLabel = contentOfString;
										[contentOfString release];
										contentOfString = nil;
									}						
								}
								else 
									if([elementName isEqualToString:NUTRITION_PERCENT])
									{
										if(contentOfString)
										{
											self.NutritionPercent = contentOfString;
											[contentOfString release];
											contentOfString = nil;
										}							
									}
									else 
										if([elementName isEqualToString:WEEK_LABEL])
										{
											if(contentOfString)
											{
												self.WeekLabel = contentOfString;
												[contentOfString release];
												contentOfString = nil;
											}							
										}
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
	if(contentOfString)
		[contentOfString appendString:string];
}

- (void)parserDidEndDocument:(NSXMLParser *)parse
{
	//myerrorcodeOBJ = (errorcodeOBJ *)[errordetails objectAtIndex:0];
	//responseStatus.returnCode = [NSString stringWithFormat:@"0"];
//	responseStatus.errorText = [NSString stringWithFormat:@""];
	//NSLog(@"**********\n\n\n\n\n\n%@\n\nreturnCode %@\nerrorText %@", responseStatus.returnCode, responseStatus.errorText);
}	


@end

/*
 
 <s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">
 <s:Body>
 <GetMemberProgressResponse xmlns="http://tempuri.org/">
 <GetMemberProgressResult xmlns:a="http://schemas.datacontract.org/2004/07/NuvitaMobileService" xmlns:i="http://www.w3.org/2001/XMLSchema-instance">
 <a:CardioLabel>cardio</a:CardioLabel>
 <a:CardioPercent>100</a:CardioPercent>
 <a:ErrorMessage i:nil="true"/>
 <a:ErrorStatus>false</a:ErrorStatus>
 <a:MobilityLabel>mobility</a:MobilityLabel>
 <a:MobilityPercent>0</a:MobilityPercent>
 <a:NutritionLabel>nutrition</a:NutritionLabel>
 <a:NutritionPercent>0</a:NutritionPercent>
 <a:WeekLabel>12/12</a:WeekLabel>
 </GetMemberProgressResult>
 </GetMemberProgressResponse>
 </s:Body>
 </s:Envelope>
 
 */



