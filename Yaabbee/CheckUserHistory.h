//
//  CheckUserHistory.h
//  NuvitaCardio
//
//  Created by John on 7/11/14.
//
//

#import <Foundation/Foundation.h>

@interface CheckUserHistory : NSObject

@property (assign) BOOL hasPairingHistory;
@property (assign) BOOL hasSessionHistory;
@property (assign) BOOL fromTest;
@property (assign) BOOL mustPrompt;
@property (assign) BOOL isVO2Starting;
@property (retain, nonatomic) NSDictionary *trendData;

+ (CheckUserHistory *)sharedAppData;
- (void)reloadCheckerHistory;

@end
