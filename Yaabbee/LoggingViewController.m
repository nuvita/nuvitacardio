//
//  LoggingViewController.m
//  NuvitaCardio
//
//  Created by John on 2/12/14.
//
//

#import "LoggingViewController.h"
#import "LoggingDetailsViewController.h"

#import "OfflineManager.h"

#import "StringFile.h"
#import "AppData.h"
#import "Logging.h"
#import "Logs.h"

#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>

@interface LoggingViewController () <MFMailComposeViewControllerDelegate> {

    NSMutableArray *requestArray;
    NSDateFormatter *formatter;
}

@property (strong, nonatomic) OfflineManager *offlineManager;

@end

@implementation LoggingViewController

#pragma mark - Public Method

- (void)initOfflineManager {
    
    NuvitaAppDelegate *appDelegate = (NuvitaAppDelegate *)[[UIApplication sharedApplication] delegate];
    self.offlineManager = [[OfflineManager alloc] init];
    self.offlineManager.managedObjectContext = appDelegate.managedObjectContext;
}

- (NSInteger)daysBetweenDate:(NSDate*)fromDateTime andDate:(NSDate*)toDateTime {
    NSDate *fromDate;
    NSDate *toDate;
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    [calendar rangeOfUnit:NSDayCalendarUnit startDate:&fromDate
                 interval:NULL forDate:fromDateTime];
    [calendar rangeOfUnit:NSDayCalendarUnit startDate:&toDate
                 interval:NULL forDate:toDateTime];
    NSDateComponents *difference = [calendar components:NSDayCalendarUnit
                                               fromDate:fromDate toDate:toDate options:0];
    
    return [difference day];
}

- (void)cleanUpExpiredLogs {
    
    NSArray *logArray = [self.offlineManager getAllLogs];
    [logArray enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        
        Logs *log = (Logs *)obj;
        Logging *logging = [NSKeyedUnarchiver unarchiveObjectWithData:log.logging];
        NSDictionary *data = [logging requestData];
        
        if ([self daysBetweenDate:[data valueForKey:@"dateTime"] andDate:[NSDate date]] >= 7) {
            [self deleteLog:log];
        }
    }];
}

- (void)deleteLog:(Logs *)log {
    [self.offlineManager deleteLog:log];
}

- (void)didTapSend {
    
    NSString *fileString = [[StringFile sharedAppData] convertLogsToString:requestArray];
    if ([[StringFile sharedAppData] saveFile:fileString]) {
        [self openEmailForm];
    }
}

- (void)openEmailForm {
    
    MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
    picker.mailComposeDelegate = self;
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"Logs.txt"];
    NSData *myData = [NSData dataWithContentsOfFile:filePath];
    
    // Present the mail composition interface.
    Class mailClass = (NSClassFromString(@"MFMailComposeViewController"));
    if ([mailClass canSendMail]) {
        //..
        [picker setToRecipients:@[@"mobileapps@nuvita.com"]];
        
        [picker setSubject:@"App Logs"];
        [picker setMessageBody:@"Attached is the txt file of all webservice request" isHTML:NO];
        [picker addAttachmentData:myData mimeType:@"application/txt" fileName:@"Request Logs.txt"];
        
        [self presentViewController:picker animated:YES completion:^{
            NSLog(@".....loaded mail view");
        }];
    }
    
}

- (BOOL)date:(NSDate*)date isBetweenDate:(NSDate*)beginDate andDate:(NSDate*)endDate {
    if ([date compare:beginDate] == NSOrderedAscending)
    	return NO;
    
    if ([date compare:endDate] == NSOrderedDescending)
    	return NO;
    
    return YES;
}

- (void)mailComposeController:(MFMailComposeViewController *)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError *)error {
    
    
    if (result == MFMailComposeResultSent)
        NSLog(@"Message has been sent");
    
    
    [self dismissViewControllerAnimated:YES completion:^{
        //...done sending email
    }];
}

#pragma mark - View Life cycle

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    UIBarButtonItem *button = [[UIBarButtonItem alloc] initWithTitle:@"Send All Logs"
                                                               style:UIBarButtonItemStylePlain
                                                              target:self
                                                              action:@selector(didTapSend)];
    
    self.navigationItem.rightBarButtonItem = button;
    
    requestArray = [[NSMutableArray alloc] init];
    
    formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MMM dd, yyyy HH:mm a"];
    
    [self initOfflineManager];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    [self cleanUpExpiredLogs];
    [requestArray removeAllObjects];
    [requestArray addObjectsFromArray:[self.offlineManager getAllLogs]];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return [requestArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellIdentifier];
    }
    
    Logs *logging = [requestArray objectAtIndex:indexPath.row];
    Logging *log = [NSKeyedUnarchiver unarchiveObjectWithData:logging.logging];
    
    NSDictionary *logInfo = [log requestData];
    cell.textLabel.text = [logInfo objectForKey:@"method"];
    cell.detailTextLabel.text = [formatter stringFromDate:[logInfo objectForKey:@"dateTime"]];
    
    UIFont *font = [UIFont systemFontOfSize:14];
    cell.textLabel.font = font;
    cell.detailTextLabel.font = font;
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    LoggingDetailsViewController *loggingDetailsViewController = [[LoggingDetailsViewController alloc] init];
    
    Logs *logging = [requestArray objectAtIndex:indexPath.row];
    Logging *log = [NSKeyedUnarchiver unarchiveObjectWithData:logging.logging];
    loggingDetailsViewController.requestLog = [log requestData];
    
    [self.navigationController pushViewController:loggingDetailsViewController animated:YES];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
