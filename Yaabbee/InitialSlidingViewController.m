//
//  InitialSlidingViewController.m
//  NuvitaCardio
//
//  Created by John on 3/28/14.
//
//

#import "InitialSlidingViewController.h"
#import "StatViewController.h"

@interface InitialSlidingViewController ()

@property (nonatomic, retain) StatViewController *statViewController;

@end

@implementation InitialSlidingViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.tabBarItem.title = @"Stat";
        
        UIImage *normalImg = [UIImage imageNamed:@"cardio.png"];
        UIImage *selectedImg = [UIImage imageNamed:@"cardio_select.png"];
        [self.tabBarItem setFinishedSelectedImage:selectedImg withFinishedUnselectedImage:normalImg];
    }
    return self;
}

- (void)updateConnectButton:(BOOL)clicked {

    UIImage *image = nil;
    if (clicked)
        image = [UIImage imageNamed:@"btn_connect_light.png"];
    else
        image = [UIImage imageNamed:@"btn_connect.png"];
    
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 90, 32)];
    [button addTarget:self action:@selector(btConnectClicked) forControlEvents:UIControlEventTouchUpInside];
    [button setBackgroundImage:image forState:UIControlStateNormal];
    
    UIView *rightButtonView = [[UIView alloc] initWithFrame:button.frame];
    rightButtonView.bounds = CGRectOffset(rightButtonView.bounds, RDX, 0);
    [rightButtonView addSubview:button];
    
    UIBarButtonItem *btnRight = [[UIBarButtonItem alloc] initWithCustomView:rightButtonView];
    self.navigationItem.rightBarButtonItem = btnRight;
    [self.navigationController presentedViewController];
}

- (void)didTapMenu {
    
    [_statViewController showLeftBarMenu];
}

- (void)btConnectClicked {
    [self updateConnectButton:YES];
    [_statViewController btConnectClicked];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //left button
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 78, 32)];
    [button addTarget:self action:@selector(didTapMenu) forControlEvents:UIControlEventTouchUpInside];
    [button setImage:[UIImage imageNamed:@"btn_menu.png"] forState:UIControlStateNormal];
    
    UIView *leftButtonView = [[UIView alloc] initWithFrame:button.frame];
    leftButtonView.bounds = CGRectOffset(leftButtonView.bounds, LDX, 0);
    [leftButtonView addSubview:button];
    
    UIBarButtonItem *signoutButton = [[UIBarButtonItem alloc] initWithCustomView:leftButtonView];
    [self.navigationItem setLeftBarButtonItem:signoutButton];
    
    //right button
    button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 90, 32)];
    [button addTarget:self action:@selector(btConnectClicked) forControlEvents:UIControlEventTouchUpInside];
    [button setBackgroundImage:[UIImage imageNamed:@"btn_connect.png"] forState:UIControlStateNormal];
    
    
    UIView *rightButtonView = [[UIView alloc] initWithFrame:button.frame];
    rightButtonView.bounds = CGRectOffset(rightButtonView.bounds, RDX, 0);
    [rightButtonView addSubview:button];
    
    UIBarButtonItem *btnRight = [[UIBarButtonItem alloc] initWithCustomView:rightButtonView];
    [self.navigationItem setRightBarButtonItem:btnRight];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didUpdateConnectBtn:)
                                                 name:@"update_connect_btn"
                                               object:nil];
    
    _statViewController = [[StatViewController alloc] initWithNibName:@"StatViewController" bundle:nil];
    self.topViewController = _statViewController;
}

- (void)didUpdateConnectBtn:(NSNotification *)obj {
    [self updateConnectButton:[obj.object boolValue]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
