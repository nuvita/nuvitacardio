//
//  errorcodeOBJ.h
//  Acclaris
//
//  Created by Sayan banerjee on 21/11/10.
//  Copyright 2010 Objectsol. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface ResponseStatus : NSObject {

	NSString *returnCode;
	NSString *errorText;
}

@property(nonatomic,retain)NSString *returnCode;
@property(nonatomic,retain)NSString *errorText;

- (BOOL)isSuccess;
- (BOOL)isFailed;
- (BOOL)isUpdate;

@end
