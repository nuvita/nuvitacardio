//
//  JFButton.m
//  NuvitaCardio
//
//  Created by John on 6/18/14.
//
//

#import "JFButton.h"

@implementation JFButton

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        _buttonTitle = [self titleForState:UIControlStateNormal];
    }
    
    return self;
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    [self setClearsContextBeforeDrawing:YES];
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    UIColor *tintColor = (!_disabled) ? [UIColor colorWithRed:0.2 green:0.4 blue:0.7 alpha:1.0] : [UIColor colorWithWhite:0.3 alpha:0.5];

    CGContextSetLineWidth(context, 3.0f);
    CGContextSetStrokeColorWithColor(context, [tintColor CGColor]);
    CGContextSetFillColorWithColor(context, [tintColor CGColor]);
    
    int weight = self.frame.size.width;
    CGRect frame = self.frame;
    
    if (_backButton) {
        
        CGContextMoveToPoint(context, 14, 5);
        CGContextAddLineToPoint(context, 4, 14);
        CGContextAddLineToPoint(context, 14, 23);
        CGContextStrokePath(context);
        
        frame.origin.x += 20;
        frame.origin.y += 5;
        
    } else {
        
        CGContextMoveToPoint(context, weight - 14, 5);
        CGContextAddLineToPoint(context, weight - 4, 14);
        CGContextAddLineToPoint(context, weight - 14, 23);
        CGContextStrokePath(context);
        
        frame.origin.y += 5;
        
    }
    
    NSString *title = [self titleForState:UIControlStateNormal];
    [title drawInRect:frame withFont:[UIFont systemFontOfSize:17.0]];
    
    [self setTitle:@"" forState:UIControlStateNormal];
}


@end
