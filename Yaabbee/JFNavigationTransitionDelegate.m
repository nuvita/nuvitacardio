//
//  JFNavigationTransitionDelegate.m
//  NuvitaCardio
//
//  Created by John on 6/14/14.
//
//

#import "JFNavigationTransitionDelegate.h"
#import "JFTransitionAnimator.h"

@implementation JFNavigationTransitionDelegate

- (id)init {
    self = [super init];
    if ( self ) {
        self.pushTransitioning = [JFTransitionAnimator transitioningWithReverse:NO];
        self.popTransitioning = [JFTransitionAnimator transitioningWithReverse:YES];
    }
    return self;
}


- (id<UIViewControllerAnimatedTransitioning>)navigationController:(UINavigationController *)navigationController
                                  animationControllerForOperation:(UINavigationControllerOperation)operation
                                               fromViewController:(UIViewController*)fromViewController
                                                 toViewController:(UIViewController*)toViewController {
    
    return operation == UINavigationControllerOperationPush ? self.pushTransitioning : self.popTransitioning;
}

@end
