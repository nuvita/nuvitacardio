//
//  HistoryViewController.h
//  NuvitaCardio
//
//  Created by S Biswas on 08/04/13.
//
//

#import <UIKit/UIKit.h>
#import "CardioLap.h"
#import "LoginResponseData.h"
#import "LoginPerser.h"

@interface HistoryViewController : UIViewController<UIActionSheetDelegate>

@property (retain, nonatomic) IBOutlet UITableView *tableViewHistory;
@property (retain, nonatomic) IBOutlet UIView *headerview;


- (void)btnEditClicked:(id)sender;
- (void)delExcersiseSession:(NSInteger)index;
- (void)openSession;

@end
