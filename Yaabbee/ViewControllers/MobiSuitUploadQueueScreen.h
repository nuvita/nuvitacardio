//
//  MobiSuitHomeScreen.h
//  syncClient
//
//  Created by Sayan Chatterjee on 29/10/11.
//  Copyright 2011 ObjectSol. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PhotoShareItemCell.h"
#import "MyController.h"
#import "UploadImageInformtion.h"

@interface MobiSuitUploadQueueScreen : UIViewController <UITableViewDelegate, UITableViewDataSource> 
{    
		//SyncSourceCell* syncingCell;
		//UIBarButtonItem* signoutButton;
		//CLLocationManager* locManager;
		//dispatch_semaphore_t locationSemaphore;
		UITextView* syncText;
		UIToolbar* toolbar;
		BOOL showDescription;
}
-(void)doMenuItemAction:(id)sender;
	- (IBAction) showSettings: (id) pId;
	
- (void) reloadTable;//: (id) pid;
	//- (void) endSync: (id) pid;
	//- (void) startTimer;
	
	//- (void) startSyncAll: (id) pid;
	- (void) setToolbarTitle: (NSString*)string;
	
	
	// UI updater callback          
	//- (void) displaySyncProgressIcon;
	//- (void) timerFireMethod:(NSTimer*)theTimer;
	
	
	- (void) disableButtons: (id) pid;
	- (void) enableButtons: (id) pid;
	
	- (void) setupBackButton;
	- (void) setButtonForStartSync;
	
	//- (void) stopGPSLocationStatus;
- (void)showAlert:(NSString *)alterStr;
-(void)buttonClickedRemove:(id)pid;
-(void)buttonClickedRetry:(id)pid;

	@end
