//
//  NewLoginViewController.m
//
//
//  Created by S Biswas on 8/7/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "NewLoginViewController.h"
#import "InitialTestViewController.h"
#import "InstructionViewController.h"

#define TABLE_OFFSETY 60.0;

@interface NewLoginViewController () <MFMailComposeViewControllerDelegate>

@property (assign) BOOL isKeyBoardVisible;
@property (strong, nonatomic) OfflineManager *offlineManager;

@property (strong, nonatomic) JBLoadingView *loadingView;

@end
 
@implementation NewLoginViewController

@synthesize tableView, navBar;
@synthesize isKeyBoardVisible;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {

        _loadingView = [[JBLoadingView alloc] init];
    }
    
    return self;
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - MFMailComposeViewControllerDelegate

- (void)mailComposeController:(MFMailComposeViewController *)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError *)error {
    
    
    if (result == MFMailComposeResultSent)
        NSLog(@"Message has been sent");
    
    
    [self dismissViewControllerAnimated:YES completion:^{
        //...done sending email
        [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:APP_CRASH];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }];
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (buttonIndex == 1) {
        [self openMailForm];
        return;
        
    }
    
    [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:APP_CRASH];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

#pragma mark - Check Crash Logs

- (void)initOfflineManager {
    
    NuvitaAppDelegate *appDelegate = (NuvitaAppDelegate *)[[UIApplication sharedApplication] delegate];
    _offlineManager = [[OfflineManager alloc] init];
    _offlineManager.managedObjectContext = appDelegate.managedObjectContext;
}

- (BOOL)crashExist {
    BOOL crash = [[[NSUserDefaults standardUserDefaults] valueForKey:APP_CRASH] boolValue];
    return crash;
}

- (NSData *)getAppCrashLog {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *logPath = [documentsDirectory stringByAppendingPathComponent:@"console.log"];
    NSData *myData = [NSData dataWithContentsOfFile:logPath];
    return myData;
}

- (void)propmtCrashReport {
    
    UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:@"Report"
                                                        message:@"NuvitaCardio experienced a problem. Would you like to report this issue??"
                                                       delegate:self
                                              cancelButtonTitle:@"No"
                                              otherButtonTitles:@"Yes", nil];
    [alertview show];
}

- (void)openMailForm {
    
    MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
    picker.mailComposeDelegate = self;
    
    NSData *myData = [self getAppCrashLog];
  
    // Present the mail composition interface.
    Class mailClass = (NSClassFromString(@"MFMailComposeViewController"));
    if ([mailClass canSendMail]) {
        //..
        [picker setToRecipients:@[@"mobileapps@nuvita.com"]];
        
        [picker setSubject:@"Problem Report"];
        [picker setMessageBody:@"Attached is the txt file of app problem report. <br/><br/> Please provide a description of what you were doing in the app when the problem occurred." isHTML:YES];
        [picker addAttachmentData:myData mimeType:@"application/log" fileName:@"console.log"];
        
        [self presentViewController:picker animated:YES completion:^{
            NSLog(@".....loaded mail view");
        }];
        
    } else {
      
        UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:@"Error"
                                                           message:@"Unable to load mail. Please set-up an email account in Settings > Mail,Contacts,Calendar."
                                                          delegate:nil
                                                 cancelButtonTitle:@"Ok"
                                                 otherButtonTitles:nil];
        [alertview show];
    }
}

#pragma mark - View lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
//    tools = [[MyTools alloc] init];
//    loadingView = [tools createActivityIndicatorWithEffects:nil];

    self.isKeyBoardVisible = NO;
    // Do any additional setup after loading the view from its nib.
    //...
    [self initOfflineManager];

//    NSArray *arr = [NSArray arrayWithObjects:@"dsa", nil];
//    NSLog(@"arr: %@", [arr objectAtIndex:1]);

    //...Check any crash reported
    if ([self crashExist]) {
        [self propmtCrashReport];
    }
    
    _navigationTransitionDelegate = [[JFNavigationTransitionDelegate alloc] init];
    self.navigationController.delegate = _navigationTransitionDelegate;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    [self.navigationController presentedViewController];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    
    NuvitaAppDelegate* app = (NuvitaAppDelegate*) [[UIApplication sharedApplication] delegate];
    app.isLoggedIn = FALSE;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:YES];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

#pragma mark -
#pragma mark Table Delegate and Data Source
#pragma mark -

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 2;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)_tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
     
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    [self configureCell:cell withIndex:indexPath];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
	
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
	cell.backgroundColor = [UIColor clearColor];  
}


- (void)configureCell:(UITableViewCell *)cell withIndex:(NSIndexPath *)indexPath{
    
    UIView *cellBackView = cell.contentView;
    
    UIImage *img = [UIImage imageNamed:@"textarea.png"];
    	
    CustomTextField *textField = [[CustomTextField alloc] initWithFrame:CGRectMake((cellBackView.frame.size.width - img.size.width)/2, 0, img.size.width, img.size.height)];
    textField.textColor = [UIColor blackColor];
    textField.font = [UIFont fontWithName:DEFAULT_NORMAL_FONT_NAME size:headerFontSize];;	
    textField.autocorrectionType = UITextAutocorrectionTypeNo;
    textField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    textField.keyboardType = UIKeyboardTypeEmailAddress;
    textField.backgroundColor = [UIColor colorWithPatternImage:img] ;
    if(indexPath.row == 0)
    {
	 userIDFld = textField;	
        textField.placeholder = @"User ID";
        userIDFld.text = [[NSUserDefaults standardUserDefaults] stringForKey:@"USER_ID"];//
        #if TARGET_IPHONE_SIMULATOR
        if([userIDFld.text length] < 1)
           userIDFld.text = @"john.bariquit@ripeconcepts.com";
        #endif
    }
    else
    {
	passwordFld = textField;
		
	textField.placeholder = @"Password";
        passwordFld.text = [[NSUserDefaults standardUserDefaults] stringForKey:@"PASSWORD"];//
        //.text = @"nuvita1428";
    #if TARGET_IPHONE_SIMULATOR
    if([passwordFld.text length] < 1)
        passwordFld.text = @"jo2014ios";
    #endif
        
	passwordFld.secureTextEntry = YES;
    }
	
	[textField setValue:[UIColor darkGrayColor] forKeyPath:@"_placeholderLabel.textColor"];
    textField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;	
    textField.tag = indexPath.row;
    textField.delegate = self;
    [cellBackView addSubview:textField];
}

#pragma mark - UITextFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    
    UITableViewCell *cell = (UITableViewCell *) [[textField superview] superview];
    NSIndexPath *iPath = [self.tableView indexPathForCell:cell];
    
    if(!self.isKeyBoardVisible){
        
        self.isKeyBoardVisible = YES;
        
        CGRect tableFrame = self.tableView.frame;
        tableFrame.origin.y -= TABLE_OFFSETY;
        
        [UIView beginAnimations:nil context:(__bridge void *)(iPath)];
        [UIView setAnimationDuration:0.3];
        [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
        [UIView setAnimationDelegate:self];
        
        self.tableView.frame = tableFrame;
        
        [UIView commitAnimations];
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    
    if (self.isKeyBoardVisible){
        
        self.isKeyBoardVisible = NO;
        CGRect tableFrame = self.tableView.frame;
        tableFrame.origin.y += TABLE_OFFSETY;
        
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.3];
        [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
        [UIView setAnimationDelegate:self];
        
        self.tableView.frame = tableFrame;
        
        [UIView commitAnimations];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
  
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - Public Methods

- (IBAction)loginClicked:(id)sender{

    [[CheckUserHistory sharedAppData] reloadCheckerHistory];
    if (![NuvitaAppDelegate isNetworkAvailable]) {

        //...load offline login
        NSDictionary *info = [NSDictionary dictionaryWithObjectsAndKeys:userIDFld.text, @"username", passwordFld.text, @"password", nil];
        if ([self.offlineManager isAuthorize:info]) {

            [[CheckUserHistory sharedAppData] setFromTest:NO];
            [self showStatPage];

        } else {
            
            UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:@"Nuvita - Offline mode"
                                                                message:@"This user is not yet added to offline mode, please login to network/wireless connection first"
                                                               delegate:nil
                                                      cancelButtonTitle:@"Ok"
                                                      otherButtonTitles:nil];
            [alertview show];
        }
        
        return;
        
    }

    [self sendRequest];
}

- (IBAction)cancelClicked:(id)sender{
    [self dismissViewControllerAnimated:YES completion:^{
        //...exit
    }];
}

#pragma mark - Web Request Service

- (void)getHealthMeasurementCurrentTrends {
    [_loadingView loadingViewStartAnimating:self.view
                            withLoadingView:[_loadingView createLoadingView:self.view lightEffect:NO]
                                       text:@"Loading data. Please wait..."];

    NSDictionary *data = [NSDictionary dictionaryWithObjectsAndKeys:[[LoginPerser getLoginResponse] memberID], @"memberId", nil];
    request *r = [[request alloc] initWithTarget:self
                                      SuccessAction:@selector(onSuccessfulFetchTrends:resData:)
                                      FailureAction:@selector(onFailedFetch)];
    
    [r getHealthMeasurementCurrentTrends:data];
}

- (void)sendRequest {

	NSString *userID = userIDFld.text;
	NSString *password = passwordFld.text;

	if ([userID length]==0) {
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sorry" message:@"Please enter a UserID" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
		[alert show];
	} else if([password length]==0) {
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sorry" message:@"Please enter a Password" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
		[alert show];
	} else {		
        //save data
        [[NSUserDefaults standardUserDefaults] setValue:userID forKey:@"USER_ID"];
        [[NSUserDefaults standardUserDefaults] setValue:password forKey:@"PASSWORD"];
        [[NSUserDefaults standardUserDefaults] synchronize];

        [_loadingView loadingViewStartAnimating:self.view
                                withLoadingView:[_loadingView createLoadingView:self.view lightEffect:NO]
                                           text:@"Signing in. Please wait..."];
				
		request *r=[[request alloc] initWithTarget:self
									 SuccessAction:@selector(onSucceffulLogin:resData:)
									 FailureAction:@selector(onFailedFetch)];
		[r loginRequest:userID
                   pass:password];
	}
}


- (void)onSucceffulLogin:(ResponseStatus *)responseStatus resData:(NSMutableArray *)arr2 {
    [_loadingView loadingViewStopAnimating:[self view]];

	if (responseStatus == nil || ![responseStatus isSuccess]) {
        
		NSString* errMsg = @"Wrong userid or passowrd." ;
		if ([responseStatus.errorText length] > 0) {
			errMsg = responseStatus.errorText;
		}
        
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Info"
                                                        message:errMsg
                                                       delegate:self
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles: nil];
		[alert show];
		return;
	}
	
	NuvitaAppDelegate* app = (NuvitaAppDelegate*) [[UIApplication sharedApplication] delegate];
    app.isLoggedIn = TRUE;
    
    LoginResponseData *responseData = [LoginPerser getLoginResponse];
    NSData *dataObject = [NSKeyedArchiver archivedDataWithRootObject:responseData];
    NSDictionary *info = [NSDictionary dictionaryWithObjectsAndKeys:
                          userIDFld.text, @"username",
                          passwordFld.text, @"password",
                          dataObject, @"loginReponseData",
                          nil];
    
    if ([self.offlineManager addAuthorize:info]) {
        //... user is valid
        [self getHealthMeasurementCurrentTrends];
    }
}

- (void)onSuccessfulFetchTrends:(ResponseStatus *)responseStatus resData:(NSObject *)obj {
    [_loadingView loadingViewStopAnimating:[self view]];

    CheckUserHistory *userHistory = [CheckUserHistory sharedAppData];
    userHistory.hasPairingHistory = [_offlineManager containsDeviceHistory];
    userHistory.hasSessionHistory = [_offlineManager containsSessionHistory];
    
    if ([responseStatus isSuccess]) {
        userHistory.trendData = [(NuvitaXMLParser *)obj info];
    }

    if ([userHistory hasPairingHistory]) {
        
        if ([userHistory trendData]) {
            userHistory.fromTest = NO;
            [self showStatPage];
            
        } else {
    
            userHistory.fromTest = YES;
            [self showVo2SequenceTest];
        }
    } else {
    
        userHistory.fromTest = YES;
        [self showInitialTest];
    }
}

- (void)onFailedFetch {
    [_loadingView loadingViewStopAnimating:[self view]];
    [self showAlertMessage:@"Problem connecting with the server. Please check your internet connection and try again"];
}

- (void)showAlertMessage:(NSString *)message {
    if ([message length] > 0) {
        UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:@"Error"
                                                            message:message
                                                           delegate:nil
                                                  cancelButtonTitle:@"Ok"
                                                  otherButtonTitles:nil];
        [alertview show];
    }
}

#pragma mark - Push View Methods

- (void)showStatPage {
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"update_connect_btn" object:[NSNumber numberWithBool:YES]];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"FINISH_INITIAL_TEST" object:[NSNumber numberWithInteger:0]];
}

- (void)showInitialTest {
    InitialTestViewController *initialTestViewController = [[InitialTestViewController alloc] initWithNibName:@"InitialTestViewController" bundle:nil];
    [self.navigationController pushViewController:initialTestViewController animated:YES];
}

- (void)showVo2SequenceTest {
 
    InstructionViewController *instructionViewController = [[InstructionViewController alloc] initWithNibName:@"InstructionViewController" bundle:nil];
    instructionViewController.history = [[CheckUserHistory sharedAppData] hasSessionHistory];
    instructionViewController.infoData = [[CheckUserHistory sharedAppData] trendData];
    
    [self.navigationController pushViewController:instructionViewController animated:YES];
    [(NuvitaAppDelegate *)[[UIApplication sharedApplication] delegate] performSelector:@selector(reloadAppDelegateNavRootViewControllerLandscape)
                                                                                withObject:nil
                                                                                afterDelay:1.0];
}

#pragma mark - rotate

//Orientation
- (BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation{
    return (toInterfaceOrientation != UIInterfaceOrientationPortraitUpsideDown || toInterfaceOrientation != UIInterfaceOrientationPortrait);
}

- (BOOL)shouldAutorotate {
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationPortrait;
}

#pragma mark - Touch Event

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}

@end
