//
//  NewLoginViewController.h
//
//
//  Created by S Biswas on 8/7/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "request.h"
#import "NuvitaAppDelegate.h"
#import "MyTools.h"
#import "CustomTextField.h"
#import "Authorize.h"
#import "OfflineManager.h"
#import "constant.h"
#import "ResponseStatus.h"
#import "LoginPerser.h"
#import "LoginResponseData.h"

#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>

#import "JFNavigationTransitionDelegate.h"

@class NewLoginViewController, JFNavigationTransitionDelegate;
@interface NewLoginViewController : UIViewController <UITextFieldDelegate> {
//	MyTools *tools;
//	UIView *loadingView;

	CustomTextField *userIDFld;
	CustomTextField *passwordFld;
}

@property (nonatomic, retain) JFNavigationTransitionDelegate *navigationTransitionDelegate;

@property (nonatomic,retain) IBOutlet UINavigationBar *navBar;
@property (nonatomic,retain) IBOutlet UITableView *tableView;

- (void)configureCell:(UITableViewCell *)cell withIndex:(NSIndexPath *)indexPath;
- (IBAction)loginClicked:(id)sender;
- (IBAction)cancelClicked:(id)sender;


- (void)sendRequest;


@end