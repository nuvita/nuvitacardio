//
//  OutBoxViewController.m
//
//
//  Created by S Biswas on 8/22/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "ShareStatusViewController.h"
#import "constant.h"
#import "KTTextView.h"

@implementation ShareStatusViewController

@synthesize subheaderLb1;
@synthesize profileIcon;
@synthesize shareImage;
@synthesize profileTextView;
@synthesize photoBtn;
@synthesize imageShare;
@synthesize delegate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

    UIImageView *textView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"logo.png"]];
    [self.navigationItem setTitleView:textView]; 

	isLoaded = NO;
	tools = [[MyTools alloc]init];
	loadingView = [tools createActivityIndicator1];	

    //right
    //UIBarButtonItem *editButton = [[UIBarButtonItem alloc] initWithTitle:@"Post" style:UIBarButtonItemStylePlain target:self action:@selector(postBtnClicked:)];
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 57, 32)];
    [button addTarget:self action:@selector(postBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
    [button setImage:[UIImage imageNamed:@"btn_post.png"] forState:UIControlStateNormal];
    
    UIView *rightButtonView = [[UIView alloc] initWithFrame:button.frame];
    rightButtonView.bounds = CGRectOffset(rightButtonView.bounds, RDX, 0);
    [rightButtonView addSubview:button];
    
    UIBarButtonItem *btnRight = [[UIBarButtonItem alloc] initWithCustomView:rightButtonView];
    [self.navigationItem setRightBarButtonItem:btnRight];

    //left
    button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 63, 32)];
    [button addTarget:self action:@selector(btnBackClicked:) forControlEvents:UIControlEventTouchUpInside];
    [button setImage:[UIImage imageNamed:@"btn_back.png"] forState:UIControlStateNormal];
    
    UIView *leftButtonView = [[UIView alloc] initWithFrame:button.frame];
    leftButtonView.bounds = CGRectOffset(leftButtonView.bounds, LDX, 0);
    [leftButtonView addSubview:button];
    
    UIBarButtonItem *btnLeft = [[UIBarButtonItem alloc] initWithCustomView:leftButtonView];
    [self.navigationItem setLeftBarButtonItem:btnLeft];
    
    
	//set font
	self.subheaderLb1.font = [UIFont fontWithName:DEFAULT_BOLD_FONT_NAME size:defaultFontSize];
    self.subheaderLb1.text = @"Share Status";
    
    //accessory view
	UIToolbar *toolbar = [[UIToolbar alloc] init];
	[toolbar setBarStyle:UIBarStyleBlackTranslucent];
	[toolbar sizeToFit];
	
	UIBarButtonItem *flexButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
	UIBarButtonItem *doneButton =[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(resignKeyboard)];
	
	NSArray *itemsArray = [NSArray arrayWithObjects:flexButton, doneButton, nil];
	[toolbar setItems:itemsArray];
	
	[self.profileTextView setInputAccessoryView:toolbar];
    
    //
//	self.profileTextView.layer.cornerRadius = 2.0f;
//	self.profileTextView.layer.borderWidth = 1.0f;
//	self.profileTextView.layer.borderColor = [[UIColor grayColor] CGColor];

    [self.profileTextView setPlaceholderText:@"Share your thoughts..."];
    self.profileTextView.font = [UIFont fontWithName:DEFAULT_NORMAL_FONT_NAME size:defaultFontSize];
    self.profileTextView.contentInset = UIEdgeInsetsMake(0.0,0.0,0.0,0.0);
    //
    LoginResponseData *loginResponseData = [LoginPerser getLoginResponse];
	
	//self.subheaderLb1.text = loginResponseData.programName;
    
    //avatar
    [self.profileIcon.layer setBorderWidth:1.0];
    [self.profileIcon.layer setBorderColor:[[UIColor darkGrayColor] CGColor]];
    
    NSString *avatarLink = loginResponseData.avator;

    UIImageView *iconImgView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 40, 40)];
    [iconImgView setImage:[UIImage imageNamed:@"avator.png"]];
    //		[iconImgView addTarget:self action:@selector(memberIconClicked:) forControlEvents:UIControlEventTouchUpInside];
    [iconImgView.layer setBorderWidth:1.0];
    [iconImgView.layer setBorderColor:[[UIColor darkGrayColor] CGColor]];
    
    [self.profileIcon addSubview:iconImgView];
    
    if (avatarLink) {
        //replace https with http. oh dear.
        [iconImgView setImageWithURL:[NSURL URLWithString:avatarLink] 
                    placeholderImage:[UIImage imageNamed:@"avator.png"]]; 
    } 
    
    //camera btn
    [self.photoBtn addTarget:self action:@selector(cameraBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
    
    [self updateUI];
    
    if (![NuvitaAppDelegate isIOS7]) {
        [self updateUILowerVersion];
    }
}

- (void)updateUILowerVersion {
    
    for (UIView *subview in [self.view subviews]) {
        
        CGRect frame = subview.frame;
        frame.origin.y -= 67;
        
        subview.frame = frame;
    }
}

- (void)postBtnClicked:(id)sender {
    [self resignKeyboard];
	[self sendRequest:YES];
}

- (void)btnBackClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)resignKeyboard {
	[self.profileTextView resignFirstResponder];
}

- (void)updateUI {
    //self.imageContent = [[NSData dataWithData:image_content]retain];
    self.shareImage.image = self.imageShare;
}

- (void)cameraBtnClicked:(id)sender {

    UIActionSheet *cameraMenuSheet = [[UIActionSheet alloc] initWithTitle:@"Add Photo" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Camera", @"Choose Photo", nil];
    [cameraMenuSheet showFromTabBar:self.tabBarController.tabBar];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    NSString *title = [actionSheet buttonTitleAtIndex:buttonIndex];
    
    // Initialize UIImagePickerController
    if ([title isEqualToString:@"Camera"]) {
        // Camera
        UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        imagePicker.delegate = self;
        imagePicker.allowsEditing = YES;
        imagePicker.showsCameraControls = YES;
        imagePicker.mediaTypes = [NSArray arrayWithObjects:(NSString *)kUTTypeImage, nil];
        
        [self presentViewController:imagePicker animated:YES completion:^{
            //...loading imagepicker
        }];
    } else if ([title isEqualToString:@"Choose Photo"]) {
    
        UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
        imagePicker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
        imagePicker.delegate = self;
        imagePicker.allowsEditing = YES;
        imagePicker.mediaTypes = [NSArray arrayWithObjects:(NSString *)kUTTypeImage, nil];
        [self presentViewController:imagePicker animated:YES completion:^{
            //...loading imagepicker
        }];
    }
}

/*
- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex {
    [self.view setNeedsDisplay];
}

// Method for saving image to photo album
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    NSString *mediaType = [info objectForKey:UIImagePickerControllerMediaType];
    
    if ([mediaType isEqualToString:(NSString *)kUTTypeImage]) {
        
        // Access the uncropped image from info dictionary
        UIImage *image = [info objectForKey:@"UIImagePickerControllerEditedImage"];
        
        //UIImage *scaledImage = [image scaleToSize:CGSizeMake(60.0, 60.0)];
        [self retrieveRawImageData:image];
        
        [self.shareImage setImage:image]; 
     }
    //picturePresent = YES;
    [self dismissModalViewControllerAnimated:NO];
}


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(NSDictionary *)editingInfo {
    
    [photoBtn setImage:image forState:UIControlStateNormal];
    
    [[picker parentViewController] dismissModalViewControllerAnimated:YES];
}

- (void)retrieveRawImageData:(UIImage *)image
{
    if(image!=nil)
    {   
        float imgW = image.size.width;
        float imgH = image.size.height;
        float expectedW = 200.0;
        float expectedH = 200.0;

        if(imgW > imgH)
            expectedW = 300.0;
        
        expectedH = imgH/(imgW/expectedW);
        
        image = [image scaleToSize:CGSizeMake(expectedW, expectedH)];
        NSData*  image_content = UIImagePNGRepresentation(image);
        //self.imageContent = [[NSData dataWithData:image_content]retain];
        //NSLog(@"%@...image_content....",self.imageContent);
        //Converting NSData to Byte array
        NSUInteger len1 = [self.imageContent length];
        NSLog(@"%d...length of imageData....%f",len1, expectedH);
   }
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [self dismissModalViewControllerAnimated:YES];
}
*/

- (void)sendRequest:(BOOL)flag {
    NSString *text = self.profileTextView.text;
	
	if ([text length]==0) {
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sorry"
                                                        message:@"Please enter the status text."
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
		[alert show];
        return;
	}

	[tools startLoading:self.navigationController.view childView:loadingView text:@"Sharing..."];
	
	[self makeRequestStarProgress:flag];
}

- (void)makeRequestStarProgress:(BOOL)flag {
    NSString* text = self.profileTextView.text;
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
	[dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];//MM-dd-yyyy HH:mm:ss
	NSString *dateString = [dateFormat stringFromDate:[NSDate date]];
	LoginResponseData *loginResponseData = [LoginPerser getLoginResponse];

    dateString = [dateString stringByReplacingOccurrencesOfString:@" " withString:@"T"];
         
	request *r=[[request alloc] initWithTarget:self
								 SuccessAction:@selector(onSucceffulLogin:resData:)
								 FailureAction:@selector(onFailureLogin)];
    
    
    NSData *imageContent = [NSData dataWithData:UIImagePNGRepresentation(self.imageShare)];

    
	if(imageContent)
    {
        [r saveWellnessWallPhoto:loginResponseData.memberID date:dateString text:text photo:imageContent];
    }
    else
    {
        [r saveWellnessWall:loginResponseData.memberID date:dateString text:text photo:@""];
    }
}

- (void)onSucceffulLogin:(ResponseStatus *)responseStatus resData:(NSObject *)obj {
    [tools stopLoading:loadingView];

	
    NSString* text = @"Sent successfully."; 
	
	if (responseStatus == nil || [responseStatus isSuccess]) {
		text = @"Could not your share status, please try again later.";
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Info"
                                                        message:text
                                                       delegate:nil
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles:nil];
        [alert show];
        return;
	}
    else
        ;//[self.delegate shouldUpdate:YES];
	    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)onFailureLogin {
	[tools stopLoading:loadingView];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
	if (buttonIndex == 0) {
		if (alertView.tag == 2) {
			
		}
	} else if (buttonIndex == 1) {
		// No
	}
}

#pragma rotate

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (BOOL)shouldAutorotate
{
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}


@end
