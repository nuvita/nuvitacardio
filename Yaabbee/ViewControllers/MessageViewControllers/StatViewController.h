//
//  MessagesViewController.h
//
//
//  Created by S Biswas on 7/22/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <AVFoundation/AVFoundation.h>
#import <AudioToolbox/AudioToolbox.h>
#import <MediaPlayer/MediaPlayer.h>
#import <CoreLocation/CoreLocation.h>
#import <QuartzCore/QuartzCore.h>
#import <CoreBluetooth/CoreBluetooth.h>

#import "ECSlidingViewController.h"
#import "MBSliderView.h"

#import "CustTableCell.h"
#import "constant.h"
#import "CustTableHeaderView.h"

#import "InputViewController.h"
#import "OfflineManager.h"

#import "Session.h"
#import "Authorize.h"
#import "CMonitor.h"

#import "request.h"
#import "NuvitaAppDelegate.h"
#import "MyTools.h"
#import "LoginPerser.h"
#import "LoginResponseData.h"
#import "NuvitaAppDelegate.h"
#import "NewLoginViewController.h"
#import "AppUtility.h"
#import "CardioLap.h"
#import "GpsLocation.h"

@class CardioWeekItem;
@class CardioWeekData;
@class StatViewController;

@interface StatViewController : UIViewController <CLLocationManagerDelegate, MKMapViewDelegate, UIActionSheetDelegate>
{
	NSMutableArray *columnsTitle1;
	NSMutableArray *columnsTitle2;
	
//	MyTools *tools;
//	UIView *loadingView;

	CardioWeekData *cardioWeekData;
	
	BOOL isLoaded;
	
	NSDate *today;
	
	UIButton *buttonPrev, *buttonNext;
	int totalElement;
    
    NSInteger zoneLowLimit;
    NSInteger zoneHighLimit;
    
    NSTimer *nsTimer;
    //BOOL isTimerRunning;

    NSInteger lapCount;
    NSInteger exerciseCount;
    //NSInteger normalCounter;

    NSInteger currHrmVal;
    
    BOOL isWorkoutStarted;
    BOOL isDisconnected;
    
    NSInteger hrmCountInterval;
    
    NSInteger currBtn1Tag;
    NSInteger currBtn2Tag;
    NSInteger currOrientation;//1- portrait, 2 - landscape
    //session time
    //NSTimeInterval timeInMiliseconds = [[NSDate date] timeIntervalSince1970];
    CLLocation* _currentLocation;

    BOOL gpsErrorLog;
    
    NSTimeInterval zoneStartTime;
    NSInteger previousZone;//0, 1, 2
    BOOL isSessionState;
    
    //
    NSTimer *nsBTCheckingTimer;
}

@property (retain, nonatomic) IBOutlet UIScrollView *scrollView;

@property (nonatomic, strong) AVAudioPlayer* player;
@property (nonatomic, retain) CLLocation* currentLocation;
@property (nonatomic, retain) CLLocationManager* locationManager;

@property (retain, nonatomic) IBOutlet UIView *viewHrmReadBg;

@property (retain, nonatomic) UIView *viewWeekContent;

@property (retain, nonatomic) IBOutlet UIButton *btnSession;


@property (retain, nonatomic) IBOutlet UILabel *lbBZ;

@property (retain, nonatomic) IBOutlet UILabel *lbIZ;
@property (retain, nonatomic) IBOutlet UILabel *lbAZ;
@property (retain, nonatomic) IBOutlet UILabel *lbIAZ;
@property (retain, nonatomic) IBOutlet UILabel *lbCal;


@property (retain, nonatomic) UIView *viewZone;
@property (retain, nonatomic) NSDate *currTime;
@property (retain, nonatomic) NSDate *currHrmTime;
//@property (assign) NSTimeInterval exerciseStartTime;

@property (retain, nonatomic) IBOutlet UILabel *lbCurrWeekLabel;
@property (retain, nonatomic) IBOutlet UILabel *lbCurrHrm;

@property (retain, nonatomic) IBOutlet UIButton *btnLap;
@property (retain, nonatomic) IBOutlet UIButton *btnStart;

@property (retain, nonatomic) IBOutlet UILabel *lbTimerVal;
@property (retain, nonatomic) IBOutlet UILabel *lbLapVal;
@property (retain, nonatomic) IBOutlet UILabel *lbLapCount;



@property (retain, nonatomic) IBOutlet UIView *viewHrmPerc;

@property (retain, nonatomic) IBOutlet UIView *viewHrmPercBg;

@property (retain, nonatomic) IBOutlet UIImageView *imageViewHrmPercProg;

@property (retain, nonatomic) IBOutlet UIView *viewZoneLowLimit;
@property (retain, nonatomic) IBOutlet UIView *viewZoneHighLimit;
@property (retain, nonatomic) IBOutlet UIButton *btnWeek;


@property (retain, nonatomic) IBOutlet UILabel *lbWeekLabel;
@property (retain, nonatomic) IBOutlet UIView *viewHrmContent;
@property (retain, nonatomic) IBOutlet UIView *viewCardioWeekBody;

@property (retain, nonatomic) IBOutlet UIView *viewCardioData;

@property(nonatomic, retain)UIView *headerView, *bodyView;

@property (retain, nonatomic) IBOutlet UILabel *lbGoalVal;

@property (retain, nonatomic) IBOutlet UILabel *lbGoalName;

@property (retain, nonatomic) IBOutlet UILabel *lbGoalPerc;
@property (retain, nonatomic) IBOutlet UIView *viewCardioWeekPercBar;
@property (retain, nonatomic) IBOutlet UIImageView *imageViewCardioWeekPercProg;

@property (retain, nonatomic) IBOutlet UILabel *lbHrmVal;
@property (retain, nonatomic) IBOutlet UIImageView *imgHeartIcon;


@property (nonatomic, retain)NSDate *today;
@property (nonatomic, retain) NSMutableArray *columnsTitle1;
@property (nonatomic, retain) NSMutableArray *columnsTitle2;
@property (nonatomic, retain) IBOutlet UITableView *tableView_;

@property (nonatomic, strong) CBCentralManager *manager;
@property (nonatomic, strong) CBPeripheral *peripheral;

- (IBAction)btnStartClicked:(id)sender;
- (IBAction)btnLapClicked:(id)sender;

- (void)showLeftBarMenu;
- (void)btConnectClicked;
- (void)buildBodyUI;
- (void)buildSubHeaderUI;
- (void)sendRequest:(BOOL)flag;
- (void)makeRequestProgress:(BOOL)flag;


- (void)getNextPrevDate:(BOOL)flag;

- (void)clearBodyUI;
- (void)clearPeripheral;

- (void)buildUI;
- (void)logoutClicked;
- (void)btConnectClicked;

- (void)updateHrmUI:(NSInteger)hrmVal;
- (void)initExerciseData;
- (void)startExerciseTimer;
- (void)updateTimerVal;
- (void)startExerciseTimer;
- (void)finishExercise;
- (void)timerEvent:(id)sender;
- (NSString *)getTimerInFormatedVal;
- (void)endExcersiseSession;

- (void)appHasGoneInBackground:(id)event;
- (void)appHasGoneInForeground:(id)event;

- (void)buildContentUI;

- (NSInteger)getOrientationMode;

- (void)updateScreen:(NSInteger)orientation;
- (NSString *)getFormatedFloatVal:(NSInteger)val;
- (void)showMessage:(NSString *)title message:(NSString *)message;
- (void)saveCardioSession:(CardioLap *)cardioLap;
- (void)playSound:(NSInteger)index;
- (void)handleZoneSound:(NSInteger)index;
- (IBAction)btnWeek:(id)sender;
- (IBAction)btnSessionClicked:(id)sender;
- (void)showSessionInfo;
- (void)showWeekInfo;

- (void)checkingBTConnection;

void systemAudioCallback(SystemSoundID soundId, void *clientData);

@end
