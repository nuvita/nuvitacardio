//
//  InBoxViewController.m
//  Yaabbee
//
//  Created by Sarfaraj Biswas on 8/21/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "MealsDetailsViewController.h"
#import "constant.h"
#import <QuartzCore/QuartzCore.h>

#define TEXTFLD_OFFSETY 100

@interface MealsDetailsViewController ()

@property (assign)BOOL isKeyBoardVisible;

@end

@implementation MealsDetailsViewController

@synthesize isKeyBoardVisible;
@synthesize descTextView,messageTable;
@synthesize nutritionDayData;
@synthesize dayKey,subTitleText,btnSave,enumMeal,rankingText,consumedCal;
@synthesize delegate;
@synthesize keyboardToolbar;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"nuvitabg.png"]];	
    UIImageView *textView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"logo.png"]];
    
    [self.navigationItem setTitleView:textView]; 
    [textView release];
    
   	UIView *headerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 27)];
	
	headerView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"header2.png"]];
	//headerView.backgroundColor = [UIColor clearColor];
	UILabel* label = nil;
	
	//week
	label = [[[UILabel	alloc] initWithFrame:CGRectMake(10, 0, headerView.frame.size.width - 20, headerView.frame.size.height)] autorelease];
	label.backgroundColor = [UIColor clearColor];
	label.font = [UIFont fontWithName:DEFAULT_BOLD_FONT_NAME size:defaultFontSize];
	label.text = self.subTitleText;//@"LUNCH Tuesday 10/27";
	label.textAlignment = UITextAlignmentLeft;
	label.textColor = [UIColor blackColor];
	//label.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleHeight;
	[headerView addSubview:label]; 
	
	[self.view addSubview: headerView];
	[headerView release];
	
	self.descTextView.font = [UIFont fontWithName:DEFAULT_NORMAL_FONT_NAME size:defaultFontSize];
	[self.descTextView.layer setBorderColor: [[UIColor darkGrayColor] CGColor]];
       [self.descTextView.layer setBorderWidth: 1.0];
       self.descTextView.layer.cornerRadius=4.0;
	[self.descTextView setBackgroundColor:[UIColor whiteColor]];//UIColor colorWithRed:173.0/255.0 green:173.0/255.0 blue:173.0/255.0 alpha:1]];
       [self.descTextView.layer setMasksToBounds:YES];
	self.descTextView.tag =1;
	
	NSDictionary* dict = [self.nutritionDayData.nutritionDayDict objectForKey:self.dayKey];
	self.descTextView.text = [dict objectForKey:DESCRIPTION_KEY];
	
	rankingArray = [[NSMutableArray alloc]initWithObjects:@"Missed", @"Very Unhealthy",@"Unhealthy",@"Moderate",@"Healthy", @"Very Healthy",nil];
	//rankingArray = [[NSMutableArray alloc]initWithObjects:@"Noset", @"VeryUnhealthy",@"Unhealthy",@"Moderate",@"Healthy", @"VeryHealthy",nil];
		
	[self.messageTable.layer setBorderWidth: 1.0];
	[self.messageTable.layer setBorderColor: [[UIColor grayColor] CGColor]];
	
	tools = [[MyTools alloc]init];
	loadingView = [tools createActivityIndicator1];	
	
	self.btnSave.font = [UIFont fontWithName:DEFAULT_BOLD_FONT_NAME size:defaultFontSize];
	[self.btnSave setTitle:@"Save" forState:UIControlStateNormal];

	self.rankingText = [dict objectForKey:HEALTH_RANKING_KEY];//@"healthy";
	self.consumedCal = [NSString stringWithFormat:@"%@", [dict objectForKey:CALORIES_CONSUMED_KEY]];
	
	UIBarButtonItem *editButton = [[UIBarButtonItem alloc] initWithTitle:@"Meals" style:UIBarButtonItemStylePlain target:self action:@selector(backBtnClicked)];
    
	[self.navigationItem setLeftBarButtonItem:editButton];
	[editButton release];
	
	//	
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];	

	
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)backBtnClicked
{
	if (self.delegate != nil) {
		[delegate backSelected:YES];
	}
	
	[self.navigationController popViewControllerAnimated:YES];
}

#pragma mark TableView
#pragma mark -

- (CGFloat)tableView:(UITableView *)_tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 44;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
	if([LoginPerser isNuvitaX])
		return 3;
       else	
		return 1;
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = nil;
    
    if (fmod(indexPath.row, 2) == 0)
        CellIdentifier = CELL_EVEN;
    
    else
        CellIdentifier = CELL_ODD;
    
    
    UITableViewCell *cell = nil;//[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
  
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];
        
       //UIView *selectedbackView = [[UIView alloc] init];
//        selectedbackView.backgroundColor = HIGHLIGHTED_CELL_COLOR;
//        cell.selectedBackgroundView = selectedbackView;
//        [selectedbackView release];
        
		
        [self configureCell:cell withIndex:indexPath];

    }
       
    if(indexPath.row == 0)	
    cell.accessoryType = UITableViewCellAccessoryDetailDisclosureButton;
      //2 weeks ago";
    // Configure the cell...
       return cell;
}

-(CGSize)calculateTextSizeWithText:(NSString *)text{
    CGSize size =  [text sizeWithFont:[UIFont systemFontOfSize:15] constrainedToSize: CGSizeMake(320.0, 480.0) lineBreakMode:UILineBreakModeWordWrap];
    
    return size;
}

-(void)configureCell:(UITableViewCell *)cell withIndex:(NSIndexPath *)indexPath{
    
	UIView *cellBackView = cell.contentView;
   
	NSDictionary* dict = [self.nutritionDayData.nutritionDayDict objectForKey:self.dayKey];

	NSString *titleText = @"Ranking";
	NSString *timeText = @"healthy";
	
	switch (indexPath.row) {
	   case 0:
		   titleText = @"Ranking";
		   //timeText = [dict objectForKey:HEALTH_RANKING_KEY];//@"healthy";		   	
		   break;
	   case 1:
		   titleText = @"Target";
		   timeText = [NSString stringWithFormat:@"%@", [dict objectForKey:TARGET_CALORIES_KEY]];//@"240";		   	
		   break;
	   case 2:
		   titleText = @"Consumed";
		   //self.consumedCal = [NSString stringWithFormat:@"%@", [dict objectForKey:CALORIES_CONSUMED_KEY]];//@"250";		   	
		   break;	   
	   default:
		   break;
	}	
	
    //CGFloat offsetX = 20.0;
    
    CGSize titleTextSize = [self calculateTextSizeWithText:titleText];
    CGRect frame = cellBackView.frame;
    frame.origin.x  = 10;
    frame.origin.y = 10;//(cell.contentView.frame.size.height - defaultFontSize)/2;
    frame.size.width = cell.frame.size.width/2 - 40;//titleTextSize.width + offsetX;
    frame.size.height = titleTextSize.height;
    
    UILabel *mainLabel = [[UILabel alloc] initWithFrame:frame];
    mainLabel.font = [UIFont fontWithName:DEFAULT_BOLD_FONT_NAME size:defaultFontSize];
    mainLabel.text = titleText;
    
	
    //right side
	
    titleTextSize =[self calculateTextSizeWithText:timeText];
    frame.origin.x = cellBackView.frame.size.width/2-40;// - 40;// - titleTextSize.width - offsetX*4;
    frame.size.width = 140;//cellBackView.frame.size.width/2;//titleTextSize.width + offsetX;
   
    switch (indexPath.row) {
		case 0:
			rankLabel = [[UILabel alloc] initWithFrame:frame];
			rankLabel.textAlignment = UITextAlignmentRight;
			rankLabel.font = [UIFont fontWithName:DEFAULT_BOLD_FONT_NAME size:defaultFontSize];
			rankLabel.text = self.rankingText;
			if ([[self.rankingText lowercaseString] isEqualToString:@"healthy"] || [[self.rankingText lowercaseString] isEqualToString:@"very healthy"]) 
				rankLabel.textColor = [UIColor orangeColor];
			else
				rankLabel.textColor = [UIColor grayColor];
			
			[cellBackView addSubview:rankLabel];
			break;
		case 1:
			targetTxtFld = [[UITextField alloc] initWithFrame:frame];
			targetTxtFld.textColor = [UIColor blackColor];
			targetTxtFld.font = [UIFont fontWithName:DEFAULT_NORMAL_FONT_NAME size:defaultFontSize];
			targetTxtFld.text=timeText;
			targetTxtFld.autocorrectionType = UITextAutocorrectionTypeNo;
			//targetTxtFld.autocapitalizationType = UITextAutocapitalizationType;
			targetTxtFld.keyboardType=UIKeyboardTypeNumberPad;
			targetTxtFld.delegate=self;
			targetTxtFld.tag=2;
			targetTxtFld.textAlignment = UITextAlignmentRight;
			targetTxtFld.borderStyle=UITextBorderStyleNone;
			targetTxtFld.enabled = NO;
			[cellBackView addSubview:targetTxtFld];
			break;
		case 2:
			consumedTxtFld = [[UITextField alloc] initWithFrame:frame];
			consumedTxtFld.textColor = [UIColor blackColor];
			consumedTxtFld.font = [UIFont fontWithName:DEFAULT_NORMAL_FONT_NAME size:defaultFontSize];
			consumedTxtFld.text = self.consumedCal;
			consumedTxtFld.autocorrectionType = UITextAutocorrectionTypeNo;
			//consumedTxtFld.autocapitalizationType = UITextAutocapitalizationTypeNone;
			consumedTxtFld.keyboardType=UIKeyboardTypeNumberPad;
			consumedTxtFld.delegate=self;
			consumedTxtFld.tag=3;
			consumedTxtFld.textAlignment = UITextAlignmentRight;
			consumedTxtFld.borderStyle=UITextBorderStyleNone;
			[cellBackView addSubview:consumedTxtFld];
			break;	   
		default:
			break;
	}	
	
    [cellBackView addSubview:mainLabel];
    [mainLabel release];
    //[timeLabel release];
 }

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if(indexPath.row == 0){	 
		[self showPickerView: 0];
	}
}


- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath
{
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
	
	if(indexPath.row == 0){	 
		[self showPickerView: 0];
	}
}



- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    
    //UITableViewCell *cell = (UITableViewCell *) [[textField superview] superview];
    
   // NSIndexPath *iPath = [self.tableView indexPathForCell:cell];
    if(self.navigationItem.rightBarButtonItem!=nil)
		self.navigationItem.rightBarButtonItem.tag=textField.tag;
	
    if(!self.isKeyBoardVisible){
	self.navigationItem.rightBarButtonItem =nil;
	UIBarButtonItem *barBtnDone = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneNote:)];
	barBtnDone.title = @"Done";
	barBtnDone.tag=textField.tag;
	self.navigationItem.rightBarButtonItem = barBtnDone;
		
        self.isKeyBoardVisible = YES;
        
        CGRect tableFrame = self.view.frame;
        tableFrame.origin.y -= TEXTFLD_OFFSETY;
        
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.3];
        [UIView setAnimationDidStopSelector:@selector(animationDidStop:finished:context:)];
        [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
        [UIView setAnimationDelegate:self];
        
        self.view.frame = tableFrame;
        
        [UIView commitAnimations];
    }
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{

    self.consumedCal = textField.text;	
    [textField resignFirstResponder];
    self.navigationItem.rightBarButtonItem = nil;
    if(self.isKeyBoardVisible){
        
        self.isKeyBoardVisible = NO;
        CGRect tableFrame = self.view.frame;
        tableFrame.origin.y += TEXTFLD_OFFSETY;
        
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.3];
        [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
        [UIView setAnimationDelegate:self];
        
        self.view.frame = tableFrame;
        
        [UIView commitAnimations];
    }
	
    return YES;
}


- (void)textViewDidBeginEditing:(UITextView *)textView
{
	self.navigationItem.rightBarButtonItem =nil;
    UIBarButtonItem *barBtnDone = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneNote:)];
    barBtnDone.title = @"Done";
    barBtnDone.tag=1;
    self.navigationItem.rightBarButtonItem = barBtnDone;
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    
}

-(IBAction)doneNote:(id)sender
{
    switch (self.navigationItem.rightBarButtonItem.tag) {
		case 1:
			[self.descTextView resignFirstResponder];
			break;
		case 2:
			[targetTxtFld resignFirstResponder];
			
			break;
		case 3:
			self.consumedCal = consumedTxtFld.text;
			[consumedTxtFld resignFirstResponder];
			break;
			
		default:
			break;
	}	
    	
	if(self.isKeyBoardVisible){
		
		self.isKeyBoardVisible = NO;
		CGRect tableFrame = self.view.frame;
		tableFrame.origin.y += TEXTFLD_OFFSETY;
		
		[UIView beginAnimations:nil context:NULL];
		[UIView setAnimationDuration:0.3];
		[UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
		[UIView setAnimationDelegate:self];
		
		self.view.frame = tableFrame;
		
		[UIView commitAnimations];
	}
	
    self.navigationItem.rightBarButtonItem = nil;
    //self.navigationItem.rightBarButtonItem = rightButton;	
}

-(int)getHealthRank:(NSString*)rank
{
	int len=[rankingArray count];
	
	for (int ii=0; ii<len;ii++) {
		
		NSString* data = [rankingArray objectAtIndex:ii];
		
		if([[data uppercaseString] isEqualToString:[rank uppercaseString]])		
		    return ii;
	}
	
	return 0;
}

//picker view

#pragma mark _
#pragma mark pickerview

-(void)showPickerView:(NSInteger)sender
{
    //NSLog(@"--showPickerView--%@, %@",rankingArray, rankLabel.text);
	
    if(myPickerView != nil) return;   
    int indx = [self getHealthRank:rankLabel.text]; 

    myPickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 200, 320, 200)];
    [self.view addSubview:myPickerView];
    myPickerView.delegate = self;
    myPickerView.showsSelectionIndicator = YES;
    myPickerView.tag = sender;
	
    [myPickerView selectRow:indx inComponent:0 animated:YES]; 
	
    doneButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [doneButton addTarget:self 
                   action:@selector(aMethod:)
         forControlEvents:UIControlEventTouchDown];
    doneButton.frame = CGRectMake(265.0, 4.0,  52.0, 30.0);
    UIImage *img = [UIImage imageNamed:@"done.png"];
    [doneButton setImage:img forState:UIControlStateNormal];
    
    [img release];
	
    [myPickerView addSubview:doneButton];	
    //[self.view addSubview:doneButton];
}

-(IBAction)aMethod:(id)sender
{
    [myPickerView removeFromSuperview];
    
    [doneButton removeFromSuperview];
    
    self.rankingText = [rankingArray objectAtIndex:[myPickerView selectedRowInComponent:0]];	
	
    myPickerView = nil;
	
     [self.messageTable reloadData];	
}

//picker view.........

// tell the picker how many rows are available for a given component
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    NSUInteger numRows = pickerView.tag == 1 ? [rankingArray count] : [rankingArray count];
    
    return numRows;
}

// tell the picker how many components it will have
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}


- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
//    if(pickerView.tag == 1)
//    {
//        [categoryBtn setTitle:[NSString stringWithFormat:@"%@",[rankingArray objectAtIndex:[pickerView selectedRowInComponent:0]]] forState:UIControlStateNormal];    }
	
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{    
        return [rankingArray objectAtIndex:row];
 }


// tell the picker the width of each row for a given component
- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component {
    CGFloat componentWidth = 180.0;
	//componentWidth = 135.0;	
	
	return componentWidth;
}

- (void)dealloc{
	[[NSNotificationCenter defaultCenter] removeObserver:self];
	self.keyboardToolbar=nil;
	
    [descTextView release];
	[messageTable release];
	[nutritionDayData release];
	[dayKey release];
	[subTitleText release];
	[btnSave release];
	[targetTxtFld release];
	[consumedTxtFld release];
	[consumedCal release];
	[rankingText release];
    [super dealloc];
}


//communication part

-(IBAction)btnSaveClicked:(id)sender
{
	NSDictionary* dict = [self.nutritionDayData.nutritionDayDict objectForKey:self.dayKey];
	
	NSString *memberID = [LoginPerser getLoginResponse].memberID;
	NSString *date = self.nutritionDayData.Date;
	NSString *desc= self.descTextView.text;
	NSString *targetCal= [NSString stringWithFormat:@"%@", [dict objectForKey:TARGET_CALORIES_KEY]];
	NSString *consumedCal= [NSString stringWithFormat:@"%@", [dict objectForKey:CALORIES_CONSUMED_KEY]];
	
	if([LoginPerser isNuvitaX])
	{
		targetCal= targetTxtFld.text;
		consumedCal= consumedTxtFld.text;
	}
	
	NSString *numMeal = self.enumMeal;//[NSString stringWithFormat:@"%d", self.enumMeal];//@"1";
	NSString *healthRank =  [NSString stringWithFormat:@"%i", [self getHealthRank:rankLabel.text]];//rankLabel.tag;
	
	//NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
//	[dateFormat setDateFormat:@"yyyy-MM-dd"];
//	
//	NSString *dateString = [dateFormat stringFromDate:self.today];
//	NSLog(@"Formated Date...%@", dateString);
	
	[tools startLoading:self.navigationController.view childView:loadingView text:LOADING_TEXT];
	request *r=[[request alloc] initWithTarget:self
								 SuccessAction:@selector(onSucceffulLogin:resData:)
								 FailureAction:@selector(onFailureLogin)];
	
	[r saveNutritionMeal:memberID date:date mealEnum:numMeal description:desc caloriesConsumed:consumedCal healthRanking:healthRank targetCalories:targetCal];
	[r release];		
}


-(void)onSucceffulLogin:(ResponseStatus *)responseStatus resData:(NSObject *)obj
{
	[tools stopLoading:loadingView];
	
	NSString* text = @"Nutrition meal successfully saved."; 
	
	if (responseStatus == nil || [responseStatus isSuccess])
	{
		text = @"Couldnot save nutrition meal to server, please try again later.";
	}
	else {
		if (self.delegate != nil) {
			[delegate backSelected:NO];
		}
		
		[self.navigationController popViewControllerAnimated:YES];
		return;
	}

	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Info" message:text delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
	[alert show];
	alert.tag=2;
	[alert release];
	return ;	
}

-(void)onFailureLogin
{
	[tools stopLoading:loadingView];
	[self clearBodyUI];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
	if (buttonIndex == 0)
	{
		if (alertView.tag == 2) {
			
			[self.navigationController popViewControllerAnimated:YES];
		}
		
	}
	else if (buttonIndex == 1)
	{
		// No
	}
}

#pragma mark -
#pragma mark UIWindow Keyboard Notifications

- (void)keyboardWillShow:(NSNotification *)notification
{	
	CGPoint beginCentre = [[[notification userInfo] valueForKey:UIKeyboardCenterBeginUserInfoKey] CGPointValue];
	CGPoint endCentre = [[[notification userInfo] valueForKey:UIKeyboardCenterEndUserInfoKey] CGPointValue];
	CGRect keyboardBounds = [[[notification userInfo] valueForKey:UIKeyboardBoundsUserInfoKey] CGRectValue];
	UIViewAnimationCurve animationCurve	= [[[notification userInfo] valueForKey:UIKeyboardAnimationCurveUserInfoKey] intValue];
	NSTimeInterval animationDuration = [[[notification userInfo] valueForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];		
	
	if (nil == keyboardToolbar) {
		
		if(nil == keyboardToolbar) {
			keyboardToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0,0,self.view.bounds.size.width,44)];
			keyboardToolbar.barStyle = UIBarStyleBlackTranslucent;
			keyboardToolbar.tintColor = [UIColor darkGrayColor];
			
			/*
			UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(dismissKeyboard:)];
			UIBarButtonItem *flex = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
			
			UISegmentedControl *control = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObjects:
																					 NSLocalizedString(@"Previous",@"Previous form field"),
																					 NSLocalizedString(@"Next",@"Next form field"),																				  
																					 nil]];
			control.segmentedControlStyle = UISegmentedControlStyleBar;
			control.tintColor = [UIColor darkGrayColor];
			control.momentary = YES;
			[control addTarget:self action:@selector(nextPrevious:) forControlEvents:UIControlEventValueChanged];			
			
			UIBarButtonItem *controlItem = [[UIBarButtonItem alloc] initWithCustomView:control];
			
			self.nextPreviousControl = control;
			
			
			NSArray *items = [[NSArray alloc] initWithObjects:controlItem, flex, barButtonItem, nil];
			[keyboardToolbar setItems:items];
			[control release];
			[barButtonItem release];
			[flex release];
			[items release];			
			*/
			keyboardToolbar.frame = CGRectMake(beginCentre.x - (keyboardBounds.size.width/2), 
											   beginCentre.y - (keyboardBounds.size.height/2) - keyboardToolbar.frame.size.height, 
											   keyboardToolbar.frame.size.width, 
											   keyboardToolbar.frame.size.height);				
			
			[self.view addSubview:keyboardToolbar];		
		}		
	}		
	
	[UIView beginAnimations:@"RS_showKeyboardAnimation" context:nil];	
	[UIView setAnimationCurve:animationCurve];
	[UIView setAnimationDuration:animationDuration];
	
	keyboardToolbar.alpha = 1.0;	
	keyboardToolbar.frame = CGRectMake(endCentre.x - (keyboardBounds.size.width/2), 
									   endCentre.y - (keyboardBounds.size.height/2) - keyboardToolbar.frame.size.height - self.view.frame.origin.y, 
									   keyboardToolbar.frame.size.width, 
									   keyboardToolbar.frame.size.height);
	
	[UIView commitAnimations];		
	
	self.isKeyBoardVisible = YES;
}

- (void)keyboardWillHide:(NSNotification *)notification
{
	if (nil == keyboardToolbar || !self.isKeyBoardVisible) {
		return;
	}	
	
	//	CGPoint beginCentre = [[[notification userInfo] valueForKey:UIKeyboardCenterBeginUserInfoKey] CGPointValue];
	CGPoint endCentre = [[[notification userInfo] valueForKey:UIKeyboardCenterEndUserInfoKey] CGPointValue];
	CGRect keyboardBounds = [[[notification userInfo] valueForKey:UIKeyboardBoundsUserInfoKey] CGRectValue];
	UIViewAnimationCurve animationCurve	= [[[notification userInfo] valueForKey:UIKeyboardAnimationCurveUserInfoKey] intValue];
	NSTimeInterval animationDuration = [[[notification userInfo] valueForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];	
	
	[UIView beginAnimations:@"RS_hideKeyboardAnimation" context:nil];	
	[UIView setAnimationCurve:animationCurve];
	[UIView setAnimationDuration:animationDuration];
	
	
	keyboardToolbar.alpha = 0.0;
	keyboardToolbar.frame = CGRectMake(endCentre.x - (keyboardBounds.size.width/2), 
									   endCentre.y - (keyboardBounds.size.height/2) - keyboardToolbar.frame.size.height,
									   keyboardToolbar.frame.size.width, 
									   keyboardToolbar.frame.size.height);
	
	[UIView commitAnimations];
}


@end
