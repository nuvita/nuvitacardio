//
//  MessagesViewController.m
//
//
//  Created by S Biswas on 7/22/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "StatViewController.h"
#import "GraphViewController.h"
#import "MapViewController.h"
#import "DevicesViewController.h"
#import "MenuViewController.h"
#import "ConnectViewController.h"
#import "InitialSlidingViewController.h"
#import "InitialTestViewController.h"

#define BTN_START_TAG 1000
#define BTN_PAUSE_TAG 1001
#define BTN_RESUME_TAG 1002

#define BTN_LAP_TAG 1000
#define BTN_END_TAG 1001

#define HRM_L_VALUE 70
#define HRM_M_VALUE 170

#define DEG2RAD (M_PI/180.0f)
#define BT_DEVICE_NOT_FOUND @"Nuvita cardio monitor not found"

#define ZONE_HIGH_LIMIT @"zone_high_limit"
#define ZONE_LOW_LIMIT @"zone_low_limit"

@interface StatViewController () <CBCentralManagerDelegate, CBPeripheralDelegate, InputViewControllerDelegate, MBSliderViewDelegate, ECSlidingViewControllerDelegate, MenuViewControllerDelegate> {

    BOOL isSessionStarted;
    BOOL isSavedClicked;
    BOOL isAlert;
    
    int initialCountdown;
    
    NSTimer *lostConnectionTimer;
    int lostConnectionCount;
    
    //..slider view
    MBSliderView *slider;

    float maximum;
    float minimum;
}


@property (nonatomic, strong) NSMutableArray *heartRateMonitors;
@property (nonatomic, strong) CBPeripheral *addedPeripheral;
@property (nonatomic, strong) NSString *peripheralSerialNo;

@property (strong, nonatomic) OfflineManager *offlineManager;
@property (strong, nonatomic) MenuViewController *menuViewController;
@property (strong, nonatomic) InitialSlidingViewController *initialSlidingViewController;

@property (strong, nonatomic) JBLoadingView *loadingView;

@end

//fb179091268895199
	
@implementation StatViewController

@synthesize columnsTitle1, columnsTitle2;
@synthesize tableView_, headerView, bodyView;

@synthesize today;

@synthesize heartRateMonitors = _heartRateMonitors;
@synthesize manager = _manager;
@synthesize peripheral = _peripheral;

@synthesize viewZone;
@synthesize currTime;
@synthesize currHrmTime;
@synthesize viewWeekContent;

@synthesize locationManager = _locationManager;
@synthesize currentLocation = _currentLocation;

@synthesize player;

#pragma mark - View lifecycle

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    NSString *xibName = @"StatViewController";
    if ([NuvitaAppDelegate isPhone5]) {
        xibName = @"StatViewControllerI5";
    }

    self = [super initWithNibName:xibName bundle:nibBundleOrNil];

    if (self) {
        // Custom initialization
        currOrientation = [self getOrientationMode];
    }
    return self;
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];

    _loadingView = [[JBLoadingView alloc] init];
  
    //left button
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 78, 32)];
    [button setImage:[UIImage imageNamed:@"btn_logout.png"] forState:UIControlStateNormal];
    [button addTarget:self
               action:@selector(logoutClicked)
     forControlEvents:UIControlEventTouchUpInside];

    
    UIView *leftButtonView = [[UIView alloc] initWithFrame:button.frame];
    leftButtonView.bounds = CGRectOffset(leftButtonView.bounds, LDX, 0);
    [leftButtonView addSubview:button];
    
    UIBarButtonItem *signoutButton = [[UIBarButtonItem alloc] initWithCustomView:leftButtonView];
    [self.navigationItem setLeftBarButtonItem:signoutButton];
    
    //right button
    button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 90, 32)];
    [button addTarget:self action:@selector(btConnectClicked) forControlEvents:UIControlEventTouchUpInside];
    [button setImage:[UIImage imageNamed:@"btn_connect.png"] forState:UIControlStateNormal];
    
    
    UIView *rightButtonView = [[UIView alloc] initWithFrame:button.frame];
    rightButtonView.bounds = CGRectOffset(rightButtonView.bounds, RDX, 0);
    [rightButtonView addSubview:button];
    
    UIBarButtonItem *btnRight = [[UIBarButtonItem alloc] initWithCustomView:rightButtonView];
    [self.navigationItem setRightBarButtonItem:btnRight];
    
    //...initialize offline mode
    [self initOfflineManager];

    
	//self.view.hidden = YES;
	isLoaded= NO;
    isSavedClicked = NO;
    isAlert = NO;
	
   //if(columnsTitle1) 
   self.columnsTitle1 = [NSMutableArray arrayWithObjects:@"Target", @"Below", @"In zone", @"Above", nil];
   self.columnsTitle2 = [NSMutableArray arrayWithObjects:@"Cals", @"Below", @"In zone", @"Above", @"In+Above ", nil];

    //HRM
    _heartRateMonitors = [[NSMutableArray alloc] init];
    _manager = [[CBCentralManager alloc] initWithDelegate:self queue:nil];

    //[self startScan];
    
    //
    isSessionState = YES;
    isSessionStarted = NO;
    isWorkoutStarted = NO;
    isDisconnected = NO;
    
    //timer part
    [self initExerciseData];
    
    //zone
    [self buildBodyUI];
    
    //NSNotificationCenter
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(appHasGoneInBackground:)
                                                 name:UIApplicationDidEnterBackgroundNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(appHasGoneInForeground:)
                                                 name:UIApplicationWillEnterForegroundNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(btConnectClicked)
                                                 name:@"RECONNECT"
                                               object:nil];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(loadAddedDevice:)
                                                 name:@"CONNECT_THIS_PERIPHERAL"
                                               object:nil];
    

    [self initLeftMenuViewController];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:YES];
    [[CheckUserHistory sharedAppData] setMustPrompt:NO];
}

- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
    [[CheckUserHistory sharedAppData] setMustPrompt:YES];

    NuvitaAppDelegate *app = (NuvitaAppDelegate *)[[UIApplication sharedApplication] delegate];
    if(!app.isLoggedIn) {

        
    } else {

        [self updateScreen:[self getOrientationMode]];
        if(!isLoaded) {
            
#if !TARGET_IPHONE_SIMULATOR
            [self startScan];
#endif
            if ([NuvitaAppDelegate isNetworkAvailable] && ![[CheckUserHistory sharedAppData] fromTest])
                [self sendRequest:YES];
            
            [self.btnSession setImage:[UIImage imageNamed:@"btn_session_active@2x.png"] forState:UIControlStateNormal];
            [self clearBodyUI];
        }
        
        isLoaded = YES;
    }
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    if( UIInterfaceOrientationIsLandscape(toInterfaceOrientation) ) {
        [self updateScreen:2];
    } else {
        [self updateScreen:1];
    }
}


- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
    NSLog( @"did rotate from orientation %d to %d in %@", toInterfaceOrientation, [self interfaceOrientation], NSStringFromClass([self class]));
}

#pragma mark - Public Method

- (void)initOfflineManager {
    
    NuvitaAppDelegate *appDelegate = (NuvitaAppDelegate *)[[UIApplication sharedApplication] delegate];
    _offlineManager = [[OfflineManager alloc] init];
    _offlineManager.managedObjectContext = appDelegate.managedObjectContext;
}

- (NSInteger)getOrientationMode {
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    if (orientation== UIInterfaceOrientationPortrait || orientation == UIInterfaceOrientationPortraitUpsideDown) {
        return 1;
    }
    else if(orientation== UIDeviceOrientationLandscapeLeft || orientation == UIDeviceOrientationLandscapeRight)
    {
        return 2;
    }
    
    return 1;
}

- (void)cleanIgnoredMonitor {
    [_offlineManager cleanIgnoredMonitorsNow];
}

- (NSString *)getFormatedFloatVal:(NSInteger)val {
    NSInteger seconds = val % 60;
    NSInteger minutes = (val / 60) % 60;
    NSInteger hours = (val / 3600);
    
    if(hours > 0)
        return [NSString stringWithFormat:@"%02i:%02i:%02i", hours, minutes, seconds];
    else
        return [NSString stringWithFormat:@"%02i:%02i", minutes, seconds];
}

- (void)getNextPrevDate:(BOOL)flag {
	if(self.today == nil)
		self.today = [NSDate date];
	
	
	NSCalendar *cal = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
	NSDateComponents *components = [cal components:( NSYearCalendarUnit | NSMonthCalendarUnit | NSWeekCalendarUnit | NSDayCalendarUnit ) fromDate:self.today];
	
	if(flag)
		[components setDay:([components day] + 7)];
	else
		[components setDay:([components day] - 7)];

	self.today = [cal dateFromComponents:components];

}

#pragma mark - Connect Button

- (void)btConnectClicked {

    [[NSNotificationCenter defaultCenter] postNotificationName:@"update_connect_btn" object:[NSNumber numberWithBool:YES]];
    if (!_peripheral) {
       [self startScan];
    }
}

#pragma mark - Logout Button

- (void)logoutClicked {
    //...remove ignored monitors
    [self cleanIgnoredMonitor];
    [self stopScan];
    _manager = nil;
    
    [self initExerciseData];
    [self btnSessionClicked:nil];
    [AppData sharedAppData].cardioLap = nil;
    isLoaded = NO;
    isWorkoutStarted = NO;
    
    if (lostConnectionTimer) {
        [lostConnectionTimer invalidate];
        lostConnectionTimer = nil;
    }

    [[CheckUserHistory sharedAppData] setMustPrompt:NO];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"update_connect_btn" object:[NSNumber numberWithBool:NO]];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"LOGOUT" object:nil];
}

#pragma mark - Background Event

- (void)appHasGoneInBackground:(id)event {
    
    if(!isWorkoutStarted && [[CheckUserHistory sharedAppData] mustPrompt]) {
        [self stopScan];
        if(_peripheral != nil)
        [_manager cancelPeripheralConnection:_peripheral];
    }
}

- (void)appHasGoneInForeground:(id)event {
    
    if(!isWorkoutStarted || (_manager && currHrmVal == 0)) {
        if ([[CheckUserHistory sharedAppData] mustPrompt]) {
            NuvitaAppDelegate *app = (NuvitaAppDelegate *)[[UIApplication sharedApplication] delegate];
            if (app.isLoggedIn)
                [self startScan];
        }
    }
    
}

#pragma mark - Update UI

- (void)buildContentUI {
    //
    [self buildBodyUI];
    
    //
    [self buildSubHeaderUI];
    
    if(currBtn2Tag == BTN_START_TAG) {
        self.btnStart.tag = BTN_START_TAG;
        [self.btnStart setImage:[UIImage imageNamed:@"btn_start.png"] forState:UIControlStateNormal];
        
        self.btnLap.tag = BTN_LAP_TAG;
        self.btnLap.hidden = YES;
        [self.self.btnLap setImage:[UIImage imageNamed:@"btn_lap.png"] forState:UIControlStateNormal];
        
    } else if(currBtn2Tag == BTN_PAUSE_TAG) {
        self.btnStart.tag = BTN_PAUSE_TAG;
        [self.btnStart setImage:[UIImage imageNamed:@"btn_pause.png"] forState:UIControlStateNormal];
        
        self.btnLap.tag = BTN_LAP_TAG;
        self.btnLap.hidden = NO;
        
        [self.self.btnLap setImage:[UIImage imageNamed:@"btn_lap.png"] forState:UIControlStateNormal];
        
    } else if(currBtn2Tag == BTN_RESUME_TAG) {
        self.btnStart.tag = BTN_RESUME_TAG;
        [self.btnStart setImage:[UIImage imageNamed:@"btn_resume.png"] forState:UIControlStateNormal];
        
        self.btnLap.tag = BTN_END_TAG;
        self.btnLap.hidden = NO;
        [self.self.btnLap setImage:[UIImage imageNamed:@"btn_stop.png"] forState:UIControlStateNormal];
        
    }
    
    [self updateTimerVal];
}

- (void)buildSubHeaderUI {
    
    self.btnSession.tag = 100;
    self.btnWeek.tag = 99;
    
    //limit set
    
    NSInteger limit1 = zoneLowLimit;
    NSInteger limit2 = zoneHighLimit;

//    float maxHR = HRM_M_VALUE - 45;
//    float xx1 = ((limit1 / maxHR) * self.viewHrmPercBg.frame.size.width) + self.viewHrmPercBg.frame.origin.x;
//    float xx2 = ((limit2 / maxHR) * self.viewHrmPercBg.frame.size.width) + self.viewHrmPercBg.frame.origin.x;

//    float maxHR = HRM_M_VALUE - 45;
//    limit1 += self.viewHrmPercBg.frame.origin.x


    maximum = zoneHighLimit + 10;
    minimum = zoneLowLimit - 10;

    float origin = self.viewHrmPercBg.frame.origin.x;
    float width = self.viewHrmPercBg.frame.size.width;
    float offset = maximum - minimum;

    float xx1 = ((limit1 - minimum) * width) / offset + origin;
    float xx2 = ((limit2 - minimum) * width) / offset + origin;

    self.viewZoneLowLimit.center = CGPointMake(xx1, self.viewZoneLowLimit.center.y);
    self.viewZoneHighLimit.center = CGPointMake(xx2, self.viewZoneHighLimit.center.y);

    UILabel *lbLow = (UILabel *)[self.viewZoneLowLimit viewWithTag:1000];
    UILabel *lbHigh = (UILabel *)[self.viewZoneHighLimit viewWithTag:1000];

    lbLow.text = [NSString stringWithFormat:@"%d", zoneLowLimit];;
    lbHigh.text = [NSString stringWithFormat:@"%d", zoneHighLimit];;
   
    [self updateHrmUI:0];
}

- (void)scaleZoneIndicator:(NSInteger)value {


    minimum = value <= minimum ? value - 5 : minimum;
    maximum = value >= maximum ? value + 5 : maximum;

    float origin = self.viewHrmPercBg.frame.origin.x;
    float width = self.viewHrmPercBg.frame.size.width;
    float offset = maximum - minimum;
    float xx1 = ((zoneLowLimit - minimum) * width) / offset + origin;
    float xx2 = ((zoneHighLimit - minimum) * width) / offset + origin;

    self.viewZoneLowLimit.center = CGPointMake(xx1, self.viewZoneLowLimit.center.y);
    self.viewZoneHighLimit.center = CGPointMake(xx2, self.viewZoneHighLimit.center.y);
}

#pragma --
#pragma updateHRMUI

- (void)updateHrmUI:(NSInteger)hrmVal {
    currHrmVal = hrmVal;

    if([AppData sharedAppData].cardioLap)
        [AppData sharedAppData].cardioLap.rhRate = hrmVal;

//    NSLog(@"drop count: %d", [[[AppData sharedAppData] cardioLap] hrZeroDropCount]);
    if (hrmVal > 0 && [[[AppData sharedAppData] cardioLap] hrZeroDropCount] >= 0 && [[[AppData sharedAppData] cardioLap] hrZeroDropCount] <= 6)
        self.lbCurrHrm.text = [NSString stringWithFormat:@"%d", hrmVal];
    else
        hrmVal = [self.lbCurrHrm.text integerValue];
    
    self.viewHrmPercBg.backgroundColor = [UIColor colorWithWhite:0.980 alpha:1.000];
    self.viewHrmPercBg.layer.borderColor = [UIColor colorWithWhite:0.636 alpha:1.000].CGColor;
    self.viewHrmPercBg.layer.borderWidth = 1.0;
    self.viewHrmPercBg.layer.cornerRadius = 4.0;

    if (hrmVal == 0)
        hrmVal = minimum;
    else
        [self scaleZoneIndicator:hrmVal];

    float width = self.viewHrmPercBg.frame.size.width;
    float offset = maximum - minimum;
    float progressW = ((hrmVal - minimum) * width) / offset;

    CGRect frame = self.imageViewHrmPercProg.frame;
    frame.origin.x = 1;
    frame.size.width = progressW > width ? width : progressW;
    self.imageViewHrmPercProg.frame = frame;
  
    NSInteger zoneType = 0;
	if(hrmVal < zoneLowLimit) {
		self.imageViewHrmPercProg.backgroundColor = CARDIO_PERC_GRAY_COLOR;
	} else if(hrmVal >= zoneLowLimit && hrmVal < zoneHighLimit) {
        zoneType = 1;
        self.imageViewHrmPercProg.backgroundColor = CARDIO_PERC_GREEN_COLOR;
    }else {
        zoneType = 2;
		self.imageViewHrmPercProg.backgroundColor = CARDIO_PERC_ORANGE_COLOR;
    }
    
    
    //reading data
    if(isWorkoutStarted) {
        
        int count = ([[NSDate date] timeIntervalSinceDate:self.currHrmTime] + 0.5);
        [[AppData sharedAppData].cardioLap addHRM:currHrmVal];
        
        if(count  >= 1) {
  
            if(currHrmVal >= zoneLowLimit && currHrmVal < zoneHighLimit)
                [AppData sharedAppData].cardioLap.zoneIn += 1;
            else if(currHrmVal >= zoneHighLimit)
                [AppData sharedAppData].cardioLap.zoneAbove += 1;
            
            
            self.currHrmTime = [NSDate date];
      
            [self handleZoneSound:zoneType];
        }
        
    }
}

- (void)buildBodyUI {
    NSString *cal = @"0";
    NSString *belowZone = @"00:00";
    NSString *inZone = @"00:00";
    NSString *aboveZone = @"00:00";
    NSString *inAboveZone = @"00:00";

    self.lbBZ.text = belowZone;
    self.lbIZ.text = inZone;
    self.lbAZ.text = aboveZone;
    self.lbIAZ.text = inAboveZone;
    self.lbCal.text = cal;
}

- (void)clearBodyUI {
    zoneHighLimit = [[[NSUserDefaults standardUserDefaults] valueForKey:ZONE_HIGH_LIMIT] integerValue];
    zoneLowLimit = [[[NSUserDefaults standardUserDefaults] valueForKey:ZONE_LOW_LIMIT] integerValue];
	
    [AppData sharedAppData].inzoneHighLimit = zoneHighLimit;
    [AppData sharedAppData].inzoneLowLimit = zoneLowLimit;
  
    [self buildSubHeaderUI];
}


- (void)buildUI {
    
	totalElement = 0;
	
	if(cardioWeekData == nil || cardioWeekData.ItemsArray == nil) return ;
	
	totalElement = [cardioWeekData.ItemsArray count];
	
    
    zoneHighLimit = [cardioWeekData.inzoneHighLimit integerValue];
    zoneLowLimit = [cardioWeekData.inzoneLowLimit integerValue];
	
    [AppData sharedAppData].inzoneHighLimit = zoneHighLimit;
    [AppData sharedAppData].inzoneLowLimit = zoneLowLimit;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:[NSNumber numberWithInteger:zoneHighLimit] forKey:ZONE_HIGH_LIMIT];
    [defaults setObject:[NSNumber numberWithInteger:zoneLowLimit] forKey:ZONE_LOW_LIMIT];
    [defaults synchronize];
    
    //buildSubHeaderUI
	[self buildSubHeaderUI];
    
	//[self buildCardioWeekUI];
	self.view.hidden = NO;
}

#pragma mark - Category Button (Session/Week)

- (IBAction)btnWeek:(id)sender {
    
    //[self playSound:1];
    
    [self.btnWeek setImage:[UIImage imageNamed:@"btn_week_active@2x.png"] forState:UIControlStateNormal];
    [self.btnSession setImage:[UIImage imageNamed:@"btn_session_inactive@2x.png"] forState:UIControlStateNormal];
    
    isSessionState = NO;
//    [self showWeekInfo];
    
    if(!isWorkoutStarted) {
        [self sendRequest:YES];
    }
}

- (IBAction)btnSessionClicked:(id)sender {
    [self.btnWeek setImage:[UIImage imageNamed:@"btn_week_inactive@2x.png"] forState:UIControlStateNormal];
    [self.btnSession setImage:[UIImage imageNamed:@"btn_session_active@2x.png"] forState:UIControlStateNormal];
    
    isSessionState = YES;
    [self showSessionInfo];
}


#pragma --
#pragma showSessionInfo

- (void)showSessionInfo
{
    self.lbCurrWeekLabel.text =  [AppUtility statHeaderDateFormat];

    self.viewHrmPerc.hidden = NO;
    self.viewCardioWeekBody.hidden = YES;
    
    //
    NSString *cal = @"0";
    NSString *belowZone = @"00:00";
    NSString *inZone = @"00:00";
    NSString *aboveZone = @"00:00";
    NSString *inAboveZone = @"00:00";
    
    if([AppData sharedAppData].cardioLap)
    {
        cal = [[AppData sharedAppData].cardioLap getCaloriesText];
        belowZone = [[AppData sharedAppData].cardioLap getBelowZoneText];
        inZone = [[AppData sharedAppData].cardioLap getInZoneText];
        aboveZone = [[AppData sharedAppData].cardioLap getAboveZoneText];
        inAboveZone = [[AppData sharedAppData].cardioLap getInAboveZoneText];
    }
    
    self.lbBZ.text = belowZone;
    self.lbIZ.text = inZone;
    self.lbAZ.text = aboveZone;
    self.lbIAZ.text = inAboveZone;
    self.lbCal.text = cal;
}

#pragma --
#pragma showWeekInfo

- (void)showWeekInfo {
    //date
    self.lbCurrWeekLabel.text =  cardioWeekData.WeekLabel;//[NSString stringWithFormat:@"Week: %@", [AppUtility statHeaderDateFormat2]];
    
    //
    self.viewHrmPerc.hidden = YES;
    self.viewCardioWeekBody.hidden = NO;
    
    //
    self.lbGoalName.text = cardioWeekData.GoalName;
    self.lbGoalPerc.text = [cardioWeekData.ProgressPerecent stringByAppendingString:@"%"];
    self.lbGoalVal.text = cardioWeekData.GoalValue;
    
    //
    self.viewCardioWeekPercBar.backgroundColor = [UIColor colorWithWhite:0.980 alpha:1.000];
    self.viewCardioWeekPercBar.layer.borderColor = [UIColor colorWithWhite:0.636 alpha:1.000].CGColor;
    self.viewCardioWeekPercBar.layer.borderWidth = 1.0;
    self.viewCardioWeekPercBar.layer.cornerRadius = 4.0;
    
    
    float progressBarW = self.viewCardioWeekPercBar.frame.size.width;
    
    int progressW = progressBarW*[cardioWeekData.ProgressPerecent intValue]/100;
	
	if(progressW > progressBarW) progressW = progressBarW;
	
	self.imageViewCardioWeekPercProg.frame = CGRectMake(self.imageViewCardioWeekPercProg.frame.origin.x, self.imageViewCardioWeekPercProg.frame.origin.y, progressW, self.imageViewCardioWeekPercProg.frame.size.height);
    
	if([cardioWeekData.ProgressPerecent intValue] < 70)
		self.imageViewCardioWeekPercProg.backgroundColor = CARDIO_PERC_GRAY_COLOR;
	else if([cardioWeekData.ProgressPerecent intValue] > 69 && [cardioWeekData.ProgressPerecent intValue] < 90)
		self.imageViewCardioWeekPercProg.backgroundColor = CARDIO_PERC_ORANGE_COLOR;
	else
		self.imageViewCardioWeekPercProg.backgroundColor = CARDIO_PERC_GREEN_COLOR;
    
    
    self.lbCal.text = cardioWeekData.CaloriesTotal;

    self.lbBZ.text = [self getFormatedFloatVal:[cardioWeekData.BelowTotal integerValue] * 60];

    self.lbIZ.text = [self getFormatedFloatVal:[cardioWeekData.InZoneTotal integerValue] * 60];
    
    self.lbAZ.text = [self getFormatedFloatVal:[cardioWeekData.AboveTotal integerValue] * 60];
    
    self.lbIAZ.text = [self getFormatedFloatVal:[cardioWeekData.InAboveTotal integerValue] * 60];

    
//    BOOL isLeftAligned = [cardioWeekData isGoalNameCal];
    //label.text = cardioWeekData.Goal;
    
    if([AppData sharedAppData].cardioLap)
    {
        NSString* cal = [[AppData sharedAppData].cardioLap getTotalCaloriesText: [cardioWeekData.CaloriesTotal floatValue]];
        
        NSString* belowZone = [[AppData sharedAppData].cardioLap getTotalBelowZoneText: [cardioWeekData.BelowTotal intValue] * 60];
        NSString* inZone = [[AppData sharedAppData].cardioLap getTotalInZoneText: [cardioWeekData.InZoneTotal intValue] * 60];
        NSString* aboveZone = [[AppData sharedAppData].cardioLap getTotalAboveZoneText: [cardioWeekData.AboveTotal intValue] * 60];
        NSString* inAboveZone = [[AppData sharedAppData].cardioLap getTotalInAboveZoneText: [cardioWeekData.InAboveTotal intValue] * 60];
        
        
        self.lbBZ.text = belowZone;
        self.lbIZ.text = inZone;
        self.lbAZ.text = aboveZone;
        self.lbIAZ.text = inAboveZone;
        self.lbCal.text = cal;
        
        //progress bar
        NSInteger totalInAboveZone = [[AppData sharedAppData].cardioLap getTotalInAboveZone:[cardioWeekData.InAboveTotal intValue] * 60] / 60;
        //NSLog(@"Percen Val %f", ((100*[cal floatValue])/[cardioWeekData.GoalValue floatValue]));
        NSInteger progPercVal = 0;
        
        if(totalInAboveZone == 0)
            progPercVal = 0;
        else
            progPercVal = lroundf((100 * totalInAboveZone)/[cardioWeekData.GoalValue floatValue]);
        
        //NSLog(@"Percentage calcualtion %@", cardioWeekData.GoalValue);
        
        self.lbGoalPerc.text = [[NSString stringWithFormat:@"%d", progPercVal] stringByAppendingString:@"%"];
        
        //
        self.viewCardioWeekPercBar.backgroundColor = [UIColor colorWithWhite:0.980 alpha:1.000];
        self.viewCardioWeekPercBar.layer.borderColor = [UIColor colorWithWhite:0.636 alpha:1.000].CGColor;
        self.viewCardioWeekPercBar.layer.borderWidth = 1.5;
        self.viewCardioWeekPercBar.layer.cornerRadius = 4.0;
        
        //
        float progressBarW = self.viewCardioWeekPercBar.frame.size.width;
        
        int progressW = progressBarW*progPercVal/100;
        
        if(progressW > progressBarW) progressW = progressBarW;
        
        self.imageViewCardioWeekPercProg.frame = CGRectMake(self.imageViewCardioWeekPercProg.frame.origin.x, self.imageViewCardioWeekPercProg.frame.origin.y, progressW, self.imageViewCardioWeekPercProg.frame.size.height);
        
        if(progPercVal < 70)
            self.imageViewCardioWeekPercProg.backgroundColor = CARDIO_PERC_GRAY_COLOR;
        else if(progPercVal > 69 &&progPercVal < 90)
            self.imageViewCardioWeekPercProg.backgroundColor = CARDIO_PERC_GREEN_COLOR;
        else
            self.imageViewCardioWeekPercProg.backgroundColor = CARDIO_PERC_ORANGE_COLOR;
        
    }    
}

//communication part

#pragma mark - Web Service Call

- (void)sendRequest:(BOOL)flag {
    
    if (![NuvitaAppDelegate isNetworkAvailable]) {
        UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:@"Error"
                                                            message:@"Unable to display current weekly data without network or wireless connection"
                                                           delegate:nil
                                                  cancelButtonTitle:@"Ok"
                                                  otherButtonTitles:nil];
        [alertview show];
        return;
    }
    
	if(self.today == nil)
		self.today = [NSDate date];

    if (![[CheckUserHistory sharedAppData] fromTest]) {
        [_loadingView loadingViewStartAnimating:self.view
                                withLoadingView:[_loadingView createLoadingView:self.view lightEffect:YES]
                                           text:@"Loading data. Please wait..."];
        [self makeRequestProgress:flag];
    }
}

- (void)didSelectDate:(id)sender {
    self.today = (NSDate *)[sender date];
}

- (void)showDatePicker {
    UIActionSheet *menu = [[UIActionSheet alloc] initWithTitle:@"This is a Test Date Picker\n\n\n\n\n\n\n\n\n\n\n\n\n\n"
                                                      delegate:self
                                             cancelButtonTitle:@"Done"
                                        destructiveButtonTitle:nil
                                             otherButtonTitles:nil];
    
    // Add the picker
    UIDatePicker *pickerView = [[UIDatePicker alloc] init];
    pickerView.datePickerMode = UIDatePickerModeDate;
    [pickerView addTarget:self action:@selector(didSelectDate:) forControlEvents:UIControlEventValueChanged];
    [menu setTag:2000];
    [menu addSubview:pickerView];
    [menu showInView:self.tabBarController.view];

    CGRect pickerRect = pickerView.bounds;
    pickerRect.origin.y = -40;
    pickerRect.size.width = 300;
    pickerView.bounds = pickerRect;
}


- (void)makeRequestProgress:(BOOL)flag {
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MM-dd-yyyy"];
    NSString *dateString = [dateFormat stringFromDate:self.today];
  
    LoginResponseData *loginResponseData = [LoginPerser getLoginResponse];
    request *r = [[request alloc] initWithTarget:self
                                 SuccessAction:@selector(onSucceffulLogin:resData:)
                                 FailureAction:@selector(onFailureLogin)];
    [r getCardioWeek:loginResponseData.memberID date:dateString];
    
}

- (void)onSucceffulLogin:(ResponseStatus *)responseStatus resData:(NSObject *)obj {
    [_loadingView loadingViewStopAnimating:[self view]];

	if (responseStatus == nil || ![responseStatus isSuccess]) {

		[self clearBodyUI];
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sorry"
                                                        message:NO_RECORD_FOUND
                                                       delegate:nil
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles: nil];
		[alert show];
		return ;
	}
	
    cardioWeekData = nil;
	cardioWeekData = (CardioWeekData *)obj;
	
	[self buildUI];
    
    if (!isSessionState) {
        [self showWeekInfo];
    }
}

- (void)onFailureLogin {
    [_loadingView loadingViewStopAnimating:[self view]];
	[self clearBodyUI];
    [self logoutClicked];

    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sorry"
                                                    message:@"No records found, Proceed with the vo2max test to know your zone level."
                                                   delegate:nil
                                          cancelButtonTitle:@"Ok"
                                          otherButtonTitles: nil];
    [alert show];
}

- (void)onSucceffulSaveCardio:(ResponseStatus *)responseStatus resData:(NSObject *)obj {
    [_loadingView loadingViewStopAnimating:[self view]];

	if (responseStatus == nil || ![responseStatus isUpdate]) {
        [self showMessage:@"Sorry" message:@"Could not save cardio session, please try again later."];
		return ;
	}
	
    [self showMessage:@"Cardio Session" message:@"Saved successfully."];
    [self syncLocalSession];
}

- (void)onFailureSaveCardio {
    [_loadingView loadingViewStopAnimating:[self view]];
}


#pragma ----------------------******************Start Peripheral Device Programe*********---------
#pragma HRM device reading part
#pragma -----------------------


#pragma mark - Start/Stop Scan methods

// Use CBCentralManager to check whether the current platform/hardware supports Bluetooth LE.
- (BOOL) isLECapableHardware {
    NSString * state = nil;
    switch ([_manager state]) {
        case CBCentralManagerStateUnsupported:
            state = @"The platform/hardware doesn't support Bluetooth Low Energy.";
            break;
        case CBCentralManagerStateUnauthorized:
            state = @"The app is not authorized to use Bluetooth Low Energy.";
            break;
        case CBCentralManagerStatePoweredOff:
            state = @"Bluetooth is currently powered off.";
            break;
        case CBCentralManagerStatePoweredOn:
            return TRUE;
        case CBCentralManagerStateUnknown:
        default:
            return FALSE;
    }

    [self updateHrmUI:0];
    return FALSE;
}

- (void)startScan {
    [self clearPeripheral];
    [_heartRateMonitors removeAllObjects];
    
    NSLog(@"Peripheral State %d", [_manager state]);
    if ([_manager state] == CBCentralManagerStatePoweredOff || _manager == nil) {
        if (!nsBTCheckingTimer)
            _manager = [[CBCentralManager alloc] initWithDelegate:self queue:nil];
    }

//    NSDictionary *scanOptions = [NSDictionary dictionaryWithObject:[NSNumber numberWithBool:YES]
//                                                            forKey:CBCentralManagerScanOptionAllowDuplicatesKey];
    [_manager scanForPeripheralsWithServices:[NSArray arrayWithObject:[CBUUID UUIDWithString:@"180D"]]
                                     options:nil];
    
    if (nsBTCheckingTimer == nil) {
        initialCountdown = 0;
        nsBTCheckingTimer = [NSTimer scheduledTimerWithTimeInterval:1.0f
                                                             target:self
                                                           selector:@selector(checkingBTConnection)
                                                           userInfo:nil
                                                            repeats:YES];
    }
}

// Request CBCentralManager to stop scanning for heart rate peripherals
- (void)stopScan {
    
    if (_peripheral) {
        
        [_manager stopScan];
        [_manager cancelPeripheralConnection:_peripheral];
        
        [_peripheral setDelegate:nil];
        _peripheral = nil;
        
        [AppData sharedAppData].serialNumber = @"";
    }
    
    if(nsBTCheckingTimer != nil)
        [nsBTCheckingTimer invalidate ];
    
    nsBTCheckingTimer = nil;
}

- (void)checkingBTConnection {
    initialCountdown ++;

    if ([_heartRateMonitors count] > 0) {
        [nsBTCheckingTimer invalidate];
        nsBTCheckingTimer = nil;
        [self stopScan];

        if ([[_offlineManager getNotIgnoredDevices] count] == 0) {
            [self connectToPeripheral:[_heartRateMonitors firstObject]];
            return;
        }

        if ([_peripheralSerialNo length] > 0) {
            [_heartRateMonitors enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                NSString *serialno = [AppData GetUUID:[[_heartRateMonitors objectAtIndex:idx] UUID]];
                if ([_peripheralSerialNo isEqualToString:serialno]) {
                    [self connectToPeripheral:(CBPeripheral *)[_heartRateMonitors objectAtIndex:idx]];
                    stop = YES;
                }
            }];

            _peripheralSerialNo = @"";
            return;
        }

        [_heartRateMonitors enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            NSString *serialno = [AppData GetUUID:[[_heartRateMonitors objectAtIndex:idx] UUID]];
            if ([_offlineManager checkForDuplicateMonitorSerialNo:serialno]) {

                [self connectToPeripheral:(CBPeripheral *)[_heartRateMonitors objectAtIndex:idx]];
                stop = YES;
            }
        }];

        return;

    } else {
        [self startScan];
    }


    if (initialCountdown == 10) {
        [nsBTCheckingTimer invalidate ];
        nsBTCheckingTimer = nil;
        [self stopScan];
        
        if([[AppData sharedAppData].serialNumber length] == 0) {
            
            UINavigationController *activeNav = (UINavigationController *)self.tabBarController.selectedViewController;
            if (self.tabBarController.selectedIndex == 0 || [[activeNav topViewController] isKindOfClass:[ConnectViewController class]]) {
         
                if (!isAlert) {
                    isAlert = YES;
                    UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:@"Sorry"
                                                                        message:BT_DEVICE_NOT_FOUND
                                                                       delegate:self
                                                              cancelButtonTitle:@"Ok"
                                                              otherButtonTitles:nil];
                    [alertview setTag:1111];
                    [alertview show];
                }
            }
        }
    }
    
}


#pragma mark - InputViewControllerDelegate

- (void)inputViewController:(InputViewController *)viewcontroller didEnterName:(NSString *)deviceName {
   
    if ([deviceName length] == 0) {
        [self loadInputView];
        
        UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:@"Error"
                                                            message:@"Please enter device name to continue."
                                                           delegate:nil
                                                  cancelButtonTitle:@"Ok"
                                                   otherButtonTitles:nil];
        [alertview show];
        return;
    }
    
    if (_peripheral) {
        
        NSDictionary *data = [NSDictionary dictionaryWithObjectsAndKeys:
                              [[AppData sharedAppData] serialNumber], @"serialnumber",
                              [_peripheral name], @"name",
                              deviceName, @"inputName",
                              [NSNumber numberWithBool:NO], @"ignored",
                              nil];

        if ([_offlineManager addNewMonitor:data withSerialNo:[[AppData sharedAppData] serialNumber]]) {
            if ([[CheckUserHistory sharedAppData] fromTest]) {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"PAIRING_COMPLETE" object:[NSNumber numberWithBool:YES]];
                return;
            }
            
            [_manager connectPeripheral:_peripheral
                                options:[NSDictionary dictionaryWithObject:[NSNumber numberWithBool:YES]
                                                                    forKey:CBConnectPeripheralOptionNotifyOnDisconnectionKey]];
        }
    }
}

- (void)loadInputView {

    InputViewController *inputViewController = [[InputViewController alloc] init];
    inputViewController.modalTransitionStyle = UIModalTransitionStylePartialCurl;
    inputViewController.delegate = self;
    
    [self.navigationController presentViewController:inputViewController animated:YES completion:^{
        
    }];
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {

    if ([alertView tag] == 1111 && buttonIndex == 0) {
        isAlert = NO;
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if ([alertView tag] == 1111) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"update_connect_btn" object:[NSNumber numberWithBool:NO]];
        [self startScan];
        return;
    }
    
    if (buttonIndex == 1) {
        [self loadInputView];
        return;
    }
    
    NSDictionary *data = [NSDictionary dictionaryWithObjectsAndKeys:
                          [[AppData sharedAppData] serialNumber], @"serialnumber",
                          [_peripheral name], @"name",
                          [NSNumber numberWithBool:YES], @"ignored",
                          nil];
    
    if ([_offlineManager addNewMonitor:data withSerialNo:[[AppData sharedAppData] serialNumber]]) {
        [self startScan];
    }
}

#pragma mark - CBCentralManager delegate methods

// Invoked when the central manager's state is updated.
- (void)centralManagerDidUpdateState:(CBCentralManager *)central {
    
    if(![self isLECapableHardware]) {
        [AppData sharedAppData].serialNumber = @"";
    }
}

// Invoked when the central discovers heart rate peripheral while scanning.
- (void) centralManager:(CBCentralManager *)central
  didDiscoverPeripheral:(CBPeripheral *)aPeripheral
      advertisementData:(NSDictionary *)advertisementData
                   RSSI:(NSNumber *)RSSI {
    
    NSLog(@"peripheral: %@", aPeripheral);
    if(![_heartRateMonitors containsObject:aPeripheral])
        [_heartRateMonitors addObject:aPeripheral];

}

- (void)loadAddedDevice:(NSNotification *)notification {
    
    _peripheralSerialNo = (NSString *)[notification object];
    self.tabBarController.selectedIndex = 0;
    [self updateHrmUI:0];
    [self startScan];
}

- (void)connectToPeripheral:(CBPeripheral *)foundPeriperal {

    if (_peripheral && ![[CheckUserHistory sharedAppData] fromTest]) {
        [_manager stopScan];
        [_manager cancelPeripheralConnection:_peripheral];
        [_peripheral setDelegate:nil];
        _peripheral = nil;
    }
    
    if ([_heartRateMonitors count] > 0) {

        _peripheral = foundPeriperal;
        [AppData sharedAppData].serialNumber = [AppData GetUUID:_peripheral.UUID];
        CMonitor *foundMonitor = [_offlineManager checkForDuplicateMonitorSerialNo:[[AppData sharedAppData] serialNumber]];
    
        if (foundMonitor && ![[foundMonitor ignored] boolValue]) {
            
            if ([[CheckUserHistory sharedAppData] fromTest])
                [[NSNotificationCenter defaultCenter] postNotificationName:@"PAIRING_COMPLETE" object:[NSNumber numberWithBool:YES]];
            
                [_manager connectPeripheral:_peripheral
                                    options:[NSDictionary dictionaryWithObject:[NSNumber numberWithBool:YES]
                                                                        forKey:CBConnectPeripheralOptionNotifyOnDisconnectionKey]];
            return;
        }
        
        if (!foundMonitor && [[_peripheral name] isEqualToString:@"Nuvita14RJM20"] && [[CheckUserHistory sharedAppData] mustPrompt]) {
            
            NSString *serial = [[AppData sharedAppData] serialNumber];
            NSString *trimmedString = [serial substringFromIndex:MAX((int)[serial length] - 6, 0)];
            UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:@"Found"
                                                                message:[NSString stringWithFormat:@"New Nuvita Cardio (...%@) monitor found.  Do you wish to pair?", trimmedString]
                                                               delegate:self
                                                      cancelButtonTitle:@"No"
                                                       otherButtonTitles:@"Yes", nil];
            [alertview show];
            return;
        }
        
        if (![[_peripheral name] isEqualToString:@"Nuvita14RJM20"]) {
            UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:@"Error!"
                                                                message:@"Invalid transmitter detected, Please connect a Nuvita Cardio monitor to continue?"
                                                               delegate:nil
                                                      cancelButtonTitle:@"Ok"
                                                       otherButtonTitles:nil];
            [alertview show];
            return;
        }
    }
    
}

- (void) centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)aPeripheral {

    [_peripheral setDelegate:self];
    [_peripheral discoverServices:nil];

    [[NSNotificationCenter defaultCenter] postNotificationName:UPDATE_DEVICE_STATUS object:nil];
    if (![[CheckUserHistory sharedAppData] fromTest])
        [[AppData sharedAppData] setActiveCentralManagerClass:self];
}

- (void) centralManager:(CBCentralManager *)central
didDisconnectPeripheral:(CBPeripheral *)aPeripheral
                  error:(NSError *)error {
    if (_peripheral) {
        [_peripheral setDelegate:nil];
        _peripheral = nil;
    }

    [AppData sharedAppData].serialNumber = @"";
    [self.imgHeartIcon setImage:[UIImage imageNamed:@"hearts_icon.png"]];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"update_connect_btn" object:[NSNumber numberWithBool:NO]];

    NSLog(@"--centralManager-didDisconnectPeripheral-%@", aPeripheral);
    
    //...update device view
    [[NSNotificationCenter defaultCenter] postNotificationName:UPDATE_DEVICE_STATUS object:nil];
    
    //...lost connection call
    if (!isDisconnected)
        [self peripheralDidLostConnection];
    
    isDisconnected = YES;
    [self updateHrmUI:0];

}

- (void) centralManager:(CBCentralManager *)central
didFailToConnectPeripheral:(CBPeripheral *)aPeripheral
                  error:(NSError *)error {
    [AppData sharedAppData].serialNumber = @"";

    //...update heart icon and connect button
    [self.imgHeartIcon setImage:[UIImage imageNamed:@"hearts_icon.png"]];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"update_connect_btn" object:[NSNumber numberWithBool:NO]];
    
    NSLog(@"Fail to connect to peripheral: %@ with error = %@", aPeripheral, [error localizedDescription]);
    if (_peripheral) {
        [_peripheral setDelegate:nil];
        _peripheral = nil;
    }
    
    //...update device view
    [[NSNotificationCenter defaultCenter] postNotificationName:UPDATE_DEVICE_STATUS object:nil];
    
    //...lost connection call
    if (!isDisconnected)
        [self peripheralDidLostConnection];
    
    isDisconnected = YES;
}


#pragma mark - CBPeripheral delegate methods

- (void) peripheral:(CBPeripheral *)aPeripheral didDiscoverServices:(NSError *)error {
    for (CBService *aService in aPeripheral.services) {
      
        /*Battery*/
        if ([aService.UUID isEqual:[CBUUID UUIDWithString:@"180F"]]) {
            [aPeripheral discoverCharacteristics:nil forService:aService];
        }
        
        /* Heart Rate Service */
        if ([aService.UUID isEqual:[CBUUID UUIDWithString:@"180D"]]) {
            [aPeripheral discoverCharacteristics:nil forService:aService];
        }
        
        /* Device Information Service */
        if ([aService.UUID isEqual:[CBUUID UUIDWithString:@"180A"]]) {
            [aPeripheral discoverCharacteristics:nil forService:aService];
        }
        
        /* GAP (Generic Access Profile) for Device Name */
        if ([aService.UUID isEqual:[CBUUID UUIDWithString:CBUUIDGenericAccessProfileString]]) {
            [aPeripheral discoverCharacteristics:nil forService:aService];
        }
    }
}

#pragma mark - Bytes Reader/Converter

unsigned long long u64(CBUUID *uuid);
unsigned long long u64(CBUUID *uuid) {
    unsigned long long ret = 0;
    const unsigned char *bytes = (const unsigned char *) uuid.data.bytes;
    for (unsigned i = 0; i < uuid.data.length; i++) {
        ret |= (((unsigned long long)bytes[uuid.data.length - 1 - i]) << (i * 8));
    }
    return ret;
}

NSString *hex(CFUUIDRef uuid);
NSString *hex(CFUUIDRef uuid) {
    CFUUIDBytes uuidBytes = CFUUIDGetUUIDBytes(uuid);
    NSMutableString *ret = [[NSMutableString alloc] init];
    for (unsigned i = 0; i < sizeof(uuidBytes); i++) {
        [ret appendFormat:@"%02X", ((unsigned char*) & uuidBytes)[i]];
    }
    return ret;
}

unsigned long long lsbFirst(NSData *data);
unsigned long long lsbFirst(NSData *data) {
    unsigned long long ret = 0;
    const unsigned char *bytes = (const unsigned char *) data.bytes;
    size_t length = data.length;
    for (unsigned i = 0; i < length; i++) {
        ret |= (((unsigned long long)bytes[i]) << (i * 8));

    }
    return ret;
}


- (void) peripheral:(CBPeripheral *)aPeripheral didDiscoverCharacteristicsForService:(CBService *)service
              error:(NSError *)error {

    for (CBCharacteristic *ch in service.characteristics) {
        unsigned long long serviceID = (u64(service.UUID) << 32) | u64(ch.UUID);
        if (ch.properties & CBCharacteristicPropertyRead) {
            NSLog(@"Requesting read of %llX", serviceID);
            [aPeripheral readValueForCharacteristic:ch];
        }

        if (ch.properties & CBCharacteristicPropertyNotify) {
            NSLog(@"Requesting notification for %llX", serviceID);
            [aPeripheral setNotifyValue:YES forCharacteristic:ch];
        }
     }
  
//    //...Battery notification
//    if ([service.UUID isEqual:[CBUUID UUIDWithString:@"180F"]]) {
//        for (CBCharacteristic *aChar in service.characteristics) {
//            if ([aChar.UUID isEqual:[CBUUID UUIDWithString:@"2A19"]]) {
//                [aPeripheral setNotifyValue:YES forCharacteristic:aChar];
////                [aPeripheral readValueForCharacteristic:aChar];
//            }
//        }
//    }
//
//    if ([service.UUID isEqual:[CBUUID UUIDWithString:@"180D"]]) {
//        for (CBCharacteristic *aChar in service.characteristics) {
//            
//            // Set notification on heart rate measurement
//            if ([aChar.UUID isEqual:[CBUUID UUIDWithString:@"2A37"]]) {
//                [_peripheral setNotifyValue:YES forCharacteristic:aChar];
//            }
//            
//            // Read body sensor location
//            if ([aChar.UUID isEqual:[CBUUID UUIDWithString:@"2A38"]]) {
//                [aPeripheral readValueForCharacteristic:aChar];
//            }
//            
//            // Write heart rate control point
//            if ([aChar.UUID isEqual:[CBUUID UUIDWithString:@"2A39"]]) {
//                uint8_t val = 1;
//                NSData* valData = [NSData dataWithBytes:(void*)&val length:sizeof(val)];
//                [aPeripheral writeValue:valData forCharacteristic:aChar type:CBCharacteristicWriteWithResponse];
//            }
//        }
//    }
//    
//    if ([service.UUID isEqual:[CBUUID UUIDWithString:CBUUIDGenericAccessProfileString]]) {
//        for (CBCharacteristic *aChar in service.characteristics) {
//            // Read device name
//            if ([aChar.UUID isEqual:[CBUUID UUIDWithString:CBUUIDDeviceNameString]]) {
//                [aPeripheral readValueForCharacteristic:aChar];
//            }
//        }
//    }
//    
//    if ([service.UUID isEqual:[CBUUID UUIDWithString:@"180A"]]) {
//        for (CBCharacteristic *aChar in service.characteristics) {
//            // Read manufacturer name
//            if ([aChar.UUID isEqual:[CBUUID UUIDWithString:@"2A29"]]) {
//                [aPeripheral readValueForCharacteristic:aChar];
//            }
//        }
//    }
}

- (void) peripheral:(CBPeripheral *)aPeripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic
              error:(NSError *)error {


    if (error) {
        NSLog(@"Characteristic %X has error %@", (int)u64(characteristic.UUID), [error description]);
        return;
    }

    if (!characteristic.value) {
        NSLog(@"Characteristic %X has no value", (int)u64(characteristic.UUID));
        return;
    }

    switch (u64(characteristic.UUID)) {

        case 0x2A37: { // Heart rate
            if ([[CheckUserHistory sharedAppData] isVO2Starting]) {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"ADDHRM" object:characteristic.value];
                return;
            }

            [self updateWithHRMData:characteristic.value];

        } break;

        case 0x2A19: { // Battery level

            NSMutableData *data = [NSMutableData dataWithData:characteristic.value];
            [data increaseLengthBy:8];

            ushort value;
            [data getBytes:&value length:sizeof(value)];
            [AppData sharedAppData].cardioBatteryLevel = [NSString  stringWithFormat: @"%d%%", value];

            //...update device view
            [[NSNotificationCenter defaultCenter] postNotificationName:UPDATE_DEVICE_STATUS object:nil];
        } break;

        default:
            break;
    }
//    NSLog(@"no error");
//    NSData *data = characteristic.value;
//
//    int mog = *(int*)([data bytes]);
//
//    NSLog(@"[data] %d", mog);
//    NSMutableData *data2 = [NSMutableData dataWithData:characteristic.value];
//    [data2 increaseLengthBy:8];
//    [data2 getBytes:&value length:sizeof(value)];
//    NSLog(@"[data] %d", value);
//    //battery
//    //2A19
//    if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:@"2A19"]]) {
//        if (characteristic.value || !error) {
//            if (u64(characteristic.UUID) == 0x2A19) {
//                NSLog(@"battery level: %d%%", (int)lsbFirst(characteristic.value));
//                [AppData sharedAppData].cardioBatteryLevel = [NSString  stringWithFormat: @"%d%%", (int)lsbFirst(characteristic.value)];
//
//                //...update device view
//                [[NSNotificationCenter defaultCenter] postNotificationName:UPDATE_DEVICE_STATUS object:nil];
//            }
//        }
//    }
//    
//    // Updated value for heart rate measurement received
//    if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:@"2A37"]]) {
//        if(characteristic.value || !error) {
//            if ([[CheckUserHistory sharedAppData] isVO2Starting]) {
//                [[NSNotificationCenter defaultCenter] postNotificationName:@"ADDHRM" object:characteristic.value];
//                return;
//            }
//            
//            [self updateWithHRMData:characteristic.value];
//        }
//    }
}

#pragma mark - Peripheral Supporting Method

- (void) updateWithHRMData:(NSData *)data {
    
    [AppData sharedAppData].serialNumber = [AppData GetUUID: _peripheral.UUID];

    if(data == nil) return;
    
    const uint8_t *reportData = [data bytes];
    uint16_t bpm = 0;
    
    if ((reportData[0] & 0x01) == 0) {
        // uint8 bpm
        bpm = reportData[1];
    } else {
        // uint16 bpm
        bpm = CFSwapInt16LittleToHost(*(uint16_t *)(&reportData[1]));
    }

    [self updateHrmUI:bpm];

    if (bpm == 0) {

        [[NSNotificationCenter defaultCenter] postNotificationName:@"update_connect_btn" object:[NSNumber numberWithBool:NO]];

        if (!isDisconnected)
            [self peripheralDidLostConnection];
    
        isDisconnected = YES;
        
    } else {

        isDisconnected = NO;
        [self.imgHeartIcon setImage:[UIImage imageNamed:@"hearts_icon_red.png"]];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"update_connect_btn" object:[NSNumber numberWithBool:YES]];
        
        if (lostConnectionTimer) {
            [lostConnectionTimer invalidate];
            lostConnectionTimer = nil;
        }
    }
}

- (void)clearPeripheral {

    if (_peripheral) {
        
        [_manager cancelPeripheralConnection:_peripheral];
        _manager = nil;
        
        [_peripheral setDelegate:nil];
        _peripheral = nil;
        
        [AppData sharedAppData].serialNumber = @"";
    }
    
}

- (void)peripheralDidLostConnection {
    if (!lostConnectionTimer) {
        lostConnectionCount = [[NSDate date] timeIntervalSince1970];
        lostConnectionTimer = [NSTimer scheduledTimerWithTimeInterval:1.0f
                                                             target:self
                                                           selector:@selector(didLostConnection)
                                                           userInfo:nil
                                                            repeats:YES];
    }
}

- (void)didLostConnection {
    int count = [[NSDate date] timeIntervalSince1970] - lostConnectionCount;
    if (count == 5) {
        isDisconnected = NO;
        [lostConnectionTimer invalidate];
        lostConnectionTimer = nil;

        //...reconnect
        [self reconnectToPeripheral];
    }
}

- (void)reconnectToPeripheral {
    
    _peripheral = [_heartRateMonitors objectAtIndex:0];
    if (_peripheral) {
        [_manager connectPeripheral:_peripheral
                                options:[NSDictionary dictionaryWithObject:[NSNumber numberWithBool:YES]
                                                                    forKey:CBConnectPeripheralOptionNotifyOnDisconnectionKey]];
    }
}

#pragma mark - Workout Method

- (void)initExerciseData {
    self.navigationItem.leftBarButtonItem.enabled = YES;

    [AppData sharedAppData].isInSession = NO;
    exerciseCount = 0;
    lapCount = 0;
    
    self.lbLapCount.text = @"0";
    
    self.btnStart.tag = BTN_START_TAG;
    [self.btnStart setImage:[UIImage imageNamed:@"btn_start.png"] forState:UIControlStateNormal];

    self.btnLap.tag = BTN_LAP_TAG;
    if(self.btnStart.tag == BTN_START_TAG)
        self.btnLap.hidden = YES;
    
    isWorkoutStarted = NO;
    
    if(nsTimer)
        [nsTimer invalidate];
    nsTimer = nil;
    
    [self updateTimerVal];
}

- (NSString *)getTimerInFormatedVal {
    NSInteger seconds = exerciseCount % 60;
    NSInteger minutes = (exerciseCount / 60) % 60;
    NSInteger hours = (exerciseCount / 3600);
    return [NSString stringWithFormat:@"%i:%02i:%02i", hours, minutes, seconds];
    
}

- (NSString *)getLapCount {
    
    NSInteger lap = (isSessionStarted) ? [[self.lbLapCount text] integerValue] + 1 : 0;
    return [NSString stringWithFormat:@"%d", lap];
}

- (void)updateTimerVal {
    NSInteger seconds = lapCount % 60;
    NSInteger minutes = (lapCount / 60) % 60;
    NSInteger hours = (lapCount / 3600);
    
    self.lbLapVal.text =  [NSString stringWithFormat:@"%i:%02i:%02i", hours, minutes, seconds];
    self.lbTimerVal.text = [self getTimerInFormatedVal];

    if(isSessionState) {
        [self showSessionInfo];
    } else {
        [self showWeekInfo];
    }
    
}

- (IBAction)btnStartClicked:(id)sender {
    
    switch (self.btnStart.tag) {
            
        case BTN_START_TAG:
            if (nsTimer == nil) {
                
                //...
                isSessionStarted = YES;
                
                //
                [AppData sharedAppData].cardioLap = [[CardioLap alloc] init];
                
                [AppData sharedAppData].cardioLap.startLapTime = [CardioLap getStartUTCDate];
                
                //
                [AppData sharedAppData].isInSession = YES;
                [AppData sharedAppData].startSessionTime = [[NSDate date] timeIntervalSince1970];
                [AppData sharedAppData].startLapTime = [[NSDate date] timeIntervalSince1970];
                
                //zone
                previousZone = 0;
                
                [self startTrackingMap];

                [self startExerciseTimer];
                
                self.btnLap.tag = BTN_LAP_TAG;
                self.btnLap.hidden = NO;
                //[self.btnLap setTitle:@"Lap" forState:UIControlStateNormal];
                [self.btnLap setImage:[UIImage imageNamed:@"btn_lap.png"] forState:UIControlStateNormal];

                
                self.btnStart.tag = BTN_PAUSE_TAG;
                [self.btnStart setImage:[UIImage imageNamed:@"btn_pause.png"] forState:UIControlStateNormal];

                //[self.btnStart setTitle:@"Pause" forState:UIControlStateNormal];
            }
            break;
        case BTN_PAUSE_TAG:
            {
                if(nsTimer) {
                    
                    [AppData sharedAppData].pauseSessionTime = [[NSDate date] timeIntervalSince1970];
                    [AppData sharedAppData].isInSession = NO;

                    [nsTimer invalidate];
                    isWorkoutStarted = NO;
                    
                    self.btnLap.tag = BTN_END_TAG;
                    self.btnLap.hidden = NO;
                    [self.btnLap setImage:[UIImage imageNamed:@"btn_stop.png"] forState:UIControlStateNormal];

                    self.btnStart.tag = BTN_RESUME_TAG;
                    [self.btnStart setImage:[UIImage imageNamed:@"btn_resume.png"] forState:UIControlStateNormal];

                }
                nsTimer = nil;
            }
            break;
        case BTN_RESUME_TAG:
            if (nsTimer == nil) {
                //
                [AppData sharedAppData].isInSession = YES;
                [AppData sharedAppData].startSessionTime += ([[NSDate date] timeIntervalSince1970] - [AppData sharedAppData].pauseSessionTime);
                [AppData sharedAppData].startLapTime += ([[NSDate date] timeIntervalSince1970] - [AppData sharedAppData].pauseSessionTime);

                [self startExerciseTimer];
                
                self.btnLap.tag = BTN_LAP_TAG;
                self.btnLap.hidden = NO;
                [self.btnLap setImage:[UIImage imageNamed:@"btn_lap.png"] forState:UIControlStateNormal];

                self.btnStart.tag = BTN_PAUSE_TAG;
                [self.btnStart setImage:[UIImage imageNamed:@"btn_pause.png"] forState:UIControlStateNormal];

            }
            
            break;
        default:
            break;
    }    
}


- (IBAction)btnLapClicked:(id)sender {
    
    switch (self.btnLap.tag) {
        case BTN_LAP_TAG:
        {
            if (lapCount >= 10) {
                lapCount = 0;
                [AppData sharedAppData].startLapTime = [[NSDate date] timeIntervalSince1970];
                [self updateTimerVal];
                
                //...count lap
                self.lbLapCount.text = [self getLapCount];
            }

        }
            break;
        case BTN_END_TAG:
        {
            isSessionStarted = NO;
            [AppData sharedAppData].isInSession = NO;
            [self endExcersiseSession];
        }
            break;

    }
    
}

- (void)endExcersiseSession {
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Do you want to end this session?"
                                                             delegate:self
                                                    cancelButtonTitle:nil
                                               destructiveButtonTitle:@"Save session"
                                                    otherButtonTitles:@"Discard session", @"Return", nil];
    
    [actionSheet setActionSheetStyle:UIActionSheetStyleBlackTranslucent];
    
    actionSheet.tag = 1000;
	[actionSheet showInView:self.tabBarController.view];
}

- (void)startExerciseTimer {
    isWorkoutStarted = YES;
    
    if(self.btnStart.tag == BTN_START_TAG) {
        [self updateTimerVal];
    }
    
    self.currTime = [NSDate date];
    self.currHrmTime = [NSDate date];
    hrmCountInterval = 0;
    
    nsTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(timerEvent:) userInfo:nil repeats:YES];
    self.navigationItem.leftBarButtonItem.enabled = NO;
}

- (void)finishExercise {
    if(nsTimer) {
        self.btnStart.tag = BTN_START_TAG;
        [nsTimer invalidate];
    }
    nsTimer = nil;
    
    [self initExerciseData];
}

- (void)timerEvent:(id)sender {
    
    self.currTime = [NSDate date];
    lapCount = ([[NSDate date] timeIntervalSince1970] - [AppData sharedAppData].startLapTime);
    exerciseCount = ([[NSDate date] timeIntervalSince1970] - [AppData sharedAppData].startSessionTime);
    
    [AppData sharedAppData].cardioLap.zoneBelow = (exerciseCount - ([AppData sharedAppData].cardioLap.zoneIn + [AppData sharedAppData].cardioLap.zoneAbove));
    [AppData sharedAppData].cardioLap.durationSeconds = exerciseCount;
    
    [self updateTimerVal];

    //...check if connection lost
    if (isWorkoutStarted || [[CheckUserHistory sharedAppData] isVO2Starting]) {

        if ([[AppData sharedAppData].cardioLap btConnectionLost]) {
            [[AppData sharedAppData] cardioLap].btConnectionLost = NO;

            self.lbCurrHrm.text = @"0";
            [self updateHrmUI:0];
            [self.imgHeartIcon setImage:[UIImage imageNamed:@"hearts_icon.png"]];
            [self playSound:3];

//            isDisconnected = NO;
            [self peripheralDidLostConnection];
        }
    }
    
    
    //...generate random number range 1-10
    int random = arc4random() % 9;
    random += 1;
    if (random == 5) {
        [self saveSessionTemporarily];
    }
    
    
    if(self.tabBarController.selectedIndex == 1) {
        GraphViewController *vc = (GraphViewController *)[[[[self.tabBarController viewControllers] objectAtIndex:1] viewControllers] objectAtIndex:0];
        [vc updateGraph];
    }
    
    if(self.tabBarController.selectedIndex == 2) {
        MapViewController *vc = (MapViewController *)[[[[self.tabBarController viewControllers] objectAtIndex:2] viewControllers] objectAtIndex:0];
        [vc updateMap];
    }
}


- (void)updateScreen:(NSInteger)orientation {
    currBtn1Tag = self.btnLap.tag;
    currBtn2Tag = self.btnStart.tag;

    currOrientation = orientation;
    
    int difference = ([NuvitaAppDelegate isPhone5]) ? 525 : 445; //454
    
    if( orientation == 2 ) {
        self.scrollView.frame = CGRectMake(0, 0, self.scrollView.frame.size.width, 280);
        self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width, difference);

    }else {
        self.scrollView.frame = CGRectMake(0, 0, self.view.frame.size.width, 545);
        self.scrollView.contentSize = CGSizeMake(320, difference);
    }
}

#pragma mark - UIActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if(actionSheet.tag == 1000)
    {
        if(buttonIndex == 0)
        {
            //save session
            isSavedClicked = YES;
            [[AppData sharedAppData].cardioLap updateHRM];
            
//#warning - here
//            CardioLap *cardioLap = [AppData sharedAppData].cardioLap;
//            BOOL newSession = [[[_offlineManager getSessionWithStartLapTimex:cardioLap.startLapTime] isNewSession] boolValue];
//            if (!newSession && [[AppData sharedAppData].arrSessionData count] > 0)
//                [[AppData sharedAppData].arrSessionData replaceObjectAtIndex:[[[AppData sharedAppData] arrSessionData] count] - 1 withObject:[cardioLap getLapData]];
//            else
//                [[AppData sharedAppData].arrSessionData addObject:[cardioLap getLapData]];
//            [[AppData sharedAppData] saveData];
            
            
            [self saveCardioSession:[AppData sharedAppData].cardioLap];
            [AppData sharedAppData].cardioLap = nil;
            [self initExerciseData];
            
            //
            MapViewController *vc = (MapViewController *)[[[[self.tabBarController viewControllers] objectAtIndex:2] viewControllers] objectAtIndex:0];
            [vc stopTrackingMap];
            [self stopTrackingMap];
        }
        else if(buttonIndex == 1)
        {
            [actionSheet dismissWithClickedButtonIndex:0 animated:NO];
            
            UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Do you want to end this session?"
                                                                     delegate:self
                                                            cancelButtonTitle:@"No"
                                                       destructiveButtonTitle:@"Yes"
                                                            otherButtonTitles: nil];
            
            [actionSheet setActionSheetStyle:UIActionSheetStyleBlackTranslucent];
            
            actionSheet.tag = 1009;
            [actionSheet showInView:self.tabBarController.view];
        }
        else if(buttonIndex == 2)
        {
            [actionSheet dismissWithClickedButtonIndex:0 animated:YES];
        }
    }
    else if (actionSheet.tag == 1009)
    {
        if(buttonIndex == 0)
        {
            //save session
//            [[AppData sharedAppData] delCardioLap:[[[AppData sharedAppData] arrSessionData] count] - 1];
            [self deleteSessionSuccess];
            [AppData sharedAppData].cardioLap = nil;
            
            [self initExerciseData];
            MapViewController *vc = (MapViewController *)[[[[self.tabBarController viewControllers] objectAtIndex:2] viewControllers] objectAtIndex:0];
            [vc stopTrackingMap];
            [self stopTrackingMap];
        }
        else
            
            [actionSheet dismissWithClickedButtonIndex:0 animated:YES];
        
    } else if (actionSheet.tag == 2000) {
        [self sendRequest:nil];
    }
    
    
}

#pragma mark - Offline Session

- (void)deleteSessionSuccess {
    if ([_offlineManager deleteLastSession]) {
        //...delete last session
    }
}

- (void)syncLocalSession {
    
    if ([_offlineManager updateSession]) {
        //...session sync
    }
}

- (void)saveSessionTemporarily {
    CardioLap *cardioLap = [AppData sharedAppData].cardioLap;
    [self saveToOfflineStore:cardioLap];
}

- (void)saveToOfflineStore:(CardioLap *)cardioLap {

    NSString *startTime = [cardioLap startLapTime];
    NSDictionary *data = [NSDictionary dictionaryWithObjectsAndKeys:
                          [[AppData sharedAppData] authorizeUser], @"authorize",
                          [cardioLap getLapData], @"cardiolap",
                          nil];
    
    if ([_offlineManager addNewSession:data withStartLapTime:startTime]) {

    }
}

- (void)saveCardioSession:(CardioLap *)cardioLap {
    [_loadingView loadingViewStartAnimating:[self view]
                            withLoadingView:[_loadingView createLoadingView:[self view] lightEffect:YES]
                                       text:@"Saving cardio session. Please wait..."];

    LoginResponseData *loginResponseData = [LoginPerser getLoginResponse];
    if (exerciseCount < 5) {
        
        UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:@"Error"
                                                            message:@"Invalid input data, please try again"
                                                           delegate:nil
                                                  cancelButtonTitle:@"Ok"
                                                  otherButtonTitles:nil];
        [alertview show];
        [_loadingView loadingViewStopAnimating:[self view]];
        return;

    }
    
    //...store data
    [self saveToOfflineStore:cardioLap];
    
    if (![NuvitaAppDelegate isNetworkAvailable]) {
        
        [_loadingView loadingViewStopAnimating:[self view]];
        UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:@"Cardio - Offline mode"
                                                            message:@"No Network/Wireless available, Sessions are temporarily saved in history list. You can manually sync these sessions once you are connected to a network."
                                                           delegate:nil
                                                  cancelButtonTitle:@"Ok"
                                                  otherButtonTitles:nil];
        [alertview show];
        return;
    }
    
	request *r = [[request alloc] initWithTarget:self
								 SuccessAction:@selector(onSucceffulSaveCardio:resData:)
								 FailureAction:@selector(onFailureSaveCardio)];
    
	[r saveCardioSession:[cardioLap getCardioSessionXml:loginResponseData.memberID] text:@"46364638743"];
}


- (void)showMessage:(NSString *)title message:(NSString *)message {
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title
                                                         message:message
                                                        delegate:nil
                                               cancelButtonTitle:@"OK"
                                               otherButtonTitles:nil];
    [alertView show];
}


#pragma mark GPS
  
- (void)startTrackingMap {
    gpsErrorLog = YES;

    if (nil == _locationManager)
        _locationManager = [[CLLocationManager alloc] init];
    
    _locationManager.delegate = self;
    _locationManager.activityType = CLActivityTypeAutomotiveNavigation;
    _locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation;//kCLLocationAccuracyBest;
    _locationManager.distanceFilter = 15.0;

    [_locationManager startUpdatingLocation];
}

- (void)stopTrackingMap {
    _locationManager.delegate = nil;
    [_locationManager stopUpdatingLocation];
    _locationManager = nil;
}

- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation {

    CLLocation *location = [[CLLocation alloc] initWithLatitude:userLocation.coordinate.latitude
                                                      longitude:userLocation.coordinate.longitude];
    
    NSDate* eventDate = location.timestamp;
    NSTimeInterval howRecent = [eventDate timeIntervalSinceNow];

    if (abs(howRecent) > 5.0) {
        return;
    }
    
    CLLocationCoordinate2D loc = [location coordinate];
    
    if(!CLLocationCoordinate2DIsValid(loc))return;
    
    // check the zero point
    if  (userLocation.coordinate.latitude == 0.0f ||
         userLocation.coordinate.longitude == 0.0f || !userLocation.updating)
        return;
    
    // check the move distance
    if ([AppData sharedAppData].cardioLap.arrGpsPos.count > 0) {
        CLLocationDistance distance = [location distanceFromLocation:_currentLocation];
        if (distance < 5)
            return;
    }
        
    _currentLocation = location;

    if([AppData sharedAppData].isInSession) {
       
        if([AppData sharedAppData].cardioLap != nil)
            [[AppData sharedAppData].cardioLap.arrGpsPos addObject:location];
    }
    
    if(self.tabBarController.selectedIndex == 2) {
        MapViewController *vc = (MapViewController *)[[[[self.tabBarController viewControllers] objectAtIndex:2] viewControllers] objectAtIndex:0];
        [vc updateMap];
    }
}


- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    if (newLocation) {
        CLLocation* userLocation = newLocation;
        
        NSDate* eventDate = userLocation.timestamp;
        NSTimeInterval howRecent = [eventDate timeIntervalSinceNow];
        
        if (abs(howRecent) > 15.0 || userLocation.horizontalAccuracy > 100) {
            return;
        }


        CLLocationCoordinate2D loc = [userLocation coordinate];
        
        if(!CLLocationCoordinate2DIsValid(loc))return;
        
        // check the zero point
        if  (userLocation.coordinate.latitude == 0.0f ||
             userLocation.coordinate.longitude == 0.0f)
            return;
        
        _currentLocation = userLocation;
        
        if([AppData sharedAppData].isInSession) {
       
            if([AppData sharedAppData].cardioLap != nil)
                [[AppData sharedAppData].cardioLap.arrGpsPos addObject:userLocation];
        }
        
        if(self.tabBarController.selectedIndex == 2) {
            MapViewController *vc = (MapViewController *)[[[[self.tabBarController viewControllers] objectAtIndex:2] viewControllers] objectAtIndex:0];
            [vc updateMap];
        }

    }
}


- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    if(!gpsErrorLog) return;
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Accurate Location Unavailable"
                                                        message:@"GPS accuracy is poor in your current location. For App to correctly track your progess you need to be outdoors with direct line of sight to the sky."
                                                       delegate:nil
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles:nil];
    [alertView show];
    
    gpsErrorLog = NO;
}

#pragma mark - Audio

- (void)handleZoneSound:(NSInteger)index {
    
    if(previousZone != index) {
        zoneStartTime = [[NSDate date] timeIntervalSince1970];
        previousZone = index;
    }
    
    if(zoneStartTime != -1 && ([[NSDate date] timeIntervalSince1970] - zoneStartTime) > 10) {
        [self playSound:index];
        zoneStartTime = -1;
    }
    
}

- (void)playSound:(NSInteger)index {
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *soundName = @"";
    
    switch(index) {
        case 0:
            if(![prefs boolForKey:SOUND_BELOW_KEY]) return;
            soundName = @"BelowZone";
            break;
        case 1:
            if(![prefs boolForKey:SOUND_IN_KEY]) return;
            soundName = @"InZone";
            break;
        case 2:
            if(![prefs boolForKey:SOUND_ABOVE_KEY]) return;
            soundName = @"AboveZone";
            break;
            
        case 3:
            if (![prefs boolForKey:SOUND_HRDISCONNECTED_KEY]) return;
            soundName = @"HRconnectionLost";
            break;
    }
    
    NSURL* soundFileURL = [[NSBundle mainBundle] URLForResource:soundName withExtension:@"mp3"];
    
    NSError *sessionError = nil;
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:&sessionError];

    SystemSoundID soundID;
    AudioServicesCreateSystemSoundID((__bridge CFURLRef)soundFileURL, &soundID);
    
    AudioServicesAddSystemSoundCompletion(soundID,
                                          NULL,
                                          NULL,
                                          systemAudioCallback,
                                          (__bridge void*) self);
    
    AudioServicesPlayAlertSound(soundID);
    if (index == 3)
        AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
    
}

void systemAudioCallback(SystemSoundID soundId, void *clientData) {
    AudioServicesRemoveSystemSoundCompletion(soundId);
    AudioServicesDisposeSystemSoundID(soundId);
}

#pragma mark - Add Custom Slider

- (void)initCustomizeSlider {
    
    NuvitaAppDelegate* app = (NuvitaAppDelegate*)[[UIApplication sharedApplication] delegate];
    slider = [[MBSliderView alloc] initWithFrame:[app.window frame]];
    [slider setBackgroundColor:[UIColor clearColor]];
    [slider setText:@"Slide to Unlock"];
    [slider setDelegate:self];
    [slider setThumbColor:[UIColor blackColor]];
    
    [app.window addSubview:slider];
}

- (void)sliderDidSlide:(MBSliderView *)slideView {
    [slider removeFromSuperview];
}

#pragma mark - ECSlidingViewControllerDelegate

- (void)didShowLeftPanel {
    NSLog(@"didShowLeftPanel");
}

- (void)initLeftMenuViewController {
    
    self.view.layer.shadowOpacity = 0.7f;
    self.view.layer.shadowRadius = 20.0f;
    self.view.layer.shadowColor = [UIColor blackColor].CGColor;
    
    _menuViewController = [[MenuViewController alloc] init];
    _menuViewController.delegate = self;
    self.slidingViewController.mydelegate = self;
    self.slidingViewController.underLeftViewController = _menuViewController;
}

- (void)showLeftBarMenu {
    
    if ([_menuViewController.slidingViewController underLeftShowing]) {
        [_menuViewController.slidingViewController resetTopView];
        
    }else {
        [_menuViewController.slidingViewController anchorTopViewTo:ECRight animations:^{
            
        } onComplete:^{
            
        }];
    }
}

#pragma mark - MenuViewControllerDelegate

- (void)menuViewController:(MenuViewController *)viewController didSelectMenu:(NSInteger)index {
    [self.slidingViewController resetTopView];
    if (index == 0) {
        [self initCustomizeSlider];
    } else if (index == 2) {
        [self showDatePicker];
    } else {
        if (!isWorkoutStarted) {
            [self logoutClicked];
        } 
    }
}

@end
